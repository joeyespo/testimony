<html>
<title> - MARK-UP OF H.R. 2844, THE CONTINUITY IN REPRESENTATION ACT OF 2003</title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]



 
   MARK-UP OF H.R. 2844, THE CONTINUITY IN REPRESENTATION ACT OF 2003

=======================================================================

                                HEARING

                               before the

                   COMMITTEE ON HOUSE ADMINISTRATION
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED EIGHTH CONGRESS

                             FIRST SESSION

                               __________

           HEARING HELD IN WASHINGTON, DC, NOVEMBER 19, 2003

                               __________

      Printed for the use of the Committee on House Administration







                   U.S. GOVERNMENT PRINTING OFFICE
92-238                       WASHINGTON : 2004
_______________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing 
Office Internet: bookstore.gpo.gov Phone: toll free (866) 512-1800, 
DC area (202) 512-1800 Fax: (202) 512-2250 Mail: stop SSOP, Washington, 
DC 20402-0001





                   COMMITTEE ON HOUSE ADMINISTRATION

                           BOB NEY, Chairman

VERNON J. EHLERS, Michigan           JOHN B. LARSON, Connecticut
JOHN L. MICA, Florida                Ranking Minority Member
JOHN LINDER, Georgia                 JUANITA MILLENDER-McDONALD, 
JOHN T. DOOLITTLE, California            California
THOMAS M. REYNOLDS, New York         ROBERT A. BRADY, Pennsylvania

                           Professional Staff

                     Paul Vinovich, Staff Director
                George Shevlin, Minority Staff Director


   MARKUP OF H.R. 2844, THE CONTINUITY IN REPRESENTATION ACT OF 2003

                              ----------                              


                      WEDNESDAY, NOVEMBER 19, 2003

                          House of Representatives,
                         Committee on House Administration,
                                                    Washington, DC.
    The committee met, pursuant to call, at 10:17 a.m., in Room 
1310, Longworth House Office Building, Hon. Robert W. Ney 
[chairman of the committee] presiding.
    Present: Representatives Ney, Ehlers, Mica, Linder, Larson, 
Millender-McDonald, and Brady.
    Staff Present: Paul Vinovich, Staff Director; Fred Hay, 
Counsel; Matt Petersen, Counsel; George Shevlin, Minority Staff 
Director; and Matt Pinkus, Minority Professional Staff.
    The Chairman. The committee is now in order for the purpose 
of consideration of House Resolution 2844, the Continuity in 
Representation Act of 2003.
    It has now been over 2 years since the horrific events of 
September 11, 2001, a day in which terrorist enemies of the 
United States murdered thousands of innocent American citizens 
in cold blood and struck devastating blows against symbols of 
our country's economic and military power. Since that grim day 
we have been forced to contemplate the dreadful possibility of 
a terrorist attack aimed at the heart of our Nation's 
government here in Washington, D.C., possibly carried out with 
nuclear, chemical, or biological weapons of mass destruction. 
Such an attack could potentially annihilate substantial 
portions of our Federal Government and kill or maim hundreds of 
Members of Congress. Though we earnestly pray we are never 
confronted with such an awful event, we have a duty as elected 
Representatives of the people of the United States to ensure 
that the people's House continues to operate effectively in the 
event of a catastrophic terrorist attack.
    I know no one wants to talk about the potential demise of 
themselves, but it is something I think we face today in our 
world, and we can just see in the last 2 years things happened 
that we wouldn't believe could have happened in this complex.
    This past September this committee held a hearing on H.R. 
2844, to allow leading thinkers on the issue of congressional 
continuity to provide insight on the many different aspects of 
this congressional and consequential issue. To restate what I 
said then, the debate on this subject essentially divides it 
into two camps: those who view a quick reconstitution of the 
House as the most important consideration and thus support a 
constitutional amendment allowing for the appointment of 
temporary replacements to fill vacant House seats, and those 
who believe retaining the House's elected character is 
paramount and therefore support expedited special elections as 
the exclusive means for reconstituting the House of 
Representatives.
    Though the two sides of the debate disagree on many 
fundamental issues, both agree that expedited elections should 
be part of the solution to this complex and difficult question. 
For this reason, the committee has scheduled this markup to 
consider H.R. 2844, which establishes a framework for 
conducting expedited special elections to fill House vacancies 
resulting from a catastrophic terrorist attack.
    As originally drafted, the Continuity in Representation Act 
calls for expedited special elections to be held within 21 days 
of the Speaker of the House announcing that more than 100 
vacancies exist in the House of Representatives. The political 
parties authorized by the State law to make nominations would 
then have up to 14 days after the Speaker's announcement to 
nominate a candidate. However, the State would not have had to 
hold an expedited special election if a regularly scheduled 
general election were to be held within 51 days of the 
Speaker's announcement, thus in essence providing the 30-day 
extension for such States.
    The original version of H.R. 2844 also contained among 
other things a provision stating that the Speaker's 
announcement of 100 or more vacancies could not be appealed.
    Today I will be offering an amendment in the nature of a 
substitute to H.R. 2844. The amendment retains the basic 
framework of the original bill but alters the time frames and 
makes other technical adjustments.
    The deadline for holding an expedited special election 
following the announcement by the Speaker of more than 100 
vacancies has been increased from 21 days to 45 days. The 
theory behind this is to make the election process work a 
little bit better. It was felt that 21 days may not be able to 
be met, but yet it did not need to go too far down the road. 
This change is deemed necessary to accommodate the concerns, 
again, of election officials who felt the 21-day time frame was 
too short and may not have allowed for adequate preparation. 
And I can tell you that those concerns were from both sides of 
the aisle of election officials.
    The amendment also shortens the time frame within which 
State party officials may nominate a candidate from 14 days to 
10 days after the Speaker's announcement. The shortened 
nomination period would provide additional time on the back end 
for election officials to print ballots, test election systems, 
recruit poll workers, et cetera, while still allowing party 
officials adequate time to make candidate nominations. Just as 
the original bill provided a 30-day extension from 21 to 51 
days for States whose election machinery was already in motion, 
the amendment likewise extends the 45-day period for holding an 
expedited special election by 30 days under those circumstances 
that I previously mentioned.
    Therefore, the amendment states that if a State is 
scheduled to hold a general election within 75 days of the 
Speaker's announcement of more than 100 vacancies, it would 
need to schedule--it would not need to schedule an expedited 
special election. Nature would take its course on what was 
already scheduled.
    Finally, the amendment deletes section 4(b) of the original 
bill. The Parliamentarian determined that this section, which 
stated that the Speaker's announcement of House vacancies in 
excess of 100 could not be appealed, was unnecessary since it 
is already duplicative of current House rules. The committee 
takes no position on any proposed constitutional amendments 
that would provide for the appointment of temporary 
replacements to fill vacant House seats, since amendments to 
the Constitution are outside. However, even if the committee 
were inclined to take a stance on issues outside of this 
jurisdiction, it is unaware of any constitutional amendments 
regarding congressional continuity that have been introduced in 
the House on which it could take a position at this time.
    Some will claim this bill is inadequate and only a 
constitutional change will address this problem. They will 
assert that expedited elections will result in 
disenfranchisement of absentee and military voters, among 
others. While no election is perfect, it is true that 
conducting them on a shortened time frame could make them even 
less perfect than we would prefer under ideal circumstances, 
and this is something I fully recognize. Those who view this as 
a defect of legislation, though, should compare the danger of 
disenfranchising some percentage of the population against the 
certainty of disenfranchising 100 percent of the population of 
the country if Members no one voted for are allowed to be 
appointed and allowed to serve.
    Regardless of one's view on the appointment question, we 
should all agree that rapid reconstitution of elected bodies 
should be our goal. This legislation advances that goal. 
Therefore, the committee seeks to move this process forward by 
marking up H.R. 2844 which furthers the essential objective, 
ensuring a functioning House would be in place with the ability 
to operate with legitimacy in the wake of a catastrophic 
attack.
    I appreciate the members for being here today, and I want 
to defer to our Ranking Member, Mr. Larson of Connecticut.
    Mr. Larson. Thank you, Mr. Chairman.
    Mr. Chairman, I want to begin by first complimenting you 
for holding the continuity of government hearing back on 
September the 24th, which ranged over the entire landscape, as 
you have noted in your comments, on the continuity of the 
government issue.
    Now, I can honestly say, though, in my relatively short 
tenure as a Member of the House, this was perhaps one of the 
most illuminating presentations and give-and-take between 
members and witnesses that I have had an occasion to 
participate in and to learn from and enjoy. I especially want 
to point out that both Chairmen Sensenbrenner and Dreier made a 
vigorous presentation of their strongly held views, and it was 
useful for those of us who may strongly disagree to engage in 
debate with those who feel passionately about this issue.
    I also want to express my thanks to the panelist scholars, 
the officials, and the group representatives who participated. 
Norm Ornstein and Tom Mann and the Continuity of Government 
Commission have led the public discussion of this issue since 
9/11. Doug Lewis of the Election Center and Minnesota Secretary 
of State Mary Kiffmeyer thoughtfully presented to us the 
difficulties in conducting elections under adverse 
circumstances and with artificial time frames. Don 
Wolfensberger of the Woodrow Wilson Center demonstrated how 
minds can change--in his case, on the subject of congressional 
disability--and consider possible compromises after exposure to 
vigorous debate.
    Representatives Frost and Baird, who oppose the bill before 
us today and support a constitutional amendment, should be 
congratulated for their leadership. Representative Frost played 
a critical role in sensitizing Members of Congress to these 
issues in his work as co-chair on the Task Force on Continuity 
with Representative Cox. Representative Baird has introduced a 
thoughtful constitutional amendment in the last Congress and is 
preparing another version, and has been working with the 
Parliamentarian on this issue since this tragic event of 
September 11th.
    I emerged from that hearing myself strongly supportive of a 
constitutional amendment, because I believe it provides the 
best remedy for what I believe the Chairman has articulated 
eloquently in his opening comments: for all of us to face a 
potential demise, and a demise that if it was of a catastrophic 
nature would probably be at the hands of terrorists. And the 
one thing that terrorists have to be assured of is that 
democracy is prepared to stand back up immediately.
    In the aftermath of September 11th, in a matter of only 
days, this Congress acted on $40 billion. This Congress acted 
to take immediate and appropriate steps. Even with the 
provisions and the enhancements in the H.R. 2844, which I 
believe is thoughtful, I clearly understand the concept of 
making sure that the House is an elected body. But I wanted to 
quote an old sage of Congress and someone who I believe 
demonstrated quite well the concerns that I share. That is 
former Senator Alan Simpson from Wyoming, who was the co-chair 
of the Continuity Commission, and who at a recent hearing said 
the troublesome part in this continuum mantra--that is we don't 
want to alter the character of the House--is it comes from 
pride. ``I am going to call it that, saying that we have always 
been directly elected by the people.'' He said. ``That phrase 
comes from those who oppose what we do. They say we can't have 
anything like that because it would alter the character of the 
House.''
    ``The people's House. A direct election. How troubling. How 
unAmerican. How undemocratic. I want to ask them a question. 
What more could alter the character of your body than bodies?''
    I agree with Senator Simpson that a functioning House, even 
in a temporarily modified form, is far better than no House at 
all. No House means no Congress, no legislation, and, in the 
end, no voice for the people and no more democracy. It would 
also, as anyone who seeks to bring down this government would 
understand, create chaos in a time when the immediate message 
and resolve of this great institution of ours should be to 
stand up immediately, just as we are seeking to stand up the 
Iraqi people immediately today to face the problems that they 
are encountering in their country. What greater example than a 
remedy for the United States Congress to be able to endure in 
the face of a catastrophe--as the Chairman points out that we 
all pray will never happen, and we will work to make sure that 
it doesn't, but we have to prepare for that possibility, and do 
so in a manner that any terrorist or any person seeking to 
bring down this government knows that our traditions will be 
buttressed by constitutional amendment ensuring a working 
government in a relatively short period of time.
    I further believe that especially for our country since 
September 11th, where we saw such a patriotic outpouring of 
people and individuals who care deeply about this country, from 
the display of flags to the volunteer efforts that took place 
all around this country, that a great educational opportunity 
would be the same kind of dialogue and discussion that we had 
here in this committee. I don't know that a constitutional 
amendment would pass, but I sincerely believe that we need the 
opportunity to bring this dialogue and debate back to our State 
legislators.
    I intend to introduce legislation myself, a constitutional 
amendment, and the Chairman points out this is not the 
committee of jurisdiction in which to do that. And it presents 
a quandary for us, because our bills are like ships passing in 
the night. The Chairman noted that there are two distinct 
efforts to address this problem, one outlined by the 
Commission, others, and supporters of the constitutional 
amendment; others as have been outlined in the bill that is 
before us today. Both are worthy proposals. I think both need 
an up-or-down vote.
    I intend to write the Speaker. I think that having sat in 
last week on what was an incredible historic moment where the 
four living Speakers talked about their speakership, talked 
about this great House, I think this is one of those moments in 
history that transcends the legislation before us and requires 
the kind of leadership that is going to recognize that what is 
more important than individual legislation or any one of us is 
that the democracy and our institution be able to stand up 
immediately.
    A constitutional amendment would provide that, and that is 
why I favor it, and that is why I, reluctantly and without 
prejudice, will vote against the proposal before us. I believe 
that we need to go forward on concurrent paths and there has to 
be implementing legislation, and the thoughtful measures that 
Mr. Sensenbrenner and Mr. Dreier and the principles on which 
they base their legislation ought to be incorporated in that.
    But my grave concern and fear is that we need to 
demonstrate to the entire world that no terrorist act can 
cripple this government. And we have three Members in this 
body, on this committee alone, who came here by special 
elections; none of them in a 45-day period. Mr. Ehlers', I 
think, selection took approximately 120 or 102 days. Ms. 
Millender-McDonald came here in a special election as well that 
took in excess of 100 days. Mr. Brady's election took 188 days.
    It is simply, while idealistic and an important matter to 
preserve, the daunting task that we face I believe requires us 
at least to have this debate and dialogue on the floor. And it 
is my hope that that will take place, though I fully 
acknowledge that that is a matter to come before another 
committee, not this.
    I thank the Chair who said to me that he would clearly 
indulge anything of this nature, but recognized that it would 
not come before this committee. And that is my sincere belief.
    I have extended remarks, Mr. Chairman, and some questions 
that I would like to ask, but at this time I would yield back 
to you.
    The Chairman. I want to thank the Ranking Member for his 
thoughtful comments.
    [The statement of Mr. Larson follows:]
    [GRAPHIC] [TIFF OMITTED] 92238A.001
    
    [GRAPHIC] [TIFF OMITTED] 92238A.002
    
    [GRAPHIC] [TIFF OMITTED] 92238A.003
    
    [GRAPHIC] [TIFF OMITTED] 92238A.004
    
    [GRAPHIC] [TIFF OMITTED] 92238A.005
    
    [GRAPHIC] [TIFF OMITTED] 92238A.006
    
    [GRAPHIC] [TIFF OMITTED] 92238A.007
    
    [GRAPHIC] [TIFF OMITTED] 92238A.008
    
    [GRAPHIC] [TIFF OMITTED] 92238A.009
    
    [GRAPHIC] [TIFF OMITTED] 92238A.010
    
    The Chairman. And we will go to other opening statements if 
we have them. Do we have other opening statement?
    I have one thing I do want to mention that the Ranking 
Member jogged in my mind that I was remiss in mentioning. We 
have a Sensenbrenner bill he has jurisdiction, and the Ranking 
Member has mentioned that. We are looking for ways to 
accommodate for the comfort level of the votes and would take 
into consideration what Congressman Larson stated.
    But, I need to point out that Representative Baird has been 
a tremendous individual in this entire process, even though I 
think from the constitutional amendment side it could take a 
long time for States to ratify. However, having said that, 
Representative Baird kept this on the front burner; he pushed 
the issue. There are also things that he has done both publicly 
and privately, frankly, through the traumas in the last couple 
of years, that have bettered everybody's life on this campus. 
And so I would be remiss if I didn't say that his bill is not 
without thought. Also, he has been a main motivator of keeping 
this issue on the front and keeping us working on it.
    With that, I would lay before the committee House 
Resolution 2844 open to amendment.
    [The information follows:]
    [GRAPHIC] [TIFF OMITTED] 92238A.011
    
    [GRAPHIC] [TIFF OMITTED] 92238A.012
    
    [GRAPHIC] [TIFF OMITTED] 92238A.013
    
    [GRAPHIC] [TIFF OMITTED] 92238A.014
    
    The Chairman. And the Chair offers an amendment in the 
nature of a substitute.
    The Chairman. Is there any discussion on the substitute?
    Mr. Larson. Mr. Chairman.
    The Chairman. Mr. Larson.
    Mr. Larson. I don't know how much of a debate you want to 
offer on this. I have some specific questions that I have with 
respect to this. And what I would like is to--if I could at 
this time go through some of the questions. And again, 
obviously, if staff can answer those questions, fine; if not, 
we are happy to submit them before the matter comes to the 
floor, if we could get a response.
    The Chairman. The Ranking Member can proceed.
    Mr. Larson. Thank you.
    Mr. Chairman, a number of questions with respect to the 
provisions of H.R. 2844 for the Members, and possibly also for 
the staff, we feel are important before the matter comes to the 
floor.
    The first is with respect to pulling the trigger. The first 
question is how exactly would the trigger be pulled so that 
special elections could take place, and what would happen if 
the House lacked a Speaker at the time, which was a question 
that was raised during our hearing?
    The second question would be, how is the number of 
vacancies required to activate the process chosen? If the House 
can pass bills or motions with more than 100 Members absent 
under normal circumstances--which we did, by the way, four 
times Monday night, November 17--why would it somehow be 
illegitimate to do the same thing in time of catastrophe?
    With regard to the question of disability, would the 
legislation do anything to address the issues of Member 
disability which could threaten the presence of a quorum on the 
floor under certain conditions? Some have even questioned the 
concept of defining a quorum.
    What would happen if special elections already in progress 
to fill vacancies, preceding a catastrophe, resulted in 
reducing the number of vacancies below 100 before special 
elections triggered under the statutes would occur? Would the 
trigger, once pulled, be impossible to stop?
    The grounds for contested elections: Isn't the bill an 
invitation to file election contests against the purported 
winners of such elections on a host of possible grounds 
presented through this legislation?
    The concern that we have heard repeatedly about absentee 
and military voters: How can military personnel and their 
families and other Americans living abroad possibly become 
aware of a special election request, request an absentee 
ballot, receive one once candidates become known, and return 
them in a time frame contemplated by the statute? Or does the 
bill simply assume that somehow everything will work out?
    Independent and other candidates on the ballot: Does the 
bill, by remaining silent about independent or nonmajority 
party candidates, assume that they could not run? Or does the 
bill assume that existing State laws could somehow cope within 
the 45-day total framework? What if they can't?
    With regard to actions or not by political parties, how 
would political parties nominate candidates within the 10 days 
allowed? Would they be required to? What happens if they don't 
or can't?
    And with regard to post-election procedures, wouldn't it 
take at least 60 days, or more in many cases, for the ultimate 
winners of the special elections to be known or finally 
certified?
    And, last, Federal court actions. Now, this is not our 
jurisdiction, but doesn't the bill provide an unreasonably 
short time frame for litigation based on the Speaker's 
announcement of a vacancy while inviting lengthy litigation 
brought on other grounds?
    And so, Mr. Chairman, those are concerns that we have with 
respect to the legislation as proposed, and we think in 
conversation with many offices of secretaries of States and 
knowing how this committee and so many Members in the Congress 
have grave concerns about unfunded State mandates and what we 
would be foisting upon the States, we think that these are 
questions that need to be answered and are problematic in going 
forward with this legislation.
    The Chairman. Thank you. Is that all the questions you 
have?
    Mr. Larson. Yes.
    The Chairman. Do you have any more?
    Mr. Larson. I do not have at this time have any more 
questions. My colleagues might, but I do not.
    The Chairman. And we are going to take a look at this, but 
I just want to comment on just a couple of items if I could.
    On the contested elections, I believe that we are going to 
have potentially contested elections with anything we do. I 
think we are going to have that possibility. As you are 
probably aware you and I are being sued in the Supreme Court 
for a contested election contest. It is by a candidate who 
thinks that this committee acted without merit, which is 
totally incorrect. But I do believe that in situations like 
this and with the trauma that would occur with whatever process 
you went with, you are going to have contested the election 
possibility, there is no question about that. When you have 100 
seats running, in a certain time frame, you probably have the 
possibility of more contested elections that could occur.
    As far as the post-election procedures, I believe that 
there are a lot of set procedures in most States. I know our 
elections were held within 12 days, in the State of Ohio, with 
the recount or certification. And I think that is going to have 
to be looked at and also to be considered. So, with regard to 
direct questions, there are questions that have to be answered 
and need to be answered, and we will need to talk to the bill's 
primary sponsor to get answers to these questions. But I did 
want to reflect right off the top.
    Mr. Larson. Mr. Chairman, also in fairness to you and to 
the proponents, we also have further data that might help them. 
The goal here on our part is to better help the Members, better 
help the legislation as it proceeds. But these are, I think, 
very legitimate questions that need to be answered, and 
obviously we would like to have them before they go to the 
floor.
    The Chairman. And I think they are legitimate. Also, for 
example, another question that hasn't been answered completely 
in my mind, to be frank, is the disability question. It is a 
question I had throughout the discussion. I asked that question 
previously. We have yet to get an answer about disability: Who 
determines disability, what is disability? Are you temporarily 
disabled? Is it permanent?
    So I also agree. So we will talk with the bill's sponsor 
and get written answers to these and, by the way, any other 
questions you might have as this continues through the process.
    Ms. Millender-McDonald. Mr. Chairman, again I thank you for 
holding this meeting, too, because this question has been 
raised many times after 9/11. And I too, as the Ranking Member 
has so admirably indicated, would love to come to an amicable 
agreement on this issue. But there are a lot of questions that 
are left unanswered, and this is a very delicate issue.
    One of the questions that come to mind for me is that we 
constantly talk about the Speaker. Now, we know when certain 
things happen, the Speaker is ushered on to an undisclosed 
area. But what if he does not have the opportunity to get 
there, will he or she have an opportunity to get to an 
undisclosed area for safety reasons? Is there anything that we 
can do in this bill to speak to the next person in authority, 
given that Speaker may not be the person who will be able to 
carry out the various sections of this bill?
    And the other question that I have is if the number of 
Members' vacancies is less than 100, will the bill's provisions 
still take effect regarding the timing for a special election?
    Those are some of the questions, along with the Ranking 
Member who spoke about the disabled, military personnel and 
others who are working overseas. We certainly have many times 
been concerned that they have been left out. So those are the 
questions that I have that will add to the ones that the 
Ranking Member has already very admirably outlined.
    The Chairman. We will also pass those on to the bill's 
primary sponsor and make sure that we get written answers back, 
and any other questions that may generate off of those 
responses also to the members.
    Other members? Is there any further discussion?
    Mr. Larson. Mr. Chairman, I would just like to--I know you 
usually say that I just wanted to seek unanimous consent to 
introduce the full text and questions that I raised during the 
course of the debate, and then ask that when a vote be taken, 
it be taken by roll.
    The Chairman. Roll call? Okay. The Chair lays before the 
committee House Resolution 2844, open to an amendment.
    And the Chair offers an amendment in the nature of a 
substitute. The question is on the amendment in the nature of a 
substitute.
    Those in favor of the amendment will say aye.
    Those opposed will say nay.
    And the Ranking Member has asked for a roll call. The clerk 
will call the roll.
    The Clerk. Mr. Ehlers.
    Mr. Ehlers. Yes.
    The Clerk. Mr. Mica.
    Mr. Mica. Aye.
    The Clerk. Mr. Linder.
    Mr. Linder. Aye.
    The Clerk. Mr. Doolittle.
    [No response.]
    The Clerk. Mr. Reynolds.
    [No response.]
    The Clerk. Mr. Larson.
    Mr. Larson. Nay.
    The Clerk. Ms. Millender-McDonald.
    Ms. Millender-McDonald. No.
    The Clerk. Mr. Brady.
    Mr. Brady. No.
    The Clerk. Chairman Ney.
    The Chairman. Aye.
    The results are 4 in favor, 3 against. The amendment is 
agreed to.
    The Chair recognizes Mr. Ehlers for the purpose of offering 
a motion.
    Mr. Ehlers. Mr. Chairman, I move that the committee order 
House Resolution 2844, as amended, reported favorably to the 
House of Representatives.
    The Chairman. The question is on the motion.
    Those in favor of the motion will say aye.
    Mr. Larson. Mr. Chairman, could we have a recorded vote on 
that?
    Mr. Larson. Mr. Chairman, I announce pursuant to the 
provisions of clause 2 of rule XI, it is my intention to seek 
not less than 2 additional calendar days provided by the rule 
to prepare additional views to be filed with the committee 
report.
    The Chairman. Without objection.
    I ask unanimous consent that members have 7 legislative 
days for statements and material to be entered in the 
appropriate place in the record.
    Without objection, the material will be entered.
    [The information follows:]

                       [COMMITTEE INSERT TO COME]

    The Chairman. I ask unanimous consent that staff be 
authorized to make technical and conforming changes on all 
matters considered by the committee at today's meeting.
    Without objection, so ordered.
    And, having completed our business, I want to thank all the 
members today. And the committee is hereby adjourned.
    [Whereupon, at 3:51 p.m., the committee was adjourned.]

                                <greek-d>

</pre></body></html>
