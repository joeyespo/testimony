<html>
<title> - TREASURY, POSTAL SERVICE, AND GENERAL GOVERNMENT APPROPRIATIONS FOR FISCAL YEAR 2001</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]



 
                 TREASURY, POSTAL SERVICE, AND GENERAL

                     GOVERNMENT APPROPRIATIONS FOR

                            FISCAL YEAR 2001

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS

                             SECOND SESSION
                                ________

  SUBCOMMITTEE ON THE TREASURY, POSTAL SERVICE, AND GENERAL GOVERNMENT 
                             APPROPRIATIONS

                      JIM KOLBE, Arizona, Chairman

 FRANK R. WOLF, Virginia            STENY H. HOYER, Maryland
 ANNE M. NORTHUP, Kentucky          CARRIE P. MEEK, Florida
 JO ANN EMERSON, Missouri           DAVID E. PRICE, North Carolina
 JOHN E. SUNUNU, New Hampshire      LUCILLE ROYBAL-ALLARD, California
 JOHN E. PETERSON, Pennsylvania     
 VIRGIL H. GOODE, Jr., Virginia     

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.

       Michelle Mrdeza, Jeff Ashford, Kurt Dodd, and Tammy Hughes,
                            Staff Assistants
                                ________

                                 PART 5

              STATEMENTS OF MEMBERS OF CONGRESS AND OTHER


                INTERESTED INDIVIDUALS AND ORGANIZATIONS


                              <snowflake>

                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
 64-677 O                   WASHINGTON : 2000

                       COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                     DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California                JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois           NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky                MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico                  JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia                STENY H. HOYER, Maryland
 TOM DeLAY, Texas                       ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                     MARCY KAPTUR, Ohio
 RON PACKARD, California                NANCY PELOSI, California
 SONNY CALLAHAN, Alabama                PETER J. VISCLOSKY, Indiana
 JAMES T. WALSH, New York               NITA M. LOWEY, New York
 CHARLES H. TAYLOR, North Carolina      JOSE E. SERRANO, New York
 DAVID L. HOBSON, Ohio                  ROSA L. DeLAURO, Connecticut
 ERNEST J. ISTOOK, Jr., Oklahoma        JAMES P. MORAN, Virginia
 HENRY BONILLA, Texas                   JOHN W. OLVER, Massachusetts
 JOE KNOLLENBERG, Michigan              ED PASTOR, Arizona
 DAN MILLER, Florida                    CARRIE P. MEEK, Florida
 JAY DICKEY, Arkansas                   DAVID E. PRICE, North Carolina
 JACK KINGSTON, Georgia                 MICHAEL P. FORBES, New York
 RODNEY P. FRELINGHUYSEN, New Jersey    CHET EDWARDS, Texas
 ROGER F. WICKER, Mississippi           ROBERT E. ``BUD'' CRAMER, Jr., 
 GEORGE R. NETHERCUTT, Jr.,             Alabama
Washington                              MAURICE D. HINCHEY, New York
 RANDY ``DUKE'' CUNNINGHAM,             LUCILLE ROYBAL-ALLARD, California
California                              SAM FARR, California
 TODD TIAHRT, Kansas                    JESSE L. JACKSON, Jr., Illinois
 ZACH WAMP, Tennessee                   CAROLYN C. KILPATRICK, Michigan
 TOM LATHAM, Iowa                       ALLEN BOYD, Florida
 ANNE M. NORTHUP, Kentucky              
 ROBERT B. ADERHOLT, Alabama            
 JO ANN EMERSON, Missouri               
 JOHN E. SUNUNU, New Hampshire          
 KAY GRANGER, Texas                     
 JOHN E. PETERSON, Pennsylvania         
 VIRGIL H. GOODE, Jr., Virginia     

                 James W. Dyer, Clerk and Staff Director

                                  (ii)

<GRAPHIC(S) NOT AVIALABLE IN TIFF FORMAT>


                           W I T N E S S E S

                              ----------                              
                                                                   Page
Berne, B.H.......................................................    46
Kelley, C.M......................................................    36
Larson, E.M......................................................     6
Sterling, E.E....................................................    32
Visclosky, Hon. P.J..............................................     2


                               I N D E X

                              ----------                              
Office of National Drug Control Policy Related Matters:
                                                                   Page
    Honorable Peter Visclosky (D-IN).............................     2
    Eric Sterling, President, The Criminal Justice Policy 
      Foundation.................................................    32
Bureau of Alcohol, Tobacco and Firearms Related Matters: 
  Collector Arms Dealers Association, Eric Larson................     6
Federal Employee Related Matters: National Treasury Employees 
  Union, Colleen M. Kelly........................................    36
General Services Administration Related Matters: Bernard H. 
  Berne, M.D., Ph.D..............................................    46

                                <all>

</pre></body></html>
