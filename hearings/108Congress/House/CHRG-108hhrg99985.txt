<html>
<title> - H.R. 4032 AND A DRAFT BILL, THE VETERANS SELF-EMPLOYMENT ACT OF 2004 </title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]


 
                H.R. 4032 AND A DRAFT BILL, THE VETERANS
                       SELF-EMPLOYMENT ACT OF 2004
=============================================================================

                                 HEARING

                                before the

                         SUBCOMMITTEE ON BENEFITS

                                  of the

                      COMMITTEE ON VETERANS' AFFAIRS

                         HOUSE OF REPRESENTATIVES

                       one hundred eighth congress

                               first session

                                  -------

                                June 16, 2004

                                  -------

          Printed for the use of the Committee on Veterans' Affairs

                              Serial No. 108-44


                                    -----

                       U.S. GOVERNMENT PRINTING OFFICE

99-985pdf                     WASHINGTON : 2005
-----------------------------------------------------------------------------
For the sale by the Superintendent of Documents, U.S. Government Printing Office  Internet: bookstore.gpo.gov  Phone: toll-free (866) 512-1800; DC area (202) 512-1800 Fax: (202) 512-2250  Mail: Stop SSOP, Washington, DC 20402-0001

<TEXT NOT AVAILABLE IN TIFF FORMAT>


</pre></body></html>
