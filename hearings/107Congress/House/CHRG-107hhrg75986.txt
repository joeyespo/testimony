<html>
<title> - IMPROVING ACADEMIC ACHIVEMENT WITH FREEDOM AND ACCOUNTABILITY</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]




 
                    IMPROVING ACADEMIC ACHIVEMENT 
                   WITH FREEDOM AND ACCOUNTABILITY

=======================================================================

                                HEARING

                               before the

                      COMMITTEE ON EDUCATION AND
                             THE WORKFORCE

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             FIRST SESSION

                               __________

             HEARING HELD IN CHICAGO, ILLINOIS, MARCH 2, 2001

                               __________

                           Serial No. 107-4

                               __________

            Printed for the use of the Committee on Education
                           and the Workforce

    [GRAPHIC] [TIFF OMITTED]

</pre></body></html>
