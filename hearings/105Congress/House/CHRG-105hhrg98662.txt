<html>
<title> - EXPECTANT MOTHERS AND SUBSTANCE ABUSE: INTERVETION AND TREATMENT CHALLENGES FOR STATE GOVERNMENT</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


<DOC>
[DOCID: f:98662.wais]
 
                EXPECTANT MOTHERS AND SUBSTANCE ABUSE: 
                 INTERVETION AND TREATMENT CHALLENGES 
                          FOR STATE GOVERNMENT 

=============================================================================


                                 HEARINGS

                                before the

                    SUBCOMMITTEE ON NATIONAL SECURITY
              INTERNATIONAL AFFAIRS, AND CRIMINAL JUSTICE

                                 of the

                        COMMITTEE ON GOVERNMENT
                          REFORM AND OVERSIGHT


                        HOUSE OF REPRESENTATIVES

                       one hundred fifth congress

                              second session

                                  -------

                               July 23, 1998

                                  -------

                            Serial No. 105-210

                                  -------

  Printed for the use of the Committee on Government Reform and Oversight


                    U.S. GOVERNMENT PRINTING OFFICE
57-689                      WASHINGTON : 1999
_____________________________________________________________________________
             For sale by the U.S. Government Printing office
Superintendent of Documents, Congressional Sales Office: Washington, DC 20402
                            ISBN 0-16-059529-2

<TEXT NOT AVAILABLE IN TIFF FORMAT>


</pre></body></html>
