<html>
<title> - H.R. 1460, THE VETERANS ENTREPRENEURSHIP ACT OF 2003; H.R. 1712, THE VETERANS FEDERAL PROCUREMENT OPPORTUNITY ACT OF 2003; AND H.R. 1716, THE VETERANS EARN AND LEARN ACT </title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]


 
                        H.R. 1460, THE VETERANS ENTREPRENEURSHIP 
                           ACT OF 2003; H.R. 1712, THE VETERANS 
                           FEDERAL PROCUREMENT OPPORTUNITY ACT OF 
                           2003; AND H.R. 1716, THE VETERANS EARN 
                           AND LEARN ACT
=======================================================================




                                HEARING

                               BEFORE THE

                        SUBCOMMITTEE ON BENEFITS
                           
                                 OF THE 

                       COMMITTEE ON VETERANS' AFFAIRS
                         HOUSE OF REPRESENTATIVES

                          ONE HUNDRED EIGHTH CONGRESS

                              FIRST SESSION
                               _____________

                               APRIL 30, 2003
                               _____________

          Printed for the use of the Committee on Veterans' Affairs

                            Serial No. 108-10


                               _____________

                      U.S. GOVERNMENT PRINTING OFFICE
96-380 PDF                    WASHINGTON  :  2004
---------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government
Printing Office Internet:  bookstore.gpo.gov Phone:  toll free (866)
512-1800; DC area (202) 512-1800 Fax: (202)512-2250 Mail: Stop SSOP,
Washington, DC 20402-0001 



TEXT NOT AVAILABLE REFER TO PDF

</pre></body></html>
