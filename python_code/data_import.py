# -*- coding: utf-8 -*-
"""
Created on Wed Dec 10 18:14:42 2014

@author: ian
"""

"""
This code brings in the filenames from the hearing directories.
These filenames will server as the observation identifiers for now.
Code modified from original found at: 
http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
""" 
import os, fnmatch
from bs4 import BeautifulSoup
from bs4 import UnicodeDammit
import re, csv
import codecs

mydir = '/Users/ian/Dropbox/Academia/Dissertation/testimony/hearings/'

def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                fullpath = os.path.join(root, basename)
                yield fullpath
                
''' 
The following code calls the process to find a file
then opens it up to read it in. This is to clean the file of HTML and 
non ASCII stuff so later processing doesn't choke.

At the end, just the filename is saved in the dataframe so it 
becomes the row observation
'''
filelist = []

for filename in find_files(mydir, '*.txt'):
    route, basename = os.path.split(filename)
    infilehandle = codecs.open(filename, 'r') #get the file   
    soup = BeautifulSoup(infilehandle.read()).text
    infilehandle.close()    #close file to prevent keeping old text
    infilehandle = codecs.open(filename, 'w', 'utf-8') #reopen file w/o old text
    infilehandle.write(soup)   #write new text to file
    infilehandle.close()        #save cleaned file
    print(len(filelist))   #Print to screen a counter to show progress
    filelist.append(basename)    #add filename to array to be used as first data column
       

# This is just some code to work with that I don't want to lose

#fout = open(mydir + 'metadata.csv', 'w')
#outfilehandle = csv.writer(fout,
#                   delimiter=",",
#                   quotechar='"',
#                   quoting=csv.QUOTE_NONNUMERIC)

# testhtml = codecs.open(mydir+"105Congress/House/CHRG-105hhrg40073.txt", "rU", "utf-8")
# print(stripNonAscii(testhtml))

#This code can take in the object directly from codecs.open
#def stripHtmlTags(htmlTxt):
#    if htmlTxt is None:
#        return None
#    else:
#        return ''.join(BeautifulSoup(htmlTxt).findAll(text=True)) 
        
#The following requires a string or buffer
#since it it using regexp
#def stripNonAscii(text):
#    return re.sub(r'[^\x00-\x7F]',' ', text)

# fileCleaner(mydir, '*.txt')