<html>
<title> - STATUS OF EFFORTS TO IDENTIFY GULF WAR</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                  STATUS OF EFFORTS TO IDENTIFY GULF WAR

                    VETERANS' ILLNESSES:  TUMOR DATA                 ______________________________________________________________


                               HEARING 

                              BEFORE THE

                  SUBCOMMITTEE ON HUMAN RESOURCES
                   
                               OF THE

                        COMMITTEE ON GOVERNMENT

                         REFORM AND OVERSIGHT

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION

                             _____________

                              MAY 14, 1998

                              ___________

                           Serial No. 105-163

Printed for the use of the Committee on Government Reform and Oversight






                        U.S. GOVERNMENT PRINTING OFFICE

50-804                        WASHINGTON : 1998
_____________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing
Office Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800
Fax: (202) 512-2250  Mail: Stop SSOP, Washington, DC  20402-0001




[TEXT NOT AVAILABLE]

</pre></body></html>
