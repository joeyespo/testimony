<html>
<title> - H.R. 699, MILITARY VOTING RIGHTS ACT OF 1997</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]




 
                H.R. 699, MILITARY VOTING RIGHTS ACT OF
                                  1997

=======================================================================

                                HEARING

                               before the

                     COMMITTEE ON VETERANS' AFFAIRS
                     U.S. HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             FIRST SESSION

                               __________

                              JUNE 4, 1997

                               __________

       Printed for the use of the Committee on Veterans' Affairs


                           Serial No. 105-11



                  U.S. GOVERNMENT PRINTING OFFICE
43-580CC                  WASHINGTON : 1997
-----------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing Office, 
http://bookstore.gpo.gov. For more information, contact the GPO Customer Contact Center, U.S. Government Printing Office. Phone 202�09512�091800, or 866�09512�091800 (toll-free). E-mail, gpo@custhelp.com.  


<TEXT FILE NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
