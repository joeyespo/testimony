<html>
<title> - MARKUP of H.R. 2134</title>
<body><pre>
[House Hearing, 109 Congress]
[From the U.S. Government Printing Office]



                                [ERRATA]
 
                          MARKUP of H.R. 2134

=======================================================================

                                 MARKUP

                               before the

                              COMMITTEE ON
                          HOUSE ADMINISTRATION

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED NINTH CONGRESS

                             SECOND SESSION

                               __________

             MEETING HELD IN WASHINGTON, DC, JULY 27, 2006

                               __________

      Printed for the use of the Committee on House Administration


                    U.S. GOVERNMENT PRINTING OFFICE
32-719                      WASHINGTON : 2007
_____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092250 Mail: Stop SSOP, Washington, DC 20402�090001


                                 ERRATA

The referenced Markup before the Committee on House 
  Administration was inadvertently printed with the wrong 
  Committee page. The correct committee information is as 
  follows:

                   COMMITTEE ON HOUSE ADMINISTRATION

                  VERNON J. EHLERS, Michigan, Chairman

BOB NEY, Ohio                        JUANITA MILLENDER-McDONALD, 
JOHN L. MICA, Florida                    California, Ranking Minority 
JOHN T. DOOLITTLE, California            Member
THOMAS M. REYNOLDS, New York         ROBERT A. BRADY, Pennsylvania,
CANDICE S. MILLER, Michigan          ZOE LOFGREN, California

                           Professional Staff
                      Will Plaster, Staff Director
               George F. Shevlin, Minority Staff Director

</pre></body></html>
