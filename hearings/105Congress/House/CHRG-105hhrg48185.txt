<html>
<title> - H.R. 716, FREEDOM FROM GOVERNMENT COMPETITION ACT OF 1997</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                   H.R. 716, FREEDOM FROM GOVERNMENT
                    
                          COMPETITION ACT OF 1997

____________________________________________________________________________


                               HEARING 

                              BEFORE THE


                 SUBCOMITTEE ON GOVERNMENT MANAGEMENT,
                   
                      INFORMATION, AND TECHNOLOGY 

                                OF THE

                        COMMITTEE ON GOVERNMENT

                         REFORM AND OVERSIGHT

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                               FIRST SESSION

                                    ON

                                H.R. 716

 TO REQUIRE THAT THE FEDERAL GOVERNMENT PROCURE FROM THE PRIVATE
 SECTOR THE GOODS AND SERVICES NECESSARY FOR THE OPERATIONS AND 
 MANAGEMENT OF CERTAIN GOVERNMENT AGENCIES, AND FOR OTHER PURPOSES

                            _______________

                           SEPTEMBER 29, 1997

                            _______________

                           SERIAL NO. 105-105

Printed for the use of the Committee on Government Reform and Oversight



                        U.S. GOVERNMENT PRINTING OFFICE

48-185                          WASHINGTON : 1998
_____________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing
Office Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800
Fax: (202) 512-2250  Mail: Stop SSOP, Washington, DC  20402-0001

                                    
[TEXT NOT AVAILABLE]
</pre></body></html>
