<html>
<title> - OVERSIGHT OF THE 2000 CENSUS: REVIEWING THE LONG AND SHORT FORM QUESTIONNAIRES </title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


 
                    OVERSIGHT OF THE 2000 CENSUS: REVIEWING
                    THE LONG AND SHORT FORM QUESTIONNAIRES
==============================================================================


                                      HEARING

                                    before the

                           SUBCOMMITTEE ON THE CENSUS

                                       of the


                                    COMMITTEE ON
                                 GOVERNMENT REFORM
                                    AND OVERSIGHT

                             HOUSE OF REPRESENTATIVES

                           one hundred fifth congress
                                  second session

                                      -------

                                    May 21, 1998

                                      -------

                                 Serial No. 105-180

                                      -------

  Printed for the use of the Committee on Government Reform and Oversight

                        U.S. GOVERNMENT PRINTING OFFICE
52-519                          WASHINGTON : 1998
____________________________________________________________________________
               For sale by the U.S.Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402
                               ISBN 0-16-058497-3


<TEXT FILE NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
