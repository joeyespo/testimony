<html>
<title> - DEPARTMENT OF TRANSPORTATION AND RELATED AGENCIES APPROPRIATIONS FOR 1999</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


<DOC>
 
                    DEPARTMENT OF TRANSPORTATION AND
                    RELATED AGENCIES APPROPRIATIONS
                                FOR 1999

========================================================================

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION
                                ________
SUBCOMMITTEE ON THE DEPARTMENT OF TRANSPORTATION AND RELATED AGENCIES APPROPRIATIONS

                    FRANK R. WOLF, Virginia, Chairman

TOM DeLAY, Texas             MARTIN OLAV SABO, Minnesota
RALPH REGULA, Ohio           ESTEBAN EDWARD TORRES, California
HAROLD ROGERS, Kentucky      JOHN W. OLVER, Massachusetts
RON PACKARD, California      ED PASTOR, Arizona
SONNY CALLAHAN, Alabama      ROBERT E. (BUD) CRAMER, Jr., Alabama
TODD TIAHRT, Kansas          
ROBERT B. ADERHOLT, Alabama  

NOTE: Under Committee Rules, Mr. Livingston, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.

John T. Blazey II, Richard E. Efford, Stephanie K. Gupta, and Linda J. Muir,
                            Subcommittee Staff
                                ________

                                 PART 2

                       1999 BUDGET JUSTIFICATIONS

 DEPARTMENT OF TRANSPORTATION:
                                                                   Page
   Federal Aviation Administration................................    1
   Federal Railroad Administration................................  588
        Grants to Amtrak..........................................  797
   Federal Transit Administration.................................  831
   Office of the Secretary........................................ 1047
 RELATED AGENCIES:
   National Transportation Safety Board........................... 1131
                                ________

         Printed for the use of the Committee on Appropriations
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
46-798 O                    WASHINGTON : 1998

------------------------------------------------------------------------

             For sale by the U.S. Government Printing Office            
        Superintendent of Documents, Congressional Sales Office,        
                          Washington, DC 20402                          







                       COMMITTEE ON APPROPRIATIONS                      

                   BOB LIVINGSTON, Louisiana, Chairman                  

JOSEPH M. McDADE, Pennsylvania         DAVID R. OBEY, Wisconsin            
C. W. BILL YOUNG, Florida              SIDNEY R. YATES, Illinois           
RALPH REGULA, Ohio                     LOUIS STOKES, Ohio                  
JERRY LEWIS, California                JOHN P. MURTHA, Pennsylvania        
JOHN EDWARD PORTER, Illinois           NORMAN D. DICKS, Washington         
HAROLD ROGERS, Kentucky                MARTIN OLAV SABO, Minnesota         
JOE SKEEN, New Mexico                  JULIAN C. DIXON, California         
FRANK R. WOLF, Virginia                VIC FAZIO, California               
TOM DeLAY, Texas                       W. G. (BILL) HEFNER, North Carolina 
JIM KOLBE, Arizona                     STENY H. HOYER, Maryland            
RON PACKARD, California                ALAN B. MOLLOHAN, West Virginia     
SONNY CALLAHAN, Alabama                MARCY KAPTUR, Ohio                  
JAMES T. WALSH, New York               DAVID E. SKAGGS, Colorado           
CHARLES H. TAYLOR, North Carolina      NANCY PELOSI, California            
DAVID L. HOBSON, Ohio                  PETER J. VISCLOSKY, Indiana         
ERNEST J. ISTOOK, Jr., Oklahoma        ESTEBAN EDWARD TORRES, California   
HENRY BONILLA, Texas                   NITA M. LOWEY, New York             
JOE KNOLLENBERG, Michigan              JOSE E. SERRANO, New York           
DAN MILLER, Florida                    ROSA L. DeLAURO, Connecticut        
JAY DICKEY, Arkansas                   JAMES P. MORAN, Virginia            
JACK KINGSTON, Georgia                 JOHN W. OLVER, Massachusetts        
MIKE PARKER, Mississippi               ED PASTOR, Arizona                  
RODNEY P. FRELINGHUYSEN, New Jersey    CARRIE P. MEEK, Florida             
ROGER F. WICKER, Mississippi           DAVID E. PRICE, North Carolina      
MICHAEL P. FORBES, New York            CHET EDWARDS, Texas                 
GEORGE R. NETHERCUTT, Jr., Washington  ROBERT E. (BUD) CRAMER, Jr., Alabama
MARK W. NEUMANN, Wisconsin             
RANDY ``DUKE'' CUNNINGHAM, California  
TODD TIAHRT, Kansas                    
ZACH WAMP, Tennessee                   
TOM LATHAM, Iowa                       
ANNE M. NORTHUP, Kentucky              
ROBERT B. ADERHOLT, Alabama            

                 James W. Dyer, Clerk and Staff Director



</pre></body></html>
