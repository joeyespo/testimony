<html>
<title> - OVERSIGHT OF U.S. REGIONAL COUNTERDRUG EFFORTS</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
             OVERSIGHT OF U.S. REGIONAL COUNTERDRUG EFFORTS

=======================================================================

                                HEARING

                               before the

                   SUBCOMMITTEE ON NATIONAL SECURITY,

              INTERNATIONAL AFFAIRS, AND CRIMINAL JUSTICE

                                 OF THE

                        COMMITTEE ON GOVERNMENT

                          REFORM AND OVERSIGHT

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION

                               __________

                             MARCH 12, 1998

                               __________

                           Serial No. 105-137

                               __________

Printed for the use of the Committee on Government Reform and Oversight






                 U.S. GOVERNMENT PRINTING OFFICE

49-698                 WASHINGTON : 1998
_________________________________________________________________
For sale by the U.S. Government Printing Office 
Superintendent of Documents, Congressional Sales Office, 
Washington, DC 20402
               ISBN 0-16-057304-1

</pre></body></html>
