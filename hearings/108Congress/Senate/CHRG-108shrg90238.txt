<html>
<title> - NOMINATIONS OF DALE CABANISS, CRAIG S. ISCOE, AND BRIAN F. HOLEMAN</title>
<body><pre>
[Senate Hearing 108-292]
[From the U.S. Government Printing Office]



                                                        S. Hrg. 108-292

   NOMINATIONS OF DALE CABANISS, CRAIG S. ISCOE, AND BRIAN F. HOLEMAN

=======================================================================


                                HEARING

                               before the


                              COMMITTEE ON
                          GOVERNMENTAL AFFAIRS
                          UNITED STATES SENATE

                      ONE HUNDRED EIGHTH CONGRESS

                             FIRST SESSION

                                 ON THE

 NOMINATIONS OF DALE CABANISS TO BE CHAIRMAN, FEDERAL LABOR RELATIONS 
 AUTHORITY; CRAIG S. ISCOE AND BRIAN F. HOLEMAN TO BE ASSOCIATE JUDGES 
           OF THE SUPERIOR COURT OF THE DISTRICT OF COLUMBIA

                               __________

                           SEPTEMBER 30, 2003

                               __________

      Printed for the use of the Committee on Governmental Affairs




90-238              U.S. GOVERNMENT PRINTING OFFICE
                            WASHINGTON : 2003
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092250 Mail: Stop SSOP, Washington, DC 20402�090001

                   COMMITTEE ON GOVERNMENTAL AFFAIRS

                   SUSAN M. COLLINS, Maine, Chairman
TED STEVENS, Alaska                  JOSEPH I. LIEBERMAN, Connecticut
GEORGE V. VOINOVICH, Ohio            CARL LEVIN, Michigan
NORM COLEMAN, Minnesota              DANIEL K. AKAKA, Hawaii
ARLEN SPECTER, Pennsylvania          RICHARD J. DURBIN, Illinois
ROBERT F. BENNETT, Utah              THOMAS R. CARPER, Deleware
PETER G. FITZGERALD, Illinois        MARK DAYTON, Minnesota
JOHN E. SUNUNU, New Hampshire        FRANK LAUTENBERG, New Jersey
RICHARD C. SHELBY, Alabama           MARK PRYOR, Arkansas
           Michael D. Bopp, Staff Director and Chief Counsel
                    Johanna L. Hardy, Senior Counsel
   Theresa Prych, Professional Staff Member, Oversight of Government 
    Management, the Federal Workforce, and the District of Columbia 
                              Subcommittee
      Joyce A. Rechtschaffen, Minority Staff Director and Counsel
  Marianne Clifford Upton, Minority Staff Director and Chief Counsel, 
  Oversight of Government Management, the Federal Workforce, and the 
                   District of Columbia Subcommittee
           Jennifer E. Hamilton, Minority Research Assistant
                      Amy B. Newhouse, Chief Clerk


                            C O N T E N T S

                                 ------                                
Opening statements:
                                                                   Page
    Senator Voinovich............................................     1
    Senator Stevens..............................................     1

                               WITNESSES
                      Tuesday, September 30, 2003

Hon. Dale Cabaniss to be Chairman, Federal Labor Relations 
  Authority......................................................     3
Hon. Eleanor Holmes Norton, a Delegate in Congress from the 
  District of Columbia...........................................     7
Craig S. Iscoe to be an Associate Judge of the Superior Court of 
  the District of Columbia.......................................     8
Brian F. Holeman to be an Associate Judge of the Superior Court 
  of the District of Columbia....................................     8

                     Alphabetical List of Witnesses

Cabaniss, Dale:
    Testimony....................................................     3
    Biographical and professional information requested of 
      nominees...................................................    15
    Pre-hearing questionnaire....................................    23
    Post-hearing questions and responses from Senator Voinovich..    47
Holeman, Brian F.:
    Testimony....................................................     8
    Biographical and professional information requested of 
      nominees...................................................    84
Iscoe, Craig S.:
    Testimony....................................................     8
    Biographical and professional information requested of 
      nominees...................................................    50
Norton, Hon. Eleanor Holmes:
    Testimony....................................................     7

                                Appendix

Hon. Paul Strauss, U.S. Senator from the District of Columbia 
  (Shadow), prepared statement...................................    11

 
   NOMINATIONS OF DALE CABANISS, CRAIG S. ISCOE, AND BRIAN F. HOLEMAN

                              ----------                              


                      TUESDAY, SEPTEMBER 30, 2003

                                       U.S. Senate,
                         Committee on Governmental Affairs,
                                                    Washington, DC.
    The Committee met, pursuant to notice, at 9:02 a.m., in 
room SD-342, Dirksen Senate Office Building, Hon. George V. 
Voinovich, presiding.
    Present: Senators Voinovich and Stevens.

             OPENING STATEMENT OF SENATOR VOINOVICH

    Senator Voinovich. The Committee will come to order.
    Good morning, and thank you all for coming. Today, the 
Governmental Affairs Committee meets to discuss the nominations 
of Dale Cabaniss for Chairman of the Federal Labor Relations 
Authority, and Brian Holeman and Craig Iscoe for Associate 
Judges of the District of Columbia Superior Court.
    We are going to begin our hearing this morning with the 
FLRA nomination, and I would like to extend my greetings to Ms. 
Cabaniss and her family, and I would like to also welcome my 
colleague, Senator Stevens, who is going to be introducing Ms. 
Cabaniss.
    Senator Stevens, I know you are a very busy Senator, and we 
look forward to your words.

              OPENING STATEMENT OF SENATOR STEVENS

    Senator Stevens. Thank you very much, Mr. Chairman. I am 
pleased to be here. I think this is the third time I have been 
here to introduce Dale Cabaniss to this Committee.
    Dale worked for our Alaska delegation, first for Senator 
Frank Murkowski, and then for me, for 13 years. She was the 
person who served as my adviser and counsel to the Governmental 
Affairs Subcommittee on Post Office and Civil Service, and she 
was also an important member of our professional staff for the 
Senate Appropriations Committee on the Subcommittee on Labor, 
Health and Human Services.
    Her Alaska roots are very deep, Mr. Chairman. Her husband, 
Mitch, who is here, Mitch Rose, was my chief of staff, and Dale 
is the mother of three children: Ben, and twin girls, Haley and 
Shelby. Her father-in-law is one of my close friends, and I 
know that family means a lot to Dale and to Mitch. We are also 
pleased that Dale's brother, Major Christian Cabaniss, a Marine 
officer, is with us today.
    Dale's father-in-law is the former director of the Alaska 
Permanent Fund. Some of you may wonder about the Permanent 
Fund, but that is the fund from which we get our Alaska 
dividends. It is a very important function of our State.
    Dale is part of a public service family. Her commitment to 
government service is obvious, and she has a great deal of 
experience. Dale's extensive experience goes beyond the 
Congress, Mr. Chairman. She was a member of the Federal Labor 
Relations Authority in several capacities. She was originally 
appointed a member of the FLRA by President Clinton in 1997 as 
a minority member then of the Board. In March 2001, she served 
as chairman of that agency, and she has been nominated now for 
another 5-year term as a member of the FLRA, and I am here this 
morning to strongly support her nomination and urge that she be 
reported to the floor.
    Dale has been an advocate of an effective bipartisan voice 
on civil service on public and private sector labor issues. She 
serves as a member of the FLRA's three-member panel that 
adjudicates cases and makes over 1,000 decisions a year. These 
disputes are often contentious, and Dale has earned the 
reputation as a fair and balanced arbitrator.
    She has also had extensive experience with the Federal 
labor-management relations statute and the Federal budget and 
appropriations process. She understands the challenges that 
Federal agencies and employees face, and her own experiences 
with government, both in the Congress and the Executive Branch, 
give her a unique perspective when it comes to resolving 
disputes that come before her agency. I know, and hope the 
Committee will agree, that this type of experience is valuable 
for all of us on the FLRA.
    Since 2001, Dale has fulfilled the role of administrative 
CEO for the entire FLRA. She has adeptly spread out her 
responsibilities, which include managing human capital, 
steering the agency through the budget and appropriations 
process, and dealing with all the issues related to agency 
planning and performance.
    In short, I am really happy to be here to present to you a 
public servant with a distinguished background who really 
deserves early approval by this Committee. I am pleased to 
serve with you on this and hope we can quickly move her 
nomination for another 5-year term on the FLRA. I appreciate 
very much your courtesy.
    Senator Voinovich. Thank you, Senator Stevens. I know that 
you would like to remain for the rest of the hearing, but you 
have other things to do. Again, thank you for being here.
    Senator Stevens. I am sure we all are going to leave rather 
soon. We have two votes in just a few minutes.
    Senator Voinovich. I understand.
    Senator Stevens. I thank you for your courtesy, Mr. 
Chairman.
    Senator Voinovich. You are more than welcome.
    Ms. Cabaniss, I think you know that I have a special 
interest in the Federal personnel issues and will be watching 
your progress as Chairman with great interest. Even though the 
FLRA is a small independent agency, its mission is vital to the 
entire Federal service system. The FLRA provides leadership, 
establishes policies, offers guidance relating to Federal 
labor-management relations. The FLRA also resolves disputes 
under and ensures compliance with the Federal service labor-
management relations statute.
    As the current chairman, you have had the opportunity to 
gain an insider's perspective on the past, present, and future 
of labor relations, and I would be very interested in your 
opinion and observations regarding the current state of Federal 
labor relations. I also look forward to hearing how you will 
continue to ensure that the Federal labor relations statute 
remains an integral part of our civil service system.
    Ms. Cabaniss, you have filed responses to a biographical 
and financial questionnaire, answered pre-hearing questions 
submitted by the Committee, and you have had your financial 
statements reviewed by the Office of Government Ethics. Without 
objection, this information will be made a part of the hearing 
record, with the exception of the financial data, which will be 
on file and available for public inspection at the Committee 
offices.
    Our Committee rules require that all witnesses at 
nomination hearings give their testimony under oath. Therefore, 
I ask you to please stand and raise your hand.
    Do you swear that the testimony that you will give before 
the Committee will be the whole truth, and nothing but the 
truth, so help you, God?
    Ms. Cabaniss. I do.
    Senator Voinovich. Let the record note that the nominee has 
responded in the affirmative.
    You have some family members here with you this morning, 
and I thought I would give you an opportunity to introduce 
those members.

 TESTIMONY OF DALE CABANISS,\1\ TO BE CHAIRMAN, FEDERAL LABOR 
                      RELATIONS AUTHORITY

    Ms. Cabaniss. My husband, Mitch Rose, is here, as Senator 
Stevens mentioned, and my brother, Major Christian Cabaniss.
---------------------------------------------------------------------------
    \1\ The biographical and professional information for Ms. Cabaniss 
appears in the Appendix on page 15.
    Pre-hearing questionnaire for Ms. Cabaniss appears in the Appendix 
on page 23.
    Post-hearing questions and responses for Ms. Cabaniss appears in 
the Appendix on page 47.
---------------------------------------------------------------------------
    Senator Voinovich. Well, we welcome them this morning, and 
I know they are very proud of your service, and I am sure your 
husband should be thanked for the sacrifice that he makes of 
his time so that you can do the job that you have been doing 
for our country.
    I will start with some standard questions we ask all 
nominees. Is there anything you are aware of in your background 
that might present a conflict of interest with the duties of 
the office to which you have been nominated?
    Ms. Cabaniss. No.
    Senator Voinovich. Do you know of anything personal or 
otherwise that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Ms. Cabaniss. No.
    Senator Voinovich. Do you agree without reservation to 
respond to any reasonable summons to appear and testify before 
any duly constituted Committee of Congress if you are 
confirmed?
    Ms. Cabaniss. Yes.
    Senator Voinovich. I have some questions that I would like 
to ask, and I know we have got some votes coming up here this 
morning. What I am going to try and do, quite frankly, is 
continue this hearing as we move along, and then I will have to 
excuse myself to go down and then come back, and we will 
continue with our other nominees that are here this morning.
    What can you do to help the Executive Branch address its 
human capital challenges? Where I am coming from is this: That 
there is some real concern about labor relations between the 
administration and the members of the unions, the Federal 
unions. What role do you think you can play to help make this a 
better relationship?
    Ms. Cabaniss. Well, I think we do things right now to help 
better relationships because I think all good relationships are 
dependent upon communication, appropriate behavior, respect for 
each other. I think we address those issues in a number of 
ways.
    Through the decisions of the three-member Authority 
decisional component, we try to make sure that the parties who 
come to us understand their rights and responsibilities under 
the statute, that we have well-reasoned decisions that help 
people not only resolve the particular dispute that they came 
to us for resolution, but they can take something from those 
decisions that can help guide their future behavior.
    More importantly, though, I think sometimes the best thing 
is for us to be able to help people learn how to interact with 
each other effectively before they resort to litigation. We 
have a lot of different kinds of ADR programs. We have several 
training programs. We do outreach, teaching people interest-
based bargaining techniques, ways for parties to learn to work 
with each other.
    I was very happy to see the other day we got a training 
request from an agency that has had contentious contract 
negotiations with its union since the previous administration. 
I was very happy to see that they are resuming talks, and they 
were actually asking first for training before they started 
negotiations and trying to figure out how they could work 
together instead of waiting until they are in an adversarial 
stance, people have hardened their positions, and all they come 
to us to do is to decide their dispute.
    Senator Voinovich. So, in effect, you are reaching out. Can 
you initiate that or do they have to come to you for it?
    Ms. Cabaniss. We have certain places where we offer 
services as part of our process. When people come to our 
General Counsel's Office and they are seeking to file a ULP, 
there is outreach that is made there. Do you really want to go 
this route? Can we help you work on your relationship?
    When you file a negotiability appeal with the three-member 
Authority decisional component, we have pre-conferences with 
the parties; we offer again our services.
    In some ways, though, it is like anyone with a problem. We 
know where our high filers are, where agencies and unions have 
difficulties. But in some ways, the best way for these kind of 
services to work is when the parties are voluntarily coming to 
us and seeking our assistance.
    Senator Voinovich. Are you involved at all in the 
negotiations or provide any training for the people that are 
involved in negotiating new personnel practices with the newly 
created Department of Homeland Security?
    Ms. Cabaniss. The only involvement we've had in that is the 
design team asked for some training this summer to be provided. 
For some of our career staff to give some training on certain 
bargaining techniques.
    Senator Voinovich. Where do you get your----
    Ms. Cabaniss. That was provided.
    Senator Voinovich. I should know this, but where does your 
budget come from?
    Ms. Cabaniss. Treasury--I was going to say Treasury, 
Postal. It's now Transportation, Treasury.
    Senator Voinovich. Is it adequate? Do you have an adequate 
budget to get the job done?
    Ms. Cabaniss. I think it's sufficient. We're fairly small. 
We've got approximately $30 million, 215 FTEs. But I don't 
think the budget's really so much the issue, and like I said, 
we have a lot of really talented employees who have a lot of 
skills that I think people could benefit from. But I think in a 
lot of ways there are systemic problems that I've seen since 
I've been a member of the Authority, not really particular to 
this administration, where you continue to see problems, 
difficult relationships in the same agencies, in the same 
facilities, over and over and over again. If we see those or we 
think it would probably be in the best interest of those 
agencies, they've got to know where they have problem 
facilities, too, to go in and provide training perhaps to their 
managers and their employees, how to more effectively manage 
their conflicts as opposed to--here we have another case again 
from X particular place.
    Senator Voinovich. Well, you have these observations, and 
you share them with the Secretaries of the Departments?
    Ms. Cabaniss. To be honest, we don't really have that role 
as an independent adjudicator.
    Senator Voinovich. Because I know that Clay Johnson has now 
taken over the management part of OMB.
    Ms. Cabaniss. Well, I think that's exactly the place to--I 
think in the past couple of years, this Committee has been very 
active in emphasizing the M in OMB rather than just the 
dollars. And I think probably most of that leadership role for 
the agencies would be more appropriately coming from OMB.
    Senator Voinovich. He seems to be very conscientious, and I 
like his attitude. He gets it. And, if it is appropriate, I 
think it would be very good if you folks could share some of 
your observations with him so that perhaps those agencies where 
we have had some real problems could get the kind of training 
that they need so that it will improve the relationships there.
    Ms. Cabaniss. Well, and it's not just training from us. I 
think, frankly, sometimes just because you make someone a 
manager they don't automatically have that skill set. And, we 
have fewer managers than we used to have in the Federal 
Government. We've reduced the layers of middle management, and 
I think in a lot of ways managers are burdened with a lot of 
responsibilities. And I think when everyone's trying to meet 
program needs, reach mission goals, I don't know that agencies 
necessarily always have the time or the money to think, now, 
what do we need to do as far as training our managers in how to 
manage conflict, how to deal with issues before--regardless of 
the FLRA, the MSPB, EEOC, any formal process they might resort 
to, what can you do within the agencies, whether it's in-house 
ADR, employee ombudsman programs, ways to manage conflicts 
before they reach the level that you're coming to someone like 
us.
    Senator Voinovich. Do you have any kind of publication that 
says these are fundamentals that you ought to have?
    Ms. Cabaniss. No, we haven't done that. That is actually 
really much more, I think, MSPB's role in the studies that they 
do. We haven't had that responsibility under our statute. But 
it's certainly something that we're interested in, and we've 
tried to put out, I think, through our ADR services and our 
training conferences, to try and help people build 
relationships and understand that they're in it for the long 
haul and that the conflict not only costs the people who are 
involved in the conflict, but the agencies and the American 
public ultimately.
    Senator Voinovich. Well, I've got some more questions, and 
I am going to submit them to you in writing. I think that we 
may have an oversight hearing one of these days with you so we 
can spend more time talking about what you do and what the 
relationships are, because I am really committed to try and 
improve the relationships with labor and this administration. 
And so often, as you have already pointed out, a lot of it has 
to do with giving the folks the training that they need so that 
they are more capable of dealing with some of these issues that 
come up.
    So thank you very much for being here, and I hope that the 
Committee will move your nomination very fast. Thanks very much 
for being here, and we thank your husband and your brother for 
showing up this morning.
    Ms. Cabaniss. Thank you.
    Senator Voinovich. Now I would like to ask Mr. Holeman and 
Mr. Iscoe to come forward. Mr. Holeman and Mr. Iscoe are 
nominated to serve 15-year terms on the District of Columbia 
Superior Court. This Committee takes its oversight 
responsibility seriously, and I welcome today's opportunity to 
discuss the Superior Court with you.
    Confirming qualified nominees to the court is critical to 
ensuring public safety in our Nation's capital. Let me state 
for the record that Mr. Holeman and Mr. Iscoe have been 
subjected to a very thorough screening process. They were 
recommended for this position by the District's Judicial 
Nomination Committee. They both have been subjected to an FBI 
background investigation and nominated by the President after 
careful scrutiny. Since the nomination was received, Committee 
staff also has conducted a separate background check and 
interviewed Mr. Holeman and Mr. Iscoe.
    I would now like to welcome my colleague, Eleanor Holmes 
Norton from the District of Columbia, who is here to offer a 
few words of introduction for our nominees. Eleanor, it is so 
nice to see you, and thank you for coming.

TESTIMONY OF HON. ELEANOR HOLMES NORTON, A DELEGATE IN CONGRESS 
                 FROM THE DISTRICT OF COLUMBIA

    Ms. Norton. My pleasure, Mr. Chairman, and may I thank you 
again for your good works for the District of Columbia.
    The President has nominated two well-qualified candidates 
to be Association Judges of the Superior Court of the District 
of Columbia.
    Brian Holeman has extensive experience in private practice 
in the District of Columbia, Maryland, Pennsylvania, and Los 
Angeles. He has his degrees from the University of Michigan Law 
School and his bachelor's from Princeton University.
    Craig Iscoe has spent his career as a prosecutor, a trial 
attorney with administrative agencies, and a law professor. He 
is now on detail from our U.S. Attorney's Office for the 
District of Columbia to the SEC, where he is conducting 
securities litigation. He has also worked as a trial attorney 
early in his career for the FTC. His work in the U.S. 
Attorney's Office has been especially extensive: jury trials in 
major criminal cases, in public corruption, major transnational 
crimes. He has worked at Main Justice in intelligence, national 
security, white-collar matters. He has been in private practice 
here in the District of Columbia at Arent, Fox, and has been a 
clinical law professor at Vanderbilt and Georgetown Law 
Schools. We are pleased that he has worked in our local 
community as an Advisory Neighborhood Commissioner. He has his 
law degree from Stanford, his LLM from Georgetown, and he is a 
Phi Beta Kappa graduate of the University of Texas at Austin.
    We are very pleased that you are holding hearings for both 
these candidates, whom we also consider well qualified to be 
judges on our Superior Court.
    Senator Voinovich. As you know, you are welcome to remain 
for the rest of the hearing, but I suspect you have other 
appointments this morning. We thank you for coming over and 
look forward to seeing you again.
    Ms. Norton. Thank you very much, Mr. Chairman.
    Senator Voinovich. As I mentioned before, it is the custom 
of this Committee to swear in all our witnesses, and, 
therefore, I ask both of you to please stand and raise your 
right hand. Do you swear the testimony you will give before 
this Committee will be the truth, the whole truth, and nothing 
but the truth, so help you God?
    Mr. Iscoe. I do.
    Mr. Holeman. I do.
    Senator Voinovich. Let the record show they have answered 
in the affirmative.
    I understand that you may have some family members here 
today, Mr. Iscoe. I suspect that your wife and two children are 
behind you, if you want to introduce them.

TESTIMONY OF CRAIG S. ISCOE,\1\ TO BE AN ASSOCIATE JUDGE OF THE 
           SUPERIOR COURT OF THE DISTRICT OF COLUMBIA

    Mr. Iscoe. Quite correct, Mr. Chairman. Thank you. I'd like 
to introduce my wife, Rosemary Hart, and my two sons, David and 
Mark.
---------------------------------------------------------------------------
    \1\ The biographical and professional information and pre-hearing 
questions for Mr. Iscoe appear in the Appendix on page 50.
---------------------------------------------------------------------------
    Senator Voinovich. We are very happy to have you here 
today, and I have studied your husband and your father's resume 
over the years, and he has chosen to serve his country in 
various capacities over the years rather than opt for a large 
law firm and make a whole bunch of money. As one who has been 
in this business for almost 37 years, I admire the fact that 
you have been willing to sacrifice so that your husband and 
father can serve his country.
    The only comment I have is that I hope there is somebody at 
the SEC to take your place because there is some serious work 
that needs to be done by the SEC to take care of some folks 
that have not done what they are supposed to do in their 
corporate responsibilities.
    Mr. Holeman, do you have any family here that you would 
like to introduce?

 TESTIMONY OF BRIAN F. HOLEMAN,\2\ TO BE AN ASSOCIATE JUDGE OF 
         THE SUPERIOR COURT OF THE DISTRICT OF COLUMBIA

    Mr. Holeman. Thank you, Mr. Chairman. I am accompanied 
today by my wife, Susan Dunnings Holeman, who is a well-
respected lawyer here in the city in her own right. She is 
Deputy General Counsel for Employee Relations with National 
Public Radio. We decided to spare the Committee our 18-month-
old, Jonathan Taylor Holeman. He's teething now, and we 
probably would not be able to get through this proceeding.
---------------------------------------------------------------------------
    \2\ The biographical and professional information and pre-hearing 
questions for Mr. Holeman appear in the Appendix on page 84.
---------------------------------------------------------------------------
    I'd like to mention as well, Mr. Chairman, that not present 
but certainly here in spirit are my mother, Joan Holeman, and 
my father-in-law, Stuart John Dunnings, Jr.
    Senator Voinovich. We thank you also for your service. I 
notice from your background that you have had a distinguished 
career, but have chosen the private practice. As one who did 
that for about 14 years, I also understand that, and you have 
got a very distinguished background in litigation and trial 
work, and we are very happy that you are here today.
    Mr. Holeman. Thank you, sir.
    Senator Voinovich. I have questions that I have to ask all 
of you. First of all, is there anything that you are aware of 
in your background that might present a conflict of interest 
with the duties of the office to which you have been nominated?
    Mr. Iscoe. No, sir, there's not.
    Mr. Holeman. No, sir.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Mr. Iscoe. No, sir, I do not.
    Mr. Holeman. No, sir.
    Senator Voinovich. And last, but not least, do you know of 
any reason, personal or otherwise, that would in any way 
prevent you from serving the full term for the office to which 
you have been nominated?
    Mr. Iscoe. No, I do not.
    Mr. Holeman. No, sir.
    Senator Voinovich. This is a question for both of you. You 
both have had a chance to observe a variety of judicial 
temperaments, and I would like each of you to kind of discuss 
what you believe to be the appropriate temperament and approach 
of a judge not only in dealing with attorneys before you, but 
in dealing with clients and witnesses appearing before the 
bench?
    Mr. Iscoe. Mr. Chairman, I believe a judge has to be civil 
and polite at all times, and from having appeared in court, I 
can tell you that makes a difference not only to the lawyers 
who are appearing in front of the judge, but also to the 
witnesses and to the entire court process.
    In addition, a good judge has to be extremely well prepared 
to have done his or her homework, to have read the pleadings, 
not just well enough to have said he's been through them, but 
to have full command of them so that at the time that the 
lawyers are appearing in front of the judge, the judge can ask 
incisive, probing questions. That does a couple of things: It 
facilitates the process, and it also lets the lawyers know that 
they should be well prepared and ready to argue legal and 
factual issues.
    A judge also should create an aura of fairness, an absolute 
feeling in the courtroom that both the litigants and all of the 
witnesses and everybody in front of the proceedings is being 
treated fairly and their arguments are being considered. Of 
course, their arguments won't always be accepted, but it's 
quite important that they know that when they appear in front 
of the judge, the judge is treating them fairly, considering 
their arguments very carefully, and making the best decisions 
possible.
    I could go on, but I want to let Mr. Holeman respond as 
well.
    Senator Voinovich. Mr. Holeman.
    Mr. Holeman. Mr. Chairman, I believe that a judge must have 
the ability to defuse the adversarial nature of the proceedings 
while at the same time allowing counsel the latitude to do 
their respective jobs, which are, frankly, the zealous 
representation of their clients. It's a balancing act.
    I think that judges must be considerate in their handling 
of matters, but at the same time, they must be firm and certain 
in their rulings.
    I think a judge has to be collegial in his leading of 
counsel through the required work, yet serious in the 
dispensation of justice, which is the reason that litigants 
come to court in the first place.
    There are other qualities that I think that a judge must 
have. A judge must be patient with litigants and with their 
counsel. A judge must always, always uphold the dignity of the 
position, must be fair in his or her respect for each 
individual who appears before him or her, because, frankly, a 
lot of our litigants are underserved otherwise in society, and 
they look to the courts for fairness.
    A judge must always be well prepared. It's very difficult 
for a judge to demand preparation of the counsel who appear 
before him or her yet fail to be prepared him- or herself.
    And, finally, Mr. Chairman, I believe that a judge must 
always, always remember the service aspect of the job. My 
belief is that if that is remembered and is placed in a 
position of prominence, then no one will have a question about 
``robe-itis,'' as it's called. It is a service position, and 
there are people who will appear with varying levels of 
intellect, varying economic status, and they must all be served 
by the judiciary.
    Senator Voinovich. Thank you very much.
    We may have some other questions that we will be submitting 
to you in writing, but I would like to thank you both for 
coming here today. I again want to emphasize that you have been 
through the Maginot Line, both of you. You have been 
interviewed and investigated. I have spent time with the 
counsel for the White House going over your backgrounds. And I 
think that we are fortunate that we have two fine individuals 
that have been nominated, and hopefully this Committee will be 
voting on your nominations sometime in the very near future.
    Again, I would like to thank you for your willingness to 
serve your country, and thank your family for the sacrifice 
that they are going to be making so that you can fulfill those 
responsibilities.
    Thank you very much for being here.
    Mr. Holeman. Thank you, Mr. Chairman, for your kind 
remarks.
    Mr. Iscoe. Thank you, Mr. Chairman.
    [Whereupon, at 9:32 a.m., the Committee was adjourned.]

                            A P P E N D I X

                              ----------                              

[GRAPHIC] [TIFF OMITTED] T0238.001

[GRAPHIC] [TIFF OMITTED] T0238.002

[GRAPHIC] [TIFF OMITTED] T0238.003

[GRAPHIC] [TIFF OMITTED] T0238.004

[GRAPHIC] [TIFF OMITTED] T0238.005

[GRAPHIC] [TIFF OMITTED] T0238.006

[GRAPHIC] [TIFF OMITTED] T0238.007

[GRAPHIC] [TIFF OMITTED] T0238.008

[GRAPHIC] [TIFF OMITTED] T0238.009

[GRAPHIC] [TIFF OMITTED] T0238.010

[GRAPHIC] [TIFF OMITTED] T0238.011

[GRAPHIC] [TIFF OMITTED] T0238.012

[GRAPHIC] [TIFF OMITTED] T0238.013

[GRAPHIC] [TIFF OMITTED] T0238.014

[GRAPHIC] [TIFF OMITTED] T0238.015

[GRAPHIC] [TIFF OMITTED] T0238.016

[GRAPHIC] [TIFF OMITTED] T0238.017

[GRAPHIC] [TIFF OMITTED] T0238.018

[GRAPHIC] [TIFF OMITTED] T0238.019

[GRAPHIC] [TIFF OMITTED] T0238.020

[GRAPHIC] [TIFF OMITTED] T0238.021

[GRAPHIC] [TIFF OMITTED] T0238.022

[GRAPHIC] [TIFF OMITTED] T0238.023

[GRAPHIC] [TIFF OMITTED] T0238.024

[GRAPHIC] [TIFF OMITTED] T0238.025

[GRAPHIC] [TIFF OMITTED] T0238.026

[GRAPHIC] [TIFF OMITTED] T0238.027

[GRAPHIC] [TIFF OMITTED] T0238.028

[GRAPHIC] [TIFF OMITTED] T0238.029

[GRAPHIC] [TIFF OMITTED] T0238.030

[GRAPHIC] [TIFF OMITTED] T0238.031

[GRAPHIC] [TIFF OMITTED] T0238.032

[GRAPHIC] [TIFF OMITTED] T0238.033

[GRAPHIC] [TIFF OMITTED] T0238.034

[GRAPHIC] [TIFF OMITTED] T0238.035

[GRAPHIC] [TIFF OMITTED] T0238.036

[GRAPHIC] [TIFF OMITTED] T0238.037

[GRAPHIC] [TIFF OMITTED] T0238.038

[GRAPHIC] [TIFF OMITTED] T0238.039

[GRAPHIC] [TIFF OMITTED] T0238.040

[GRAPHIC] [TIFF OMITTED] T0238.041

[GRAPHIC] [TIFF OMITTED] T0238.042

[GRAPHIC] [TIFF OMITTED] T0238.043

[GRAPHIC] [TIFF OMITTED] T0238.044

[GRAPHIC] [TIFF OMITTED] T0238.045

[GRAPHIC] [TIFF OMITTED] T0238.046

[GRAPHIC] [TIFF OMITTED] T0238.047

[GRAPHIC] [TIFF OMITTED] T0238.048

[GRAPHIC] [TIFF OMITTED] T0238.049

[GRAPHIC] [TIFF OMITTED] T0238.050

[GRAPHIC] [TIFF OMITTED] T0238.051

[GRAPHIC] [TIFF OMITTED] T0238.052

[GRAPHIC] [TIFF OMITTED] T0238.053

[GRAPHIC] [TIFF OMITTED] T0238.054

[GRAPHIC] [TIFF OMITTED] T0238.055

[GRAPHIC] [TIFF OMITTED] T0238.056

[GRAPHIC] [TIFF OMITTED] T0238.057

[GRAPHIC] [TIFF OMITTED] T0238.058

[GRAPHIC] [TIFF OMITTED] T0238.059

[GRAPHIC] [TIFF OMITTED] T0238.060

[GRAPHIC] [TIFF OMITTED] T0238.061

[GRAPHIC] [TIFF OMITTED] T0238.062

[GRAPHIC] [TIFF OMITTED] T0238.063

[GRAPHIC] [TIFF OMITTED] T0238.064

[GRAPHIC] [TIFF OMITTED] T0238.065

[GRAPHIC] [TIFF OMITTED] T0238.066

[GRAPHIC] [TIFF OMITTED] T0238.067

[GRAPHIC] [TIFF OMITTED] T0238.068

[GRAPHIC] [TIFF OMITTED] T0238.069

[GRAPHIC] [TIFF OMITTED] T0238.070

[GRAPHIC] [TIFF OMITTED] T0238.071

[GRAPHIC] [TIFF OMITTED] T0238.072

[GRAPHIC] [TIFF OMITTED] T0238.073

[GRAPHIC] [TIFF OMITTED] T0238.074

[GRAPHIC] [TIFF OMITTED] T0238.075

[GRAPHIC] [TIFF OMITTED] T0238.076

[GRAPHIC] [TIFF OMITTED] T0238.077

[GRAPHIC] [TIFF OMITTED] T0238.078

[GRAPHIC] [TIFF OMITTED] T0238.079

[GRAPHIC] [TIFF OMITTED] T0238.080

[GRAPHIC] [TIFF OMITTED] T0238.081

[GRAPHIC] [TIFF OMITTED] T0238.082

[GRAPHIC] [TIFF OMITTED] T0238.083

[GRAPHIC] [TIFF OMITTED] T0238.084

[GRAPHIC] [TIFF OMITTED] T0238.085

[GRAPHIC] [TIFF OMITTED] T0238.086

[GRAPHIC] [TIFF OMITTED] T0238.087

[GRAPHIC] [TIFF OMITTED] T0238.088

[GRAPHIC] [TIFF OMITTED] T0238.089

[GRAPHIC] [TIFF OMITTED] T0238.090

[GRAPHIC] [TIFF OMITTED] T0238.091

[GRAPHIC] [TIFF OMITTED] T0238.092

[GRAPHIC] [TIFF OMITTED] T0238.093

[GRAPHIC] [TIFF OMITTED] T0238.094

[GRAPHIC] [TIFF OMITTED] T0238.095

[GRAPHIC] [TIFF OMITTED] T0238.096

[GRAPHIC] [TIFF OMITTED] T0238.097

[GRAPHIC] [TIFF OMITTED] T0238.098

[GRAPHIC] [TIFF OMITTED] T0238.099

[GRAPHIC] [TIFF OMITTED] T0238.100


</pre></body></html>
