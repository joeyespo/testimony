<html>
<title> - OVERSIGHT HEARING</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]





             THE FISCAL YEAR 2000 BUDGET REQUEST OF
                 THE U.S. FISH AND WILDLIFE SERVICE
_______________________________________________________________________
 
                         OVERSIGHT HEARING

                              BEFORE THE

               SUBCOMMITTEE ON FISHERIES CONSERVATION,
                          WILDLIFE AND OCEANS

                                 OF THE

                       COMMITTEE ON RESOURCES
                      HOUSE OF REPRESENTATIVES

                        ONE HUNDRED SIXTH CONGRESS

                             FIRST SESSION

                                _______

                       MARCH 4, 1999, WASHINGTON, DC

                                _______

             Printed for the use of the Committee on Resources


[GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]


 Available via the World Wide Web: http://www.access.gpo.gov/congress/house
                                   or
                 Committee address: http://www.house.gov/resources

                    U.S. GOVERNMENT PRINTING OFFICE
  55-837 CC               WASHINGTON : 1999
_______________________________________________________________________
               For sale by the U.S. Government Printing Office
  Superintendent of Documents, Congressional Sales Office, 
  Washington, DC 20402

<TEXT NOT AVAILABLE>


</pre></body></html>
