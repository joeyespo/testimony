<html>
<title> - THE RISING PRICE OF A QUALITY POSTSECONDARY EDUCATION: FACT OR FICTION </title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
 THE RISING PRICE OF A QUALITY POSTSECONDARY EDUCATION: FACT OR FICTION
=======================================================================


                                HEARING

                                before the
                         COMMITTEE ON EDUCATION 
                   
                            AND THE WORKFORCE

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             SECOND SESSION

                               __________

            HEARING HELD IN WASHINGTON, DC, OCTOBER 3, 2002

                               __________

                           Serial No. 107-83

                               __________


  Printed for the use of the Committee on Education and the Workforce





82-592                        
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
FAX: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001
</pre></body></html>
