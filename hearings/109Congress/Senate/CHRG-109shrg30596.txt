<html>
<title> - NOMINATIONS OF WAYNE C. BEYER AND STEPHEN T. CONBOY</title>
<body><pre>
[Senate Hearing 109-902]
[From the U.S. Government Printing Office]



                                                        S. Hrg. 109-902

          NOMINATIONS OF WAYNE C. BEYER AND STEPHEN T. CONBOY

=======================================================================

                                HEARING

                               before the

                              COMMITTEE ON
               HOMELAND SECURITY AND GOVERNMENTAL AFFAIRS
                          UNITED STATES SENATE


                       ONE HUNDRED NINTH CONGRESS

                             SECOND SESSION

                                 ON THE

  NOMINATIONS OF WAYNE C. BEYER TO BE MEMBER, FEDERAL LABOR RELATIONS 
AUTHORITY, AND STEPHEN T. CONBOY TO BE U.S. MARSHAL, SUPERIOR COURT OF 
                        THE DISTRICT OF COLUMBIA


                               __________

                           SEPTEMBER 13, 2006

                               __________

        Available via http://www.access.gpo.gov/congress/senate

                       Printed for the use of the
        Committee on Homeland Security and Governmental Affairs


                               __________


                     U.S. GOVERNMENT PRINTING OFFICE
                            WASHINGTON : 2007
30-596 PDF

For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001



        COMMITTEE ON HOMELAND SECURITY AND GOVERNMENTAL AFFAIRS

                   SUSAN M. COLLINS, Maine, Chairman
TED STEVENS, Alaska                  JOSEPH I. LIEBERMAN, Connecticut
GEORGE V. VOINOVICH, Ohio            CARL LEVIN, Michigan
NORM COLEMAN, Minnesota              DANIEL K. AKAKA, Hawaii
TOM COBURN, Oklahoma                 THOMAS R. CARPER, Delaware
LINCOLN D. CHAFEE, Rhode Island      MARK DAYTON, Minnesota
ROBERT F. BENNETT, Utah              FRANK LAUTENBERG, New Jersey
PETE V. DOMENICI, New Mexico         MARK PRYOR, Arkansas
JOHN W. WARNER, Virginia

                   Brandon L. Milhorn, Staff Director
            Jennifer A. Hemingway, Professional Staff Member
   Theresa Prych, Professional Staff Member, Oversight of Government 
    Management, the Federal Workforce, and the District of Columbia
                              Subcommittee

    David Cole, Professional Staff Member, Oversight of Government 
    Management, the Federal Workforce, and the District of Columbia 
                              Subcommittee

             Michael L. Alexander, Minority Staff Director
         Adam R. Sedgewick, Minority Professional Staff Member
     Jennifer L. Tyree, Minority Counsel, Oversight of Government 
    Management, the Federal Workforce, and the District of Columbia 
                              Subcommittee
                  Trina Driessnack Tyrer, Chief Clerk


                            C O N T E N T S

                                 ------                                
Opening statement:
                                                                   Page
    Senator Voinovich............................................     1

                               WITNESSES

                     Wednesday, September 13, 2006

Hon. Judd Gregg, a U.S. Senator from the State of New Hampshire..     1
Wayne C. Beyer, to be Member, Federal Labor Relations Authority..     3
Stephen T. Conboy, to be U.S. Marshal, Superior Court of the 
  District of Columbia...........................................     4

                     Alphabetical List of Witnesses

Beyer, Wayne C.:
    Testimony....................................................     3
    Prepared statement...........................................     9
    Biographical and professional information....................    10
    Responses to pre-hearing questions...........................    18
    Letter from U.S. Office of Government Ethics.................    30
    Letter of support from Hon. Charles Bass and Hon. Jeb 
      Bradley, Representatives in Congress from the State of New 
      Hampshire..................................................    31

Conboy, Stephen T.:
    Testimony....................................................     4
    Prepared statement...........................................    32
    Biographical and professional information....................    34
    Responses to pre-hearing questions...........................    39
    Additional questions submitted by Senator Lautenberg.........    56
    Letter from U.S. Office of Government Ethics.................    58

Gregg, Hon. Judd:
    Testimony....................................................     1

 
          NOMINATIONS OF WAYNE C. BEYER AND STEPHEN T. CONBOY

                              ----------                              


                     WEDNESDAY, SEPTEMBER 13, 2006

                                       U.S. Senate,
                           Committee on Homeland Security  
                                  and Governmental Affairs,
                                                    Washington, DC.
    The Committee met, pursuant to notice, at 2:33 p.m., in 
room 342, Dirksen Senate Office Building, Hon. George V. 
Voinovich, presiding.
    Present: Senator Voinovich.

             OPENING STATEMENT OF SENATOR VOINOVICH

    Senator Voinovich. The Committee will come to order. Today, 
the Committee on Homeland Security and Governmental Affairs 
meets to consider the nominations of Wayne Beyer to be a Member 
of the Federal Labor Relations Authority and Stephen Conboy to 
be U.S. Marshal for the Superior Court of the District of 
Columbia.
    I would like to extend my warm regards to both Mr. Beyer 
and Mr. Conboy. I would like to say how pleased I am that both 
of you continue to use your talents to serve your Nation. I 
would also like to thank your families, who make significant 
sacrifices in order for you to pursue professions in public 
service.
    Mr. Beyer, the Federal Labor Relations Authority has the 
responsibility to adjudicate disputes arising out of the Civil 
Service Reform Act of 1978, including determining what is 
negotiable through collective bargaining agreements, appeals 
over unfair labor practices, and hearing petitions for union 
representation of Federal employees.
    While governor of Ohio, I spent a significant amount of my 
time working to improve labor-management partnerships in Ohio 
State agencies. Mr. Beyer, I look forward to discussing with 
you the status of labor-management relations within the Federal 
Government.
    I welcome my friend and Senator and colleague, Senator Judd 
Gregg, who is here to introduce Mr. Beyer. Senator Gregg, we 
are very happy that you are here today with us, and we look 
forward to hearing from you.

TESTIMONY OF HON. JUDD GREGG, A U.S. SENATOR FROM THE STATE OF 
                         NEW HAMPSHIRE

    Senator Gregg. Thank you, Senator Voinovich, and it is a 
pleasure to be here before your Committee to introduce and 
strongly support the nomination of Wayne Beyer.
    Mr. Beyer and I go back a long way, over 20 years, 
actually, as his career started out in New Hampshire. He went 
to Dartmouth College and then got a graduate degree, I believe, 
at Harvard. He practiced law in New Hampshire for a significant 
amount of time. I have known him as a friend and as someone who 
always represented a commitment to public service. Back when I 
was governor, I tried to sign him up to come into the State 
government, but at that time, he was a young lawyer just trying 
to get started, and it is a little bit expensive to come into 
the State service, but he has requited himself extraordinarily 
well.
    As a member of this Administration, as an Administrative 
Appeals Judge, he understands the issues which will be before 
him on the Federal Labor Relations Authority. He will bring 
integrity, intelligence, and capability to this, and he is 
fair-minded and that is what you want from someone in this 
position.
    I hope this Committee will act favorably on his nomination, 
and I appreciate the chance to have the opportunity to come 
here and testify on his behalf.
    Senator Voinovich. Thank you very much. I know that you 
have a busy schedule today, so I thank you very much for coming 
today.
    Senator Gregg. Thank you.
    Senator Voinovich. Mr. Beyer, Senator Gregg must think a 
great deal of you.
    Mr. Beyer. Thank you.
    Senator Voinovich. Mr. Conboy, as you are well aware, the 
U.S. Marshals Service is our oldest Federal law enforcement 
agency. In the District of Columbia, the Marshals Service has 
the significant responsibility of providing law enforcement for 
the Federal courts.
    With 23 years of experience with the U.S. Marshals Service, 
Mr. Conboy has served in numerous positions throughout the 
agency, including Deputy U.S. Marshal, Senior Inspector, 
Supervisory U.S. Marshal, Chief Deputy, and his current 
position as Acting U.S. Marshal for the Superior Court of the 
District of Columbia. Prior to joining the Marshals Service, 
Mr. Conboy served in the U.S. Marine Corps.
    I believe that both of the nominees today are well 
qualified for the positions for which they have been nominated, 
and I look forward to hearing from them about their 
qualifications and other reasons for pursuing public service.
    It is the custom of this Committee to swear in witnesses, 
and if you will both stand up, I will swear you in. Do you 
swear that the testimony you are about to give is the truth and 
nothing but the truth, so help you, God?
    Mr. Beyer. I do.
    Mr. Conboy. I do.
    Senator Voinovich. Thank you. I understand that both of you 
have friends and relatives here today, and supportive 
colleagues, and I thought that I might give you an opportunity 
to introduce them. Mr. Beyer, we will start with you.
    Mr. Beyer. Thank you, Senator. I am here with Dale 
Cabaniss, the Chair of the FLRA; Colleen Kiko, who is the 
General Counsel of the FLRA. I note that Carol Waller Pope, who 
is the other member of the FLRA, is also here, and I don't see 
anyone else. Thank you, Senator.
    Senator Voinovich. Thank you. Mr. Conboy.
    Mr. Conboy. Thank you, Senator. I would like to first 
recognize my biggest supporter, my wife of 30 years, Elizabeth. 
She is a teacher with Fairfax County Public Schools and has 
spent the past 2 years earning a second Master's degree in 
education with Virginia Tech and the immediate past year as an 
assistant principal intern at Lorton Station. I am most proud 
of her. Our two daughters, Anna and Sarah, could not be here 
today.
    I would like to introduce, as well, the Hon. Chief Judge 
Rufus G. King III, the Hon.----
    Senator Voinovich. It is nice to have you here with us. 
Thank you for being here.
    Mr. Conboy [continuing]. Judge Gregory Jackson; the Hon. 
Pete Elliott, U.S. Marshal for the Northern District of Ohio; 
the Hon. George Walsh, U.S. Marshal for the District of 
Columbia. I may mention that there are actually two districts 
within the District of Columbia, U.S. District and the Superior 
Court, as well. There are a number of other friends and 
supporters here.
    Senator Voinovich. We are glad to have all of you here, and 
Mrs. Conboy, I thank you for the sacrifice that you have made 
so that your husband can serve. I am sure you thank him for the 
sacrifice he makes so you can serve our public schools.
    Mrs. Conboy. Absolutely.
    Senator Voinovich. We have questions that we ask all of the 
nominees here before this Committee. I will ask these questions 
of both of you. First, is there anything that you are aware of 
in your background that might present a conflict of interest 
with the duties of the office to which you have been nominated?
    Mr. Beyer. No, Senator.
    Mr. Conboy. No, sir.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Mr. Beyer. No, Senator.
    Mr. Conboy. No, Senator.
    Senator Voinovich. Do you have any reason, personal or 
otherwise, that would in any way prevent you from serving the 
full term for the office to which you have been nominated?
    Mr. Beyer. No, Senator.
    Mr. Conboy. No, Senator.
    Senator Voinovich. I would welcome comments from you, Mr. 
Beyer, about why you are interested in being appointed. I will 
then call on you, Mr. Conboy.

 TESTIMONY OF WAYNE C. BEYER,\1\ TO BE A MEMBER, FEDERAL LABOR 
                      RELATIONS AUTHORITY

    Mr. Beyer. Thank you, Senator. I do have a brief statement.
---------------------------------------------------------------------------
    \1\ The prepared statement of Mr. Beyer appears in the Appendix on 
page 9.
---------------------------------------------------------------------------
    Chairman Voinovich and distinguished Members of the 
Committee, I am honored to appear before you today as the 
President's nominee to be a Member of the Federal Labor 
Relations Authority. My career is about evenly divided between 
private practice in New Hampshire and public service here in 
Washington. The submissions provide the details.
    My strengths include an ability to understand the facts and 
analyze and apply the law, write well analytically, work 
productively, and work collegially. Four-and-a-half years as an 
Administrative Appeals Judge adjudicating cases arising under 
worker protection laws will be good preparation for the FLRA if 
I am fortunate enough to serve in that capacity.
    I want to recognize and thank those who have contributed to 
the nomination process, Katja Bullock of the White House, Dale 
Cabaniss, Chair of the FLRA, the Senate staff, especially 
Jennifer Hemingway, my friend, Judd Gregg, the senior Senator 
from New Hampshire, for his kind remarks, the Committee for its 
time and attention, and, of course, the President for the 
confidence placed in me. The only way I can prove my gratitude 
is to perform to the best of my ability if I am confirmed for 
this important position.
    I will answer any questions that you have, Senator.
    Senator Voinovich. Thank you. Mr. Conboy.

    TESTIMONY OF STEPHEN T. CONBOY,\1\ TO BE U.S. MARSHALL, 
           SUPERIOR COURT OF THE DISTRICT OF COLUMBIA

    Mr. Conboy. Thank you, Chairman Voinovich. I would like to 
thank our President and Commander in Chief for nominating me to 
this position and to the Attorney General for appointing me to 
be the Acting U.S. Marshal, a position that I have held since 
January 2004, and for his confidence in me for this nomination. 
I would like to recognize and express my sincere appreciation 
to Mayor Anthony Williams for recommending me to the White 
House for this position.
---------------------------------------------------------------------------
    \1\ The prepared statement of Mr. Conboy appears in the Appendix on 
page 32.
---------------------------------------------------------------------------
    As a career Deputy U.S. Marshal with 23 years of experience 
with the U.S. Marshals Service, I am most proud of being 
associated with such a fine cadre of brave and dedicated men 
and women that I have the privilege of working with at Superior 
Court. The District of Columbia can be proud of the tremendous 
service that they provide to both this community and to their 
Nation each day.
    I look forward to responding to whatever questions the 
Committee may have.
    Senator Voinovich. Thank you.
    Mr. Beyer, while the statute divides the Board membership 
between the two political parties, I believe its judicial 
function demands members of the Federal Labor Relations 
Authority to not hold political biases. Do you agree with that?
    Mr. Beyer. Yes, I do, Senator.
    Senator Voinovich. What is your philosophy in approaching 
this position?
    Mr. Beyer. I look at this position as a quasi-judicial 
position. It is not a policy-making or management position. I 
would approach each case on its merits, scrupulously applying 
the law to the facts of each individual case without any 
predisposition, without any bias. As I think the Senator knows, 
I have had a similar role for the last 4\1/2\ years in the 
Department of Labor, and I think this would be a good 
opportunity for me to continue in that kind of role.
    Senator Voinovich. Do you have any comments about the 
current state of labor relations in the Federal Government?
    Mr. Beyer. I think, Senator, they are good at the 
Department of Labor under the guidance of Secretary of Labor 
Elaine Chao. Outside of the Department of Labor, my knowledge 
is a little bit more secondhand and more anecdotal. I realize 
that there is some fluctuation with regard to the Department of 
Defense and Homeland Security. By and large, I think labor 
relations are quite stable within the Federal Government, with 
perhaps those exceptions. I look forward, hopefully, to making 
my own contribution through the decisions of the cases that 
arise before me as a member of the FLRA.
    Senator Voinovich. Mr. Conboy, having served as Acting U.S. 
Marshal for a while, you have had an insight into some of the 
challenges inherent in the job. Could you share with me what 
you think is the biggest challenge facing the U.S. Marshals 
Service Superior Court Office?
    Mr. Conboy. Senator, I believe the biggest challenge would 
be in the formulation as to how we approach the resources that 
are allocated at Superior Court. The functions at Superior 
Court are very unique to the Marshals Service in that I really 
serve as the de facto sheriff, if you would. It is a very 
challenging environment. We perform functions that are not 
performed anywhere else in the country by the Marshals Service. 
It is a challenge to ensure these----
    Senator Voinovich. Could you give me some examples of that?
    Mr. Conboy. Well, one would be performing evictions for the 
District of Columbia. That is a function that is primarily 
executed by a county sheriff. We perform upwards of 60 of those 
a day within the District of Columbia. It is a very challenging 
job--the security aspect of it, the accountability of 
performing that on a day-to-day basis.
    Senator Voinovich. Do you have the resources you need to 
get the job done? This Committee heard testimony from Secretary 
Chertoff yesterday and questioned him about whether or not he 
had the resources to do the job we have asked him to do.
    Mr. Conboy. I believe that we are using the resources that 
you have provided to us to the very best of our ability. Of 
course, we could always do more, and we are certainly always 
performing analysis for what we need to get the job done.
    Senator Voinovich. What steps have you taken to ensure that 
all of your employees, including the detention enforcement 
officers and the Deputy U.S. Marshals, are provided equality of 
opportunity in terms of training?
    Mr. Conboy. We have a very vigorous program that ensures 
that deputies at Superior Court are provided the training and 
the detention officers are provided the training that is 
required to get the job done. We have mandatory basic and 
refresher training that is put on at FLETC in Brunswick, 
Georgia.
    Senator Voinovich. Do you have a tough time recruiting 
employees?
    Mr. Conboy. Of course, that is performed on a national 
level, Senator, so I know that it is an ongoing process. It is 
a very difficult and cumbersome process, and I would commend 
our Human Resources Division for the work that they do in 
getting those men and women into the ranks.
    Senator Voinovich. In effect, they scour the country for 
people that might be interested and provide you with a pool of 
available applicants?
    Mr. Conboy. Yes, they do, Senator.
    Senator Voinovich. I understand that, in 2004, the U.S. 
Marshals Service entered into an intergovernmental agreement 
with the District of Columbia Department of Corrections for the 
transportation of prisoners. How is this agreement working?
    Mr. Conboy. I believe that agreement is working absolutely 
fantastic. It has been an absolute win-win for the Federal 
Government and for our partners in the District of Columbia. It 
ensures the timely and safe delivery of prisoners to the 
courthouse, and, of course, that is something that allows us to 
free up deputies to perform other functions, such as pursuing 
fugitives.
    Senator Voinovich. Once someone is convicted, what is the 
status of the jail facilities?
    Mr. Conboy. I am sorry, Senator, the status of the jail 
facilities?
    Senator Voinovich. I remember the conditions of the Federal 
facilities in Ohio when I was governor. So I am curious what is 
the condition of the jail facilities today? Once these folks 
are convicted, I suspect that you are the ones that have to 
take them wherever they are going to end up in jail.
    Mr. Conboy. That is correct, Senator.
    Senator Voinovich. What about the capacity? Do you have 
enough jail space out there today or are the facilities 
crowded?
    Mr. Conboy. I believe we do, Senator. That population 
fluctuates daily. It is something that we in the criminal 
justice community keep an eye on very closely. Certainly, there 
has been mandates and caps over there. Presently, we do not 
have a concern.
    One of the differences is that the prisoners coming from 
Superior Court are not remanded to the U.S. Marshals Service 
until such time as they are sentenced, unlike U.S. District 
Court, where they are remanded as soon as they are taken into 
custody and ordered so by the court. So we really--the 
population issue, and it is a shared issue, it is not just the 
Department of Corrections, it is Superior Court and U.S. 
District Court, and it is something that, as partners, we have 
to keep our eye on all the time. Parolee issues, prisoners that 
are being arrested on a daily basis because of new crime 
initiatives, those all have impacts on the population.
    But I will say that we, as partners, have done an 
absolutely fantastic job in formulating a Memorandum of 
Understanding that expedites the process so that as soon as 
they receive a judgment and commitment, we have a time frame in 
place where we are removing them from the District to their 
designated facilities within 21 days. So it is a very timely 
process, and it is one that is being used as a template across 
the country.
    Senator Voinovich. So you believe you have adequate 
facilities to hold convicted individuals during the interim 
period, and, within 21 days, you transport them to wherever 
they have been sentenced to? The reason I am asking is that in 
my State, we are seeing a tremendous overcrowding of our 
prisons. There has been, for some reason, an uptake in crime. I 
remember while I was governor, things started to subside a bit, 
but now it appears they are again overcrowded. You are telling 
me that you are not having that problem on the Federal level?
    Mr. Conboy. Presently, no, not within the District of 
Columbia.
    Senator Voinovich. Do either one of you have anything else 
you would like to say, other than your desire for the Committee 
to move quickly?
    Mr. Beyer. No, but thank you, Senator, very much for the 
opportunity to appear before this Committee.
    Senator Voinovich. Great. I am pleased that both of you are 
here, and again, as I mentioned in my earlier remarks, thank 
you for your willingness to serve your country in the capacity 
that the President has nominated you. I wish you good luck, and 
we will do what we can to move your nominations along.
    Mr. Beyer. Thank you.
    Mr. Conboy. Thank you.
    Senator Voinovich. The hearing is adjourned.
    [Whereupon, at 2:55 p.m., the Committee was adjourned.]

                            A P P E N D I X

                              ----------                              

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

[GRAPHIC] [TIFF OMITTED] 

                                 <all>

</pre></body></html>
