<html>
<title> - JOB CORPS OVERSIGHT PART II: VOCATIONAL TRAINING STANDARDS </title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


 
                   JOB CORPS OVERSIGHT PART II: VOCATIONAL
                                TRAINING STANDARDS
==============================================================================


                                      HEARING

                                    before the

                        SUBCOMMITTEE ON HUMAN RESOURCES

                                       of the


                                    COMMITTEE ON
                                 GOVERNMENT REFORM
                                    AND OVERSIGHT

                             HOUSE OF REPRESENTATIVES

                           one hundred fifth congress
                                  second session

                                      -------

                                   July 29, 1998

                                      -------

                                 Serial No. 105-184

                                      -------

  Printed for the use of the Committee on Government Reform and Oversight

                        U.S. GOVERNMENT PRINTING OFFICE
52-520                         WASHINGTON : 1998
____________________________________________________________________________
               For sale by the U.S.Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402
                               ISBN 0-16-058378-0


<TEXT FILE NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
