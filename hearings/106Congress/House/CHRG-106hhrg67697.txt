<html>
<title> - HEARING ON: H.R. 3112, TO AMEND THE COLORADO UTE INDIAN WATER RIGHTS SETTLEMENT ACT TO PROVIDE FOR A FINAL SETTLEMENT OF THE CLAIMS OF THE COLORADO UTE INDIAN TRIBES, AND FOR OTHER PURPOSES</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]




H.R. 3112, TO AMEND THE COLORADO UTE INDIAN WATER RIGHTS SETTLEMENT ACT 
  TO PROVIDE FOR A FINAL SETTLEMENT OF THE CLAIMS OF THE COLORADO UTE 
                 INDIAN TRIBES, AND FOR OTHER PURPOSES

=======================================================================

                                HEARING

                               before the

                    SUBCOMMITTEE ON WATER AND POWER

                                 of the

                         COMMITTEE ON RESOURCES
                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS

                             SECOND SESSION

                               __________

                      MAY 11, 2000, WASHINGTON, DC

                               __________

                           Serial No. 106-92

                               __________

           Printed for the use of the Committee on Resources


 Available via the World Wide Web: http://www.access.gpo.gov/congress/
                                 house
                                   or
           Committee address: http://www.house.gov/resources

                                 ______


                    U.S. GOVERNMENT PRINTING OFFICE
67-697                     WASHINGTON : 2000


                         COMMITTEE ON RESOURCES

                      DON YOUNG, Alaska, Chairman
W.J. (BILLY) TAUZIN, Louisiana       GEORGE MILLER, California
JAMES V. HANSEN, Utah                NICK J. RAHALL II, West Virginia
JIM SAXTON, New Jersey               BRUCE F. VENTO, Minnesota
ELTON GALLEGLY, California           DALE E. KILDEE, Michigan
JOHN J. DUNCAN, Jr., Tennessee       PETER A. DeFAZIO, Oregon
JOEL HEFLEY, Colorado                ENI F.H. FALEOMAVAEGA, American 
JOHN T. DOOLITTLE, California            Samoa
WAYNE T. GILCHREST, Maryland         NEIL ABERCROMBIE, Hawaii
KEN CALVERT, California              SOLOMON P. ORTIZ, Texas
RICHARD W. POMBO, California         OWEN B. PICKETT, Virginia
BARBARA CUBIN, Wyoming               FRANK PALLONE, Jr., New Jersey
HELEN CHENOWETH-HAGE, Idaho          CALVIN M. DOOLEY, California
GEORGE P. RADANOVICH, California     CARLOS A. ROMERO-BARCELO, Puerto 
WALTER B. JONES, Jr., North              Rico
    Carolina                         ROBERT A. UNDERWOOD, Guam
WILLIAM M. (MAC) THORNBERRY, Texas   PATRICK J. KENNEDY, Rhode Island
CHRIS CANNON, Utah                   ADAM SMITH, Washington
KEVIN BRADY, Texas                   CHRIS JOHN, Louisiana
JOHN PETERSON, Pennsylvania          DONNA MC CHRISTENSEN, Virgin 
RICK HILL, Montana                       Islands
BOB SCHAFFER, Colorado               RON KIND, Wisconsin
JIM GIBBONS, Nevada                  JAY INSLEE, Washington
MARK E. SOUDER, Indiana              GRACE F. NAPOLITANO, California
GREG WALDEN, Oregon                  TOM UDALL, New Mexico
DON SHERWOOD, Pennsylvania           MARK UDALL, Colorado
ROBIN HAYES, North Carolina          JOSEPH CROWLEY, New York
MIKE SIMPSON, Idaho                  RUSH D. HOLT, New Jersey
THOMAS G. TANCREDO, Colorado

                     Lloyd A. Jones, Chief of Staff
                   Elizabeth Megginson, Chief Counsel
              Christine Kennedy, Chief Clerk/Administrator
                John Lawrence, Democratic Staff Director
                                 ------                                

               Subcommittee on Water and Power Resources

                JOHN T. DOOLITTLE, California, Chairman
KEN CALVERT, California              CALVIN M. DOOLEY, California
RICHARD W. POMBO, California         GEORGE MILLER, California
HELEN CHENOWETH-HAGE, Idaho          PETER A. DeFAZIO, Oregon
GEORGE P. RADANOVICH, California     OWEN B. PICKETT, Virginia
WILLIAM M. (MAC) THORNBERRY, Texas   ADAM SMITH, Washington
GREG WALDEN, Oregon                  DONNA MC CHRISTENSEN, Virgin 
MIKE SIMPSOM, Idaho                      Islands
                                     GRACE F. NAPOLITANO, California
                  Robert Faber, Staff Director/Counsel
                   Joshua Johnson, Professional Staff
                      Steve Lanich, Minority Staff


                            C O N T E N T S

                              ----------                              
                                                                   Page

Hearing held May 11, 2000........................................     1

Statement of Members:
    Doolittle, Hon. John T., a Representative in Congress from 
      the State of California....................................     1
        Prepared statement of....................................     1
    McInnis, Hon. Scott, a Representative in Congress from the 
      State of Colorado, prepared statement of...................     4

Statement of Witnesses:
    Baker, Jr., John E., Chairman, Southern UTE Indian Tribe, 
      Ignacio, Colorado..........................................    23
        Prepared statement of....................................    25
    Hayes, David J., Deputy Secretary, Department of Interior, 
      Washington, DC.............................................     2
        Prepared statement of....................................    10
    House, Chairman, Ernest, UTE Mountain UTE Tribe, Towaoc, 
      Colorado...................................................    33
        Prepared statement of....................................    35
    Remington, Sage Douglas, Director, Southern UTE Grassroots 
      Organization, Ignacio, Colorado............................    39
        Prepared statement of....................................    41

Additional Material Supplied:
    Black, Michael, with Taxpayers for the Animas River, prepared 
      statement of...............................................    65
    Griswold, Richard K., President of the Animas-La Plata Water 
      Conservancy District, prepared statement of................    46
    Kroeger, Fred V., President of the Southwestern Water 
      Conservation District, prepared statement of...............    61
    San Juan Water Commission in New Mexico, prepared statement 
      of.........................................................    56
    Turney, Thomas C., State Engineer with the State of New 
      Mexico, prepared statement of..............................    52

 
 HEARING ON: H.R. 3112, TO AMEND THE COLORADO UTE INDIAN WATER RIGHTS 
 SETTLEMENT ACT TO PROVIDE FOR A FINAL SETTLEMENT OF THE CLAIMS OF THE 
           COLORADO UTE INDIAN TRIBES, AND FOR OTHER PURPOSES

                              ----------                              


                         THURSDAY, MAY 11, 2000

                  House of Representatives,
                   Subcommittee on Water and Power,
                                    Committee on Resources,
                                                    Washington, DC.
    The subcommittee met, pursuant to other business, at 2:48 
p.m., in room 1334, Longworth House Office Building, Hon. John 
T. Doolittle (chairman of the subcommittee) presiding.

   STATEMENT OF HON. JOHN T. DOOLITTLE, A REPRESENTATIVE IN 
             CONGRESS FROM THE STATE OF CALIFORNIA

    Mr. Doolittle. We will now proceed to the hearing on 
Animas-La Plata. I thank the members. Let me invite our 
witnesses to come forward. This is a legislative hearing on 
H.R. 3112.
    May I ask you to please rise and raise your right hands. Do 
you solemnly swear or affirm under the penalty of perjury that 
the testimony given will be the whole truth and nothing but the 
truth?
    Mr. Hayes. I do.
    Mr. Baker. I do.
    Mr. House. I do.
    Mr. Remington. I do.
    Mr. Doolittle. Thank you. Let the record reflect that each 
answered affirmatively. We are very happy to welcome you here, 
gentlemen, and we will begin hearing from the Deputy Secretary 
of Interior, Mr. David Hayes. Welcome.
    [The prepared statement of John T. Doolittle follows]

Statement of Hon. John T. Doolittle, a Representative in Congress from 
                        the State of California

    H.R. 3023, Greater Yuma Port Authority
    H.R. 4132, Water Resources Research Act
    S. 1211, Colorado River Basin Salinity Control Act

Legislative Hearing on:

    H. R. 3112, ``to amend the Colorado Ute Indian Water Rights 
Settlement Act to provide for a final settlement of the claims 
of the Colorado Ute Indian Tribes, and for other purposes.
    Today we will first markup the following three bills, 
followed by a legislative hearing on H.R. 3112, the Animas La 
Plata Project. The bills to markup include:

    1.  H.R. 3023, Greater Yuma Port Authority

    To convey to the Greater Yuma Port Authority an area of 
land currently controlled by the Bureau of Reclamation 
consisting of approximately 330 acres, at fair market value, 
just east of the City of San Luis, for the construction of a 
commercial Port of Entry.

    2.  H.R. 4132, Water Resources Research Act

    To reauthorize grants for water resources research and 
technology institutes established under the Water Resources 
Research Act of 1984.
    These state water resource research institutes, under the 
authority of the Water Resources Research Act, have established 
an effective federal/state partnership in water resources, 
education and information transfer. They work with state and 
federal agencies and water resources stakeholders in their home 
states while acting as a network for the exchange of water 
resources research and information transfer among the states. I 
am pleased that among the 17 cosponsors we already have, many 
of them are members of this Subcommittee, on both sides of the 
aisle. I look forward to the rest of you joining us in 
cosponsoring this bill.

    3.  S. 1211, Colorado River Basin Salinity Control Act

    To increase the current Colorado River Basin Salinity 
Control Act authorization from $75 million to $175 million. In 
addition, the legislation requires the secretary to file a 
report to address salt contributions from Bureau of Land 
Management lands.
    Prior to 1995, Reclamation's efforts to reduce salinity 
were costing between $70 to more than $100 for each ton of salt 
that was controlled. One of the important steps we took in 
reforming this program in 1996 was the introduction of private 
parties and the use of market forces to bring those costs down. 
It is a testament to the use of entrepreneurial methods that I 
can now report that costs for salt removal are around $30 per 
ton. In addition, for every $100 spent by the federal 
government, an additional $43 is spent by the Basin States.
    H.R. 3112, an amendment to ``the Colorado Ute Indian Water 
Rights Settlement Act to provide for a final settlement of the 
claims of the Colorado Ute Indian Tribes.''
    Two years ago, this Subcommittee held a hearing to address 
issues raised by the Romer-Schoettler process regarding what 
direction to take with the Animas La Plata Project, and to hear 
testimony from project proponents, opponents, and the 
Administration. During the hearing, then Counselor to the 
Secretary of the Interior, David Hayes indicated the desire of 
the Administration to address additional issues. On August 11, 
1998, the Secretary of Interior presented an Administration 
Proposal to implement the Settlement Act. H.R. 3112 is an 
attempt by project beneficiaries to implement these 
negotiations and come to a final Settlement Act.
    Since the hearing the Tribes, and other project 
beneficiaries have agreed to a much smaller ALP Project. In 
fact, if you consider the full cost of project, as anticipated 
in 1968, in 1999 dollars, the project would have an estimated 
price of $754 million. The bill before us today, specifically 
regarding the ALP project, is estimated to costs between $180 
and $240 million. This is a drastic reduction in the project 
cost.
    I commend the Project beneficiaries for their continuing 
flexibility to work with the Administration to draft 
legislation that meets their demands, and one that provides an 
equitable settlement to the tribes. I would also just like to 
note the municipal and industrial water beneficiaries 
commitment to fully fund their portion of the capital 
investment--in full and up front.
    I look forward to hearing the testimony and discussing the 
issues with the witnesses.

 STATEMENT OF DAVID J. HAYES, DEPUTY SECRETARY, DEPARTMENT OF 
                   INTERIOR, WASHINGTON, DC.

    Mr. Hayes. Thank you, Mr. Chairman and members of the 
committee. I have submitted a written statement that I would 
appreciate being entered into the record, Mr. Chairman.
    In my oral comments, I would like to just briefly review 
some of the highlights of what has brought us here to this 
hearing today on H.R. 3112. This hearing, as the chairman noted 
at the outset, is on a bill that is intended to provide a final 
settlement for the claims of the Colorado Ute Indian Tribes in 
Southwestern Colorado and Northern New Mexico. Those remaining 
claims exist in the Animas and La Plata river basins in 
Southeastern Colorado and their resolution also requires a 
resolution of issues associated
    with the Animas-La Plata project that was originally 
authorized in 1968 and which has not been built. We are very 
interested in resolving these very difficult issues once and 
for all and we thank Congressman McInnis for introducing the 
bill.
    [The prepared statement of Mr. McInnis follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.001
    
    [GRAPHIC] [TIFF OMITTED] T7697.002
    
    [GRAPHIC] [TIFF OMITTED] T7697.003
    
    [GRAPHIC] [TIFF OMITTED] T7697.004
    
    Mr. Hayes. The administration supports H.R. 3112 with some 
modifications that are identified in my testimony. The reason 
we support passage of this legislation is that we feel strongly 
that it is time to resolve the longstanding claims of the 
Colorado Ute Tribes, the Southern Utes and the Mountain Utes. 
In 1986, Congress passed the Colorado Ute Indian Water Rights 
Final Settlement Agreement, which identified the legal rights 
of the two tribes to reserve water under the Winters doctrine. 
It anticipated that those legal rights would be implemented 
through the construction of the Animas-La Plata project that 
originally was authorized in 1968.
    Unfortunately, however, that project could not be 
implemented in the form in which it was authorized in 1968, 
and, in fact, this administration identified serious problems 
both with the original project and also with the project 
commonly known as ``ALP Lite'', which emerged out of the Romer-
Schoettler process that the chairman referred to earlier in his 
comments. The administration was concerned that even ALP Lite 
was too large a project, raised significant environmental 
concerns and financial concerns.
    Yet Secretary Babbitt and the administration appreciated 
the fact that it was necessary to bring closure to the tribal 
water rights and we proposed in August 1998 that a further 
scaled-down project that was geared toward the tribal water 
right proceed for full environmental review. That review has 
been undertaken and it is because the review has been 
undertaken with full public process that we are here today 
believing it is time to go forward and write the final chapter 
of Animas-La Plata.
    The bill that Congressman McInnis has introduced is, in 
essence, a consensus bill among the tribes, the administration, 
and the project proponents. It eliminates the irrigation 
component of the original ALP project, which was a serious 
concern for environmental purposes and also cost effectiveness 
purposes. It is a much scaled-down reservoir, off-stream 
facility, that still allows the Animas River to remain free-
flowing.
    Thirdly, it incorporates non-structural concepts and 
anticipates that the tribes will purchase some of their water 
rights while also having a structural facility for certainty 
for the bulk of their water rights. And it is premised on full 
environmental review, full implementation under the Endangered 
Species Act and under NEPA. And it finally anticipates that 
some water would be available for municipalities in the area, 
but only unsubsidized water for M&I uses only.
    And most importantly, it will finally resolve the Ute 
Tribes' water rights. We think that is important not only 
because of the trust responsibility that we owe to the tribes, 
but also because resolution of those water rights will avoid 
the uncertainty of water rights in the entire Southwestern 
Colorado and Northern New Mexico area. If we do not bring 
closure here, the Settlement Act of 1968 will go by the 
wayside, litigation will commence, and potentially very 
significant senior water rights claims will be brought against 
current non-Indian water users in Southern Colorado and 
Northern New Mexico. We think a consensual solution along the 
lines of what has been developed among the parties and with the 
administration's help as reflected in Congressman McInnis's 
bill is the way to go.
    A final point, we do think it is important to deauthorize 
the ALP project and make it clear that this is the final 
chapter. We also think it is important to reflect the concept 
of full repayment for the M&I users. We think there is a way to 
do this that will be consistent with the needs of the water 
users and the interests of all parties.
    Finally, I will note that an important feature of this bill 
and the administration's proposal is a drinking water pipeline 
for the Navajo Nation between Farmington and Shiprock, a much 
needed replacement of an important municipal water supply that 
the current pipeline of which has lived beyond its useful life.
    Thank you, Mr. Chairman, for holding this hearing and 
providing the opportunity to testify.
    Mr. Doolittle. Thank you.
    [The prepared statement of Mr. Hayes follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.005
    
    [GRAPHIC] [TIFF OMITTED] T7697.006
    
    [GRAPHIC] [TIFF OMITTED] T7697.007
    
    [GRAPHIC] [TIFF OMITTED] T7697.008
    
    [GRAPHIC] [TIFF OMITTED] T7697.009
    
    [GRAPHIC] [TIFF OMITTED] T7697.010
    
    [GRAPHIC] [TIFF OMITTED] T7697.011
    
    [GRAPHIC] [TIFF OMITTED] T7697.012
    
    [GRAPHIC] [TIFF OMITTED] T7697.013
    
    [GRAPHIC] [TIFF OMITTED] T7697.014
    
    [GRAPHIC] [TIFF OMITTED] T7697.015
    
    [GRAPHIC] [TIFF OMITTED] T7697.016
    
    [GRAPHIC] [TIFF OMITTED] T7697.017
    
    Mr. Doolittle. We are in the middle of a vote, but I think 
we have time to take the testimony of one more person before 
that happens, so we will hear from Mr. John E. Baker, Jr., 
chairman of the Southern Ute Indian Tribe, Ignacio, Colorado.

STATEMENT OF JOHN E. BAKER, JR., CHAIRMAN, SOUTHERN UTE INDIAN 
                    TRIBE, IGNACIO, COLORADO

    Mr. Baker. Thank you very much, Mr. Chairman. I want to 
thank each and every one of you for allowing me this 
opportunity to speak on behalf of my tribe.
    Good afternoon. My name is John E. Baker, Jr. I am the 
chairman of the Southern Ute Indian Tribe. On behalf of the 
tribe and the Tribal Council, I offer unqualified support for 
the enactment of H.R. 3112. We hope that the bill can be 
promptly enacted into law.
    Last fall, I was elected chairman of the tribe on a 
platform that included a new approach to tribal government. A 
lot has changed since I took office, but one thing has not, the 
strong support for the Animas-La Plata project. ALP is the only 
way to provide the tribe with a water supply to meet its 
present and future needs. The Tribal Council continues to 
support ALP just as a prior Tribal Council did when my father, 
John E. Baker, Jr., was chairman, just as the council did when 
my uncle, Chris A. Baker, Sr., was chairman, just as the 
council did when my predecessor, Clement J. Frost, was 
chairman, and just as the council did during the many years of 
leadership under Leonard C. Burch.
    The Tribal Council is elected to lead the Southern Ute 
Indian Tribe. Over the years, the council has sought a firm and 
reliable supply of water to serve as the foundation for tribal 
economic growth and we as we move into the new century. The 
present council, like past councils, understands that economic 
success in the arid Southwest requires a dependable water 
supply. Water will be needed whether the future of the Southern 
Ute Indian Tribe includes continued success in natural resource 
development or reflects the recreation and tourist industry 
that is now an important part of the economy of the Four 
Corners region. With an ever-growing tribal membership, we also 
need houses and domestic water supply on the west side of our 
reservation no matter what economic enterprises the tribe 
ultimately undertakes.
    Based on the studies of the draft EIS, we know that storage 
is required to provide the tribe with a firm and flexible 
supply of water. The United States promised that the tribe 
would have such a water supply in 1868 when it created the Ute 
Reservation. It confirmed that promise in 1988 when it passed 
the Colorado Ute Indian Water Rights Settlement Act in 1988, 
102 Stat. 2973. Now is the time for the United States to carry 
out those commitments.
    The project that would be constructed under the present 
legislation is much different than the originally proposed ALP. 
It is much different than Phase I of the project which was to 
be constructed under the terms of the 1986 Settlement Agreement 
and the 1988 Settlement Act. It is also much different than the 
ALP Lite project, which was proposed only 2 years ago. All of 
the changes that have been made respond to arguments by the 
project opponents.
    First of all, the project is now unquestionably an Indian 
water rights project.
    Second, the major environmental issues associated with 
irrigation and the Endangered Species Act have been eliminated 
by downsizing the project.
    Third, the cost of the project has been greatly reduced.
    Fourth, the proposed legislation contains no short cut to 
environmental compliance.
    Despite these major changes to the project, there is still 
opposition. The opponents would oppose any water project, no 
matter how small its impact and no matter who gets the 
benefits. Congress should not be swayed by the arguments that 
are raised against the project but should move forth to carry 
out the promises made to the two Ute Tribes. The bill is still 
a good solution to a very difficult problem. It should be 
passed.
    In closing, Mr. Chairman, I want to express my appreciation 
for your work on this matter. I also want to note Congressman 
McInnis's leadership on this difficult issue. We appreciate his 
hard work and support.
    Finally, we want to say thank you to Secretary Babbitt, 
Deputy Secretary Hayes, and the Department of the Interior for 
their work on these matters. We also want to state our 
appreciation for the sacrifices made by our non-Indian 
neighbors who have never wavered in their insistence that the 
United States should honor its commitments to the two Ute 
Tribes.
    In closing, I sit here as a veteran of this country and I 
believe that we live in the greatest country of the world and I 
think it is only honorable that we honor the agreements made 
between the United States and the Southern Ute Indian Tribe. 
Thank you.
    Mr. Doolittle. Thank you.
    [The prepared statement of Mr. Baker follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.018
    
    [GRAPHIC] [TIFF OMITTED] T7697.019
    
    [GRAPHIC] [TIFF OMITTED] T7697.020
    
    [GRAPHIC] [TIFF OMITTED] T7697.021
    
    [GRAPHIC] [TIFF OMITTED] T7697.022
    
    [GRAPHIC] [TIFF OMITTED] T7697.023
    
    [GRAPHIC] [TIFF OMITTED] T7697.024
    
    [GRAPHIC] [TIFF OMITTED] T7697.025
    
    Mr. Doolittle. The committee will now recess for the votes, 
which I think there are two, so we should be back here in about 
20 minutes.
    [Recess.]
    Mr. Doolittle. The subcommittee will reconvene and we will 
hear from Mr. Ernest House, chairman of the Ute Mountain Tribe 
in Colorado. Mr. House?

 STATEMENT OF ERNEST HOUSE, CHAIRMAN, UTE MOUNTAIN UTE TRIBE, 
                        TOWAOC, COLORADO

    Mr. House. My name is Ernest House, Sr. I am the tribal 
chairman for the Ute Mountain Ute Tribe. I am honored to 
testify today in support of H.R. 3112, the Colorado Ute 
Settlement Act Amendment of 1999.
    It is difficult to describe my tribe's long-term commitment 
to obtain a fair and just settlement of our water claims in 
Southwest Colorado and Northwest New Mexico. I thought the best 
way to do so might be to introduce you to my father, Thomas 
House, Sr., who served on the Ute Mountain Ute Tribal Council 
more than a quarter of a century ago.
    I also wanted to demonstrate to the chairman and members of 
the committee that the real reason we have remained so 
committed in spite of the many years which have passed and 
delays which have been endured. The Ute Mountain Tribe and our 
sister tribe, Southern Utes, entered into a settlement and have 
patiently waited for its implementation for the future of our 
tribe. Joining me today are my son and daughter, the picture of 
our future generation who will benefit from this firm supply of 
water. I ask you to welcome my son, Ernest House Jr. and my 
daughter, Michelle House.
    I was a young tribal member, like them, in 1968 when my 
grandfather, the late Chief Jack House appeared before the 
House Interior and Insular Affairs to testify in support of the 
Animas-La Plata project. I was also a tribal chairman in 1986 
when the Southern Ute Tribe and Ute Mountain Ute Tribe signed 
the original settlement agreement with the State of Colorado. 
The agreement eventually became the Colorado Ute Indian Water 
Rights Settlement Act of 1988, which was passed by Congress and 
signed into law by President Ronald Reagan. Needless to say, 
the Ute Mountain Ute people rejoiced with the Southern Ute 
people and our non-Indian neighbors because we thought we had 
finally won the long battle to acquire a firm water supply to 
meet the present and future needs of our Indian people of 
Southwest Colorado.
    Two years ago when we came before the subcommittee in 
support of legislation to implement the Colorado Ute Indian 
Water Rights Settlement Act, the Clinton administration refused 
to support our cause. Today, however, I understand, hopefully, 
that the Clinton administration will support H.R. 3112, along 
with the State of Colorado, New Mexico, and sister tribe, the 
Southern Ute Indians. H.R. 3112 will provide for the final 
settlement of the water rights claims of the Southern Ute 
Indian and Ute Mountain Ute Tribes in Southwestern Colorado. 
The legislation creates substantial new water supply to the Ute 
Tribes and supplemental supplies to the non-Indian communities 
in our area.
    We urge you to promptly consider and approve the 
legislation so that at the conclusion of our environmental 
process, the Secretary of the Interior can move forward with a 
record of decision and construction of this crucially needed 
water supply project.
    As a tribal leader on the national, State, and local level 
for several years, I have witnessed on many occasions major 
confrontation and battles between Indians and non-Indians on 
various issues affecting tribal sovereignty, tribal natural 
resources, social and economic issues.
    Finally, I would like to thank on behalf of the Ute 
Mountain Ute tribe and our other tribal and non-tribal partners 
in Colorado and New Mexico, both the Congress and the Clinton 
administration for their leadership. Through the efforts 
undertaken different times with different styles, a commitment 
has emerged from both the U.S. Congress and the Clinton 
administration to resolve once and for all this lengthy 
struggle to find a legally and scientifically supportive 
solution. We are indeed fortunate that the elected leaders from 
the tribes, La Plata and Montezuma Counties, and the States of 
Colorado and New Mexico, and elected and non-elected officials 
in Denver and Washington, DC., have put aside their differences 
and worked to develop a rational and equitable solution.
    It is time to move forward. I want my father to see this 
project in his lifetime and I am hopeful my children will be 
able to focus their efforts on other important tribal issues 
and will not 10 years from now be sitting here where I am today 
asking for the water for my people.
    I thank you, Chairman, for the time that you have allowed 
me. Also, for the record, I would like to say that the State of 
Colorado has submitted their support and it is in the record, 
to let the committee know that the State of Colorado also 
supports the water project. Thank you very much.
    Mr. Doolittle. Thank you.
    [The prepared statement of Mr. House follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.026
    
    [GRAPHIC] [TIFF OMITTED] T7697.027
    
    [GRAPHIC] [TIFF OMITTED] T7697.028
    
    [GRAPHIC] [TIFF OMITTED] T7697.029
    
    Mr. Doolittle. Our next witness is Mr. Sage Douglas 
Remington, Director of the Southern Utes Grassroots 
Organization from Colorado. I might note, it is my 
understanding that Mr. Remington has been quite ill and has 
spent a good deal of time here today waiting for us to get 
through this hearing, so we hope you are feeling better and I 
appreciate your patience. Mr. Remington?

  STATEMENT OF SAGE DOUGLAS REMINGTON, DIRECTOR, SOUTHERN UTE 
           GRASSROOTS ORGANIZATION, IGNACIO, COLORADO

    Mr. Remington. Thank you. My name is Sage Douglas Remington 
and I am a representative of the Southern Ute Grassroots 
Organization, which represents Southern Ute Tribal members who 
are deeply concerned about the Animas-La Plata project and its 
long-term effect on water resources of the Southern Ute Tribe.
    First of all, a number of groups that are concerned about 
the ALP water project are not present because verbal testimony 
has been limited to one representative. Therefore, I am not in 
a position to address or cover their issues of concern.
    Our main concern is that there has been little 
communication between the Southern Ute Tribal membership and 
the Tribal Council about the long-term benefits of the Animas-
La Plata water project. We are not opposed to ALP. What we are 
opposed to is the autocratic attitude of the Southern Ute 
Tribal Council.
    The ALP water project does not meet the legal mandate of 
NEPA or other fiscal and environmental laws and the Bureau of 
Reclamation has ignored the alternative proposed by the 
opponents to the project. The indefinite purposes and needs for 
the project violates NEPA, the Clean Water Act, and is not 
consistent with sound taxpayer public policy.
    The SEIS states that the purpose of and the need for the 
preferred ALP alternative is to implement the Colorado Ute 
Water Rights Settlement Act by providing the Ute Tribes with an 
assured long-term water supply and water acquisition fund in 
order to satisfy the tribes' senior water rights claims as 
quantified in the Settlement Act and to provide for identified 
M&I water needs in the project area.
    What the concerned members of the tribe believe is that the 
proposed project does not implement the Settlement Act. 
Instead, it proposes an entirely different configuration of the 
water development as originally proposed. Settling Indian water 
claims may be a purpose, but implementing the 1988 Settlement 
Agreement clearly is not.
    The Bureau's position is that no uses of the water must be 
identified because it implements an Indian water rights 
settlement. The proposed action does not, however, identify any 
need for non-Indian water in the project. As a matter of fact, 
it does not even justify why non-Indian water is needed, if 
needed at all. There is no valid comparison of the alternatives 
if the purpose of the project is not clearly defined.
    Absent is any specification of how the Ute Tribes will 
actually use their water. According to the SEIS, the ultimate 
use of project water, about three-fourths of the total water 
supply, by the Ute Tribes would be more specifically defined by 
those tribes as future needs develop. The SEIS lists a number 
of non-binding different water uses that the tribes may or may 
not decide to pursue in the future. Noticeably absent is a 
discussion of firm tribal plans to utilize their water. It 
appears that the only presently foreseeable use is tribal water 
marketing.
    The failure to identify actual uses for tribal water is a 
fundamental law that infects the analysis undertaken by the 
Bureau of Reclamation in the SEIS. Without knowing how Ute 
water will be used, it is difficult to rationalize whether or 
not the proposed reservoir is necessary. Other than providing 
water uses proximate to the proposed reservoir or within the 
Animas basin, it is difficult to see what advantages the Ridges 
basin reservoir will achieve that could not be accomplished by 
exchange using the numerous other Federal storage facilities in 
the greater project area. The end uses of the water are 
connected and interdependent actions and NEPA on them must be 
completed before the project can be approved.
    The failure to specify actual water uses is not remedied by 
the speculative non-binding uses offered in the SEIS. While 
several of these may have merit at some point in the future, 
others, notably the single largest water use proposed for a 
project, a coal-fired power plant which the tribal membership 
does not know anything about, are speculative and painfully 
strain even the most optimistic assumptions about future 
development in the Four Corners region. No private sector 
investor, whether for-profit or not-for-profit, would ever 
direct valuable resources into such a pipe dream regardless of 
the beneficiaries.
    The Animas-La Plata project is not needed to facilitate 
tribal water marketing and will impose serious legal and 
economic penalties on the tribal attempts to market water. 
Because the SEIS offers no foreseeable use for the majority of 
tribal water other than marketing, Interior and other Federal 
departments have failed to justify any actual need for the 
Animas-La Plata project.
    The membership of SUGO feels that if the settlement of the 
Indian water rights claim is truly a purpose of the project, 
then the issue of tribal water leasing or sale out of State 
must be considered. Thank you.
    Mr. Doolittle. Thank you.
    [The prepared statement of Mr. Remington follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.030
    
    [GRAPHIC] [TIFF OMITTED] T7697.031
    
    [GRAPHIC] [TIFF OMITTED] T7697.032
    
    [GRAPHIC] [TIFF OMITTED] T7697.033
    
    Mr. Doolittle. It seems to me over the 10-years I have been 
on this committee, I have heard several hearings on Animas-La 
Plata, so hopefully we will actually make this happen this 
time.
    I would like to include in the record statements from 
Richard K. Griswold, President of the Animas-La Plata Water 
Conservancy District; Thomas C. Turney, State engineer with the 
State of New Mexico; the San Juan Water Commission in New 
Mexico; Fred V. Kroeger, President of the Southwestern Water 
Conservation District; and Michael Black with the Taxpayers for 
the Animas River.
    [The prepared statement of Mr. Griswold follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.034
    
    [GRAPHIC] [TIFF OMITTED] T7697.035
    
    [GRAPHIC] [TIFF OMITTED] T7697.036
    
    [GRAPHIC] [TIFF OMITTED] T7697.037
    
    [GRAPHIC] [TIFF OMITTED] T7697.038
    
    [The prepared statement of Mr. Turney follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.039
    
    [GRAPHIC] [TIFF OMITTED] T7697.040
    
    [GRAPHIC] [TIFF OMITTED] T7697.041
    
    [The prepared statement of the San Juan Water Commission 
follows:]

[GRAPHIC] [TIFF OMITTED] T7697.042

[GRAPHIC] [TIFF OMITTED] T7697.043

[GRAPHIC] [TIFF OMITTED] T7697.044

[GRAPHIC] [TIFF OMITTED] T7697.045

    [The prepared statement of Mr. Kroeger follows:]

    [GRAPHIC] [TIFF OMITTED] T7697.046
    
    [GRAPHIC] [TIFF OMITTED] T7697.047
    
    [GRAPHIC] [TIFF OMITTED] T7697.048
    
    [The prepared statement of Mr. Black follows:]
    [GRAPHIC] [TIFF OMITTED] T7697.049
    
    [GRAPHIC] [TIFF OMITTED] T7697.050
    
    [GRAPHIC] [TIFF OMITTED] T7697.051
    
    Mr. Doolittle. Mr. Hayes, it has been implied that this 
legislation impedes the tribe from interstate water marketing 
and I wondered if you agree that the tribe has given up any 
right to market their water or would they be required in any 
case, with or without this bill, to abide by State law in 
marketing their water.
    Mr. Hayes. Mr. Chairman, the marketing constraints have 
been identified in the existing Animas-La Plata legislation and 
the proposed amendments would not affect that in any way. So 
the status quo in terms of authorization would continue in 
place, and there are some restrictions on interstate marketing 
in the current authorization.
    Mr. Doolittle. So we are not making anything worse in that 
regard by the present legislation?
    Mr. Hayes. There is no change.
    Mr. Doolittle. There is language in this bill, I think, 
intended to address the administration's concern about the 
desire to deauthorize the project; it is on page seven at the 
bottom, paragraph three, which says, ``if constructed, the 
facility described in paragraph 1(a) shall not be used in 
conjunction with any other facility authorized as part of the 
Animas-La Plata project without express authorization from 
Congress.'' Does that legislation meet your concern and cause 
you to support the bill, Mr. Hayes?
    Mr. Hayes. On this issue, the language that have set forth 
would address the issue satisfactorily from the 
administration's perspective, Mr. Chairman.
    Mr. Doolittle. Thank you. This project is much reduced from 
what it was. Personally, I would have preferred the larger 
version, but perhaps some of you would, too. In any event, this 
is where we are. It has been a long, difficult process. I 
appreciate the patience you have had with our committee. It 
really would not have taken very long if we had not been 
delayed by those votes.
    The subcommittee is pretty familiar, I think, with this 
project because of the number of times it has been before us. I 
would hope we could go to markup fairly soon after this hearing 
and go from there and hopefully it can move through the Senate 
side expeditiously.
    I would like to thank all of you for your attendance today. 
We may have further questions and I would ask you to respond 
expeditiously to those that we would tender following the 
hearing. With that, the hearing is adjourned.
    [Whereupon, at 4:19 p.m., the subcommittee was adjourned.]


</pre></body></html>
