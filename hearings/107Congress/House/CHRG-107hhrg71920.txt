<html>
<title> - DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES APPROPRIATIONS FOR 2002</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]




 
                 DEPARTMENT OF THE INTERIOR AND RELATED
                    AGENCIES APPROPRIATIONS FOR 2002

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS
                              FIRST SESSION
                                ________
   SUBCOMMITTEE ON THE DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES
                     JOE SKEEN, New Mexico, Chairman
 RALPH REGULA, Ohio                  NORMAN D. DICKS, Washington
 JIM KOLBE, Arizona                  JOHN P. MURTHA, Pennsylvania
 CHARLES H. TAYLOR, North Carolina   JAMES P. MORAN, Virginia
 GEORGE R. NETHERCUTT, Jr.,          MAURICE D. HINCHEY, New York
Washington                           MARTIN OLAV SABO, Minnesota 
 ZACH WAMP, Tennessee
 JACK KINGSTON, Georgia
 JOHN E. PETERSON, Pennsylvania     
                     
 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
   Deborah Weatherly, Loretta Beaumont, Joel Kaplan, and Christopher 
                                 Topik,
                            Staff Assistants
                                ________
                                 PART 4
                  Justification of the Budget Estimates
                                                                   Page
 Indian Health Service............................................    1
 Navajo and Hopi Indian Relocation................................  395
 Institute of American Indian and Alaska Native...................  415
 Smithsonian Institution..........................................  439
 National Gallery of Art..........................................  603
 John F. Kennedy Center...........................................  655
 Woodrow Wilson International Center..............................  727
 National Endowment for the Arts..................................  781
 National Endowment for the Humanities............................  897
 IMLS Office of Museum Services...................................  993
 Commission of Fine Arts.......................................... 1065
 Advisory Council on Historic Preservation........................ 1081
 National Capital Planning Commission............................. 1127
 Holocaust Memorial Council....................................... 1175
 Presidio Trust................................................... 1237
                                ________
         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 71-920                     WASHINGTON : 2001





                       COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                  DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky             NORMAN D. DICKS, Washington
 JOE SKEEN, New Mexico               MARTIN OLAV SABO, Minnesota
 FRANK R. WOLF, Virginia             STENY H. HOYER, Maryland
 TOM DeLAY, Texas                    ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                  MARCY KAPTUR, Ohio
 SONNY CALLAHAN, Alabama             NANCY PELOSI, California
 JAMES T. WALSH, New York            PETER J. VISCLOSKY, Indiana
 CHARLES H. TAYLOR, North Carolina   NITA M. LOWEY, New York
 DAVID L. HOBSON, Ohio               JOSE E. SERRANO, New York
 ERNEST J. ISTOOK, Jr., Oklahoma     ROSA L. DeLAURO, Connecticut
 HENRY BONILLA, Texas                JAMES P. MORAN, Virginia
 JOE KNOLLENBERG, Michigan           JOHN W. OLVER, Massachusetts
 DAN MILLER, Florida                 ED PASTOR, Arizona
 JACK KINGSTON, Georgia              CARRIE P. MEEK, Florida
 RODNEY P. FRELINGHUYSEN, New Jersey DAVID E. PRICE, North Carolina
 ROGER F. WICKER, Mississippi        CHET EDWARDS, Texas
 GEORGE R. NETHERCUTT, Jr.,          ROBERT E. ``BUD'' CRAMER, Jr., 
Washington                           Alabama
 RANDY ``DUKE'' CUNNINGHAM,          PATRICK J. KENNEDY, Rhode Island
California                           JAMES E. CLYBURN, South Carolina
 TODD TIAHRT, Kansas                 MAURICE D. HINCHEY, New York
 ZACH WAMP, Tennessee                LUCILLE ROYBAL-ALLARD, California
 TOM LATHAM, Iowa                    SAM FARR, California
 ANNE M. NORTHUP, Kentucky           JESSE L. JACKSON, Jr., Illinois
 ROBERT B. ADERHOLT, Alabama         CAROLYN C. KILPATRICK, Michigan
 JO ANN EMERSON, Missouri            ALLEN BOYD, Florida
 JOHN E. SUNUNU, New Hampshire       CHAKA FATTAH, Pennsylvania
 KAY GRANGER, Texas                  STEVEN R. ROTHMAN, New Jersey    
 JOHN E. PETERSON, Pennsylvania
 JOHN T. DOOLITTLE, California
 RAY LaHOOD, Illinois
 JOHN E. SWEENEY, New York
 DAVID VITTER, Louisiana
 DON SHERWOOD, Pennsylvania
   
 VIRGIL H. GOODE, Jr., Virginia     
   
                 James W. Dyer, Clerk and Staff Director

                                  (ii)



              [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]


[Pages 1 - 1256--The official Committee record contains additional material here]
</pre></body></html>
