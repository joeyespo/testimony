<html>
<title> - EMPLOYING VETERAN'S OF OUR ARMED FORCES </title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]



 
               EMPLOYING VETERAN'S OF OUR ARMED FORCES                    ----------------------------------------------------------------------- 


                              HEARING

                             BEFORE THE

                   COMMITTEE ON VETERAN'S AFFAIRS

                     HOUSE OF REPRESENTATIVES

                    ONE HUNDRED EIGHTH CONGRESS 

                          SECOND SESSION

                          --------------

                          MARCH 24, 2004 

                           ----------- 

     Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 108-33



                    U.S. GOVERNMENT PRINTING OFFICE
98-034PDF                 WASHINGTON DC:  2005
---------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing
Office  Internet: bookstore.gpo.gov Phone: toll free (866)512-1800
DC area (202)512-1800  Fax: (202) 512-2250 Mail Stop SSOP, 
Washington, DC 20402-0001


<TEXT NOT AVAILABLE>

</pre></body></html>
