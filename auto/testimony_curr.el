(TeX-add-style-hook
 "testimony_curr"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("nag" "l2tabu" "orthodox") ("caption" "center") ("xcolor" "table")))
   (TeX-run-style-hooks
    "latex2e"
    "nag"
    "article"
    "art11"
    "geometry"
    "amssymb"
    "amsmath"
    "graphicx"
    "fullpage"
    "setspace"
    "rotating"
    "subfigure"
    "longtable"
    "color"
    "type1cm"
    "lettrine"
    "epstopdf"
    "caption"
    "xcolor"
    "harvard"
    "microtype"
    "appendix"
    "hyperref")
   (LaTeX-add-bibliographies
    "/Users/ian/Dropbox/common/library")))

