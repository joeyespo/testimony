<html>
<title> - OPERATIONS OF THE BOARD OF VETERANS' APPEALS AND COURT OF VETERANS APPEALS, AND REVIEW OF H.R. 3212, WITH RESPECT TO THE COURT OF VETERANS APPEALS RETIREMENT PLAN</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
               OPERATIONS OF THE BOARD OF VETERANS' APPEALS AND 
                COURT OF VETERANS APPEALS, AND REVIEW OF H.R.
                  3212, WITH RESPECT TO THE COURT OF VETERANS 
                            APPEALS RETIREMENT PLAN 

=======================================================================





                                 HEARING

                               before the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                              SECOND SESSION

                               ------------



                               JUNE 10, 1998

                               ------------




        Printed for the use of the Committee on Veterans' Affairs


                           Serial No. 105-39


                    U.S. GOVERNMENT PRINTING OFFICE
 51-115 CC                   WASHINGTON: 1998

-----------------------------------------------------------------------


           For sale by the U.S. Government Printing Office

      Superintendent of Documents, Congressional Sales Office,
                           Washington, DC 20402

                            ISBN 0-16-057708-X

                  <TEXT NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
