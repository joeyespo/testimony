<html>
<title> - FORMATION OF THE VA CHICAGO HEALTH CARE SYSTEM</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                  FORMATION OF THE VA CHICAGO HEALTH CARE 
                                  SYSTEM 

=============================================================================


                             FIELD HEARING

                               before the

              SUBCOMMITTEE ON OVERSIGHT AND INVESTIGATIONS

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                         HOUSE OF REPRESENTATIVES

                        ONE HUNDRED FIFTH CONGRESS

                               FIRST SESSION

                                 ----------

                              OCTOBER 16, 1997

                                 ----------

         Printed for the use of the Committee on Veterans' Affairs


                             Serial No. 105-24


                     U.S. GOVERNMENT PRINTING OFFICE

  46-317 CC                  WASHINGTON : 1998

-----------------------------------------------------------------------------

               For sale by the U.S. Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402
                            ISBN 0-16-056585-0

                    <TEXT NOT AVAILABLE IN TIFF FORMAT>


</pre></body></html>
