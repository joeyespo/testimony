<html>
<title> - MILITARY CONSTRUCTION, VETERANS AFFAIRS, AND RELATED AGENCIES APPROPRIATIONS FOR 2011</title>
<body><pre>
[House Hearing, 111 Congress]
[From the U.S. Government Printing Office]


 
     MILITARY CONSTRUCTION, VETERANS AFFAIRS, AND RELATED AGENCIES 
                        APPROPRIATIONS FOR 2011 

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED ELEVENTH CONGRESS
                             SECOND SESSION
                                ________
  SUBCOMMITTEE ON MILITARY CONSTRUCTION, VETERANS AFFAIRS, AND RELATED 
                        AGENCIES APPROPRIATIONS
                      CHET EDWARDS, Texas, Chairman
 SAM FARR, California                           ZACH WAMP, Tennessee
 JOHN T. SALAZAR, Colorado                      ANDER CRENSHAW, Florida
 NORMAN D. DICKS, Washington                    C. W. BILL YOUNG, Florida
 PATRICK J. KENNEDY, Rhode Island               JOHN CARTER, Texas
 SANFORD D. BISHOP, Jr., Georgia
 MARION BERRY, Arkansas
 STEVE ISRAEL, New York             
                                    

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
     Tim Peterson, Sue Quantius, Walter Hearne, and Mary C. Arnold,
                           Subcommittee Staff

                                ________

                                 PART 3
                  JUSTIFICATION OF THE BUDGET ESTIMATES
                                AIR FORCE
                                                                   Page
 Military Construction............................................    1
 Family Housing...................................................  277
                           RESERVE COMPONENTS
 Air National Guard...............................................  341
 Air Force Reserve................................................  441

                                ________

         Printed for the use of the Committee on Appropriations
















 Part 3--MILITARY CONSTRUCTION, VETERANS AFFAIRS, AND RELATED AGENCIES 
                        APPROPRIATIONS FOR 2011
                                                                      




















     MILITARY CONSTRUCTION, VETERANS AFFAIRS, AND RELATED AGENCIES 
                        APPROPRIATIONS FOR 2011

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED ELEVENTH CONGRESS
                             SECOND SESSION
                                ________
  SUBCOMMITTEE ON MILITARY CONSTRUCTION, VETERANS AFFAIRS, AND RELATED 
                        AGENCIES APPROPRIATIONS
                      CHET EDWARDS, Texas, Chairman
 SAM FARR, California                     ZACH WAMP, Tennessee
 JOHN T. SALAZAR, Colorado                ANDER CRENSHAW, Florida
 NORMAN D. DICKS, Washington              C. W. BILL YOUNG, Florida
 PATRICK J. KENNEDY, Rhode Island         JOHN CARTER, Texas
 SANFORD D. BISHOP, Jr., Georgia
 MARION BERRY, Arkansas
 STEVE ISRAEL, New York             

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
     Tim Peterson, Sue Quantius, Walter Hearne, and Mary C. Arnold,
                           Subcommittee Staff

                                ________

                                 PART 3
                  JUSTIFICATION OF THE BUDGET ESTIMATES
                                AIR FORCE
                                                                   Page
 Military Construction............................................    1
 Family Housing...................................................  277
                           RESERVE COMPONENTS
 Air National Guard...............................................  341
 Air Force Reserve................................................  441

                                ________

                         U.S. GOVERNMENT PRINTING OFFICE 

55-040 PDF                       WASHINGTON : 2010 

For sale by the Superintendent of Documents, U.S. Government Printing 
Office Internet: bookstore.gpo.gov Phone: toll free (866) 512-1800; 
DC area (202) 512-1800 Fax: (202) 512-2104 Mail: Stop IDCC, 
Washington, DC 20402-0001 


                            COMMITTEE ON APPROPRIATIONS

                   DAVID R. OBEY, Wisconsin, Chairman

 NORMAN D. DICKS, Washington               JERRY LEWIS, California
 ALAN B. MOLLOHAN, West Virginia           C. W. BILL YOUNG, Florida
 MARCY KAPTUR, Ohio                        HAROLD ROGERS, Kentucky
 PETER J. VISCLOSKY, Indiana               FRANK R. WOLF, Virginia
 NITA M. LOWEY, New York                   JACK KINGSTON, Georgia
 JOSE E. SERRANO, New York                 RODNEY P. FRELINGHUYSEN, New   
 ROSA L. DeLAURO, Connecticut                Jersey
 JAMES P. MORAN, Virginia                  TODD TIAHRT, Kansas
 JOHN W. OLVER, Massachusetts              ZACH WAMP, Tennessee
 ED PASTOR, Arizona                        TOM LATHAM, Iowa
 DAVID E. PRICE, North Carolina            ROBERT B. ADERHOLT, Alabama
 CHET EDWARDS, Texas                       JO ANN EMERSON, Missouri
 PATRICK J. KENNEDY, Rhode Island          KAY GRANGER, Texas
 MAURICE D. HINCHEY, New York              MICHAEL K. SIMPSON, Idaho
 LUCILLE ROYBAL-ALLARD, California         JOHN ABNEY CULBERSON, Texas
 SAM FARR, California                      MARK STEVEN KIRK, Illinois
 JESSE L. JACKSON, Jr., Illinois           ANDER CRENSHAW, Florida
 CAROLYN C. KILPATRICK, Michigan           DENNIS R. REHBERG, Montana
 ALLEN BOYD, Florida                       JOHN R. CARTER, Texas
 CHAKA FATTAH, Pennsylvania                RODNEY ALEXANDER, Louisiana
 STEVEN R. ROTHMAN, New Jersey             KEN CALVERT, California
 SANFORD D. BISHOP, Jr., Georgia           JO BONNER, Alabama
 MARION BERRY, Arkansas                    STEVEN C. LaTOURETTE, Ohio
 BARBARA LEE, California                   TOM COLE, Oklahoma
 ADAM SCHIFF, California
 MICHAEL HONDA, California
 BETTY McCOLLUM, Minnesota
 STEVE ISRAEL, New York
 TIM RYAN, Ohio
 C.A. ``DUTCH'' RUPPERSBERGER, 
  Maryland
 BEN CHANDLER, Kentucky
 DEBBIE WASSERMAN SCHULTZ, Florida
 CIRO RODRIGUEZ, Texas
 LINCOLN DAVIS, Tennessee
 JOHN T. SALAZAR, Colorado
 ------ ------                      

                 Beverly Pheto, Clerk and Staff Director

                                  (ii)


[TEXT NOT AVAILABLE IN TIFF FORMAT REFER TO PDF]


</pre></body></html>
