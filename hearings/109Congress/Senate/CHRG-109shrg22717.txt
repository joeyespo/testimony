<html>
<title> - NOMINATION HEARING OF RICHARD RAYMOND TO BE UNDER SECRETARY FOR FOOD SAFETY, DEPARTMENT OF AGRICULTURE</title>
<body><pre>
[Senate Hearing 109-455]
[From the U.S. Government Printing Office]



                                                        S. Hrg. 109-455
 
 NOMINATION HEARING OF RICHARD RAYMOND TO BE UNDER SECRETARY FOR FOOD 
                   SAFETY, DEPARTMENT OF AGRICULTURE

=======================================================================

                                HEARING

                               before the

                       COMMITTEE ON AGRICULTURE,
                        NUTRITION, AND FORESTRY

                          UNITED STATES SENATE


                       ONE HUNDRED NINTH CONGRESS

                             FIRST SESSION


                               __________

                             JUNE 22, 2005

                               __________

                       Printed for the use of the
           Committee on Agriculture, Nutrition, and Forestry


  Available via the World Wide Web: http://www.agriculture.senate.gov



                                 ______

                    U.S. GOVERNMENT PRINTING OFFICE
22-717                      WASHINGTON : 2006
_____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092250 Mail: Stop SSOP, Washington, DC 20402�090001


           COMMITTEE ON AGRICULTURE, NUTRITION, AND FORESTRY



                   SAXBY CHAMBLISS, Georgia, Chairman

RICHARD G. LUGAR, Indiana            TOM HARKIN, Iowa
THAD COCHRAN, Mississippi            PATRICK J. LEAHY, Vermont
MITCH McCONNELL, Kentucky            KENT CONRAD, North Dakota
PAT ROBERTS, Kansas                  MAX BAUCUS, Montana
JAMES M. TALENT, Missouri            BLANCHE L. LINCOLN, Arkansas
CRAIG THOMAS, Wyoming                DEBBIE A. STABENOW, Michigan
RICK SANTORUM, Pennsylvania          E. BENJAMIN NELSON, Nebraska
NORM COLEMAN, Minnesota              MARK DAYTON, Minnesota
MICHEAL D. CRAPO, Idaho              KEN SALAZAR, Colorado
CHARLES E. GRASSLEY, Iowa

            Martha Scott Poindexter, Majority Staff Director

                David L. Johnson, Majority Chief Counsel

              Steven Meeks, Majority Legislative Director

                      Robert E. Sturm, Chief Clerk

                Mark Halverson, Minority Staff Director

                                  (ii)

  
                            C O N T E N T S

                              ----------                              
                                                                   Page

Hearing(s):

Nomination Hearing of Richard Raymond to be Under Secretary for 
  Food Safety, Department of Agriculture.........................    01

                              ----------                              

                        Wednesday June 22, 2005
                    STATEMENTS PRESENTED BY SENATORS

Chambliss, Hon. Saxby, a U.S. Senator from Georgia, Chairman, 
  Committee on Agriculture, Nutrition, and Forestry..............    01
Hagel, Hon. Chuck, a U.S. Senator from Nebraska..................    02
Nelson, Hon. Ben, a U.S. Senator from Nebraska...................    07
Salazar, Hon. Ken, a U.S. Senator from Colorado..................    09
                              ----------                              

                               WITNESSES

Raymond, Richard, M.D., of Nebraska, to be Under Secretary for 
  Food Safety, U.S. Department of Agriculture....................    03
                              ----------                              

                                APPENDIX

Prepared Statements:
    Harkin, Hon. Tom.............................................    12
    Nelson, Hon. Ben.............................................    13
    Raymond, M.D. Richard........................................    16
Document(s) Submitted for the Record:
    Raymond, M.D. Richard (Biographical Information).............    20
Questions and Answers Submitted for the Record:
    Harkin, Hon. Tom.............................................    50
    Grassley, Hon. Charles E.....................................    72



                               NOMINATIONS

                              ----------                              


                        WEDNESDAY, JUNE 22, 2005

                                       U.S. Senate,
          Committee on Agriculture, Nutrition and Forestry,
                                                    Washington, DC.
    The committee met, pursuant to notice, at 10:10 a.m., in 
room SR-328A, Russell Senate Office Building, Hon. Saxby 
Chambliss, chairman of the committee, presiding.
    Present or Submitting a Statement: Senators Chambliss, 
Grassley, Nelson and Salazar.

STATEMENT OF HON. SAXBY CHAMBLISS, A U.S. SENATOR FROM GEORGIA, 
  CHAIRMAN, COMMITTEE ON AGRICULTURE, NUTRITION, AND FORESTRY

    The Chairman. Good morning. We are pleased to be here today 
to consider the nomination of Dr. Richard Raymond to be the 
Under Secretary of Agriculture for Food Safety. Dr. Raymond 
currently serves as the Director of the Nebraska Department of 
Health and Human Services Regulation and Licensure.
    As Director of Regulation and Licensure, he oversees 
regulatory programs involving health care and environmental 
issues that affect public health. He was appointed to the 
Regulation and Licensure Director position by then-Governor 
Mike Johanns on October 26, 2004.
    While I apologize for being late, I have been sitting with 
the former Governor Nebraska in my office for the last few 
minutes discussing some agricultural issues, Dr. Raymond.
    Since January 1999, Dr. Raymond has also been serving as 
Nebraska's Chief Medical Officer, overseeing public health 
programs, including disease prevention, health promotion and 
preparedness planning. Dr. Raymond is currently President of 
the Association of State and Territorial Health Officials and 
has served on the Association's Preparedness Committee and Food 
Safety Committee for 3 years.
    He is a lifelong Nebraska resident who practiced medicine 
in rural Nebraska for 17 years. Dr. Raymond also established 
and directed a community-based family practice residency for 
Clarkson Medical Center for 10 years. Dr. Raymond is joined 
today by his wife, Jane, and by Marlene Goroff, a family 
friend.
    We are also pleased that Senator Hagel, who is my good 
friend, is with us today to introduce Dr. Raymond to the 
committee.
    Senator Hagel, welcome to the Ag Committee and we look 
forward to your introduction.
    [The prepared statement of Senator Tom Harkin can be found 
in the appendix on page 12.]

  STATEMENT OF HON. CHUCK HAGEL, A U.S. SENATOR FROM NEBRASKA

    Senator Hagel. Mr. Chairman, thank you. It is always a 
privilege to be with you, and especially in these hallowed 
halls of glory and power in the Senate Agriculture Committee 
room. So I know I am not worthy, but nonetheless you have 
allowed me a reprieve and a visa for 1 hour.
    The Chairman. You will get an extra bag of peanuts for 
those comments.
    [Laughter.]
    Senator Hagel. Thank you, sir. Georgia peanuts.
    The Chairman. That is right.
    Senator Hagel. Mr. Chairman, I am pleased and very proud 
this morning to be here to introduce this committee to a very 
distinguished Nebraskan.
    The United States food supply, as we all know, is the 
safest in the world. It is critical that the U.S. continue that 
standard and remain on the cutting edge of food safety. Strong 
leadership and innovative thinking is essential to that goal. I 
believe the President's nominee to be the next Under Secretary 
of Agriculture for Food Safety fulfills those requirements.
    Mr. Chairman, I am very, very proud to introduce to this 
committee the individual the President believes is best 
qualified to provide that leadership, Dr. Richard Raymond. His 
humble upbringing in Loup City, Nebraska, and passion for 
public service gave him the foundation to become one of our 
Nations' most knowledgeable and hard-working public health 
professionals.
    Over the last 6 years, Dr. Raymond has served as the Chief 
Medical Officer for the Nebraska Health and Human Services 
System, as you have noted. His innovation and efforts have been 
instrumental in crafting Nebraska's nationally recognized 
public health system.
    Dr. Raymond and I have worked closely together on 
initiatives to enhance and expand public health care in 
Nebraska and the public health workforce in rural America. I am 
confident that his leadership, experience and depth of 
knowledge will maintain and strengthen U.S. food safety 
standards. President Bush and Secretary Johanns could not have 
picked a more respected professional for this job.
    I might note on a personal level, Mr. Chairman, that Dr. 
Raymond was telling me, not unlike so many dynamics in America, 
that his father once worked for the USDA in 1942, then enlisted 
in the Army to serve in World War II, and that may have well 
been an early seed, a seedling, a Raymond seedling, for a 
future distinguished senior public official in the United 
States Department of Agriculture.
    So again, Mr. Chairman, thank you for allowing me the 
privilege to be here this morning to introduce Dr. Raymond, and 
I am confident that he will serve with great distinction and 
honor.
    Thank you.
    The Chairman. Senator, thank you very much for being here.
    Dr. Raymond, I will tell you, you couldn't have a better 
person to bring with you to introduce you to this committee 
than my good friend, Chuck Hagel.
    Senator Hagel. Thank you. Now, he is on his own.
    The Chairman. You don't want to be under oath with him. Is 
that what you are telling us?
    Senator Hagel. I get nervous under oath.
    [Laughter.]
    Senator Hagel. Thank you, Mr. Chairman.
    The Chairman. Dr. Raymond, we welcome you. As I am required 
to do, I will ask you to stand and raise your right hand, 
please.
    Do you swear that the testimony you are about to give 
before this committee shall be the truth, the whole truth and 
nothing but the truth, so help you God?
    Dr. Raymond. I do.
    The Chairman. Do you agree that, if confirmed, you will 
appear before any duly constituted committee of Congress if 
asked?
    Dr. Raymond. I do.
    The Chairman. Thank you very much and we look forward to 
your comments.

 STATEMENT OF RICHARD RAYMOND, M.D., OF NEBRASKA, TO BE UNDER 
   SECRETARY FOR FOOD SAFETY, U.S. DEPARTMENT OF AGRICULTURE

    Dr. Raymond. Thank you, Mr. Chairman. Mr. Chairman, Senator 
Harkin and other distinguished members of the committee who 
will review this testimony, I am grateful for the opportunity 
to appear before you this morning as President Bush's nominee 
for Under Secretary for Food Safety at the United States 
Department of Agriculture. I want to thank the President and 
Secretary Johanns for their support and trust in nominating me 
for this important position. I also want to thank Senator Hagel 
for taking time from his busy schedule and for his kind 
introduction.
    For the record, I am Richard Raymond from the State of 
Nebraska, and I am very humbled, and at the same time very 
honored to have been nominated to serve our Nation in this 
capacity.
    As you mentioned, with me today is my wife, Jane, to whom I 
have been married for 35 years, and she is accompanied by her 
best friend from her high school days, Marlene Goroff, who was 
also her bridesmaid at our wedding 35 years ago.
    Our children would like to have been here today, but work 
obligations did not allow that. We have a son who lives in 
Kansas City, Kansas, and a daughter who lives in Fort Collins, 
Colorado.
    All of Jane's and my grandparents were Nebraska farmers and 
all of our parents grew up on Nebraska farms. As Senator Hagel 
mentioned, I think it is very interesting that my father's 
first W-2 form when he came off of the farm was with the then-
Farm Security Administration, a branch of USDA at that time.
    I grew up in the small rural town of Loup City, a town of 
1,400 people. In a town of that size, in the summertime you 
work on farms, and you work on farms on the weekends as the 
only source of income. My wife grew up in Wilber, another 
Nebraska town, a community of about 1,400. I went to college in 
Hastings, Nebraska, attended medical school at the University 
of Nebraska Medical Center, and then did a rotating internship 
in Spokane, Washington.
    Because of the appreciation and the value that Jane I have 
for living in small, rural areas and rural communities, we did 
return to Nebraska to practice family medicine and to raise our 
family in the O'Neill, Nebraska, area, a rural community area 
with a population of about 4,400 people.
    My interest in public service and public health is deeply 
rooted in the community of O'Neill, and also in the small town 
of Loup City and the people I have served as a medical 
physician for over 30 years. As a family doctor in a rural 
community, I was the key public health official to my friends, 
my neighbors and my family.
    Their trust was not only rewarding, but it sparked in me a 
greater interest in public service and public health. After 17 
years of rural practice, I was fortunate to have the 
opportunity to establish and then direct a community-based 
family practice residency program in Omaha, Nebraska. That 
residency program had a goal of training young family 
physicians in the needs and rewards of practicing in rural 
communities, and we also prepared them to succeed in rural 
practice, which is quite different than an urban practice.
    My appointment in January 1999 as the Chief Medical Officer 
for the Nebraska Health and Human Services System by then-
Governor Johanns presented me with the opportunity to further 
extend the reach of public health. In the past, food safety 
officials in the public health community have focused almost 
entirely on preventing and responding to unintentional 
contamination of the food supply.
    But especially since September 11, 2001, we have come to 
realize that public health must include defenses against forces 
that would intentionally contaminate food, water and other of 
our life necessities. Governor Johanns recognized this and 
asked me to lead Nebraska's bioterrorism preparedness and 
response planning. The Nebraska Health and Human Services 
System has a staff of almost 6,000 people located in 100 
locations throughout Nebraska and a budget of slightly under 
$2.5 billion per year.
    The Nebraska Public Health System under my leadership went 
from only 20 counties with a health department to having newly 
established multi-county public health departments that now 
serve the citizens in all 93 counties of Nebraska to help us 
meet the new reality and the threats that we are faced with.
    If I am confirmed as Under Secretary for Food Safety, I 
will use my past experience as a scientist, medical doctor, 
educator and public health advocate and spokesperson to assure 
that America's food supply continues to be the safest in the 
world.
    I applaud the recent progress and improvements that have 
been made to our food safety system. I recognize this success 
is because of the tremendous efforts of thousands of dedicated 
public health professionals within the Food Safety and 
Inspection Service at USDA, but I know we can and we must do 
better.
    I believe that by working with this team of public health 
professionals and by cooperating, communicating and 
collaborating with other internal and external stakeholders 
that we can make additional strides in improving our food 
supply.
    Mr. Chairman, I assure you that if I am confirmed by the 
committee and then by the U.S. Senate, I will work with you and 
the members of this committee to earn your respect and your 
support as we work together to make our food supply the safest 
possible.
    Thank you, and I look forward to trying to answer any 
questions you may have.
    The Chairman. Thank you very much, Dr. Raymond.
    You have clearly had a distinguished career in rural 
medicine, as well as in public health administration. Could you 
share with the committee how you believe these and any other 
experiences have specifically prepared you to lead the USDA 
Food Safety Inspection Service?
    Dr. Raymond. Mr. Chairman, I have come to believe strongly 
in the principles of public health. Any time we are faced with 
a problem and need a solution, I think we need to do a thorough 
assessment, that we need to develop policy to address those 
issues, and then we need to come back and do an assurance to 
make sure that the policy is being met and that we have 
improved the public's health and safety.
    Having worked in a small State, population-wise, to prepare 
us for any public health emergency, be it intentional or 
unintentional, has required working with multiple individuals 
from multiple walks of life, multiple advocates and multiple 
associations to combine what could be viewed as a limited 
personnel population to gather the expertise from everybody we 
possibly could.
    I think in Nebraska we have done a wonderful job of 
building our preparedness efforts by using people internal and 
external, and listening and responding and doing the 
assessments.
    The Chairman. Given your background as a local medical 
practitioner and in rural public health programs, could you 
characterize for the committee your own personal opinion of the 
present safety of the U.S. food supply?
    Dr. Raymond. Mr. Chairman, I believe the food supply safety 
is the best it has ever been right now. That said, I also know, 
as I said in my opening remarks, that we can also do better. 
And I think anytime you can do better, then good is not good 
enough.
    The Chairman. I understand that one of your duties as the 
Chief Medical Officer in Nebraska was to help direct the 
bioterrorism preparedness efforts in your State related to 
public health and food safety. In addition, I know that you 
served on the Food Safety Committee for the Association of 
State and Territorial Health Officials.
    Would you explain to the committee in a little more detail 
what your responsibilities and activities were in those two 
areas?
    Dr. Raymond. Certainly. Mr. Chairman, I would like to give 
you an example of what we did in Nebraska. I don't remember the 
exact date, but when we got the guidelines for the cooperative 
agreement from the Centers for Disease Control and Protection 
that said how the $1.1 billion was going to be distributed to 
the States to build preparedness, Nebraska was informed that we 
would get $8.5 million.
    In the guidelines, there was a list of about 30 individuals 
or associations that needed to be on the advisory committee to 
help guide us on how we were going to spend those dollars to 
better prepare Nebraska for bioterrorism possibilities.
    I looked at that list that night. Things like the Red 
Cross, the Office of Rural Health, EMS, hospitals 
associations--they were all on there. But one large constituent 
that was not on that list was agriculture, and the following 
morning when I got to work, I called Merlin Carlson, who at 
that time was the Director of Ag for Nebraska and told Merlin 
we have $8.5 million to help prepare Nebraska for biological 
attack, and I think that includes agro terrorism, Merlin.
    He was in my office in 5 minutes with a cup of coffee and 
we sat down and made a plan to include ag on our committee, and 
we also continued to support ag financially from those grants 
to run what is called the LEDRS Program in Nebraska.
    The Chairman. Nebraska is a State that has a lot of 
production of agricultural products both for feed use as well 
as for human consumption. You also are one of the leading 
livestock producers, as well as one of the leading packaging 
and processing States in the country.
    With the leadership that you have provided to the folks in 
Nebraska, are you confident from the farm to the grocery store 
there are mechanisms in place to detect any potential input of 
bioterrorism agents in that food supply such that the people of 
Nebraska and other parts of the country that buy those products 
would be safe?
    Dr. Raymond. Mr. Chairman, as I said earlier, I know we are 
dramatically better than we were four or 5 years ago. That 
doesn't mean we are where we need to be. When you look at the 
rates of recalls, the rates of human illness from food 
pathogens and the rates of positive sample testing of the 
products being produced in America and you see those rates 
going down dramatically over the last 4 years due to the work 
of the Food Safety Inspection Service, that in itself says our 
food is safer because what we do to protect our food from 
accidental contamination also helps protect our food supply 
chain from intentional contamination.
    That said, we will look at all options, if I am confirmed, 
to find the most effective and most efficient way to accomplish 
the goals of the Office of Food Safety, and that certainly 
involves and includes protection against intentional 
contamination.
    The Chairman. If you are confirmed as Under Secretary for 
Food Safety, what are some of the key challenges or 
opportunities related to U.S. food safety that you look forward 
to addressing in that new role?
    Dr. Raymond. One of the things that we do need to do--and I 
look at them as both challenges and opportunities--one thing 
that I do feel that I have learned from the briefings and the 
reading that I have done is that we can improve our 
communications. I think we can improve our communications 
within the system, within the agency, within the Department and 
within other Federal agencies that have responsibility to 
assure food safety. I think we can also improve our 
communications with our constituents, with the public and with 
industry.
    I am certain that can be done. I know it has to be done. 
One of the responses to any type of infectious disease, 
intentional or unintentional, is rapid communications with the 
public and with the producers, and I know we can do better 
there.
    The other challenge, but also opportunity, I see is to 
improve our laboratory network system. We must have a robust 
laboratory system to promptly find sources of contamination and 
identity them so we can identity and treat them, and the labs 
must be able to communicate with each other. If we have a 
contaminant in California and one in Florida that has the same 
DNA footprint, that might be intentional as opposed to 
accidental. We must be able to do that better than the capacity 
that we currently have.
    The Chairman. The reason I was late getting here was I was 
visiting with the Secretary about an issue of critical 
importance not just to the livestock industry, but to all 
Americans who enjoy beef and other livestock products. That is 
this issue of BSE that has raised its ugly head over the last 
couple of years in this country as well as with some of our 
trading partners.
    Your perspective may not necesarily involve the trade 
implications of BSE. However, that is one of the issues the 
Secretary and I were talking about this morning, but also 
critically important is the food safety side of the issue of 
BSE.
    Would you comment and give us your thoughts on what 
potential there is for harm being done to the domestic food 
chain because of the possibility or even the probability of BSE 
being found in the United States?
    Dr. Raymond. Mr. Chairman, I know that opening the borders 
to trade is one of the Secretary's very top priorities, and 
therefore it will be done of my top priorities. While I won't 
be involved in the actual trade discussions, my job, I think, 
is to assure the Secretary with the best science possible that 
the public's health is protected by the current measures that 
we have instituted in this country to protect against us BSE. I 
do think they are working.
    I do think, if confirmed, the Secretary will look to me to 
be a partner. I do feel, and I think he feels, that the best 
marketing tool for our producers is to guarantee the public a 
safe product.
    [The prepared statement of Dr. Richard Raymond can be found 
in the appendix on page 16.]
    The Chairman. It looks like I am being surrounded by 
Nebraskans this morning. We have been joined by one of your 
finest, and that is my good friend, Senator Ben Nelson.
    Senator Nelson.

   STATEMENT OF HON. BEN NELSON, A U.S. SENATOR FROM NEBRASKA

    Senator Nelson. Well, thank you, Mr. Chairman. I am sorry 
to be late, as well. I had an unavoidable conflict that I had 
previously scheduled, but I would like to say thank you for 
holding this hearing and giving the opportunity to me now to 
make some comments about Dr. Raymond.
    One only needs to review Dr. Pierson's statement last month 
in the Federal Workforce and Agency Subcommittee to understand 
the complexity of the food safety regulatory system. I might 
add also my thanks for Dr. Pierson's service to USDA in his 
role as Acting Under Secretary.
    As Dr. Raymond appears on the scene, I think he inherits 
the world's safest food supply and knows that as time goes by, 
that is what we need to continue to be able to have and protect 
and project for the rest of the world at a time when we end up 
with trade implications with countries unilaterally taking 
exception to our food safety, as in the case of the country 
Japan in working their way through what appears to me to be 
nothing more than a non-tariff trade barrier on our beef today.
    I have got a longer statement, very positive, about Dr. 
Raymond, with all the experience he has had in practicing in 
rural areas, that I would like to put in for the record.
    The Chairman. Without objection.
    [The prepared statement of Senator Ben Nelson can be found 
in the appendix on page 13.]
    Senator Nelson. My question today is as you are looking at 
FSIS--and it is a food safety, food security issue today--do 
you have the necessary tools to be able to do what is required 
to assure the world and the Nation that we will continue to 
have the world's safest food supply?
    Dr. Raymond. Senator Nelson, thank you for that question. I 
do. From the information that I have been able to gain in the 
last couple of weeks since the nomination was announced, I do 
feel the tools are there. I also feel we can make more 
efficient use of those tools and do a better job and expand 
some of what we do by taking a look at how the agency goes 
about its business, if I get that opportunity.
    Senator Nelson. In that regard--and maybe you don't know, 
but I hope that if you don't, you will be able to find out--
what kind of coordination is there between food safety today 
and food security, which could also raise a question about the 
ultimate food safety of American products?
    Dr. Raymond. Senator, I think food safety and food 
protection go hand in hand. I really do feel you can't have one 
without the other. I think whether it is an intentional or 
unintentional contamination of our food supply, in Nebraska 
when we received those very important bioterrorism preparedness 
dollars, what we did was we spent every single dollar to 
improve our public health preparedness for Mother Nature, for 
tornadoes, for West Nile Virus, but also for intentional acts 
of contamination such as small pox and other events. They 
really do go hand in hand.
    Senator Nelson. Is there a system in place or a mechanism 
to keep that coordination, if you know?
    Dr. Raymond. Senator, I do know that the agency has hired 
additional personnel, with their sole responsibility being 
protection of the food supply from intentional contamination, 
and that they have actually established a separate office to 
make sure that everything is being done that can be done to 
protect that.
    Senator Nelson. I think there is a test that is being run 
on one critter that was, I guess, tested initially positive for 
BSE. Do we know the age of that animal?
    Dr. Raymond. Senator, all I know is having read that that 
animal was born before the food bans were put into place.
    Senator Nelson. So we are still, as far as we are aware, OK 
with animals 30 months or under being free of BSE. Is that 
fair?
    Dr. Raymond. Senator, I think it is fair for me to respond 
that I am comfortable with that ruling from what I have read 
and learned and studied, yes.
    Senator Nelson. OK. Thank you very much, Mr. Chairman. 
Thank you, and I look forward to working with you, Dr. Raymond. 
I am sorry that you are leaving Nebraska, but we are glad you 
are here.
    Dr. Raymond. It was a very difficult decision to make 
because of my love for Nebraska, Senator, as I am sure it was 
when you came to work here.
    Senator Nelson. It has been. Thank you.
    The Chairman. We have been joined by Senator Grassley and 
Senator Salazar. Do either of you have any questions or 
comments for this witness?
    Senator Salazar.

  STATEMENT OF HON. KEN SALAZAR, A U.S. SENATOR FROM COLORADO

    Senator Salazar. Just a short comment.
    Dr. Raymond, I very much enjoyed our meeting yesterday. I 
recognize that when we come from the kinds of backgrounds that 
we come from, the rural areas, and have an opportunity to serve 
our country, it is a privilege. It also puts a warm place in 
our hearts to make sure that we are doing everything we can for 
agriculture and for rural communities. I very much look forward 
to working with you on the issues that will confront the 
Department on food safety.
    As we spoke yesterday, one of those issues that I want us 
to work together on is the whole issue of the Canadian border 
and making sure that the road map to opening up that border is 
a road map that is articulated and one that I can fully 
support. So I look forward to working with you upon your 
confirmation, as well as with other members of the USDA on that 
effort.
    Thank you very much.
    The Chairman. Dr. Raymond, thank you very much for being 
here this morning. It is our intention to move this nomination 
through the confirmation process as quickly as possible. In 
that regard, the record will be open for 5 days to give any 
other member of the committee the opportunity to submit 
questions to you. And if that should happen, we would ask that 
you please respond to those very promptly so that we can 
continue this nomination through the process.
    Thank you very much, Dr. Raymond, for your willingness to 
serve and we will look forward to staying in touch.
    Dr. Raymond. Thank you, Mr. Chairman.
    [The biographical information of Dr. Raymond can be found 
in the appendix on page 20.]
    [Whereupon, at 10:35 a.m., the committee was adjourned.]
      
=======================================================================


                            A P P E N D I X

                             June 22, 2005



      
=======================================================================

[GRAPHIC] [TIFF OMITTED] T2717.001

[GRAPHIC] [TIFF OMITTED] T2717.002

[GRAPHIC] [TIFF OMITTED] T2717.003

[GRAPHIC] [TIFF OMITTED] T2717.004

[GRAPHIC] [TIFF OMITTED] T2717.005

[GRAPHIC] [TIFF OMITTED] T2717.006

[GRAPHIC] [TIFF OMITTED] T2717.007

      
=======================================================================


                   DOCUMENTS SUBMITTED FOR THE RECORD

                             June 22, 2005



      
=======================================================================

[GRAPHIC] [TIFF OMITTED] T2717.008

[GRAPHIC] [TIFF OMITTED] T2717.009

[GRAPHIC] [TIFF OMITTED] T2717.010

[GRAPHIC] [TIFF OMITTED] T2717.011

[GRAPHIC] [TIFF OMITTED] T2717.012

[GRAPHIC] [TIFF OMITTED] T2717.013

[GRAPHIC] [TIFF OMITTED] T2717.014

[GRAPHIC] [TIFF OMITTED] T2717.015

[GRAPHIC] [TIFF OMITTED] T2717.016

[GRAPHIC] [TIFF OMITTED] T2717.017

[GRAPHIC] [TIFF OMITTED] T2717.018

[GRAPHIC] [TIFF OMITTED] T2717.019

[GRAPHIC] [TIFF OMITTED] T2717.020

[GRAPHIC] [TIFF OMITTED] T2717.021

[GRAPHIC] [TIFF OMITTED] T2717.022

[GRAPHIC] [TIFF OMITTED] T2717.023

[GRAPHIC] [TIFF OMITTED] T2717.024

[GRAPHIC] [TIFF OMITTED] T2717.025

[GRAPHIC] [TIFF OMITTED] T2717.026

[GRAPHIC] [TIFF OMITTED] T2717.027

[GRAPHIC] [TIFF OMITTED] T2717.028

[GRAPHIC] [TIFF OMITTED] T2717.029

[GRAPHIC] [TIFF OMITTED] T2717.030

[GRAPHIC] [TIFF OMITTED] T2717.031

[GRAPHIC] [TIFF OMITTED] T2717.032

[GRAPHIC] [TIFF OMITTED] T2717.033

[GRAPHIC] [TIFF OMITTED] T2717.034

[GRAPHIC] [TIFF OMITTED] T2717.035

      
=======================================================================


                         QUESTIONS AND ANSWERS

                             June 22, 2005



      
=======================================================================

[GRAPHIC] [TIFF OMITTED] T2717.036

[GRAPHIC] [TIFF OMITTED] T2717.037

[GRAPHIC] [TIFF OMITTED] T2717.038

[GRAPHIC] [TIFF OMITTED] T2717.039

[GRAPHIC] [TIFF OMITTED] T2717.040

[GRAPHIC] [TIFF OMITTED] T2717.041

[GRAPHIC] [TIFF OMITTED] T2717.042

[GRAPHIC] [TIFF OMITTED] T2717.043

[GRAPHIC] [TIFF OMITTED] T2717.044

[GRAPHIC] [TIFF OMITTED] T2717.045

[GRAPHIC] [TIFF OMITTED] T2717.046

[GRAPHIC] [TIFF OMITTED] T2717.047

[GRAPHIC] [TIFF OMITTED] T2717.048

[GRAPHIC] [TIFF OMITTED] T2717.049

[GRAPHIC] [TIFF OMITTED] T2717.050

[GRAPHIC] [TIFF OMITTED] T2717.051

[GRAPHIC] [TIFF OMITTED] T2717.052

[GRAPHIC] [TIFF OMITTED] T2717.053

[GRAPHIC] [TIFF OMITTED] T2717.054

[GRAPHIC] [TIFF OMITTED] T2717.055

[GRAPHIC] [TIFF OMITTED] T2717.056

[GRAPHIC] [TIFF OMITTED] T2717.057

[GRAPHIC] [TIFF OMITTED] T2717.058

[GRAPHIC] [TIFF OMITTED] T2717.059

[GRAPHIC] [TIFF OMITTED] T2717.060

[GRAPHIC] [TIFF OMITTED] T2717.061

[GRAPHIC] [TIFF OMITTED] T2717.062

[GRAPHIC] [TIFF OMITTED] T2717.063

[GRAPHIC] [TIFF OMITTED] T2717.064

[GRAPHIC] [TIFF OMITTED] T2717.065

[GRAPHIC] [TIFF OMITTED] T2717.066

[GRAPHIC] [TIFF OMITTED] T2717.067

[GRAPHIC] [TIFF OMITTED] T2717.068

[GRAPHIC] [TIFF OMITTED] T2717.069

[GRAPHIC] [TIFF OMITTED] T2717.070

[GRAPHIC] [TIFF OMITTED] T2717.071

[GRAPHIC] [TIFF OMITTED] T2717.072

[GRAPHIC] [TIFF OMITTED] T2717.073

                                 <all>

</pre></body></html>
