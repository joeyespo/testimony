<html>
<title> - THE RISE OF DRUG-RELATED VIOLENT CRIME IN RURAL AMERICA: FINDING SOLUTIONS TO A GROWING PROBLEM</title>
<body><pre>
[Senate Hearing 110-357, Part err]
[From the U.S. Government Printing Office]


                                                        S. Hrg. 110-357
 
                                [ERRATA] 
   THE RISE OF DRUG-RELATED VIOLENT CRIME IN RURAL AMERICA: FINDING 
                     SOLUTIONS TO A GROWING PROBLEM 

=======================================================================

                                HEARING

                               before the

                       COMMITTEE ON THE JUDICIARY
                          UNITED STATES SENATE

                       ONE HUNDRED TENTH CONGRESS

                             SECOND SESSION

                               __________

                             MARCH 24, 2008

                               __________

                            Rutland, Vermont

                               __________

                          Serial No. J-110-81

                               __________

         Printed for the use of the Committee on the Judiciary


                       U.S. GOVERNMENT PRINTING OFFICE 

42-608 PDF                       WASHINGTON : 2008 

For sale by the Superintendent of Documents, U.S. Government Printing 
Office Internet: bookstore.gpo.gov Phone: toll free (866) 512-1800; 
DC area (202) 512-1800 Fax: (202) 512-2104 Mail: Stop IDCC, 
Washington, DC 20402-0001 


                                  

    On the cover page change FIRST SESSION to read SECOND 
SESSION.

</pre></body></html>
