<html>
<title> - NOMINATIONS OF ERIC WASHINGTON, STEPHEN GLICKMAN, AND HIRAM PUIG-LUGO</title>
<body><pre>
[Senate Hearing 106-136]
[From the U.S. Government Printing Office]


                                                        S. Hrg. 106-136


 
 NOMINATIONS OF ERIC WASHINGTON, STEPHEN GLICKMAN, AND HIRAM PUIG-LUGO

=======================================================================


                                HEARING

                               BEFORE THE

                              COMMITTEE ON
                          GOVERNMENTAL AFFAIRS
                          UNITED STATES SENATE

                       ONE HUNDRED SIXTH CONGRESS

                             FIRST SESSION

                                 ON THE

 NOMINATIONS OF ERIC WASHINGTON AND STEPHEN GLICKMAN, TO BE ASSOCIATE 
  JUDGES OF THE DISTRICT OF COLUMBIA COURT OF APPEALS, AND HIRAM PUIG-
 LUGO, TO BE ASSOCIATE JUDGE OF THE DISTRICT OF COLUMBIA SUPERIOR COURT

                               __________

                             APRIL 20, 1999

                               __________

      Printed for the use of the Committee on Governmental Affairs


                                <snowflake>


                      U.S. GOVERNMENT PRINTING OFFICE
 57-781 cc                   WASHINGTON : 1999
_______________________________________________________________________
For sale by the Superintendent of Documents, Congressional Sales Office
         U.S. Government Printing Office, Washington, DC 20402



                   COMMITTEE ON GOVERNMENTAL AFFAIRS

                   FRED THOMPSON, Tennessee, Chairman
WILLIAM V. ROTH, Jr., Delaware       JOSEPH I. LIEBERMAN, Connecticut
TED STEVENS, Alaska                  CARL LEVIN, Michigan
SUSAN M. COLLINS, Maine              DANIEL K. AKAKA, Hawaii
GEORGE V. VOINOVICH, Ohio            RICHARD J. DURBIN, Illinois
PETE V. DOMENICI, New Mexico         ROBERT G. TORRICELLI, New Jersey
THAD COCHRAN, Mississippi            MAX CLELAND, Georgia
ARLEN SPECTER, Pennsylvania          JOHN EDWARDS, North Carolina
JUDD GREGG, New Hampshire
             Hannah S. Sistare, Staff Director and Counsel
                       Johanna L. Hardy, Counsel
      Joyce A. Rechtschaffen, Minority Staff Director and Counsel
          Peter A. Ludgin, Minority Professional Staff Member
                 Darla D. Cassell, Administrative Clerk



                            C O N T E N T S

                                 ------                                
Opening statements:
                                                                   Page
    Senator Voinovich............................................     1

                               WITNESSES
                        Tuesday, April 20, 1999

Representative Eleanor Holmes Norton, from the District of 
  Columbia.......................................................     2
Eric Washington, to be Associate Judge of the District of 
  Columbia Court of Appeals......................................     3
Stephen Glickman, to be Associate Judge of the District of 
  Columbia Court of Appeals......................................     3
Hiram Puig-Lugo, to be Associate Judge of the District of 
  Columbia Superior Court........................................     4

                     Alphabetical List of Witnesses

Glickman, Stephen:
    Testimony....................................................     3
    Biographical and professional information....................    28
Holmes, Rep. Eleanor Holmes:
    Testimony....................................................     2
Puig-Lugo, Hiram:
    Testimony....................................................     4
    Biographical and professional information....................    60
Washington, Eric:
    Testimony....................................................     3
    Biographical and professional information....................     7

                                APPENDIX

Senator Paul Strauss, Shadow U.S. Senator Elected by the Voters 
  of the District of Columbia, prepared statement................     5



 NOMINATIONS OF ERIC WASHINGTON, STEPHEN GLICKMAN, AND HIRAM PUIG-LUGO

                              ----------                              


                        TUESDAY, APRIL 20, 1999

                                       U.S. Senate,
                         Committee on Governmental Affairs,
                                                    Washington, DC.
    The Committee met, pursuant to notice, at 10:30 a.m., in 
room SD-342, Dirksen Senate Office Building, Hon. George V. 
Voinovich, Chairman, Subcommittee on Oversight of Government 
Management, Restructuring and the District of Columbia, 
presiding.
    Present: Senators Voinovich and Durbin.

             OPENING STATEMENT OF SENATOR VOINOVICH

    Senator Voinovich. The hearing will come to order. I would 
like to welcome everyone here this morning, especially our 
nominees, D.C. Superior Court Judge Eric Washington, Mr. 
Stephen Glickman, who have been nominated to serve as Associate 
Judges for the District of Columbia Court of Appeals, and Mr. 
Hiram Puig-Lugo--do I have that correct?
    Mr. Puig-Lugo. Yes, sir, you do.
    Senator Voinovich. That is like Voinovich. In fact, my 
granddaughter just turned two and I was finding out whether or 
not she can pronounce her last name. She still cannot. 
[Laughter.]
    Mr. Puig-Lugo has been nominated to serve as an Associate 
Judge for the District of Columbia Superior Court.
    Let me state for the record that all of our nominees have 
been subjected to a very thorough screening process. They were 
all recommended by the District's Judicial Nomination 
Committee, subjected to FBI background investigations, and 
subsequently nominated by the President of the United States.
    Since the nominations were received, the Committee staff 
has also conducted separate background checks and interviews 
with each of the nominees. I thought maybe one other member of 
the Committee would be here, but maybe they will come in a 
little later.
    We are pleased to have with us today District of Columbia 
Representative Eleanor Holmes Norton. Eleanor and I have known 
each other a long time. We still cannot figure out when we 
first met, Eleanor, but it was a good occasion. She has done an 
outstanding job of representing the District and I am pleased 
that I am going to have an opportunity to work with you as part 
of my responsibilities as Subcommittee Chairman here. Eleanor 
has been gracious to come and introduce the candidates, and 
Eleanor, we are pleased to have you here.

  TESTIMONY OF ELEANOR HOLMES NORTON, REPRESENTATIVE FROM THE 
                      DISTRICT OF COLUMBIA

    Ms. Norton. Thank you, Senator. If I may say so, Mr. 
Chairman, we are very pleased to see that this Committee is 
chaired by a very distinguished former mayor of the City of 
Cleveland.
    I am particularly pleased this morning to introduce three 
Washingtonians who have had outstanding careers and who have 
rendered distinguished service to the law and to their 
community, two for the city's Court of Appeals and one for the 
D.C. Superior Court.
    Judge Eric Washington is now an Associate Judge on the D.C. 
Superior Court. Before becoming Judge, Judge Washington served 
as the principal Deputy D.C. Corporation Counsel, which is the 
position just below the Corporation Counsel for the District of 
Columbia, the city's chief legal officer. He came there from 
Hogan and Hartson, where he was a partner. He was earlier 
associated with Fulbright and Jaworski. Judge Washington was 
educated at Tufts and Columbia Law School. His community 
activities have included work with the Boys and Girls Club. He 
and his wife have three children.
    Stephen Glickman is the managing partner in the law firm 
here of Zuckerman, Spaeder, Goldstein, Taylor and Kolker. His 
career includes 4 years of service on the staff of the D.C. 
Public Defenders Service. He was also before that a staff 
attorney with the Federal Trade Commission. Mr. Glickman is a 
Phi Beta Kappa graduate of Cornell University and a 1973 
graduate of Yale Law School. After graduating from law school, 
he clerked on the Connecticut Supreme Court. His service to the 
community has included board of directors of the Neighborhood 
Legal Services Corporation. He and his wife have two children. 
He is being nominated to the Court of Appeals, as well.
    Nominated for the D.C. Superior Court is Hiram E. Puig-
Lugo. He is a trial attorney now with the Civil Rights 
Division, Criminal Section, of the Justice Department. Mr. 
Puig-Lugo has been before that Deputy Chief of the Trial 
Division of the Public Defenders Service of this city, working 
himself to that position from a trial attorney. Mr. Puig-Lugo 
is a graduate of the University of Wisconsin and in 1988 of its 
law school. He has served as an instructor for judicial reform 
in a project in El Salvador. He is co-editor of a D.C. practice 
manual. His community activities have included the board of 
directors of the Hispanic Bar Association. He has one son.
    It is my very distinct pleasure to recommend these three 
nominees to you, Mr. Chairman.
    Senator Voinovich. Thank you very much.
    If the nominees could please stand and if you will raise 
your right hand, I would like to swear you in. Do you solemnly 
swear or affirm that the testimony you will give to the 
Committee today will be the truth, the whole truth, and nothing 
but the truth, so help you, God?
    Mr. Glickman. I do.
    Judge Washington. I do.
    Mr. Puig-Lugo. I do.
    Senator Voinovich. Please be seated.
    Welcome, Judge Washington. We are pleased to have you here 
today. I understand that you are accompanied by members of your 
family. Would you like to introduce them?

TESTIMONY OF HON. ERIC WASHINGTON,\1\ TO BE ASSOCIATE JUDGE OF 
           THE DISTRICT OF COLUMBIA COURT OF APPEALS

    Judge Washington. I would. Thank you, Mr. Chairman. With me 
today is my wife, Sheryl, and my daughter, Erica, who is 
representing the children in my family. Out of deference to 
this Committee and my fellow nominees, I thought my 2-year-old 
son probably should best not come today and my older daughter 
had obligations in school. But Erica is here and I am very 
pleased to have her here with my wife.
---------------------------------------------------------------------------
    \1\ The biographical information and questionnaire of Judge 
Washington appears in the Appendix on page 7.
---------------------------------------------------------------------------
    Senator Voinovich. We are very happy to have you here 
today. If you would like to make a statement, we would 
appreciate it.
    Judge Washington. With respect to the Committee, I wanted 
to thank you, Mr. Chairman, for expeditiously scheduling these 
hearings. I hear quite often from the judges on the Court of 
Appeals how anxious they are to have new persons up there to 
help them. They have been shorthanded. I want to thank your 
staff especially. They have been very gracious and very 
professional and have moved this process along and have been 
very forthcoming and helpful in every respect. So I want to 
thank the Committee and the Committee staff for this 
opportunity.
    I do want to say that I am humbled by the opportunity to 
serve as an Associate Judge of the D.C. Court of Appeals. It is 
a position that comes with enormous responsibilities and I am 
looking forward to having an opportunity to work with my fellow 
judges on that court and to hopefully make a difference in the 
city. Thank you.
    Senator Voinovich. Thank you. I appreciate the nice words 
about the staff. I am learning as a new Senator that you are 
only as good as the staff and they have been very, very 
conscientious about moving us to this hearing today.
    Mr. Glickman, we are glad to have you here today with us. 
If you would like to introduce anyone from your family, we 
would appreciate it.

TESTIMONY OF STEPHEN GLICKMAN,\2\ TO BE ASSOCIATE JUDGE OF THE 
             DISTRICT OF COLUMBIA COURT OF APPEALS

    Mr. Glickman. Thank you. I would like to introduce my wife, 
Ann Glickman, who has come here as a representative of our 
family. I guess my kids are still in school.
---------------------------------------------------------------------------
    \2\ The biographical information and questionnaire of Judge 
Glickman appears in the Appendix on page 28.
---------------------------------------------------------------------------
    Thank you for the courtesies that you and the staff have 
shown me. I would like to second, if I may, my colleague's 
remarks. The staff and yourself, Senator, have just been very 
helpful to us in every regard and we are very appreciative.
    Like Judge Washington, I, too, am thrilled and honored and 
humbled by the opportunity that awaits me and I am looking 
forward to it.
    Senator Voinovich. Thank you.
    Mr. Puig-Lugo, we are glad to have you here. If you would 
like to make an opening statement or introduce anyone, we would 
welcome it.

 TESTIMONY OF HIRAM PUIG-LUGO,\1\ TO BE ASSOCIATE JUDGE OF THE 
              DISTRICT OF COLUMBIA SUPERIOR COURT

    Mr. Puig-Lugo. Thank you. Yes, sir. Good morning. It is an 
honor for me to appear before you as a nominee to the Superior 
Court of the District of Columbia. It was through the cases 
that I handled in her courtrooms that I came to know, 
appreciate, and value our community. I am excited and humbled 
by the opportunity to join Superior Court in her mission of 
serving the District of Columbia and her residents.
---------------------------------------------------------------------------
    \1\ The biographical information and questionnaire of Judge Puig-
Lugo appears in the Appendix on page 60.
---------------------------------------------------------------------------
    There are several people whom I would like to acknowledge 
in the hearing room today who have been friends, supporters, 
and mentors over the past few years. One of them is the Hon. 
Ricardo Urbina of the United States District Court for the 
District of Columbia, the Hon. Richard Roberts from the United 
States District Court for the District of Columbia, Chief Judge 
Eugene Hamilton from the Superior Court of the District of 
Columbia, and Mari Carmen Aponte, formerly of the District of 
Columbia Judicial Nomination Commission.
    I would like to echo the comments of Judge Washington and 
Mr. Glickman thanking you, Senator, and the Committee staff for 
your assistance over the last few months.
    Senator Voinovich. Thank you very much.
    One of the things we have all learned in life is that, so 
often, our success in life depends on people who have taken an 
interest in this and have encouraged us and it is nice that you 
have your mentors with you today.
    Mr. Puig-Lugo. Thank you.
    Senator Voinovich. As I mentioned to you, there are three 
questions that I would like to ask of each of you. They are 
required under the statute.
    Judge Washington, I will start with you. First of all, is 
there anything which you are aware of in your background which 
might present a conflict of interest with the duties of the 
office to which you have been nominated?
    Judge Washington. No, Mr. Chairman.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Judge Washington. No, Mr. Chairman.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from serving the 
full term for the office to which you have been nominated?
    Judge Washington. None that I am aware of, Mr. Chairman.
    Senator Voinovich. Mr. Glickman, is there anything of which 
you are aware of in your background which might present a 
conflict of interest with the duties of the office to which you 
have been nominated?
    Mr. Glickman. No, Mr. Chairman.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Mr. Glickman. No, I do not, Mr. Chairman.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from serving the 
full term for the office to which you have been nominated?
    Mr. Glickman. No, I do not, Mr. Chairman.
    Senator Voinovich. Mr. Puig-Lugo, is there anything which 
you are aware of in your background which might present a 
conflict of interest with the duties of the office to which you 
have been nominated?
    Mr. Puig-Lugo. No, Mr. Chairman.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Mr. Puig-Lugo. No, Mr. Chairman.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from serving the 
full term for the office to which you have been nominated?
    Mr. Puig-Lugo. No, sir, I do not.
    Senator Voinovich. I would like to bring to your attention 
that the Committee has received letters of support of the 
nominees, and without objection, they will be placed in the 
record.
    [The prepared statement of Mr. Strauss follows:]

PREPARED STATEMENT OF SENATOR PAUL STRAUSS, SHADOW U.S. SENATOR ELECTED 
               BY THE VOTERS OF THE DISTRICT OF COLUMBIA
    Chairman Voinovich, and Members of the Committee on Governmental 
Affairs, I am Paul Strauss, the U.S. Senator elected by the voters of 
the District of Columbia, a position sometimes referred to as the 
Shadow Senator. I am also an attorney who practices in our local 
courts.
    In each of those capacities, I appreciate the opportunity to 
provide this statement on behalf of my constituents in the District of 
Columbia. I am present here today to wholeheartedly express my support 
of Judge Erik T. Washington, nominated for Associate Judge of the 
District of Columbia.
    I have known Judge Washington since before he was a judge, when we 
both actively worked together with a community organization in the 
District of Columbia. I have had the privilege to appear before Judge 
Washington as an attorney and have come to know him on the bench, as 
well as being a personal friend.Despite that friendship, he never 
showed me any bias or favor and has always demonstrated the highest 
levels of integrity, both on and off the bench. He even chastised me 
once, when I properly deserved it.
    He has been a lawyer with the District's Corporation Counsel, and 
has also worked in private practice with much success. He is respected 
by his colleagues, the members of the bar and the Washington legal 
community in general.
    As this Committee should know from my involvement with past 
nominations, I am not hesitant to call attention to deficiencies of a 
nominee. When concerns about Patricia Broderick arose at the end of the 
last session, I asked the voting members whose interest in this process 
were not as direct as mine to vote on my behalf. Were I seated with the 
full rights and privileges of a U.S. Senator, I would vote to confirm 
Judge Washington without hesitation. Today I ask you to vote yes for 
me, and let this Honorable Judge take his place on my jurisdiction's 
highest court.

    Senator Voinovich. For the purposes of the people that are 
here today, I want you to know that this very short hearing 
that we are having today was preceded by an enormous amount of 
work and time and effort on the part of the nominees and other 
people and I personally have reviewed your resumes and your 
records and the letters of recommendation in your files and 
have visited with representatives of the President and also the 
Justice Department. Just so everyone knows, they have been 
through the gauntlet and I am sure that they have been 
anticipating and looking forward to this day.
    I do not believe there is anything more to come before the 
Committee this morning. I again would like to let our nominees 
know that we will be moving as quickly as we can to a markup 
hearing sometime early in May and, hopefully, by the 15th or 
so, you can go to work. I know that your future colleagues are 
looking forward to your service in the respective courts to 
which you have been nominated. Congratulations to you.
    Judge Washington. Thank you.
    Mr. Glickman. Thank you.
    Mr. Puig-Lugo. Thank you, Mr. Chairman.
    Senator Voinovich. The record will remain open for 5 days 
after the conclusion of the hearing.
    The meeting is adjourned.
    [Whereupon, at 10:45 a.m., the Subcommittee was adjourned.]


                            A P P E N D I X

                              ----------                              


[GRAPHIC] [TIFF OMITTED] T7781.001

[GRAPHIC] [TIFF OMITTED] T7781.002

[GRAPHIC] [TIFF OMITTED] T7781.003

[GRAPHIC] [TIFF OMITTED] T7781.004

[GRAPHIC] [TIFF OMITTED] T7781.005

[GRAPHIC] [TIFF OMITTED] T7781.006

[GRAPHIC] [TIFF OMITTED] T7781.007

[GRAPHIC] [TIFF OMITTED] T7781.008

[GRAPHIC] [TIFF OMITTED] T7781.009

[GRAPHIC] [TIFF OMITTED] T7781.010

[GRAPHIC] [TIFF OMITTED] T7781.011

[GRAPHIC] [TIFF OMITTED] T7781.012

[GRAPHIC] [TIFF OMITTED] T7781.013

[GRAPHIC] [TIFF OMITTED] T7781.014

[GRAPHIC] [TIFF OMITTED] T7781.015

[GRAPHIC] [TIFF OMITTED] T7781.016

[GRAPHIC] [TIFF OMITTED] T7781.017

[GRAPHIC] [TIFF OMITTED] T7781.018

[GRAPHIC] [TIFF OMITTED] T7781.019

[GRAPHIC] [TIFF OMITTED] T7781.020

[GRAPHIC] [TIFF OMITTED] T7781.021

[GRAPHIC] [TIFF OMITTED] T7781.022

[GRAPHIC] [TIFF OMITTED] T7781.023

[GRAPHIC] [TIFF OMITTED] T7781.024

[GRAPHIC] [TIFF OMITTED] T7781.025

[GRAPHIC] [TIFF OMITTED] T7781.026

[GRAPHIC] [TIFF OMITTED] T7781.027

[GRAPHIC] [TIFF OMITTED] T7781.028

[GRAPHIC] [TIFF OMITTED] T7781.029

[GRAPHIC] [TIFF OMITTED] T7781.030

[GRAPHIC] [TIFF OMITTED] T7781.031

[GRAPHIC] [TIFF OMITTED] T7781.032

[GRAPHIC] [TIFF OMITTED] T7781.033

[GRAPHIC] [TIFF OMITTED] T7781.034

[GRAPHIC] [TIFF OMITTED] T7781.035

[GRAPHIC] [TIFF OMITTED] T7781.036

[GRAPHIC] [TIFF OMITTED] T7781.037

[GRAPHIC] [TIFF OMITTED] T7781.038

[GRAPHIC] [TIFF OMITTED] T7781.039

[GRAPHIC] [TIFF OMITTED] T7781.040

[GRAPHIC] [TIFF OMITTED] T7781.041

[GRAPHIC] [TIFF OMITTED] T7781.042

[GRAPHIC] [TIFF OMITTED] T7781.043

[GRAPHIC] [TIFF OMITTED] T7781.044

[GRAPHIC] [TIFF OMITTED] T7781.045

[GRAPHIC] [TIFF OMITTED] T7781.046

[GRAPHIC] [TIFF OMITTED] T7781.047

[GRAPHIC] [TIFF OMITTED] T7781.048

[GRAPHIC] [TIFF OMITTED] T7781.049

[GRAPHIC] [TIFF OMITTED] T7781.050

[GRAPHIC] [TIFF OMITTED] T7781.051

[GRAPHIC] [TIFF OMITTED] T7781.052

[GRAPHIC] [TIFF OMITTED] T7781.053

[GRAPHIC] [TIFF OMITTED] T7781.054

[GRAPHIC] [TIFF OMITTED] T7781.055

[GRAPHIC] [TIFF OMITTED] T7781.056

[GRAPHIC] [TIFF OMITTED] T7781.057

[GRAPHIC] [TIFF OMITTED] T7781.058

[GRAPHIC] [TIFF OMITTED] T7781.059

[GRAPHIC] [TIFF OMITTED] T7781.060

[GRAPHIC] [TIFF OMITTED] T7781.061

[GRAPHIC] [TIFF OMITTED] T7781.062

[GRAPHIC] [TIFF OMITTED] T7781.063

[GRAPHIC] [TIFF OMITTED] T7781.064

[GRAPHIC] [TIFF OMITTED] T7781.065

[GRAPHIC] [TIFF OMITTED] T7781.066

[GRAPHIC] [TIFF OMITTED] T7781.067

[GRAPHIC] [TIFF OMITTED] T7781.068

[GRAPHIC] [TIFF OMITTED] T7781.069

[GRAPHIC] [TIFF OMITTED] T7781.070

[GRAPHIC] [TIFF OMITTED] T7781.071

[GRAPHIC] [TIFF OMITTED] T7781.072

[GRAPHIC] [TIFF OMITTED] T7781.073

[GRAPHIC] [TIFF OMITTED] T7781.074

[GRAPHIC] [TIFF OMITTED] T7781.075

[GRAPHIC] [TIFF OMITTED] T7781.076

[GRAPHIC] [TIFF OMITTED] T7781.077

[GRAPHIC] [TIFF OMITTED] T7781.078

[GRAPHIC] [TIFF OMITTED] T7781.079

[GRAPHIC] [TIFF OMITTED] T7781.080


                                  <all>

</pre></body></html>
