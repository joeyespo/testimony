<html>
<title> - LEGISLATIVE BRANCH APPROPRIATIONS FOR 2013</title>
<body><pre>
[House Hearing, 112 Congress]
[From the U.S. Government Printing Office]


 
                         LEGISLATIVE BRANCH APPROPRIATIONS 
                                    FOR 2013 
=======================================================================                              



                                  HEARINGS

                                  BEFORE A

                          COMMITTEE ON APPROPRIATIONS

                            HOUSE OF REPRESENTATIVES

                          ONE HUNDRED AND TWELFTH CONGRESS

                                 SECOND SESSION

                                    ------

                       SUBCOMMITTEE ON LEGISLATIVE BRANCH

                       ANDER CRENSHAW, Florida, Chairman
STEVEN C. LATOURETTE, Ohio              MICHAEL M. HONDA, California
JO ANN EMERSON, Missouri                DAVID E. PRICE, North Carolina
DENNY REHBERG, Montana                  SANFORD D. BISHOP, Jr., Georgia
KEN CALVERT, California                

NOTE: Under Committee Rules, Mr. Rogers, as Chairman of the Full 
  Committee, and Mr. Dicks, as Ranking Minority Member of the Full 
  Committee, are authorized to sit as Members of all Subcommittees.

                         ELISABETH C. DAWSON, Clerk
                      JENNIFER KISIAH, Professional Staff
                         CHUCK TURNER, Professional Staff

                                     -------

                                      PART 2

                       FISCAL YEAR 2013 LEGISLATIVE BRANCH
 
                              APPROPRIATIONS REQUESTS

                                      ------

               Printed for the use of Committee on Appropriations


[TEXT NOT AVAILABLE REFER TO PDF]


</pre></body></html>
