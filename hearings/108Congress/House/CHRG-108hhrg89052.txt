<html>
<title> - ORGANIZATIONAL MEETING</title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]



                     ORGANIZATIONAL MEETING OF THE
                      JOINT COMMITTEE ON PRINTING

=======================================================================

                         ORGANIZATIONAL MEETING

                               before the

                      JOINT COMMITTEE ON PRINTING
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED EIGHTH CONGRESS

                             FIRST SESSION

                               __________

             HEARING HELD IN WASHINGTON, DC, APRIL 9, 2003

                               __________

      Printed for the use of the Committee on House Administration



89-052              U.S. GOVERNMENT PRINTING OFFICE
                            WASHINGTON : 2003
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpr.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092250 Mail: Stop SSOP, Washington, DC 20402�090001


                      JOINT COMMITTEE ON PRINTING

               HOUSE                               SENATE

ROBERT W. NEY, Ohio, Chairman        SAXBY CHAMBLISS, Georgia,
                                     Vice Chairman
JOHN T. DOOLITTLE, California        MARK DAYTON, Minnesota,
                                     Ranking Member
JOHN LINDER, Georgia                 DANIEL K. INOUYE, Hawaii
JOHN B. LARSON, Connecticut          THAD COCHRAN, Mississippi
ROBERT A. BRADY, Pennsylvania        GORDON SMITH, Oregon


 
                         ORGANIZATIONAL MEETING

                        WEDNESDAY, APRIL 9, 2003

                  House of Representatives,
                                       U.S. Senate,
                               Joint Committee on Printing,
                                                    Washington, DC.
    The joint committee met, pursuant to call, at 4 p.m., in 
Room H-139, the Capitol, Hon. Robert W. Ney [chairman of the 
joint committee] presiding.
    Representatives Present: Representatives Ney, Linder, 
Larson, and Brady.
    Senators Present: Senators Chambliss, Smith, and Dayton.
    Mr. Ney. The committee will come to order. I want to thank 
everybody in advance for their flexibility and cooperation in 
planning and preparing for this meeting.
    Today the Joint Committee on Printing is holding its first 
meeting of the 108th Congress to welcome all members to the 
committee, to announce the chairman, vice chairman and ranking 
member, and finally to adopt our committee rules for the 108th 
Congress.
    As the chairman, pursuant to House Resolution 134 and 
Senate Resolution 84, I would like to welcome all of the 
distinguished members of the committee for this Congress: 
Senator Saxby Chambliss of Georgia, Senator Cochran of 
Mississippi, Senator Gordon Smith is here of Oregon, Senator 
Dayton is coming from Minnesota, Senator Inouye of Hawaii, 
Congressman Doolittle of California, Congressman Linder of 
Georgia; our ranking member, Congressman Larson of Connecticut, 
and Congressman Brady is here from Pennsylvania. Many of us 
have already had an opportunity to discuss our expectations for 
what I think will be a very productive Congress. I think we all 
look forward to working together on a lot of important issues.
    I am also very encouraged by the enthusiasm of our new 
public printer, Bruce James, with whom I have had the 
opportunity to meet with personally and discuss many of the 
issues facing him as he works with us to bring necessary 
improvements that I think we have waited quite a while for at 
that agency.
    During the 108th Congress, there are going to be many 
issues for the joint committee to review, including the 
constitutional challenges of Title 44. Currently, the General 
Accounting Office is studying this matter. I look forward to 
reviewing their findings. Also I have confidence in Mr. James 
to continue conversations with Mr. Mitch Daniels, Director of 
the Office of Management and Budget, working to reach an 
agreement regarding this issue.
    Furthermore, I would like to thank Senator Dayton and his 
staff for all their hard work in the last Congress on the 
proposed revisions to the government printing and binding 
regulations issued by this joint committee. Working with the 
Interagency Council on Printing and Publications Services, the 
proposed revisions include updates to reflect the technological 
advancements in printing that have occurred over the last 10 
years. Also changing the regulations to guidelines meets the 
needs of the agencies by providing useful guidance in their 
printing efforts while addressing the constitutional issues. I 
will continue to work with all of you to monitor these issues 
as they surface throughout this Congress.
    The Joint Committee on Printing was statutorily established 
in 1846. The principal purpose of the joint committee is to 
oversee the functions of the Government Printing Office and 
general printing procedures of the Federal Government. Senator 
Chambliss is now here. Welcome.
    The authority vested in the Joint Committee on Printing is 
derived from Title 44 of the U.S. Code, and the committee is 
hereby responsible for ensuring compliance by Federal entities 
to these laws and the government printing and binding 
regulations.
    The distinguished Chair of the Senate Rules and 
Administration Committee, Senator Lott, has designated another 
member of the Rules Committee to serve in his place as member 
and vice chairman pursuant to S. Con. Res. 20. With passage of 
this concurrent resolution, it is my pleasure to announce 
Senator Chambliss as the new Vice Chair of the Joint Committee 
on Printing. We all wanted to wait on you, but Mr. Linder 
overrode us to begin without you. He did it to you in the House 
and he is doing it to you now that you are in the Senate.
    In addition, when both bodies are controlled by the same 
party, the senior member of the minority party named to the 
Joint Committee on Printing in the House of Congress opposite 
that of the chair shall be the ranking minority member of the 
joint committee. Therefore, I would like to announce Senator 
Dayton as the ranking minority member of the Joint Committee on 
Printing.
    Finally, I would like to let the members know the Office of 
Photography is here to take a photograph of the members who are 
present after the meeting adjourns. I want to thank you and 
welcome you to the JCP.
    I will recognize the Vice Chair for any remarks he may 
have.
    Senator Chambliss. I just asked my scout if she had my long 
opening statement. I don't have any. I am pleased to have my 
good friend Gordon Smith here with me. We look forward to 
working with you, Bob.
    Senator Smith. I want to know what Saxby is going to do 
with all this power, though.
    Mr. Ney. We will have you take it national.
    At this time, I would like to recognize the distinguished 
ranking member, Senator Dayton, but he has not arrived yet.
    We would like to adopt the committee rules. The Chair lays 
before the committee a resolution adopting the committee rules 
for the Joint Committee on Printing for the 108th Congress. I 
do want to thank our vice chairman and ranking member and their 
staff for working on these issues. No changes were made, I 
would note, to the committee rules for this Congress. I believe 
there is agreement. Is there any discussion?
    Senator Chambliss. Mr. Chairman, I move that the resolution 
adopting the committee rules for the 108th Congress be 
approved.
    Mr. Ney. The question is on the motion. All those in favor 
say aye. Those opposed will say nay. The ayes have it. The 
motion is agreed to and the committee rules are approved.
    Does anyone else care to make any statements?
    Senator Smith. Here is our ranking member.
    Mr. Larson. I would defer to Senator Dayton.
    Mr. Ney. Senator Dayton, would you like to say anything?
    Senator Dayton. I am sorry to be late, Mr. Chairman.
    Mr. Ney. I would like to praise you for the record for your 
great work.
    Senator Dayton. It took me two months to get a quorum, and 
you have got one in less than 10 minutes. Impressive.
    Mr. Ney. I was offering free steak dinners.
    Senator Dayton. Obviously, this is in much better hands in 
2 years than before. Thanks.
    Mr. Ney. Anyone else?
    Mr. Larson. Mr. Chairman, I would only add that Mr. Brady 
and myself are looking forward to working with you and the 
entire committee. Hailing from the State of Connecticut where 
the general assembly meets in joint committee all the time 
between the House and Senate, this is a welcome addition and an 
honor to serve here. As you pointed out, this has been a 
committee since 1846, it has been established. I share with you 
the concern about making sure that we work with Mr. James, whom 
we have met with to focus on resolution 44 that we find to be 
very onerous and hope that agreement can be reached along those 
lines. I am especially delighted to see one of our own make it 
to the rarefied atmosphere of the United States Senate and look 
forward to working with Saxby as well.
    Mr. Ney. Anyone else?
    Senator Chambliss. My colleague said a step up. Some would 
say it is a step down.
    Mr. Ney. Anyone else? I have just a few technical things.
    I now wish to ask unanimous consent that members have 7 
legislative days to submit material into the record, and for 
those statements and materials to be entered into the 
appropriate place in the record. Without objection, the 
material will be entered.
    I also ask unanimous consent that the staff be authorized 
to make technical and conforming changes to all matters 
affecting our committee at today's meeting. If there is no 
objection, it is so ordered.
    That will complete our agenda of business. I certainly do 
thank you.
    [Whereupon, at 4:15 p.m., the joint committee was 
adjourned.]

</pre></body></html>
