<html>
<title> - MARKUP OF H.R. 406, H.R. 6122, H. CON. RES. 132, AND H.R. 1402</title>
<body><pre>
[House Hearing, 112 Congress]
[From the U.S. Government Printing Office]






     MARKUP OF H.R. 406, H.R. 6122, H. CON. RES. 132, AND H.R. 1402

=======================================================================

                                MEETING

                               before the

                           COMMITTEE ON HOUSE
                             ADMINISTRATION
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED TWELFTH CONGRESS

                             SECOND SESSION

                               __________

                 HELD IN WASHINGTON, DC, JULY 19, 2012

                               __________

      Printed for the use of the Committee on House Administration



<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>





                       Available on the Internet:
   http://www.gpoaccess.gov/congress/house/administration/index.html




                                _____

                  U.S. GOVERNMENT PRINTING OFFICE

75-655                    WASHINGTON : 2012
-----------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing 
Office Internet: bookstore.gpo.gov Phone: toll free (866) 512-1800; DC 
area (202) 512-1800 Fax: (202) 512-2104  Mail: Stop IDCC, Washington, DC 
20402-0001


















                   COMMITTEE ON HOUSE ADMINISTRATION

                DANIEL E. LUNGREN, California, Chairman
GREGG HARPER, Mississippi            ROBERT A. BRADY, Pennsylvania
PHIL GINGREY, M.D., Georgia            Ranking Minority Member
AARON SCHOCK, Illinois               ZOE LOFGREN, California
TODD ROKITA, Indiana                 CHARLES A. GONZALEZ, Texas
RICHARD B. NUGENT, Florida

                           Professional Staff

                      Philip Kiko, Staff Director
                  Jamie Fleet, Minority Staff Director

 
     MARKUP OF H.R. 406, H.R. 6122, H. CON. RES. 132, AND H.R. 1402

                              ----------                              


                        THURSDAY, JULY 19, 2012

                  House of Representatives,
                 Committee on House Administration,
                                                    Washington, DC.
    The committee met, pursuant to call, at 10:09 a.m., in room 
1310, Longworth House Office Building, Hon. Daniel E. Lungren 
(chairman of the committee) presiding.
    Present: Representatives Lungren, Harper, Gingrey, Nugent, 
Lofgren, and Gonzalez.
    Staff Present: Phil Kiko, Staff Director and General 
Counsel; Peter Schalestock, Deputy General Counsel; Kimani 
Little, Parliamentarian; Joe Wallace, Legislative Clerk; Yael 
Barash, Assistant Legislative Clerk; Salley Wood, 
Communications Director; Linda Ulrich, Director of Oversight; 
Dominic Storelli, Oversight Staff; Bob Sensenbrenner, Elections 
Counsel; Matt Pinkus, Minority Senior Policy Analyst; Khalil 
Abboud, Minority Elections Staff; and Greg Abbott, Minority 
Professional Staff.
    The Chairman. I now call to order the Committee on House 
Administration for today's committee markup. A quorum is 
present, so we may proceed.
    Today we have four items to consider. We had originally 
planned to consider a fifth bill, H.R. 1974; however, some of 
our colleagues identified some issues with it for which they 
wanted additional time to work out, and after consulting with 
Representative Quigley, the sponsor, they asked that we remove 
it from today's schedule, and we have done so.
    Our first bill today is H.R. 406, a bill to amend the 
Federal Election Campaign Act of 1971 so as to allow candidates 
to designate an individual who would be authorized to disburse 
campaign funds in the event of the death of the candidate. 
Under current law, only the campaign treasurer is authorized to 
disburse these funds.
    First introduced by Congressman Walter Jones in 2008, this 
bill will give candidates more flexibility when deciding who 
should have responsibility over where campaign funds are 
distributed.
    The second bill is H.R. 6122, a bill to revise the 
authority of the Librarian of Congress to accept gifts and 
bequests on behalf of the Library. This bill expands the 
Library's ability to receive gifts and authorizes the Librarian 
of Congress to accept gifts of securities for immediate 
disbursement, personal property valued up to $25,000, 
nonpersonal services or voluntary and uncompensated personal 
services. It would also require the Librarian to describe such 
gift or bequest valued at $1,000 or more in the annual report 
of the Library of Congress.
    The third measure to be considered is H. Con. Res. 132, 
authorizing additional funds for printing the publication 
Hispanic Americans in Congress. This resolution will bring the 
printed number of Hispanic Americans in Congress on par with 
the previous publications Women in Congress and Black Americans 
in Congress.
    Finally, the committee will consider H.R. 1402, introduced 
by Mr. Kildee and cosponsored by our ranking member Mr. Brady, 
which will authorize the Architect of the Capitol to establish 
battery recharging stations for privately owned vehicles in 
House parking areas. Users of these stations will be charged a 
fee, approved by the committee, to ensure that there is no net 
cost to the government.
    I thank my colleagues for being here today, and I would now 
recognize Mr. Gonzalez for any opening statement that he may 
wish to make.
    Mr. Gonzalez. I waive opening, Mr. Chairman. Thank you.
    The Chairman. That was the finest presentation that I have 
heard from you in a long time. No. Thank you very much for your 
courtesy. I understand that we are attempting to try and deal 
with Members' schedules with other committees. So I appreciate 
that.
    I would now call up and lay before the committee H.R. 406, 
H.R. 6122, and H. Con. Res. 132. The committee will consider 
these matters en bloc.
    [The information follows:]


<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>


    The Chairman. Would Representative Gonzalez like to be 
heard on any of these matters?
    Mr. Gonzalez. Again, Mr. Chairman, we have gone over these. 
They are not controversial. And it is actually welcoming news 
that today it will be an efficient use of our time on matters 
that are important, but that we have a meeting of the minds. So 
I yield back.
    The Chairman. I thank the gentleman.
    Would any other Member like to be heard on any of these 
matters?
    The gentleman from Georgia.
    Mr. Gingrey. Mr. Chairman, thank you very much. And very 
briefly, I want to say that I fully support H. Con. Res. 132. 
This, of course, would ensure the printing and production of 
Hispanic Americans in Congress.
    I believe that it is important that we honor and recognize 
all Hispanic Americans who have served in this body and, of 
course, in the other Chamber. At the same time, though, I 
wanted to make this statement, because when we have 
significantly reduced funding for the legislative branch by 
10.5 percent during this 112th Congress, I believe that we must 
continue to find ways that we can save taxpayer dollars.
    We must acknowledge that the primary source of information 
is now online. We have had recent hearings on that. Through the 
leadership of this committee, we have seen the decline in 
physical production of the Congressional Record, Federal 
Register, and U.S. Code.
    So, Mr. Chairman, as we move forward in printing all 
documents related to the history of Members of Congress, I 
would suggest that we focus more in the digital space that 
reflects the mode of communication that most represents how our 
constituents choose today to get their information about 
Members. So I ask my colleagues to keep this important point in 
mind as we move forward in the future.
    I yield back. I thank the chairman.
    The Chairman. I thank the gentleman for his comments.
    Are there any others who wish to make comments?
    If not, I would move that H.R. 406, H.R. 6122, and H. Con. 
Res. 132 be ordered favorably to the House. The question is on 
the motion.
    All those in favor of the motion, signify by saying aye. 
Aye.
    All opposed, signify by saying nay.
    In the opinion of the chair, the ayes have it, and the 
motion is agreed to.
    Without objection, the motion to reconsider is laid upon 
the table.
    I would ask unanimous consent that the committee staff be 
authorized to make any necessary technical or conforming 
changes to the measures the committee just considered. Without 
objection, it is so ordered.
    Does any Member wish to file supplemental, minority, or 
additional views for inclusion in the committee reports to the 
House?
    Okay. The next item on the agenda is H.R. 1402, to 
authorize the Architect of the Capitol to establish battery 
recharging stations for privately owned vehicles in parking 
areas under the jurisdiction of the House of Representatives at 
no net cost to the Federal Government.
    I now call up and lay before the committee H.R. 1402.
    Without objection, the bill will be considered as read and 
open for amendment at any point.
    [The information follows:]


<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>


    The Chairman. I do have an amendment to H.R. 1402, and the 
amendment should be in the Members' packets.
    If there is no objection, the amendment will be considered 
as read.
    [The information follows:]


<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>


    
    The Chairman. And I would now recognize myself to speak in 
favor of the amendment.
    This amendment adopts language that parallels the Senate's 
companion electric car bill. Under this amendment, if the 
Architect of the Capitol determined Members and staff using 
electric cars were receiving a subsidy, the committee would 
receive a plan from the Architect. If the committee did not act 
within 60 days, the Architect would take appropriate steps to 
increase rates or fees to the Members using it or staff using 
it to ensure that there would be no subsidy. I would urge all 
Members to support this amendment.
    Is there any other Member who wishes to be heard on the 
amendment?
    If not, without objection, the question is on the 
amendment.
    All those in favor, signify by saying aye. Aye.
    All opposed, signify by saying nay.
    In the opinion of the chair, the ayes have it. The 
amendment is agreed to.
    I move that the committee report H.R. 1402 favorably to the 
House with an amendment.
    So the question is on the motion.
    All those in favor of the motion, signify by saying aye. 
Aye.
    All opposed, signify by saying nay.
    In the opinion of the chair, the ayes have it, and the 
motion is agreed to.
    Without objection, the motion to reconsider is laid on the 
table.
    I ask unanimous consent that the committee staff be 
authorized to make any necessary technical or conforming 
changes to the measures the committee just considered.
    Without objection, it is so ordered.
    Does any Member wish to file supplemental, minority, or 
additional views for inclusion in the committee report to the 
House?
    All right. If not, there are no more items on today's 
agenda, and I thank the Members for their participation in this 
markup.
    I now adjourn the meeting.
    [Whereupon, at 10:20 a.m., the committee was adjourned.]

                                  <all>

</pre></body></html>
