<html>
<title> - ENERGY AND WATER DEVELOPMENT APPROPRIATIONS FOR 2009</title>
<body><pre>
[House Hearing, 110 Congress]
[From the U.S. Government Printing Office]


 
                      ENERGY AND WATER DEVELOPMENT
                        APPROPRIATIONS FOR 2009

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED TENTH CONGRESS
                             SECOND SESSION
                                ________
              SUBCOMMITTEE ON ENERGY AND WATER DEVELOPMENT
                  PETER J. VISCLOSKY, Indiana, Chairman
 CHET EDWARDS, Texas
 ED PASTOR, Arizona
 MARION BERRY, Arkansas
 CHAKA FATTAH, Pennsylvania
 STEVE ISRAEL, New York
 TIM RYAN, Ohio
 JOSE E. SERRANO, New York
 JOHN W. OLVER, Massachusetts       DAVID HOBSON, Ohio
                                    ZACH WAMP, Tennessee
                                    JO ANN EMERSON, Missouri
                                    MICHAEL K. SIMPSON, Idaho
                                    DENNIS R. REHBERG, Montana
                                    KEN CALVERT, California

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
             Dixon Butler, Terry Tyborowski, Taunja Berquam,
             Robert Sherman, and Lori Maes, Staff Assistants

                                ________

                                 PART 9
                                WITNESSES

                                   S

                                ________

         Printed for the use of the Committee on Appropriations
      PART 9--ENERGY AND WATER DEVELOPMENT APPROPRIATIONS FOR 2009
                                                                      ?
?
                                                                      ?

                      ENERGY AND WATER DEVELOPMENT

                        APPROPRIATIONS FOR 2009

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED TENTH CONGRESS
                             SECOND SESSION
                                ________
              SUBCOMMITTEE ON ENERGY AND WATER DEVELOPMENT
                  PETER J. VISCLOSKY, Indiana, Chairman
 CHET EDWARDS, Texas
 ED PASTOR, Arizona
 MARION BERRY, Arkansas
 CHAKA FATTAH, Pennsylvania
 STEVE ISRAEL, New York
 TIM RYAN, Ohio
 JOSE E. SERRANO, New York
 JOHN W. OLVER, Massachusetts       DAVID HOBSON, Ohio
                                    ZACH WAMP, Tennessee
                                    JO ANN EMERSON, Missouri
                                    MICHAEL K. SIMPSON, Idaho
                                    DENNIS R. REHBERG, Montana
                                    KEN CALVERT, California

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
             Dixon Butler, Terry Tyborowski, Taunja Berquam,
             Robert Sherman, and Lori Maes, Staff Assistants

                                ________

                                 PART 9
                                WITNESSES

                                   S

                                ________
         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 44-198                     WASHINGTON : 2008

                                  COMMITTEE ON APPROPRIATIONS

                   DAVID R. OBEY, Wisconsin, Chairman

 JOHN P. MURTHA, Pennsylvania
 NORMAN D. DICKS, Washington
 ALAN B. MOLLOHAN, West Virginia
 MARCY KAPTUR, Ohio
 PETER J. VISCLOSKY, Indiana
 NITA M. LOWEY, New York
 JOSE E. SERRANO, New York
 ROSA L. DeLAURO, Connecticut
 JAMES P. MORAN, Virginia
 JOHN W. OLVER, Massachusetts
 ED PASTOR, Arizona
 DAVID E. PRICE, North Carolina
 CHET EDWARDS, Texas
 ROBERT E. ``BUD'' CRAMER, Jr., 
Alabama
 PATRICK J. KENNEDY, Rhode Island
 MAURICE D. HINCHEY, New York
 LUCILLE ROYBAL-ALLARD, California
 SAM FARR, California
 JESSE L. JACKSON, Jr., Illinois
 CAROLYN C. KILPATRICK, Michigan
 ALLEN BOYD, Florida
 CHAKA FATTAH, Pennsylvania
 STEVEN R. ROTHMAN, New Jersey
 SANFORD D. BISHOP, Jr., Georgia
 MARION BERRY, Arkansas
 BARBARA LEE, California
 TOM UDALL, New Mexico
 ADAM SCHIFF, California
 MICHAEL HONDA, California
 BETTY McCOLLUM, Minnesota
 STEVE ISRAEL, New York
 TIM RYAN, Ohio
 C.A. ``DUTCH'' RUPPERSBERGER, 
Maryland
 BEN CHANDLER, Kentucky
 DEBBIE WASSERMAN SCHULTZ, Florida
 CIRO RODRIGUEZ, Texas              JERRY LEWIS, California
                                    C. W. BILL YOUNG, Florida
                                    RALPH REGULA, Ohio
                                    HAROLD ROGERS, Kentucky
                                    FRANK R. WOLF, Virginia
                                    JAMES T. WALSH, New York
                                    DAVID L. HOBSON, Ohio
                                    JOE KNOLLENBERG, Michigan
                                    JACK KINGSTON, Georgia
                                    RODNEY P. FRELINGHUYSEN, New Jersey
                                    TODD TIAHRT, Kansas
                                    ZACH WAMP, Tennessee
                                    TOM LATHAM, Iowa
                                    ROBERT B. ADERHOLT, Alabama
                                    JO ANN EMERSON, Missouri
                                    KAY GRANGER, Texas
                                    JOHN E. PETERSON, Pennsylvania
                                    VIRGIL H. GOODE, Jr., Virginia
                                    RAY LaHOOD, Illinois
                                    DAVE WELDON, Florida
                                    MICHAEL K. SIMPSON, Idaho
                                    JOHN ABNEY CULBERSON, Texas
                                    MARK STEVEN KIRK, Illinois
                                    ANDER CRENSHAW, Florida
                                    DENNIS R. REHBERG, Montana
                                    JOHN R. CARTER, Texas
                                    RODNEY ALEXANDER, Louisiana
                                    KEN CALVERT, California
                                    JO BONNER, Alabama

                  Rob Nabors, Clerk and Staff Director

                             (ii)

<GRAPHICS NOT AVAILABLE IN TIFF FORMAT>


                           W I T N E S S E S

                              ----------                              
                                                                   Page
Abboud, Jeffrey..................................................   180
Abidi, Professor Mongi...........................................   435
Adcock, Paul.....................................................   345
Albritton, Jason.................................................   301
Ames, E. L., III.................................................   336
Anthes, R. A.....................................................   431
Ashley, Marion...................................................   347
Bajura, Richard..................................................   149
Baker, Jennifer..................................................   373
Barnett, J. A....................................................   105
Barry, H. J., III................................................   207
Bently, C. R.....................................................   159
Berry, Mike......................................................   417
Binette, Aja.....................................................   323
Binney, P. D.....................................................    39
Blagojevich, R. R................................................   361
Blum, J. O.......................................................   332
Bowers, K. W.....................................................   337
Bowman, F. L.....................................................   316
Bridges, J. H....................................................   429
Britton, Del.....................................................   129
Brostmeyer, Shirley..............................................   167
Buer, Stein......................................................   377
Burton, Carly....................................................   420
Bye, Dr. Raymond, Jr.............................................   161
Caan, G. M.......................................................   104
Callahan, Kateri.................................................    49
Christiansen, D. A...............................................   101
Clark, Vaughn....................................................   276
Crane, Professor Carl............................................   435
Culp, David......................................................   133
Cunningham, Ron..................................................   445
D'Antonio, J. R., Jr.............................................    93
Dean, Bob........................................................   391
DeGood, Alan.....................................................   359
Derman, Edward...................................................   373
Detweiler, Hans..................................................   175
Donaldson, J. C..................................................   351
Dowd, Wayne......................................................   354
Duggal, Dr. V. K.................................................   202
Duncan, Mark.....................................................   384
Duncan, Roger....................................................   199
Freeman, S. D....................................................   121
Freudenthal, Dave................................................   439
Frost, C. J......................................................   381
Gawell, Karl.....................................................   189
Gensler, Katherine...............................................   370
Glasener, Karl...................................................    15
Gordon, Hon. Bart................................................     6
Green, W. R. (Will)..............................................    20
Hamel, A. C......................................................    85
Hinkle, Jerome...................................................   305
Hintz, Don.......................................................    24
Holdgreve, Chris.................................................   285
Huffman, G. P...................................................96, 109
Huffman, Russell.................................................   383
Hunter, Dr. S. M.................................................   230
Kanjorski, Hon. P. E.............................................     3
Kell, Scott......................................................   178
Kemper, Doug.....................................................   102
Kerkhoven, Paul..................................................   269
Knatz, Geraldine.................................................   121
Koland, Dave.....................................................   184
Kucinich, Hon. D. J..............................................     1
Kuhn, R. E.......................................................   103
Landsberger, Professor Sheldon...................................   435
Langley, Reece...................................................   421
Larson, Troy.....................................................   233
Latture, Paul, II................................................   236
Leahey, J. A.....................................................   295
Leeman, Dr. Christoph............................................   221
Litterer, Ron....................................................   291
Lynch, R. S......................................................   211
Mayfield, Barry..................................................   366
Minsky, R. S.....................................................   158
Minthorn, A. C...................................................    98
Morrison, Ernie..................................................   226
Nadel, Steven....................................................    28
Nadel, Steven....................................................    79
Narvaiz, Susan...................................................   395
Nimrod, Peter....................................................   248
Nylander, Charles................................................   447
O'Brien, T. J....................................................   243
Oberg, Brad......................................................   213
Olguin, Michelle.................................................   381
Oman, Alicia.....................................................   261
Pesata, Levi.....................................................   220
Porter, John.....................................................   382
Preau, E. J., Jr.................................................   239
Preston, Michael.................................................   206
Raabe, Peter.....................................................    40
Rahnema, Farzad..................................................   321
Richardson, Bill.................................................   274
Ritter, Bill, Jr.................................................   424
Robertson, Gordon................................................    55
Rose, Robert.....................................................   426
Rowan, Linda.....................................................    70
Sailors, J. L....................................................   115
Saliba, D. L.....................................................    44
Schuchat, Sam....................................................   142
Severn, Aaron....................................................    34
Shirley, Joe, Jr.................................................   310
Skok, Andrew.....................................................   147
Smith, B. S......................................................    75
Smith, S. B......................................................   111
Solomon, David...................................................   441
Stanton, J. S....................................................   373
Sturdivant, B. S.................................................   459
Taylor, W. E., (``Dub'').........................................   280
Terry, David.....................................................    66
Tonko, P. D......................................................   311
Twarog, Daniel...................................................   261
VanAmerongen, Deborah............................................   311
Wagenknecht, Brad................................................   265
Wallace, Rob.....................................................   194
Walton, Izaak....................................................   217
Waterman, Diana..................................................   261
Watney, W. L.....................................................   228
Wehe, Professor D. K.............................................   435
Weinstein, Dr. Harel.............................................    86
Whitehead, B. T..................................................   382
Wilkinson, E. W..................................................   315
Winkler, Anita...................................................   325
Wood, Professor John.............................................   435
Yamagata, Ben....................................................   170
Yoon, Roe-Hoan...................................................   137
Younger, Pat.....................................................    89
Younger, Pat.....................................................   118
Younger, Pat.....................................................   329
Younger, Pat.....................................................   341
Zion, Mark.......................................................   418

                                  <all>
</pre></body></html>
