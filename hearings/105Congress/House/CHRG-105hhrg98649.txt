<html>
<title> - IMPLEMENTATION OF THE ELECTRONIC FREEDOM OF INFORMATION AMENDMENTS OF 1996: IS ACCESS TO GOVERNMENT INFORMATION IMPROVING?</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


<DOC>
[DOCID: f:98649.wais]

 
                IMPLEMENTATION OF THE ELECTRONIC FREEDOM 
                  OF INFORMATION AMENDMENTS OF 1996: IS 
                 ACCESS TO GOVERNMENT INFORMATION IMPROVING? 

=============================================================================


                                 HEARINGS

                                before the

                 SUBCOMMITTEE ON GOVERNMENT MANAGEMENT,
                        INFORMATION, AND TECHNOLOGY

                                 of the

                               COMMITTEE ON 
                             GOVERNMENT REFORM 
                               AND OVERSIGHT


                        HOUSE OF REPRESENTATIVES

                       one hundred fifth congress

                              second session

                                  -------

                               June 9, 1998

                                  -------

                            Serial No. 105-197

                                  -------

  Printed for the use of the Committee on Government Reform and Oversight


                    U.S. GOVERNMENT PRINTING OFFICE
54-565                      WASHINGTON : 1999
_____________________________________________________________________________
             For sale by the U.S. Government Printing office
Superintendent of Documents, Congressional Sales Office: Washington, DC 20402
                            ISBN 0-16-058402-7

[TEXT NOT AVAILABLE REFER TO PDF]



</pre></body></html>
