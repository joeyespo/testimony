<html>
<title> - STATUS OF WOMEN'S HEALTH CARE PROGRAMS IN THE DEPARTMENT OF VETERANS AFFAIRS</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]


 
  STATUS OF WOMEN'S HEALTH CARE PROGRAMS IN THE DEPARTMENT OF VETERANS 
                                AFFAIRS 

=======================================================================

                                HEARING

                               before the

                         SUBCOMMITTEE ON HEALTH

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             SECOND SESSION
                               __________

                            OCTOBER 2, 2002

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 107-42

                               ----------

                       U.S. GOVERNMENT PRINTING OFFICE 

93-355 PDF                       WASHINGTON : 2004 

For sale by the Superintendent of Documents, U.S. Government Printing 
Office Internet: bookstore.gpo.gov Phone: toll free (866) 512-1800; 
(202) 512-1800 Fax: (202) 512-2250 Mail: Stop SSOP, 
Washington, DC 20402-0001 

[TEXT NOT AVAILABLE IN TIFF FORMAT]




</pre></body></html>
