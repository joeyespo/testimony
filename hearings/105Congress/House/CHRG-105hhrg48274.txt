<html>
<title> - EARLY HEAD START: GOALS AND CHALLENGES </title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                 EARLY HEAD START: GOALS AND CHALLENGES
                    ____________________________________________________________________________


                               HEARING 

                              BEFORE THE


                    SUBCOMMITTEE ON HUMAN RESOURCES
 
                                OF THE

                        COMMITTEE ON GOVERNMENT

                         REFORM AND OVERSIGHT

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                              SECOND SESSION

                               _____________

                             FEBRUARY 19, 1998

                                ___________

                            Serial No. 105-109
                                
                                 ________

Printed for the use of the Committee on Government Reform and Oversight






                        U.S. GOVERNMENT PRINTING OFFICE

48-274                        WASHINGTON : 1998
_____________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing
Office Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800
Fax: (202) 512-2250  Mail: Stop SSOP, Washington, DC  20402-0001



[TEXT NOT AVAILABLE]

</pre></body></html>
