<html>
<title> - TO MEASURE THE PROGRESS OF RELIEF, RECOVERY, RECONSTRUCTION, AND DEVELOPMENT EFFORTS IN HAITI FOLLOWING THE EARTHQUAKE OF JANUARY 12, 2010, AND FOR OTHER PURPOSES</title>
<body><pre>
[House Hearing, 112 Congress]
[From the U.S. Government Printing Office]


 
   TO MEASURE THE PROGRESS OF RELIEF, RECOVERY, RECONSTRUCTION, AND 
 DEVELOPMENT EFFORTS IN HAITI FOLLOWING THE EARTHQUAKE OF JANUARY 12, 
                      2010, AND FOR OTHER PURPOSES

=======================================================================

                                 MARKUP

                               BEFORE THE

                            SUBCOMMITTEE ON
                         THE WESTERN HEMISPHERE

                                 OF THE

                      COMMITTEE ON FOREIGN AFFAIRS
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED TWELFTH CONGRESS

                             FIRST SESSION

                                   ON

                               H.R. 1016

                               __________

                             MARCH 31, 2011

                               __________

                           Serial No. 112-19

                               __________

        Printed for the use of the Committee on Foreign Affairs


 Available via the World Wide Web: http://www.foreignaffairs.house.gov/

                                 ______



                  U.S. GOVERNMENT PRINTING OFFICE
65-493                    WASHINGTON : 2011
-----------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing Office, 
http://bookstore.gpo.gov. For more information, contact the GPO Customer Contact Center, U.S. Government Printing Office. Phone 202�09512�091800, or 866�09512�091800 (toll-free). E-mail, gpo@custhelp.com.  

                      COMMITTEE ON FOREIGN AFFAIRS

                 ILEANA ROS-LEHTINEN, Florida, Chairman
CHRISTOPHER H. SMITH, New Jersey     HOWARD L. BERMAN, California
DAN BURTON, Indiana                  GARY L. ACKERMAN, New York
ELTON GALLEGLY, California           ENI F.H. FALEOMAVAEGA, American 
DANA ROHRABACHER, California             Samoa
DONALD A. MANZULLO, Illinois         DONALD M. PAYNE, New Jersey
EDWARD R. ROYCE, California          BRAD SHERMAN, California
STEVE CHABOT, Ohio                   ELIOT L. ENGEL, New York
RON PAUL, Texas                      GREGORY W. MEEKS, New York
MIKE PENCE, Indiana                  RUSS CARNAHAN, Missouri
JOE WILSON, South Carolina           ALBIO SIRES, New Jersey
CONNIE MACK, Florida                 GERALD E. CONNOLLY, Virginia
JEFF FORTENBERRY, Nebraska           THEODORE E. DEUTCH, Florida
MICHAEL T. McCAUL, Texas             DENNIS CARDOZA, California
TED POE, Texas                       BEN CHANDLER, Kentucky
GUS M. BILIRAKIS, Florida            BRIAN HIGGINS, New York
JEAN SCHMIDT, Ohio                   ALLYSON SCHWARTZ, Pennsylvania
BILL JOHNSON, Ohio                   CHRISTOPHER S. MURPHY, Connecticut
DAVID RIVERA, Florida                FREDERICA WILSON, Florida
MIKE KELLY, Pennsylvania             KAREN BASS, California
TIM GRIFFIN, Arkansas                WILLIAM KEATING, Massachusetts
TOM MARINO, Pennsylvania             DAVID CICILLINE, Rhode Island
JEFF DUNCAN, South Carolina
ANN MARIE BUERKLE, New York
RENEE ELLMERS, North Carolina
VACANT
                   Yleem D.S. Poblete, Staff Director
             Richard J. Kessler, Democratic Staff Director
                                 ------                                

                 Subcommittee on the Western Hemisphere

                     CONNIE MACK, Florida, Chairman
MICHAEL T. McCAUL, Texas             ELIOT L. ENGEL, New York
JEAN SCHMIDT, Ohio                   ALBIO SIRES, New Jersey
DAVID RIVERA, Florida                ENI F.H. FALEOMAVAEGA, American 
CHRISTOPHER H. SMITH, New Jersey         Samoa
ELTON GALLEGLY, California           DONALD M. PAYNE, New Jersey


                            C O N T E N T S

                              ----------                              
                                                                   Page

                               MARKUP OF

H.R. 1016, To measure the progress of relief, recovery, 
  reconstruction, and development efforts in Haiti following the 
  earthquake of January 12, 2010, and for other purposes.........     2
  Amendment in the nature of a substitute to H.R. 1016...........    11
  Amendment to the amendment in the nature of a substitute to 
    H.R. 1016 offered by the Honorable Connie Mack, a 
    Representative in Congress from the State of Florida, and 
    chairman, Subcommittee on the Western Hemisphere.............    20

                                APPENDIX

Markup notice....................................................    24
Markup minutes...................................................    25


   TO MEASURE THE PROGRESS OF RELIEF, RECOVERY, RECONSTRUCTION, AND 
 DEVELOPMENT EFFORTS IN HAITI FOLLOWING THE EARTHQUAKE OF JANUARY 12, 
                      2010 AND FOR OTHER PURPOSES

                              ----------                              


                        THURSDAY, MARCH 31, 2011

                  House of Representatives,
            Subcommittee on the Western Hemisphere,
                              Committee on Foreign Affairs,
                                                    Washington, DC.
    The subcommittee met, pursuant to notice, at 3:02 p.m., in 
room 2172 Rayburn House Office Building, Hon. Connie Mack 
(chairman of the subcommittee) presiding.
    Mr. Mack. The subcommittee will finally come to order. 
After the conclusion of this brief business meeting, we will 
proceed immediately into today's hearing. Pursuant to notice, 
for purposes of a markup, I call up the resolution H.R. 1016, 
To measure the progress of relief, recovery, reconstruction, 
and development efforts in Haiti following the earthquake of 
January 12, 2010, and for other purposes.
    [H.R. 1016 follows:]

    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    
    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    

    Mr. Mack. Without objection, the bipartisan amendment in 
the nature of a substitute that was provided previously to 
members' offices and made available and online will be 
considered as base text, and is open for amendment at any 
point.
    [The amendment in the nature of a substitute to H.R. 1016 
follows:]

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

    Mr. Mack. I also want to thank the ranking member, Mr. 
Engel, and the minority staff for their assistance in updating 
and amending this text. I think that means you. Also, all 
members are able to insert remarks on this measure into the 
record, should they choose to do so.
    Before turning to myself to offer a brief amendment and 
speak, I will recognize myself briefly for a few opening words. 
On January 12, 2010, Haiti was devastated by the most powerful 
earthquake to strike that nation in over 200 years.
    Since that time, the people of Haiti have been impacted by 
a hurricane with widespread flooding. The challenge of 
Presidential and legislative elections in this difficult 
environment, and a health epidemic impacting hundreds of 
thousands of Haitians.
    While Haiti has been a focus of the State Department's 
activities in the Western Hempisphere, oversight of the 
implementation of U.S. assistance has been a priority for the 
Foreign Affairs Committee. I look forward to continuing to work 
with everyone to make sure U.S. dollars are spent in an 
effective, responsible, and transparent manner, as we continue 
to support the people of Haiti.
    And at this time, I would like--I have an amendment on 
behalf of Mr. Engel at the desk. The clerk will report the 
amendment.
    Mr. Gately. Amendment to the amendment in the nature of a 
substitute to H.R. 1016, offered by Mr. Mack of Florida.
    [The amendment referred to follows:]

    <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
    

    Mr. Mack. Without objection, the amendment is considered as 
read for purposes of amendment. And I am pleased to recognize 
myself again to speak on this amendment. Unless there are other 
members who wish to strike the last word and briefly speak, we 
can move to a vote.
    The question is on agreeing to the amendement offered by 
myself and Mr. Engel. All those in favor say aye.
    [Ayes.]
    All those opposed say no. In the opinion of the chair, the 
ayes have it, and the amendment is agreed to.
    Hearing no other amendments, the question now occurs on 
adopting the bill as amended. All those in favor, say aye.
    [Ayes.]
    All those opposed, say no. In the opinion of the chair, the 
ayes have it.
    The bill as amended is agreed to, and without objection the 
motion to reconsider is laid on the table. Without objection, 
the bill will be reported favorably to the full committee in 
the form of a single amendment in the nature of a substitute 
incorporating the amendments adopted here today, and the staff 
is directed to make any technical and conforming amendments.
    That concludes our business, and without objection, the 
subcommittee stands adjourned.
    [Whereupon, at 3:06 p.m., the subcommittee was adjourned.]
                                     

                                     

                            A P P E N D I X

                              ----------                              


     Material Submitted for the Hearing Record<greek-l>Notice deg.

<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>

                               <greek-l>Minutes deg.

                               <GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>
                               
                                 <all>

</pre></body></html>
