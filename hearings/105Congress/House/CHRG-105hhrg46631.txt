<html>
<title> - DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES APPROPRIATIONS FOR 1999</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                 DEPARTMENT OF THE INTERIOR AND RELATED
                    AGENCIES APPROPRIATIONS FOR 1999

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS
                             SECOND SESSION
                                ________

   SUBCOMMITTEE ON THE DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES

                      RALPH REGULA, Ohio, Chairman
 JOSEPH M. McDADE, Pennsylvania        SIDNEY R. YATES, Illinois
 JIM KOLBE, Arizona                    JOHN P. MURTHA, Pennsylvania
 JOE SKEEN, New Mexico                 NORMAN D. DICKS, Washington
 CHARLES H. TAYLOR, North Carolina     DAVID E. SKAGGS, Colorado
 GEORGE R. NETHERCUTT, Jr.,            JAMES P. MORAN, Virginia
Washington
 DAN MILLER, Florida
 ZACH WAMP, Tennessee

 NOTE: Under Committee Rules, Mr. Livingston, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.

 Deborah Weatherly, Loretta Beaumont, Joel Kaplan, and Christopher Topik,
                            Staff Assistants
                                ________

                                 PART 4

                  Justification of the Budget Estimates
                                                                   Page
 Indian Health Services...........................................    1
 Navajo and Hopi Indian Relocation................................  239
 Institute of American Indian and Alaska Native...................  259
 Smithsonian Institution..........................................  279
 National Gallery of Art..........................................  461
 John F. Kennedy Center...........................................  583
 Woodrow Wilson International Center..............................  693
 National Endowment for the Arts..................................  745
 National Endowment for the Humanities............................  807
 IMLS Office of Museum Services...................................  893
 Commission of Fine Arts..........................................  927
 Advisory Council on Historic Preservation........................  941
 National Capital Planning Commission.............................  989
 Holocaust Memorial Council....................................... 1039
                                ________

         Printed for the use of the Committee on Appropriations
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
 46-631 O                   WASHINGTON : 1998
------------------------------------------------------------------------------
            For sale by the U.S. Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402


                          COMMITTEE ON APPROPRIATIONS

                   BOB LIVINGSTON, Louisiana, Chairman

 JOSEPH M. McDADE, Pennsylvania      DAVID R. OBEY, Wisconsin
 C. W. BILL YOUNG, Florida           SIDNEY R. YATES, Illinois
 RALPH REGULA, Ohio                  LOUIS STOKES, Ohio
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois        NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky             MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico               JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia             VIC FAZIO, California
 TOM DeLAY, Texas                    W. G. (BILL) HEFNER, North Carolina
 JIM KOLBE, Arizona                  STENY H. HOYER, Maryland
 RON PACKARD, California             ALAN B. MOLLOHAN, West Virginia
 SONNY CALLAHAN, Alabama             MARCY KAPTUR, Ohio
 JAMES T. WALSH, New York            DAVID E. SKAGGS, Colorado
 CHARLES H. TAYLOR, North Carolina   NANCY PELOSI, California
 DAVID L. HOBSON, Ohio               PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma     ESTEBAN EDWARD TORRES, California
 HENRY BONILLA, Texas                NITA M. LOWEY, New York
 JOE KNOLLENBERG, Michigan           JOSE E. SERRANO, New York
 DAN MILLER, Florida                 ROSA L. DeLAURO, Connecticut
 JAY DICKEY, Arkansas                JAMES P. MORAN, Virginia
 JACK KINGSTON, Georgia              JOHN W. OLVER, Massachusetts
 MIKE PARKER, Mississippi            ED PASTOR, Arizona
RODNEY P. FRELINGHUYSEN, New Jersey  CARRIE P. MEEK, Florida
 ROGER F. WICKER, Mississippi        DAVID E. PRICE, North Carolina
 MICHAEL P. FORBES, New York         CHET EDWARDS, Texas                                                      GEORGE R. NETHERCUTT, Jr.,           ROBERT E. (BUD) CRAMER, Jr., Alabama
Washington
 MARK W. NEUMANN, Wisconsin
 RANDY ``DUKE'' CUNNINGHAM, 
California
 TODD TIAHRT, Kansas
 ZACH WAMP, Tennessee
 TOM LATHAM, Iowa
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama

                 James W. Dyer, Clerk and Staff Director


                                  (ii)
</pre></body></html>
