<html>
<title> - NOMINATION OF GREGORY EUGENE JACKSON</title>
<body><pre>
[Senate Hearing 108-735]
[From the U.S. Government Printing Office]



                                                        S. Hrg. 108-735

                  NOMINATION OF GREGORY EUGENE JACKSON

=======================================================================

                                HEARING

                               before the

                              COMMITTEE ON
                          GOVERNMENTAL AFFAIRS
                          UNITED STATES SENATE


                      ONE HUNDRED EIGHTH CONGRESS

                             SECOND SESSION

                                 ON THE

 NOMINATION OF GREGORY EUGENE JACKSON, TO BE AN ASSOCIATE JUDGE OF THE 
               SUPERIOR COURT OF THE DISTRICT OF COLUMBIA

                               __________

                            OCTOBER 5, 2004

                               __________

      Printed for the use of the Committee on Governmental Affairs



                    U.S. GOVERNMENT PRINTING OFFICE
97-047                      WASHINGTON : 2004
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092250 Mail: Stop SSOP, Washington, DC 20402�090001

                   COMMITTEE ON GOVERNMENTAL AFFAIRS

                   SUSAN M. COLLINS, Maine, Chairman
TED STEVENS, Alaska                  JOSEPH I. LIEBERMAN, Connecticut
GEORGE V. VOINOVICH, Ohio            CARL LEVIN, Michigan
NORM COLEMAN, Minnesota              DANIEL K. AKAKA, Hawaii
ARLEN SPECTER, Pennsylvania          RICHARD J. DURBIN, Illinois
ROBERT F. BENNETT, Utah              THOMAS R. CARPER, Delaware
PETER G. FITZGERALD, Illinois        MARK DAYTON, Minnesota
JOHN E. SUNUNU, New Hampshire        FRANK LAUTENBERG, New Jersey
RICHARD C. SHELBY, Alabama           MARK PRYOR, Arkansas

           Michael D. Bopp, Staff Director and Chief Counsel
                    Johanna L. Hardy Senior Counsel
             Jennifer Hemingway, Professional Staff Member
    David Cole, Professional Staff Member, Oversight of Government 
    Management, the Federal Workforce, and the District of Columbia 
                              Subcommittee
      Joyce A. Rechtschaffen, Minority Staff Director and Counsel
           Adam Sedgewick, Minority Professional Staff Member
  Marianne Clifford Upton, Minority Staff Director and Chief Counsel, 
                              Oversight of
   Government Management, the Federal Workforce, and the District of 
                         Columbia Subcommittee
                      Amy B. Newhouse, Chief Clerk


                            C O N T E N T S

                                 ------                                
Opening statements:
                                                                   Page
    Senator Voinovich............................................     1
    Senator Pryor................................................     5

                               WITNESSES
                        Tuesday, October 5, 2004

Hon. Eleanor Holmes Norton, a Delegate in Congress from the 
  District of Columbia...........................................     2
Gregory Eugene Jackson, to be an Associate Judge of the Superior 
  Court of the District of Columbia..............................     2
    Biographical and Professional Information....................     7

                                APPENDIX

Paul Strauss, a Shadow Senator from the District of Columbia, 
  prepared statement.............................................    22

 
                  NOMINATION OF GREGORY EUGENE JACKSON

                              ----------                              


                        TUESDAY, OCTOBER 5, 2004

                                       U.S. Senate,
                         Committee on Governmental Affairs,
                                                    Washington, DC.
    The Committee met, pursuant to notice, at 10:15 a.m., in 
room SD-342, Dirksen Senate Office Building, Hon. George V. 
Voinovich presiding.
    Present: Senators Voinovich and Pryor.

             OPENING STATEMENT OF SENATOR VOINOVICH

    Senator Voinovich. Good morning and welcome. Today, the 
Committee on Governmental Affairs meets to consider the 
nomination of Gregory Jackson to be Associate Judge for the 
Superior Court of the District of Columbia. Mr. Jackson has 
served as General Counsel for the District of Columbia 
Department of Corrections since 1999. As General Counsel, he 
advises the Director of Corrections on legal and administrative 
issues, coordinates the defense against civil cases filed 
against the Department, and serves as the primary point of 
contact for the local courts and other criminal justice 
agencies.
    During this period, he represented the Department of 
Corrections in hearings in the Superior Court and the U.S. 
District Court, primarily on issues related to prisoners' 
conditions of confinement in the District of Columbia jail.
    Prior to this position, Mr. Jackson served as an Assistant 
U.S. Attorney for the District of Columbia from 1986 to 1999, 
13 years. During that period, Mr. Jackson primarily focused on 
criminal litigation and served half his time as an attorney 
supervisor.
    Additionally, Mr. Jackson has served as Legal Advisor to 
the U.S. Nuclear Regulatory Commission from 1978 to 1986. Some 
time I would like to talk to you about that, Mr. Jackson, 
because I now have them under my jurisdiction.
    Mr. Jackson holds a B.A. from Rutgers College and a J.D. 
from the Newark College of Law. I believe him to be well 
qualified for the position to which he has been nominated.
    I now welcome my colleague, Delegate Eleanor Holmes Norton 
of the District of Columbia, who is here to introduce Mr. 
Jackson. Ms. Norton and I have known each other way before 
either one of us were here. I think I first got to know you, 
Eleanor, when I was mayor of the city of Cleveland. Thank you 
for coming over, and I want to apologize to you and to Mr. 
Jackson and all of your friends who are here today for being 
late, but we had a vote over in the Senate and one of the 
things I have learned since I have come to the Senate, as 
contrasted to when I was governor, is that I do not control my 
own schedule.

STATEMENT OF HON. ELEANOR HOLMES NORTON, A DELEGATE IN CONGRESS 
                 FROM THE DISTRICT OF COLUMBIA

    Ms. Holmes Norton. Thank you very much, Mr. Chairman, and 
if I may say so, your own rescue of the city of Cleveland was a 
model for what has happened in recent years in this city as 
well, and we are very pleased and gratified at your 
contributions to the District of Columbia since you have been 
Chairman of the Subcommittee.
    I am very pleased to recommend to you today Gregory 
Jackson, whom the President has nominated to our D.C. Superior 
Court, which is our local court of jurisdiction but under the 
jurisdiction of this Committee. I believe that Mr. Jackson's 
prior posts and his entire career well qualify him to be an 
Associate Judge of the Superior Court. You have outlined his 
recent positions. I think that particularly his extensive 
litigation experience best qualifies him to be a trial judge of 
our Superior Court.
    His 13 years as an Assistant U.S. Attorney for the U.S. 
Attorney for the District of Columbia where he tried 44 cases 
on most of which he was the lead counsel. He was Chief of the 
Felony Section of the U.S. Attorney's Office.
    Mr. Jackson received a 4-year scholarship to Rutgers 
University, graduated from the university and then received his 
J.D. from the university as well. We are proud of him in the 
District of Columbia and proud to recommend him to you today, 
Mr. Chairman.
    Thank you very much.
    Senator Voinovich. Thank you very much. As you know, you 
are welcome to remain for the rest of the hearing, but I know 
you have got a busy schedule like all of us, so thank you so 
much for being here today.
    Ms. Holmes Norton. Thank you, Mr. Chairman.
    Senator Voinovich. It's the custom of the Committee to 
swear in witnesses and therefore I ask, Mr. Jackson, if you 
will stand up. Do you swear that the testimony that you are 
about to give before this Committee is the truth, the whole 
truth and nothing but the truth?
    Mr. Jackson. I do.
    Senator Voinovich. Let the record show that Mr. Jackson 
answered in the affirmative. I understand that you may have 
some family members here today and other friends. I would like 
to give you the opportunity to introduce all the folks that 
have come out today to support your nomination.

  TESTIMONY OF GREGORY EUGENE JACKSON, TO BE ASSOCIATE JUDGE, 
              DISTRICT OF COLUMBIA SUPERIOR COURT

    Mr. Jackson. Thank you, Mr. Chairman, and if I may I would 
like to introduce my wife, Sheila Robinson, who is behind me; 
my mother Mary Jackson; my godparents, Mr. and Mrs. Frank 
Walker; and my cousin Rader Banks, who have come here to share 
this occasion with me. I would also like to introduce my 
friends and my extended family who have come out to share this 
occasion and be present here to support me. I have to tell you 
that while this opportunity is a real blessing for me, the 
greater blessing, if you will, is to have friends and 
colleagues, acquaintances, long-time family members, extended 
family members, who have supported me throughout this process 
and indeed throughout all of my endeavors, but most 
particularly during this time, and who have come here to share 
this occasion with my family and with me, and I want to thank 
them very much and let them know how much I really appreciate 
all of their support.
    Senator Voinovich. One of the things that I have noticed as 
time goes on is that all of us in public service could not do 
what we have been able to do without the sacrifice of our 
spouse and our friends, and it is nice to have them all here to 
see you today. I know they are all very proud of you and I am 
sure each one of them have had something to do with your 
career. As I say, in life, we look around, and all we are is a 
reflection of the people that love us, and it is nice to have 
your loved ones here with you today.
    Would you like to share with us a statement about why you 
want this nomination?
    Mr. Jackson. Senator, I would like to say that my whole 
career, as you have noted in the summary that you gave, has 
been devoted to public service. This is an opportunity to do 
what many may characterize as the ultimate in public service. I 
have been fortunate and blessed to have the opportunity to have 
a number of experiences that I think have well prepared me for 
this opportunity, certainly the opportunity to be a trial 
attorney and a prosecutor in the U.S. Attorney's Office, to 
develop the trial skills and to understand the process in the 
way that I have.
    But I have to also say that I am very proud of the 
opportunity that I have had to serve as General Counsel for the 
D.C. Department of Corrections. It has given me a different 
perspective, if you will, on the criminal justice system, an 
opportunity to understand what happens beyond the mere 
prosecution of the case, and the importance of and the 
difficulty of the decisions that judges sometimes have to make, 
and to understand how that process takes place and what all is 
involved. I believe that these opportunities have certainly 
shaped me.
    They have influenced me, they have guided me in a number of 
ways, and they have brought me to this point. I am very excited 
about the opportunity to possibly serve as an Associate Judge 
of the Superior Court of the District of Columbia.
    Senator Voinovich. Thank you. We have some questions that 
we ask all of our nominees. Is there anything that you are 
aware of in your background that might present a conflict of 
interest with the duties of the office to which you have been 
nominated?
    Mr. Jackson. Nothing that I am aware of, Senator.
    Senator Voinovich. Do you know of any reason, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of the office to 
which you have been nominated?
    Mr. Jackson. No, there is nothing that I am aware of, sir.
    Senator Voinovich. And do you know of any reason, personal 
or otherwise, that would in any way prevent you from serving 
the full term for the office to which you have been nominated?
    Mr. Jackson. No, sir.
    Senator Voinovich. Thank you very much. I would like to ask 
you a couple of other questions. Whether it is through your 
experience appearing before judges and observing your 
colleagues, I am sure you have observed a variety of judicial 
temperaments. I am always concerned about the judicial 
temperaments of our judges. I would like you to discuss what 
you believe to be the appropriate temperament and approach of a 
judge not only in dealing with attorneys appearing before you 
but in dealing with their clients and witnesses coming before 
the bench.
    Mr. Jackson. Mr. Chairman, I think it is critical and 
fundamental that as a judge, one must be fair and impartial. 
That is the basic foundation of what a judge must do. But in 
addition to that, one must be respectful of all the parties 
that appear before them.
    I think that it is critical that all the parties who appear 
before a judge feel confident and know that they will be 
treated with fairness, with respect, and that ultimately while 
they may agree or disagree, like or dislike the outcome, they 
nevertheless know that they have been treated fairly and with 
respect throughout the entire process.
    Senator Voinovich. I think that is really important. You 
have been able to observe a lot of judges over the years as I 
have. Back in my own State, I have seen people that become 
judges and their personalities change. To quote the Bible, ``He 
that exalts himself shall be humbled and he that humbles 
himself shall be exalted,'' and I think this sometimes is 
forgotten.
    I am sure you have seen what I am talking about over the 
years. It is very important for people that come before you to 
feel that you have treated them with dignity, respect, and 
openness. I feel that your experience with the correction 
system will help you to put yourself in other people's shoes, 
which is very important in this position.
    Looking at your current position as General Counsel for the 
District of Columbia Department of Corrections and your 
dealings with the District Superior Court, do you see areas 
that need improvement, whether it be a communication or 
management problem? How would you go about implementing 
improvements to correct these problems as a judge in the 
District Court System?
    Mr. Jackson. Mr. Chairman, one of the unintended 
consequences of the Revitalization Act and the closure of 
Lorton and the change in the way in which sentenced felons are 
incarcerated here in the District of Columbia has necessitated 
the improved communications and improved relationships among 
all the criminal justice partners that operate here in the 
District of Columbia, and that includes the D.C. Superior 
Court.
    We also work very closely with the U.S. District Court, the 
marshals, the U.S. Marshal Service in both courts, the Federal 
Bureau of Prisons, the U.S. Parole Commission, the Court 
Services and Offender Supervision Agency. There is now a 
collaborative effort on the part of all of those agencies to 
improve the system.
    We meet regularly with the court and the court staff on a 
variety of different issues. We have contact people at the 
Superior Court that I talk to in some instances daily and 
sometimes multiple times during the course of the day, working 
out individual problems and addressing systemic problems and 
issues that arise.
    This is an ongoing and evolving process as we go forward 
trying to improve the criminal justice system in the District 
of Columbia. It is critical that we have open lines of 
communication, and we do now have that, Senator. Everyone is 
working very hard to indeed make our system a model system, if 
you will. It is unique in so many different ways and it 
presents a number of different challenges and also a number of 
different opportunities. We are always exploring those 
opportunities to try to make the process better.
    Senator Voinovich. How about the pre-sentencing reports? I 
know one of the things that we found when I was Governor of 
Ohio, that the pre-sentencing workups left a lot to be desired. 
Once an individual had their day in court and they were 
sentenced, so often when they went off to jail, the prison did 
not know anything about them. Could you comment on the pre-
sentencing reports?
    Mr. Jackson. I do not personally see the presentence 
reports on a regular basis. However, I know that we have had 
discussions with the Court Services and Offender Supervision 
Agency that oversees the preparation of those presentence 
reports to improve the process by which that information is 
exchanged and transmitted to us and ultimately to the Federal 
Bureau of Prisons for those people who are sentenced felons 
that are sentenced to periods of incarceration and are 
transferred to the Federal Bureau of Prisons.
    Recently, we have worked diligently with the Federal Bureau 
of Prisons, the Superior Court, the Marshal Service and the 
U.S. Parole Commission to improve the process by which that 
information is transferred and indeed improve the time by which 
those offenders are prepared and ultimately transferred to the 
Federal Bureau of Prisons, and we have succeeded, sir, in 
reducing that time from roughly about 65 days to about 23 days.
    Senator Voinovich. Well, it is a big issue, and I know that 
my Director of Corrections who is still serving the State of 
Ohio, Reggie Wilkinson, has spent a lot of time on pre-
sentencing reports. We have found that because we improved pre-
sentencing, that we are better able to deal with people that 
are incarcerated and to try to make a difference in their lives 
so that when they get out of jail that they can go on and have 
productive lives.
    I think that is very important. It is very important to 
you, too, when you are sitting on the bench and really knowing 
who is this individual, and they are just not some number.
    We have been joined by Senator Pryor from Arkansas, and 
Senator, I would like to call on you for any comment that you 
would like to make, and I appreciate the fact that very rarely 
do any of my colleagues show up for these nominations. So thank 
you for coming.

               OPENING STATEMENT OF SENATOR PRYOR

    Senator Pryor. Thank you, Mr. Chairman. I appreciate you 
and your diligence on a variety of issues, but when it comes to 
judges, I have a pretty basic test, and that is is the person 
qualified, is there anything in their background that we need 
to know about, can they be fair and impartial, and maybe one or 
two others.
    I am not aware of anything in Mr. Jackson's background that 
would give me any pause on this, and as far as I understand, 
there is really no controversy around this nomination. Is that 
fair to say?
    Senator Voinovich. That is more than fair to say.
    Senator Pryor. OK. Then I really do not have any questions. 
Thank you.
    Senator Voinovich. Thank you. If there are no further 
questions, I want to thank you, Mr. Jackson, for your responses 
today. I would also like to thank your family, friends, and 
colleagues for coming today to offer their support for you. I 
know that you are anxious to be confirmed and invested, and the 
next process is to have the Committee, as a whole, consider 
your nomination and then report it to the Senate for final 
action. We will do everything in our power to move this along.
    We are supposed to be wrapping up this week, but we are 
going to be back for a lame duck starting on November 15. We 
will do everything we can to move this along so that it does 
not carry over into next year. I know that you are needed on 
the bench today and we will do what we can to make it happen. 
So thanks again for being here today.
    Mr. Jackson. Thank you very much, Mr. Chairman, Senator 
Pryor.
    Senator Voinovich. The hearing is adjourned.
    [Whereupon, at 10:30 a.m., the Committee adjourned.]


                            A P P E N D I X

                              ----------                              

[GRAPHIC] [TIFF OMITTED] T7047.001

[GRAPHIC] [TIFF OMITTED] T7047.002

[GRAPHIC] [TIFF OMITTED] T7047.003

[GRAPHIC] [TIFF OMITTED] T7047.004

[GRAPHIC] [TIFF OMITTED] T7047.005

[GRAPHIC] [TIFF OMITTED] T7047.006

[GRAPHIC] [TIFF OMITTED] T7047.007

[GRAPHIC] [TIFF OMITTED] T7047.008

[GRAPHIC] [TIFF OMITTED] T7047.009

[GRAPHIC] [TIFF OMITTED] T7047.010

[GRAPHIC] [TIFF OMITTED] T7047.011

[GRAPHIC] [TIFF OMITTED] T7047.012

[GRAPHIC] [TIFF OMITTED] T7047.013

[GRAPHIC] [TIFF OMITTED] T7047.014

[GRAPHIC] [TIFF OMITTED] T7047.015

[GRAPHIC] [TIFF OMITTED] T7047.016

[GRAPHIC] [TIFF OMITTED] T7047.017

[GRAPHIC] [TIFF OMITTED] T7047.018

[GRAPHIC] [TIFF OMITTED] T7047.019

                                 <all>

</pre></body></html>
