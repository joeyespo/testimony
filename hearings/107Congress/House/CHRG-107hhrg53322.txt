<html>
<title> - GAO'S REPORT, ``VETERANS' EMPLOYMENT AND TRAINING SERVICE: FLEXIBILITY AND ACCOUNTABILITY NEEDED TO IMPROVE SERVICE TO VETERANS,'' AND THE VA'S IMPLEMENTATION OF THE VOCATIONAL REHABILITATION AND EMPLOYMENT PROGRAM</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]




 
GAO'S REPORT, ``VETERANS' EMPLOYMENT AND TRAINING SERVICE: FLEXIBILITY 
  AND ACCOUNTABILITY NEEDED TO IMPROVE SERVICE TO VETERANS,'' AND THE 
  VA'S IMPLEMENTATION OF THE VOCATIONAL REHABILITATION AND EMPLOYMENT 
                                PROGRAM

=======================================================================

                                HEARING

                               before the

                        SUBCOMMITTEE ON BENEFITS

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             FIRST SESSION

                                   ON

                               __________

                            OCTOBER 30, 2001

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 107-15

                  U.S. GOVERNMENT PRINTING OFFICE
81-750PS                  WASHINGTON : 2002
-----------------------------------------------------------------------
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092104 Mail: Stop IDCC, Washington, DC 20402�090001


<TEXT NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
