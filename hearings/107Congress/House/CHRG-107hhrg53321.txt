<html>
<title> - VA'S ABILITY TO RESPOND TO DOD CONTINGENCIES AND NATIONAL EMERGENCIES</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]




 
 VA'S ABILITY TO RESPOND TO DOD CONTINGENCIES AND NATIONAL EMERGENCIES

=======================================================================

                                HEARING

                               before the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             FIRST SESSION

                               __________

                            OCTOBER 15, 2001

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 107-14



                  U.S. GOVERNMENT PRINTING OFFICE
81-562PS                  WASHINGTON : 2002
-----------------------------------------------------------------------
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092104 Mail: Stop IDCC, Washington, DC 20402�090001

<TEXT NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
