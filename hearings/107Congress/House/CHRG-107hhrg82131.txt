<html>
<title> - STATUS OF FINANCIAL MANAGEMENT AT THE U.S. DEPARTMENT OF EDUCATION </title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
                    STATUS OF FINANCIAL MANAGEMENT AT THE
                        U.S. DEPARTMENT OF EDUCATION
===========================================================================

                                   HEARING

                                  BEFORE THE

                       SUBCOMMITTEE ON SELECT EDUCATION

                                    OF THE

                          COMMITTEE ON EDUCATION AND
                                 THE WORKFORCE


                           HOUSE OF REPRESENTATIVES


                         ONE HUNDRED SEVENTH CONGRESS

                                SECOND SESSION

                                    -------

                HEARING HELD IN WASHINGTON, DC, APRIL 10, 2002

                                    -------
                                Serial No. 107-56

                                    -------


              Printed for the use of the Committee on Education
                                and the Workforce






82-131
____________________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800  DC area (202) 512-1800  FAX: (202) 512-2250  Mail: Stop SSOP, Washington, DC 20402-0001

<TEXT NOT AVAILABLE>

</pre></body></html>
