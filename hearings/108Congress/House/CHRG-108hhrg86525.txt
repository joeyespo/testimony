<html>
<title> - AGRICULTURE, RURAL DEVELOPMENT, FOOD</title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]



 
                  AGRICULTURE, RURAL DEVELOPMENT, FOOD

                  AND DRUG ADMINISTRATION, AND RELATED

                    AGENCIES APPROPRIATIONS FOR 2004

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED EIGHTH CONGRESS
                              FIRST SESSION
                                ________
     SUBCOMMITTEE ON AGRICULTURE, RURAL DEVELOPMENT, FOOD AND DRUG 
                  ADMINISTRATION, AND RELATED AGENCIES

                     HENRY BONILLA, Texas, Chairman


 JAMES T. WALSH, New York            MARCY KAPTUR, Ohio
 JACK KINGSTON, Georgia              ROSA L. DeLAURO, Connecticut
 GEORGE R. NETHERCUTT, Jr.,          MAURICE D. HINCHEY, New York
Washington                           SAM FARR, California
 TOM LATHAM, Iowa                    ALLEN BOYD, Florida
 JO ANN EMERSON, Missouri
 VIRGIL H. GOODE, Jr., Virginia
 RAY LaHOOD, Illinois               
                                    
                                    

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
   Henry E. Moore, Martin P. Delgado, Maureen Holohan, and Joanne L. 
                        Perdue, Staff Assistants

                                ________

                                 PART 3
               TESTIMONY OF MEMBERS OF CONGRESS AND OTHER
                INTERESTED INDIVIDUALS AND ORGANIZATIONS


                                ________

         Printed for the use of the Committee on Appropriations
                                 Part 3

                              Testimony of

                          Members of Congress

                                  and

                                 Public

                               Witnesses

     AGRICULTURE, RURAL DEVELOPMENT, FOOD AND DRUG ADMINISTRATION,

              AND RELATED AGENCIES APPROPRIATIONS FOR 2004
                  AGRICULTURE, RURAL DEVELOPMENT, FOOD

                  AND DRUG ADMINISTRATION, AND RELATED

                    AGENCIES APPROPRIATIONS FOR 2004


                                ________

         Printed for the use of the Committee on Appropriations
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE

 86-525                     WASHINGTON : 2003



                      COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                        DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California                   JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky                   NORMAN D. DICKS, Washington
 FRANK R. WOLF, Virginia                   MARTIN OLAV SABO, Minnesota
 JIM KOLBE, Arizona                        STENY H. HOYER, Maryland
 JAMES T. WALSH, New York                  ALAN B. MOLLOHAN, West Virginia
 CHARLES H. TAYLOR, North Carolina         MARCY KAPTUR, Ohio
 DAVID L. HOBSON, Ohio                     PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma           NITA M. LOWEY, New York
 HENRY BONILLA, Texas                      JOSE E. SERRANO, New York
 JOE KNOLLENBERG, Michigan                 ROSA L. DeLAURO, Connecticut
 JACK KINGSTON, Georgia                    JAMES P. MORAN, Virginia
 RODNEY P. FRELINGHUYSEN, New Jersey       JOHN W. OLVER, Massachusetts
 ROGER F. WICKER, Mississippi              ED PASTOR, Arizona
 GEORGE R. NETHERCUTT, Jr.,                DAVID E. PRICE, North Carolina
 Washington                                CHET EDWARDS, Texas
 RANDY ``DUKE'' CUNNINGHAM,                ROBERT E. ``BUD'' CRAMER, Jr., 
California                                 Alabama
 TODD TIAHRT, Kansas                       PATRICK J. KENNEDY, Rhode Island
 ZACH WAMP, Tennessee                      JAMES E. CLYBURN, South Carolina
 TOM LATHAM, Iowa                          MAURICE D. HINCHEY, New York
 ANNE M. NORTHUP, Kentucky                 LUCILLE ROYBAL-ALLARD, California
 ROBERT B. ADERHOLT, Alabama               SAM FARR, California
 JO ANN EMERSON, Missouri                  JESSE L. JACKSON, Jr., Illinois
 KAY GRANGER, Texas                        CAROLYN C. KILPATRICK, Michigan
 JOHN E. PETERSON, Pennsylvania            ALLEN BOYD, Florida
 VIRGIL H. GOODE, Jr., Virginia            CHAKA FATTAH, Pennsylvania
 JOHN T. DOOLITTLE, California             STEVEN R. ROTHMAN, New Jersey
 RAY LaHOOD, Illinois                      SANFORD D. BISHOP, Jr., Georgia
 JOHN E. SWEENEY, New York                 MARION BERRY, Arkansas            
 DAVID VITTER, Louisiana
 DON SHERWOOD, Pennsylvania
 DAVE WELDON, Florida
 MICHAEL K. SIMPSON, Idaho
 JOHN ABNEY CULBERSON, Texas
 MARK STEVEN KIRK, Illinois
 ANDER CRENSHAW, Florida            
                                    
                                    
                 James W. Dyer, Clerk and Staff Director

                                  (ii)

                [GRAPHIC(S)NOT AVAILABLE IN TIFF FORMAT] 



                          ORGANIZATIONAL INDEX

                              ----------                              
                                                                   Page
Ad Hoc Coalition.................................................   210
Advanced Medical Technology Association..........................   422
Alachua County Board of County Commissioners.....................   379
American Farm Bureau Federation..................................   245
American Honey Producers Association.............................   339
American Indian Higher Education Consortium......................   266
American Rivers..................................................   230
American Sheep Industry Association..............................   331
American Society for Microbiology...............................45, 221
American Society for Nutritional Sciences........................    83
Association of State Dam Safety Officials........................   314
Aquatica Tropicals, Inc..........................................   186
Arizona Shrimp Company...........................................   457
Austwell Aqua Farm, Inc..........................................   458
Bear Creek Fisheries.............................................   189
Berkshire Technology Group, Inc..................................   195
Blyzo Fish Farm..................................................   161
California Agricultural Commissioners & Sealers Association......    64
California Farm Bureau Consortium................................   448
Carla's Water Haven, Inc.........................................   163
Catfish Farmers of America.......................................   183
Ceatech USA, Inc.................................................   459
Central California Ozone Study Coalition.........................   271
Coalition on Funding Agricultural Research Missions..............   412
Coalition to Promote U.S. Agricultural Exports...................   259
College of the Marshall Islands..................................   151
Colorado River Basin Salinity Control Forum................31, 226, 364
Consortium of Social Science Associations........................   250
Council on Food, Agricultural and Resource Economics.............   250
Darden Restaurants...............................................   460
Defenders of Wildlife............................................    89
Easter Seals.....................................................   351
Foshee Farms, Inc................................................   192
Friends of Agricultural Research.................................    69
Great Lakes Indian Fish and Wildlife Commission..................   385
Grocery Manufacturers of America.................................   238
Harligen Shrimp Farms, Inc.......................................   462
Harrison Fish Farm...............................................   166
Hawaii Fish Company..............................................   154
High Health Aquaculture, Inc.....................................   463
Humane Society of the United States..............................   390
Illinois Soybean Association.....................................   436
International Association of Fish and Wildlife Agencies..........   285
Inter-Tribal Bison Cooperative...................................   294
Island Aquaculture...............................................   144
Joslin Diabetes Center...........................................   122
Lummi Nation.....................................................   356
Martha's Vineyard Shellfish Group, Inc...........................   180
Medical Device Manufacturers Association.........................   263
Metropolitan Water District of Southern California...............   275
National Association of Conservation Districts...................   404
National Cattlemen's Beef Association............................   310
National Council of Farmer Cooperatives..........................   367
National CSFP Association........................................    61
National Potato Council..........................................    79
National Rural Housing Coalition.................................    55
National Rural Telecom Association, Inc..........................   323
National Telecommunications Cooperative Association..............   279
National Turfgrass Evaluation Program............................    39
National Watershed Coalition.....................................   298
Navaho Nation Division of Natural Resources......................   240
New Jersey Aquaculture Coalition.................................   176
New York State Department of Agriculture and Markets.............   137
Northwest Indian Fisheries Commission............................   358
Oklahoma Fire Ant Research & Management Advisory Committee.......   343
Organization for the Promotion and Advancement of Small 
  Telecommunications Companies...................................   109
Pickled Vegetable Industry.......................................    26
R&G Shrimp Company...............................................   464
Red River Valley Association.....................................   114
Regional Aquaculture Centers.....................................   148
Rural Coalition..................................................   426
Rural Enterprises of Oklahoma....................................   375
Scientific Hatcheries............................................   198
Seminole Tribe of Florida........................................   216
Society for Animal Protective Legislation........................   381
South Carolina Shrimp Growers Association........................   465
Southern Illinois University.....................................   444
Sustainable Agriculture Coalition................................   369
Taylor Shellfish Farms...........................................   157
Texas Aquaculture Association....................................   469
United Egg Producers.............................................    50
University of Idaho..............................................   204
University of Southern Mississippi...............................   104
U.S. Agricultural Export Development Council.....................   208
U.S. Apple Association...........................................    96
U.S. Marine Farming Consortium...................................   452
United States Telecom Association................................   395
University of Illinois....................................432, 440, 444
University of Missouri...........................................   444
Upper Mississippi River Basin Association........................    34
Utah Division of Water Resources.................................   202
Wildlife Society.................................................   100
Wyoming State Engineer's Office..................................   430
Zeigler Bros. Inc................................................   470

                                  <all>

</pre></body></html>
