<html>
<title> - DEPARTMENTS OF COMMERCE, JUSTICE, AND</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
                 DEPARTMENTS OF COMMERCE, JUSTICE, AND

                   STATE, THE JUDICIARY, AND RELATED

                    AGENCIES APPROPRIATIONS FOR 2003

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS
                             SECOND SESSION
                                ________
  SUBCOMMITTEE ON THE DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE 
                    JUDICIARY, AND RELATED AGENCIES
                    FRANK R. WOLF, Virginia, Chairman
 HAROLD ROGERS, Kentucky             JOSE E. SERRANO, New York
 JIM KOLBE, Arizona                  ALAN B. MOLLOHAN, West Virginia
 CHARLES H. TAYLOR, North Carolina   LUCILLE ROYBAL-ALLARD, California
 RALPH REGULA, Ohio                  ROBERT E. ``BUD'' CRAMER, Jr., 
 TOM LATHAM, Iowa                    Alabama
 DAN MILLER, Florida                 PATRICK J. KENNEDY, Rhode Island 
 DAVID VITTER, Louisiana            
                                    
 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
   Mike Ringler, Christine Kojac, Leslie Albright, and John F. Martens
                           Subcommittee Staff
                                ________
                                 PART 3

                  Justification of the Budget Estimates

 Department of State..............................................    1
 Broadcasting Board of Governors..................................  854

                              <snowflake>

                                ________
         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 79-814                     WASHINGTON : 2002




                       COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                  DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky             NORMAN D. DICKS, Washington
 JOE SKEEN, New Mexico               MARTIN OLAV SABO, Minnesota
 FRANK R. WOLF, Virginia             STENY H. HOYER, Maryland
 TOM DeLAY, Texas                    ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                  MARCY KAPTUR, Ohio
 SONNY CALLAHAN, Alabama             NANCY PELOSI, California
 JAMES T. WALSH, New York            PETER J. VISCLOSKY, Indiana
 CHARLES H. TAYLOR, North Carolina   NITA M. LOWEY, New York
 DAVID L. HOBSON, Ohio               JOSE E. SERRANO, New York
 ERNEST J. ISTOOK, Jr., Oklahoma     ROSA L. DeLAURO, Connecticut
 HENRY BONILLA, Texas                JAMES P. MORAN, Virginia
 JOE KNOLLENBERG, Michigan           JOHN W. OLVER, Massachusetts
 DAN MILLER, Florida                 ED PASTOR, Arizona
 JACK KINGSTON, Georgia              CARRIE P. MEEK, Florida
 RODNEY P. FRELINGHUYSEN, New Jersey DAVID E. PRICE, North Carolina
 ROGER F. WICKER, Mississippi        CHET EDWARDS, Texas
 GEORGE R. NETHERCUTT, Jr.,          ROBERT E. ``BUD'' CRAMER, Jr., 
Washington                           Alabama
 RANDY ``DUKE'' CUNNINGHAM,          PATRICK J. KENNEDY, Rhode Island
California                           JAMES E. CLYBURN, South Carolina
 TODD TIAHRT, Kansas                 MAURICE D. HINCHEY, New York
 ZACH WAMP, Tennessee                LUCILLE ROYBAL-ALLARD, California
 TOM LATHAM, Iowa                    SAM FARR, California
 ANNE M. NORTHUP, Kentucky           JESSE L. JACKSON, Jr., Illinois
 ROBERT B. ADERHOLT, Alabama         CAROLYN C. KILPATRICK, Michigan
 JO ANN EMERSON, Missouri            ALLEN BOYD, Florida
 JOHN E. SUNUNU, New Hampshire       CHAKA FATTAH, Pennsylvania
 KAY GRANGER, Texas                  STEVEN R. ROTHMAN, New Jersey    
 JOHN E. PETERSON, Pennsylvania
 JOHN T. DOOLITTLE, California
 RAY LaHOOD, Illinois
 JOHN E. SWEENEY, New York
 DAVID VITTER, Louisiana
 DON SHERWOOD, Pennsylvania
   
 VIRGIL H. GOODE, Jr., Virginia     

                 James W. Dyer, Clerk and Staff Director

                                  (ii)



              [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]



</pre></body></html>
