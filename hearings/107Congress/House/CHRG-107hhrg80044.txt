<html>
<title> - ECONOMIC RECOVERY AND ASSISTANCE TO WORKERS</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
                  ECONOMIC RECOVERY AND ASSISTANCE TO
                                WORKERS

=======================================================================

                                HEARING

                              BEFORE THE

                     COMMITTEE ON EDUCATION AND
                              THE WORKFORCE

                       HOUSE OF REPRESENTATIVES


                      ONE HUNDRED SEVENTH CONGRESS

                              FIRST SESSION

                               __________

         HEARING HELD IN WASHINGTON, D.C. NOVEMBER 14, 2001

                               __________

                           Serial No. 107-40
                               __________

          Printed for the use of the Committee on Education
                           and the Workforce


80-044              U.S. GOVERNMENT PRINTING OFFICE
                            WASHINGTON : 2002
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
      Internet: bookstore.gpo.gov  (202) 512�091800  Fax: (202) 512�092250 
                Mail: Stop SSOP, Washington, DC 20402�090001

<TEXT NOT AVAILABLE>

</pre></body></html>
