<html>
<title> - DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE JUDICIARY, AND RELATED AGENCIES APPROPRIATIONS FOR 2001 DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE JUDICIARY, AND RELATED AGENCIES APPROPRIATIONS FOR 2001</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]



 
      DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE JUDICIARY,
              AND RELATED AGENCIES APPROPRIATIONS FOR 2001
                 DEPARTMENTS OF COMMERCE, JUSTICE, AND
                   STATE, THE JUDICIARY, AND RELATED
                    AGENCIES APPROPRIATIONS FOR 2001

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS
                             SECOND SESSION
                                ________
  SUBCOMMITTEE ON THE DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE 
                    JUDICIARY, AND RELATED AGENCIES
                    HAROLD ROGERS, Kentucky, Chairman
 JIM KOLBE, Arizona                  JOSE E. SERRANO, New York
 CHARLES H. TAYLOR, North Carolina   JULIAN C. DIXON, California
 RALPH REGULA, Ohio                  ALAN B. MOLLOHAN, West Virginia
 TOM LATHAM, Iowa                    LUCILLE ROYBAL-ALLARD, California
 DAN MILLER, Florida
 ZACH WAMP, Tennessee               
                    
 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
    Gail Del Balzo, Jennifer Miller, Mike Ringler, and Christine Ryan
                           Subcommittee Staff
                                ________
                                 PART 3

                  Justification of the Budget Estimates

 Department of State..............................................    1
 Maritime Administration.......................................... 1327
 The Judiciary.................................................... 1505

                              <snowflake>

                                ________
         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 64-154                     WASHINGTON : 2000




                        COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                  DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois        NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky             MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico               JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia             STENY H. HOYER, Maryland
 TOM DeLAY, Texas                    ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                  MARCY KAPTUR, Ohio
 RON PACKARD, California             NANCY PELOSI, California
 SONNY CALLAHAN, Alabama             PETER J. VISCLOSKY, Indiana
 JAMES T. WALSH, New York            NITA M. LOWEY, New York
 CHARLES H. TAYLOR, North Carolina   JOSE E. SERRANO, New York
 DAVID L. HOBSON, Ohio               ROSA L. DeLAURO, Connecticut
 ERNEST J. ISTOOK, Jr., Oklahoma     JAMES P. MORAN, Virginia
 HENRY BONILLA, Texas                JOHN W. OLVER, Massachusetts
 JOE KNOLLENBERG, Michigan           ED PASTOR, Arizona
 DAN MILLER, Florida                 CARRIE P. MEEK, Florida
 JAY DICKEY, Arkansas                DAVID E. PRICE, North Carolina
 JACK KINGSTON, Georgia              MICHAEL P. FORBES, New York
 RODNEY P. FRELINGHUYSEN, New Jersey CHET EDWARDS, Texas
 ROGER F. WICKER, Mississippi        ROBERT E. ``BUD'' CRAMER, Jr., 
 GEORGE R. NETHERCUTT, Jr.,          Alabama
Washington                           MAURICE D. HINCHEY, New York
 RANDY ``DUKE'' CUNNINGHAM,          LUCILLE ROYBAL-ALLARD, California
California                           SAM FARR, California
 TODD TIAHRT, Kansas                 JESSE L. JACKSON, Jr., Illinois
 ZACH WAMP, Tennessee                CAROLYN C. KILPATRICK, Michigan
 TOM LATHAM, Iowa                    ALLEN BOYD, Florida                 
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama
 JO ANN EMERSON, Missouri
 JOHN E. SUNUNU, New Hampshire
 KAY GRANGER, Texas
 JOHN E. PETERSON, Pennsylvania
 VIRGIL H. GOODE, Jr., Virginia     
                   
                 James W. Dyer, Clerk and Staff Director

                                  (ii)


[Pages 1 - 1314--The official Committee record contains additional material here]

                    [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT] 




                            C O N T E N T S

                              ----------                              

                          DEPARTMENT OF STATE

                                                                   Page
Overview.........................................................     4
Directory of Former ACDA and USIA Accounts.......................     9
Summary Tables...................................................     8
Administration of Foreign Affairs................................    16
    Performance Plan.............................................  1803
    State Programs...............................................    85
        Office of the Secretary..................................   131
        Political-Military Affairs...............................   149
        Office of Protocol.......................................   167
        Legislative Affairs......................................   176
        Oceans and International Environmental and Scientific 
          Affairs................................................   185
        Office of the Legal Adviser..............................   199
        Economic and Business Affairs............................   210
        Intelligence and Research................................   222
        African Affairs..........................................   235
        Near Eastern Affairs.....................................   254
        South Asian Affairs......................................   271
        East Asian and Pacific Affairs...........................   286
        Inter-American Affairs...................................   305
        European and Canadian Affairs............................   321
        International Organization Affairs.......................   352
        Office of Public Affairs.................................   370
        Bureau of Consular Affairs...............................   380
        Management...............................................   392
        Bureau of Finance and Management Policy..................   401
        Bureau of Personnel......................................   421
        Office of Medical Services...............................   440
        Foreign Service Institute................................   449
        Democracy, Human Rights, and Labor.......................   463
        Office of Administration.................................   475
        Diplomatic Security......................................   499
        Counterterrorism.........................................   521
        Chief Information Officer................................   526
        Diplomatic Telecommunications Service Program Office.....   558
        Office of Foreign Missions...............................   569
        Post Assignment Travel...................................   579
        Population, Refugees, and Migration......................   584
        International Criminal Justice...........................   589
        Foreign Service National Separation Liability Trust Fund.   595
        Border Security Program..................................   600
Other Administration of Foreign Affairs Appropriations:
    Office of Inspector General..................................   607
    Representation Allowances....................................   648
    Buying Power Maintenance Fund................................   655
    Protection of Foreign Missions and Officials.................   656
    Security and Maintenance of United States Missions...........   664
    Emergencies in the Diplomatic and Consular Service...........   770
    Repatriation Loans Program...................................   777
    Payment to the American Institute in Taiwan..................   781
    Payment to the Foreign Service Retirement and Disability Fund   794
International Organizations and Conferences:
    Overview.....................................................   799
    Contributions to International Organizations.................   800
    Contributions for International Peacekeeping Activities......   881
    Arrearage Payments...........................................   901
International Commissions:
    Overview.....................................................   902
    International Boundary and Water Commission, Salaries and 
      Expenses...................................................   903
    International Boundary and Water Commission, Construction, 
      O&M........................................................   916
    International Fisheries Commissions..........................   948
    International Boundary Commissions...........................   928
    International Joint Commission...............................   934
    Border Environment Cooperation Commission....................   942
Related Appropriations:
    Overview.....................................................   953
    Payment to the Asia Foundation...............................   954

                        MARITIME ADMINISTRATION

Organization Chart...............................................   968
Summary Statement................................................   969
Overview.........................................................   971
Resource Summary Tables..........................................   974
Performance Plans................................................   979
Maritime Security Program........................................  1019
Operations and Training..........................................  1035
Maritime Guaranteed Loan (Title XI)..............................  1062
Operating-Differential Subsidy...................................  1083
Ocean Freight Differential.......................................  1093
Federal Ship Financing Fund......................................  1100
Other Accounts...................................................  1105
Adminisrative Provisions.........................................  1114
General Provisions...............................................  1115

                             THE JUDICIARY

Summary of Budgetary Requirements for Fiscal Year 1999...........  1125
Supreme Court of the United States...............................  1132
United States Court of Appeals for the Federal Circuit...........  1163
United States Court of International Trade.......................  1176
Court of Appeals, District Courts, and Other Judicial Services...  1187
    Salaries and Expenses........................................  1206
    Defender Services............................................  1280
    Fees of Jurors and Commissioners.............................  1308
    Court Security...............................................  1318
Administrative Office of the U.S. Courts.........................  1331
Federal Judicial Center..........................................  1350
Payments to Judiciary Trust Funds................................  1362
United States Sentencing Commission..............................  1369
Violent Crime Reduction Trust Fund...............................  1378
General Provisions...............................................  1384

                                <greek-d>
</pre></body></html>
