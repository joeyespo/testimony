<html>
<title> - [ERRATA] STATE AND LOCAL FUSION CENTERS AND THE ROLE OF DHS</title>
<body><pre>
[House Hearing, 109 Congress]
[From the U.S. Government Printing Office]



 
                                [ERRATA]
           STATE AND LOCAL FUSION CENTERS AND THE ROLE OF DHS

=======================================================================

                                HEARING

                               before the

                     SUBCOMMITTEE ON INTELLIGENCE,
           INFORMATION SHARING, AND TERRORISM RISK ASSESSMENT

                                 of the

                     COMMITTEE ON HOMELAND SECURITY
                     U.S. HOUSE OF REPRESENTATIVES

                       ONE HUNDRED NINTH CONGRESS

                             SECOND SESSION

                               __________

                           SEPTEMBER 7, 2006

                               __________

                           Serial No. 109-99

                               __________

       Printed for the use of the Committee on Homeland Security
                                     
[GRAPHIC] [TIFF OMITTED] TONGRESS.#13

                                     

  Available via the World Wide Web: http://www.gpoaccess.gov/congress/
                               index.html

                               __________



                     U.S. GOVERNMENT PRINTING OFFICE

36-604 PDF                 WASHINGTON DC:  2006
---------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing
Office  Internet: bookstore.gpo.gov Phone: toll free (866)512-1800
DC area (202)512-1800  Fax: (202) 512-2250 Mail Stop SSOP, 
Washington, DC 20402-0001


                     COMMITTEE ON HOMELAND SECURITY

                   Peter T. King, New York, Chairman

Don Young, Alaska                    Bennie G. Thompson, Mississippi
Lamar S. Smith, Texas                Loretta Sanchez, California
Curt Weldon, Pennsylvania            Edward J. Markey, Massachusetts
Christopher Shays, Connecticut       Norman D. Dicks, Washington
John Linder, Georgia                 Jane Harman, California
Mark E. Souder, Indiana              Peter A. DeFazio, Oregon
Tom Davis, Virginia                  Nita M. Lowey, New York
Daniel E. Lungren, California        Eleanor Holmes Norton, District of 
Jim Gibbons, Nevada                  Columbia
Rob Simmons, Connecticut             Zoe Lofgren, California
Mike Rogers, Alabama                 Sheila Jackson-Lee, Texas
Stevan Pearce, New Mexico            Bill Pascrell, Jr., New Jersey
Katherine Harris, Florida            Donna M. Christensen, U.S. Virgin 
Bobby Jindal, Louisiana              Islands
Dave G. Reichert, Washington         Bob Etheridge, North Carolina
Michael T. McCaul, Texas             James R. Langevin, Rhode Island
Charlie Dent, Pennsylvania           Kendrick B. Meek, Florida
Ginny Brown-Waite, Florida

                                 ______

 SUBCOMMITTEE ON INTELLIGENCE, INFORMATION SHARING, AND TERRORISM RISK 
                               ASSESSMENT

                   Rob Simmons, Connecticut, Chairman

Curt Weldon, Pennsylvania            Zoe Lofgren, California
Mark E. Souder, Indiana              Loretta Sanchez, California
Daniel E. Lungren, California        Jane Harman, California
Jim Gibbons, Nevada                  Nita M. Lowey, New York
Stevan Pearce, New Mexico            Sheila Jackson-Lee, Texas
Bobby Jindal, Louisiana              James R. Langevin, Rhode Island
Charlie Dent, Pennsylvania           Kendrick B. Meek, Florida
Ginny Brown-Waite, Florida           Bennie G. Thompson, Mississippi 
Peter T. King, New York (Ex          (Ex Officio)
Officio)



                                  (II)

</pre></body></html>
