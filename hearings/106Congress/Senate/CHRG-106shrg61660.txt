<html>
<title> - NATIVE HAWAIIAN EDUCATION REAUTHORIZATION</title>
<body><pre>
[Senate Hearing 106-385]
[From the U.S. Government Printing Office]


                                                 S. Hrg. 106-385, Pt. 4
 
                            NATIVE HAWAIIAN EDUCATION
                                 REAUTHORIZATION

=======================================================================

                                HEARING

                               BEFORE THE

                         COMMITTEE ON INDIAN AFFAIRS
                            UNITED STATES SENATE

                          ONE HUNDRED SIXTH CONGRESS
    
                                FIRST SESSION

                                     ON

                                 S. 1767

           TO AMEND THE ELEMENTARY AND SECONDARY EDUCATION ACT OF  
              1965 TO IMPROVE NATIVE HAWAIIAN EDUCATION PROGRAMS 

                               __________

                               DECEMBER 1, 1999
                              WAILUKU, MAUI, HI

                               __________
                              
                                 PART 4
                               __________


                      U.S. GOVERNMENT PRINTING OFFICE
61-660 CC                    WASHINGTON  :  2000
---------------------------------------------------------------------
                 For sale by the U.S. Government Printing Office 
            Superintendent of Documents, Congressional Sales Office, 
                             Washington, DC 20402


[TEXT NOT AVAILABLE REFER TO PDF]
</pre></body></html>
