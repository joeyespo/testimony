<html>
<title> - OVERSIGHT HEARING ON THE DEPARTMENT OF VETERANS AFFAIRS LIFE INSURANCE PROGRAM </title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]


 
                OVERSIGHT HEARING ON THE DEPARTMENT OF
                VETERANS AFFAIRS LIFE INSURANCE PROGRAM
==============================================================================

                                 HEARING

                                before the

                         SUBCOMMITTEE ON BENEFITS


                                  of the

                      COMMITTEE ON VETERANS' AFFAIRS

                         HOUSE OF REPRESENTATIVES

                       one hundred eighth congress

                               first session

                                  -------

                             September 25, 2003

                                  -------

          Printed for the use of the Committee on Veterans' Affairs

                              Serial No. 108-23


                                    -----

                       U.S. GOVERNMENT PRINTING OFFICE

95-308pdf                     WASHINGTON : 2004
-----------------------------------------------------------------------------
For the sale by the Superintendent of Documents, U.S. Government Printing Office  Internet: bookstore.gpo.gov  Phone: toll-free (866) 512-1800; DC area (202) 512-1800 Fax: (202) 512-2250  Mail: Stop SSOP, Washington, DC 20402-0001

<TEXT NOT AVAILABLE IN TIFF FORMAT>


</pre></body></html>
