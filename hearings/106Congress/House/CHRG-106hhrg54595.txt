<html>
<title> - DEPARTMENT OF TRANSPORTATION AND RELATED AGENCIES APPROPRIATIONS FOR 2000</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]



 
   DEPARTMENT OF TRANSPORTATION AND RELATED AGENCIES APPROPRIATIONS
                                FOR 2000

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS
                              FIRST SESSION
                                ________

 SUBCOMMITTEE ON THE DEPARTMENT OF TRANSPORTATION AND RELATED AGENCIES 
                             APPROPRIATIONS
                    FRANK R. WOLF, Virginia, Chairman
 TOM DeLAY, Texas                  MARTIN OLAV SABO, Minnesota
 RALPH REGULA, Ohio                JOHN W. OLVER, Massachusetts
 HAROLD ROGERS, Kentucky           ED PASTOR, Arizona
 RON PACKARD, California           CAROLYN C. KILPATRICK, Michigan
 SONNY CALLAHAN, Alabama           JOSE E. SERRANO, New York
 TODD TIAHRT, Kansas               JAMES E. CLYBURN, South Carolina
 ROBERT B. ADERHOLT, Alabama
 KAY GRANGER, Texas                 

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
 John T. Blazey II, Richard E. Efford, Stephanie K. Gupta, and Linda J. 
                        Muir, Subcommittee Staff
                                ________

                                 PART 1
                       2000 BUDGET JUSTIFICATIONS

DEPARTMENT OF TRANSPORTATION:
                                                                   Page
   Budget in Brief and Performance Plan...........................    1
   Bureau of Transportation Statistics............................  199
   Coast Guard....................................................  281
   Federal Highway Administration.................................  681
   National Highway Traffic Safety Administration................. 1063
   Office of Inspector General.................................... 1261
   Research and Special Programs Administration................... 1433
   Saint Lawrence Seaway Development Corporation.................. 1669
   Surface Transportation Board................................... 1719
RELATED AGENCIES:
   Architectural and Transportation Barriers Compliance Board..... 1737
                                ________

         Printed for the use of the Committee on Appropriations

                       U.S. GOVERNMENT PRINTING OFFICE
54-595                         WASHINGTON : 1999


                     COMMITTEE ON APPROPRIATIONS

                 C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                          DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California                     JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois                NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky                     MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico                       JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia                     STENY H. HOYER, Maryland
 TOM DeLAY, Texas                            ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                          MARCY KAPTUR, Ohio
 RON PACKARD, California                     NANCY PELOSI, California
 SONNY CALLAHAN, Alabama                     PETER J. VISCLOSKY, Indiana
 JAMES T. WALSH, New York                    NITA M. LOWEY, New York
 CHARLES H. TAYLOR, North Carolina           JOSE E. SERRANO, New York
 DAVID L. HOBSON, Ohio                       ROSA L. DeLAURO, Connecticut
 ERNEST J. ISTOOK, Jr., Oklahoma             JAMES P. MORAN, Virginia
 HENRY BONILLA, Texas                        JOHN W. OLVER, Massachusetts
 JOE KNOLLENBERG, Michigan                   ED PASTOR, Arizona
 DAN MILLER, Florida                         CARRIE P. MEEK, Florida
 JAY DICKEY, Arkansas                        DAVID E. PRICE, North Carolina
 JACK KINGSTON, Georgia                      CHET EDWARDS, Texas
 RODNEY P. FRELINGHUYSEN, New Jersey         ROBERT E. ``BUD'' CRAMER, Jr., 
 ROGER F. WICKER, Mississippi                 Alabama
 MICHAEL P. FORBES, New York                 JAMES E. CLYBURN, South Carolina
 GEORGE R. NETHERCUTT, Jr.,                  MAURICE D. HINCHEY, New York
Washington                                   LUCILLE ROYBAL-ALLARD, California
 RANDY ``DUKE'' CUNNINGHAM,                  SAM FARR, California
California                                   JESSE L. JACKSON, Jr., Illinois
 TODD TIAHRT, Kansas                         CAROLYN C. KILPATRICK, Michigan
 ZACH WAMP, Tennessee                        ALLEN BOYD, Florida
 TOM LATHAM, Iowa
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama
 JO ANN EMERSON, Missouri
 JOHN E. SUNUNU, New Hampshire
 KAY GRANGER, Texas
 JOHN E. PETERSON, Pennsylvania     
                                    

                 James W. Dyer, Clerk and Staff Director

                                  (ii)



[The official Committee record contains additional material here.]


</pre></body></html>
