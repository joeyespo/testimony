<html>
<title> - 78-244 PDF</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]


 
78-244 PDF

                                 ______

2007


 
     H.R. 2792, THE DISABLED VETERANS SERVICE DOGS AND HEALTH CARE 
        IMPROVEMENT ACT OF 2001 AND RELATED LEGISLATIVE MATTERS

=======================================================================

                                HEARING

                               before the

                         SUBCOMMITTEE ON HEALTH

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             FIRST SESSION
                               __________

                           SEPTEMBER 6, 2001

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 107-10

                  U.S. GOVERNMENT PRINTING OFFICE
78-244                    WASHINGTON : 2002
-----------------------------------------------------------------------
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092104 Mail: Stop IDCC, Washington, DC 20402�090001

<TEXT FILE NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
