<html>
<title> - H.R. 3249, THE FEDERAL RETIREMENT COVERAGE</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
               H.R. 3249, THE FEDERAL RETIREMENT COVERAGE

                         CORRECTIONS ACT
______________________________________________________________


                               HEARING 

                              BEFORE THE

                    SUBCOMITTEE ON THE CIVIL SERVICE
                   
                                OF THE

                        COMMITTEE ON GOVERNMENT

                         REFORM AND OVERSIGHT

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION

                                   ON

                               H.R. 3249


TO PROVIDE FOR THE RECTIFICATION OF CERTAIN RETIREMENT COVERAGE ERRORS
AFFECTING FEDERAL EMPLOYEES, AND FOR OTHER PURPOSES

                               _____________

                             FEBRUARY 24, 1998

                                ___________

                             Serial No. 105-129
                                  ________


Printed for the use of the Committee on Government Reform and Oversight






                        U.S. GOVERNMENT PRINTING OFFICE

49-294                        WASHINGTON : 1998
_____________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing
Office Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800
Fax: (202) 512-2250  Mail: Stop SSOP, Washington, DC  20402-0001


                             
[TEXT NOT AVAILABLE]

</pre></body></html>
