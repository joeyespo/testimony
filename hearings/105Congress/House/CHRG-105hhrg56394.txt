<html>
<title> - COMBATING TERRORISM: IMPLEMENTATION AND STATUS OF THE DEPARTMENT OF DEFENSE DOMESTIC PREPAREDNESS PROGRAM</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
  COMBATING TERRORISM: IMPLEMENTATION AND STATUS OF THE DEPARTMENT OF 
                 DEFENSE DOMESTIC PREPAREDNESS PROGRAM 

=======================================================================




                                HEARING

                               before the

                   SUBCOMMITTEE ON NATIONAL SECURITY,
              INTERNATIONAL AFFAIRS, AND CRIMINAL JUSTICE

                                 of the

                        COMMITTEE ON GOVERNMENT
                          REFORM AND OVERSIGHT
                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION

                               __________

                            OCTOBER 2, 1998

                               __________

                           Serial No. 105-208

                               __________

Printed for the use of the Committee on Government Reform and Oversight











                 U.S. GOVERNMENT PRINTING OFFICE

56-394                 WASHINGTON : 1999
_________________________________________________________________
        For sale by the U.S. Government Printing  Office 
      Superintendent of Documents, Congressional Sales Office 
                       Washington, DC 20402
                     ISBN 0-16-059635-1

<TEXT NOT AVAILABLE>
</pre></body></html>
