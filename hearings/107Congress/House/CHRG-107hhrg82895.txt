<html>
<title> - HOMELAND SECURITY: TRACKING INTERNATIONAL STUDENTS IN HIGHER EDUCATION - PROGRESS AND ISSUES SINCE 9/11</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
HOMELAND SECURITY: TRACKING INTERNATIONAL STUDENTS IN HIGHER EDUCATION 
                   - PROGRESS AND ISSUES SINCE 9/11

=======================================================================

                                HEARING

                               before the

              SUBCOMMITTEE ON 21ST CENTURY COMPETITIVENESS

                                and the

                    SUBCOMMITTEE ON SELECT EDUCATION

                                 of the

                COMMITTEE ON EDUCATION AND THE WORKFORCE
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             SECOND SESSION

                               __________

           HEARING HELD IN WASHINGTON, DC, SEPTEMBER 24, 2002

                               __________

                           Serial No. 107-79

                               __________

  Printed for the use of the Committee on Education and the Workforce





82-895
_____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001

</pre></body></html>
