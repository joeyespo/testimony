<html>
<title> - ENERGY AND WATER DEVELOPMENT APPROPRIATIONS FOR 1999</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


[House Committee on Appropriations]
[Energy and Water Development Appropriations for 1999]
[Part 2]
[DOCID: f:46224p1.xxx.done]
 
                      ENERGY AND WATER DEVELOPMENT
                        APPROPRIATIONS FOR 1999

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS
                             SECOND SESSION
                                ________

              SUBCOMMITTEE ON ENERGY AND WATER DEVELOPMENT

                JOSEPH M. McDADE, Pennsylvania, Chairman

 HAROLD ROGERS, Kentucky               VIC FAZIO, California
 JOE KNOLLENBERG, Michigan             PETER J. VISCLOSKY, Indiana
 RODNEY P. FRELINGHUYSEN, New Jersey   CHET EDWARDS, Texas
 MIKE PARKER, Mississippi              ED PASTOR, Arizona
 SONNY CALLAHAN, Alabama
 JAY DICKEY, Arkansas

 NOTE: Under Committee Rules, Mr. Livingston, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.

       James D. Ogsbury, Jeanne L. Wilson, and Donald M. McKinnon,
                            Staff Assistants
                                ________

                                 PART 2

                 Department of Army--Corps of Engineers
                 Fiscal Year 1999 Budget Justifications
                                                                   Page
 Civil Works Budget Request.......................................    1
 Great Lakes and Ohio River Division..............................  117
 Mississippi Valley Division......................................  325
 North Atlantic Division..........................................  774
 Northwestern Division............................................ 1052
 Pacific Ocean Division........................................... 1197
 South Atlantic Division.......................................... 1257
 South Pacific Division........................................... 1424
 Southwestern Division............................................ 1712
 Remaining Items.................................................. 1862
                                ________

         Printed for the use of the Committee on Appropriations

                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
 46-224 O                   WASHINGTON : 1998
------------------------------------------------------------------------------
                   For sale by the U.S. Government Printing Office
 Superintendent of Documents, Congressional Sales Office, Washington, DC 20402
                            ISBN 0-16-056146-9





                           COMMITTEE ON APPROPRIATIONS

                       BOB LIVINGSTON, Louisiana, Chairman

 JOSEPH M. McDADE, Pennsylvania            DAVID R. OBEY, Wisconsin
 C. W. BILL YOUNG, Florida                 SIDNEY R. YATES, Illinois
 RALPH REGULA, Ohio                        LOUIS STOKES, Ohio
 JERRY LEWIS, California                   JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois              NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky                   MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico                     JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia                   VIC FAZIO, California
 TOM DeLAY, Texas                          W. G. (BILL) HEFNER, North Carolina
 JIM KOLBE, Arizona                        STENY H. HOYER, Maryland
 RON PACKARD, California                   ALAN B. MOLLOHAN, West Virginia
 SONNY CALLAHAN, Alabama                   MARCY KAPTUR, Ohio
 JAMES T. WALSH, New York                  DAVID E.  SKAGGS, Colorado
 CHARLES H. TAYLOR, North Carolina         NANCY PELOSI, California
 DAVID L. HOBSON, Ohio                     PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma           ESTEBAN EDWARD TORRES, California
 HENRY BONILLA, Texas                      NITA M. LOWEY, New York
 JOE KNOLLENBERG, Michigan                 JOSE E. SERRANO, New York
 DAN MILLER, Florida                       ROSA L. DeLAURO, Connecticut
 JAY DICKEY, Arkansas                      JAMES P. MORAN, Virginia
 JACK KINGSTON, Georgia                    JOHN W. OLVER, Massachusetts
 MIKE PARKER, Mississippi                  ED PASTOR, Arizona
 RODNEY P. FRELINGHUYSEN, New Jersey       CARRIE P. MEEK, Florida
 ROGER F. WICKER, Mississippi              DAVID E. PRICE, North Carolina
 MICHAEL P. FORBES, New York               CHET EDWARDS, Texas
 GEORGE R. NETHERCUTT, Jr., Washington   ROBERT E. (BUD) CRAMER, Jr., Alabama
 MARK W. NEUMANN, Wisconsin
 RANDY ``DUKE'' CUNNINGHAM, California
 TODD TIAHRT, Kansas
 ZACH WAMP, Tennessee
 TOM LATHAM, Iowa
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama


                 James W. Dyer, Clerk and Staff Director

                                  (ii)



      [The official Committee record contains additional material here.]
</pre></body></html>
