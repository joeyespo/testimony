<html>
<title> - NOMINATION OF JOSHUA GOTBAUM</title>
<body><pre>
[Senate Hearing 106-331]
[From the U.S. Government Printing Office]


                                                        S. Hrg. 106-331


 
                      NOMINATION OF JOSHUA GOTBAUM

=======================================================================


                                HEARING

                               BEFORE THE

                              COMMITTEE ON
                          GOVERNMENTAL AFFAIRS
                          UNITED STATES SENATE

                       ONE HUNDRED SIXTH CONGRESS

                             FIRST SESSION

                                 ON THE

 NOMINATION OF JOSHUA GOTBAUM, TO BE CONTROLLER, OFFICE OF MANAGEMENT 
                               AND BUDGET

                               __________

                            OCTOBER 28, 1999

                               __________

      Printed for the use of the Committee on Governmental Affairs


                                <snowflake>


                      U.S. GOVERNMENT PRINTING OFFICE
 61-294 cc                   WASHINGTON : 2000
------------------------------------------------------------------------------
                   For sale by the U.S. Government Printing Office
 Superintendent of Documents, Congressional Sales Office, Washington, DC 20402



                   COMMITTEE ON GOVERNMENTAL AFFAIRS

                   FRED THOMPSON, Tennessee, Chairman
WILLIAM V. ROTH, Jr., Delaware       JOSEPH I. LIEBERMAN, Connecticut
TED STEVENS, Alaska                  CARL LEVIN, Michigan
SUSAN M. COLLINS, Maine              DANIEL K. AKAKA, Hawaii
GEORGE V. VOINOVICH, Ohio            RICHARD J. DURBIN, Illinois
PETE V. DOMENICI, New Mexico         ROBERT G. TORRICELLI, New Jersey
THAD COCHRAN, Mississippi            MAX CLELAND, Georgia
ARLEN SPECTER, Pennsylvania          JOHN EDWARDS, North Carolina
JUDD GREGG, New Hampshire
             Hannah S. Sistare, Staff Director and Counsel
                        Robert J. Shea, Counsel
                      Henry R. Wray, GAO Detailee
      Joyce A. Rechtschaffen, Minority Staff Director and Counsel
          Peter A. Ludgin, Minority Professional Staff Member
                 Darla D. Cassell, Administrative Clerk



                            C O N T E N T S

                                 ------                                
Opening statements:
                                                                   Page
    Senator Thompson.............................................     1
    Senator Durbin...............................................     7

                                WITNESS
                       Thursday, October 28, 1999

Joshua Gotbaum, to be Controller, Office of Management and Budget
    Testimony....................................................     2

                                APPENDIX

    Biographical and financial information.......................    13
    Prehearing questions.........................................    19


NOMINATION OF JOSHUA GOTBAUM TO BE CONTROLLER, OFFICE OF MANAGEMENT AND 
                                 BUDGET

                              ----------                              


                       THURSDAY, OCTOBER 28, 1999

                                       U.S. Senate,
                         Committee on Governmental Affairs,
                                                    Washington, DC.
    The Committee met, pursuant to notice, at 10:07 a.m., in 
room SD-628, Dirksen Senate Office Building, Hon. Fred 
Thompson, Chairman of the Committee, presiding.
    Present: Senators Thompson and Durbin.

             OPENING STATEMENT OF CHAIRMAN THOMPSON

    Chairman Thompson. This morning, the Governmental Affairs 
Committee is holding a hearing to consider the nomination of 
Joshua Gotbaum to be Controller in the Office of Management and 
Budget. The Controller is charged with oversight of 
implementation of the Chief Financial Officers Act, a piece of 
legislation Congress passed to remedy decades of serious 
neglect in Federal financial management.
    According to the General Accounting Office, major problems 
in the government's financial systems include its inability to 
properly account for and report billions of dollars of 
property, equipment, materials, and supplies; determine the 
proper amount of various reported liabilities, including post-
retirement health benefits for military employees, accounts 
payable, and other liabilities; and actively report major 
portions of the net cost of government operations.
    The importance of the position of Controller cannot be 
overstated. Proper financial management is critical to the 
efficient operation of our Federal Government. This Controller 
will have a heavy burden to bear in moving the government 
toward greater fiscal responsibility, especially as we prepare 
for the first performance reports under the Results Act.
    Mr. Gotbaum filed responses to a biographical and financial 
questionnaire,\1\ answered pre-hearing questions submitted by 
the Committee,\2\ and had his financial statements reviewed by 
the Office of Government Ethics.
---------------------------------------------------------------------------
    \1\ The biographical and financial questionnaire appears in the 
Appendix on page 13.
    \2\ The responses to pre-hearing questions appears in the Appendix 
on page 19.
---------------------------------------------------------------------------
    Without objection, this information will be made a part of 
the hearing record, with the exception of the financial data, 
which is on file in the Committee offices.
    In addition, the hearing record will remain open for 2 
weeks.
    Our Committee rules require that all witnesses at 
nomination hearings give their testimony under oath. Mr. 
Gotbaum, would you please stand and raise your right hand? Do 
you solemnly swear to tell the truth, the whole truth, and 
nothing but the truth, so help you, God?
    Mr. Gotbaum. I do.
    Chairman Thompson. Please be seated.
    Mr. Gotbaum, do you have anyone you would like to introduce 
at this time?

   TESTIMONY OF JOSHUA GOTBAUM, TO BE CONTROLLER, OFFICE OF 
                     MANAGEMENT AND BUDGET

    Mr. Gotbaum. Thank you very much, Senator. If I might, I 
would like to introduce my wife's grandmother, Mrs. Winnifred 
Dunn.
    Chairman Thompson. How do you do?
    Mr. Gotbaum. She came up from Roanoke, Virginia.
    Chairman Thompson. Pleased to have you with us.
    Mr. Gotbaum. My wife, unfortunately, could not be here. She 
is at her father's hospital bed.
    Chairman Thompson. I understand. Well, we wish you the best 
in that regard.
    Do you have a statement to make at this time?
    Mr. Gotbaum. I have a brief statement, sir.
    Chairman Thompson. Proceed.
    Mr. Gotbaum. I am honored to have been nominated by the 
President for the position of Controller in the Office of 
Federal Financial Management at OMB. As this Committee above 
all knows, the Controller and OFFM are a quite important part 
of the architecture you set in place almost a decade ago with 
the CFO Act.
    There has been very real progress since then. The creation 
of CFOs in major agencies, the development of accounting 
standards, and agency and government-wide financial reports, 
and the beginnings of improved financial systems.
    I also should mention beyond the CFO Act the Government 
Performance and Results Act, which has encouraged the Executive 
Branch to hold itself accountable to financial as well as other 
performance standards.
    It is important that we all recognize that there is much 
yet to do. We have made real advances in financial 
accountability, but many agencies are lagging in financial 
management. Many of our financial systems are not even ``solid 
state''--they are paper--much less ``state-of-the-art.''
    Finally, all of us in the Federal Government, those who are 
doing very well and those who are doing less well, are still 
exploring the best ways to measure performance and results and 
are still looking how to incorporate those measures into 
everyday management of our programs and to our decisions about 
resources and into our plans for the future.
    I agreed to be nominated for this position because as one 
who cares intensely about management in the Federal Government, 
I hope to continue this progress. I am a realist. I know there 
are only 15 months left in this administration. Nonetheless, I 
believe that these are issues which are not partisan. I think 
they enjoy the support of people of good will throughout the 
parties and in the Congress and in the Executive Branch. And I 
think they ought to be a legacy of this administration for the 
next. And so that is the reason I hope to be there and to do 
so.
    In closing, let me say again that I am enormously grateful 
to this Committee for considering my nomination, for giving me 
this opportunity today. I look forward to answering your 
questions. I hope to earn your support and, if confirmed, the 
chance to work with you and others to advance the goals of 
better government that I think we all share.
    Chairman Thompson. Thank you very much.
    As indicated earlier, the Committee submitted some 
substantive pre-hearing questions to the nominee, and the 
nominee has also met with staff to discuss a variety of issues 
of congressional concern regarding the financial management of 
the Federal Government. Your written responses to these 
questions will be placed in the record.
    I will start my questioning with these three questions that 
we ask of all nominees. Is there anything that you are aware of 
in your background which might present a conflict of interest 
with the duties of the office to which you have been nominated?
    Mr. Gotbaum. No, sir.
    Chairman Thompson. Do you know of anything, personal or 
otherwise, that would in any way prevent you from fully and 
honorably discharging the responsibilities of Controller at the 
Office of Management and Budget?
    Mr. Gotbaum. No, sir.
    Chairman Thompson. Do you agree without reservation to 
respond to any reasonable summons to appear and testify before 
any duly constituted committee of Congress if you are 
confirmed?
    Mr. Gotbaum. Yes.
    Chairman Thompson. All right. You pointed out several of 
the statutes that Congress has enacted over the last decade: 
The Chief Financial Officers Act, the Government Management 
Reform Act, and the Federal Financial Management Improvement 
Act. If our progress could be measured in terms of pieces of 
legislation that have been enacted, we would be in great shape.
    Each of these management reforms seeks to improve the 
financial management of the Federal Government, and this has 
been something that GAO has identified for a long time as being 
at the heart of the management problem. Up until fairly 
recently, there was very little in terms of accountability, 
very little in terms of determining what it costs to produce 
what we produce in the government.
    In one of its most recent reports on the government's 
implementation of the Federal Financial Management Improvements 
Act, GAO wrote this: ``The historic inability of many Federal 
agencies to accurately record and report financial management 
data on both a year-end and an ongoing basis for decisionmaking 
and oversight purposes continues to be a serious weakness. 
There has been little discernible progress since last year.''
    Mr. Gotbaum, you are in a position to effect greater 
progress in this area. In your view, first of all, are the 
staff resources dedicated to financial management issues 
sufficient for OMB to identify and correct systemic problems, 
improve government-wide financial management practices, and 
implement these statutory requirements?
    Mr. Gotbaum. It is a fair question, Senator. Let me say 
first that I have read, obviously in preparation for this 
position, the GAO report and met and sat with not only the 
folks in OMB but the various chief financial officers and met 
with the IGs, etc. I think the picture presented by GAO is an 
accurate mixed picture, meaning we clearly have made real 
progress, and it would be unfair to the people who have worked 
on it not to recognize the real progress. It is also entirely 
accurate to say that there is a long way to go.
    And I can't tell you that I know, not having gotten in 
there yet, that we have the exact right level of resources. I 
think it is important to say, however, that--because I have 
been at OMB for a couple of years--that the way the 
organization works (because it really is a matrix organization) 
if it does its job well, is to leverage the resources of all of 
OMB.
    And so, from my perspective, Senator, the issue is: Can and 
does and will OFFM and the folks in the management side of OMB 
leverage the resources, leverage the expertise, leverage the 
contacts, and leverage the clout of the divisions (the so-
called Resource Management Organizations, or ``RMO's'') that 
have individual staff members assigned to individual agencies? 
In some areas, I know that they have done that and have done 
that well.
    When the Health Care Financing Administration a couple of 
years ago came forward and said ``We are in trouble, we know we 
are in trouble, we are not sure that we can comply with Y2K, 
and it is going to take both additional resources and effort,'' 
the folks from the management side of OMB and the resource side 
working together. While no one would say that we are out of 
that woods yet, it is clear that there has been a lot of 
effort.
    So I think the critical thing here, Senator, is that the 
organization be active, that it leverage the rest of OMB, that 
it leverage the CFOs' Council. One of the things that I have 
got to say, Senator, that is enormously encouraging is to go to 
a CFO Council meeting, because agency after agency after 
agency, these are folks who are recognizing that they have 
common problems. They have common challenges and common issues, 
and they are working together to solve them.
    And as you know, Senator, there are lots of places in the 
Federal Government where agency after agency after agency faces 
the same issue and doesn't know it; doesn't look to anybody 
else for help and doesn't look to anybody else for advice. And 
so what's really encouraging about the CFO Council is that it 
is a real mechanism for solving common problems together.
    And so what I hope would happen, if confirmed, is that 
using these resources and setting up some kind of standards and 
reporting for progress, those two things together ought to 
provide the encouragement we need to make the progress we need. 
I think the GAO is accurate in their report. But if it takes a 
periodic GAO report to figure out when we are or are not up to 
snuff, then we are not going to be up to snuff, because one of 
the basic tenets of Management 101 is if you don't measure it, 
you can't improve it. And so that is what----
    Chairman Thompson. That is what the Results Act is supposed 
to do for us.
    Mr. Gotbaum. Yes, sir, it is.
    Chairman Thompson. Ultimately. And it has taken a good 
while to get there. These reports are coming in, the 
performance plans and so forth, and they are not up to snuff. 
But you are going to be in the center of this, and that means 
that you are going to be in a key position to do something 
about it, because it is really--we talk too much in terms of 
crises around here, but it looks to me like that we are facing 
one in terms of the way the government operates.
    We are arguing now over a 1 percent across the board or 1.4 
percent across the board, and trying to fill a $4 billion hole. 
GAO has identified over $200 billion of waste and fraud, and 
pointing out to these agencies, in pretty specific terms, where 
we are losing money.
    The Navy has identified--is it $300 billion?--$3 billion 
that they identified as lost in transit. So, you know, we are 
trying to get a handle on that, and one of the reasons it has 
been so difficult to get a handle on it has been because of the 
inadequacies in our financial system.
    Now we have passed the Results Act, and I think as most 
everyone knows, what we are trying to do there is identify what 
our goals are in government. What are we really trying to do? 
Not churn paper, but what results are we really trying to 
achieve for the citizens, and then figuring out some way to 
measure that. What are the results of what we are doing?
    But that is all based on adequate financial data, and the 
GAO tells us we have lousy financial data. And so that is at 
the heart of it.
    Much of the data required by the Results Act comes from 
financial systems that are in place, and according to GAO, many 
of the government's financial accounting systems are badly 
flawed.
    They wrote recently that: Agencies do not have a single 
integrated financial system to rely on and they rely on ad hoc 
programming and analysis of data that is not reconciled and 
often requires adjustments. As a result, the risk of material 
misstatements increases and reliable data cannot be produced in 
a timely and efficient manner. This is in the case of most 
Federal agencies.
    According to the GAO, most agencies' fiscal year 2000 
performance plans suffer from the same three key weaknesses as 
their fiscal year 1999 plans, one of which was the lack of 
credible performance data. In fact, GAO found that the plans of 
20 out of 24 major agencies provide little confidence that 
their performance data will be credible.
    So I would ask you what steps will you take to ensure that 
we have a good foundation of performance data with which to 
proceed in implementing performance-based management.
    Mr. Gotbaum. This is a very important question. It is one I 
got a chance to discuss with your staff in the interview.
    There is a part, Senator, where I agree with and a part 
where, I have got to be honest, I disagree with the emphasis in 
the GAO's report. We all think that the Results Act is an 
incredibly important issue. We also recognize--and we are 
pretty honest about it--that we are in the early stages of it. 
There is a range: Some agencies do, in my view, an extremely 
good job. Some agencies do a job that I wouldn't want to have 
to defend. Most agencies are in the middle and are working at 
this seriously but aren't there yet.
    I want to draw an important distinction because I think it 
matters to this Committee on an ongoing basis: For many 
agencies, they don't yet know what are the right measures of 
performance. GAO is raising a second issue--which is a real 
issue, but it is a second issue--which is that they also don't 
have established systems and the kind of audit trail that you 
would need to ascertain the validity of those performance 
measures.
    Because we are still in the developmental stages of GPRA, 
because we are still at the point where we are trying to nudge 
agencies to think about what kind of information--(what kind of 
performance information to use for our grants program versus an 
operating program, what kind of performance information to use 
for procurement rather than other kinds of systems, etc.)--the 
danger is that we could freeze agency decisions too early.
    If agencies get the sense that the GAO and us and you care 
more about the verifiability of performance data, they are 
going to take easy hits. They are going to start picking the 
easily quantifiable stuff: ``How many grant applications I have 
processed?'' rather than ``What is my average time,'' or ``How 
satisfied are the recipients of my grants over the service they 
got?'' This is not to say that GAO shouldn't push and you 
shouldn't push on the issue of verifiability of financial 
systems. There is plenty of room for improvement there. But 
what I would hope is that you also push on the broader first 
job of GPRA, which is to say to agencies: Are you using the 
best performance measures you can? Are they performance 
measures that are really suited to what your job is?
    I got to say that in the 2 years that we have had the 
Performance Act, there has been real improvement. We are a long 
way from Valhalla, but there has been real improvement. And 
what that says to me personally is that we can make more 
progress.
    Chairman Thompson. I was talking to some people yesterday 
about--they were talking about, as one of their measurable 
criteria, the number of reports they had produced. And I said, 
``What if they are lousy reports?''
    Mr. Gotbaum. Yes, sir.
    Chairman Thompson. See, that is what we have got to get to. 
That is what you are talking about, isn't it?
    Mr. Gotbaum. Yes. And I will give you another for instance. 
This is a case where OMB was later to the game than I wish we 
had been.
    Each year we send a huge set of instructions to the Federal 
agencies saying these are the rules by which you should prepare 
your budget reports. It is called Circular A11. And this year 
for the first time, we said to agencies: In addition to your 
GPRA reports, you got to start integrating your performance 
information into your budget justifications, into your budget 
submissions. Now, we have suggested this in the past, but now 
it is in words of one sentence--in words of a few syllables, 
directly in the A-11.
    Now, I am a realist. Do I think that as a result of that 
change 4 months ago that all of a sudden every agency's budget 
submission is now going to be rife with the kind of performance 
data that you would like and we would like? No, sir, I don't. 
But I do think it can--and if we enforce it aggressively--step 
up the quality.
    Chairman Thompson. Oh, I think that is very important. I 
think that one of the big things that we have been lacking is 
some connection between performance and budget. And we have got 
to do better in Congress. We have these hearings and identify 
these problems, and these agencies waste/lose millions, 
sometimes billions of dollars. Down the hall they are having 
some kind of appropriations process that hardly takes it into 
account. But it has got to be a combination of Congress plus 
the OMB.
    Most of the attention is on the ``B'' part over there 
because people don't credit for the ``M'' much unless there is 
some easily understood number like fewer employees, government 
employees, or something like that. When you look at it, first 
of all, most of them are military and, second of all, we are 
outsourcing stuff. So it is not costing the government any 
less. There are a few less bodies on the full-time payroll.
    So you have got a real problem, but we understand, I think, 
the nature of that and how difficult it is, but how important 
the job that you are going to take on is, because you are the 
guys who are supposed to be managing, seeing that the agencies 
do what they are supposed to do. They clearly have not been, 
and a key part of that problem is the financial management 
problem, and that is where you are going to be. So it looks to 
me like you understand that and you are going to come in with 
some fresh energy and maybe some fresh ideas as to how to break 
through, and we look forward to working with you on that.
    Mr. Gotbaum. Thank you, Sir.
    Chairman Thompson. Senator Durbin.

              OPENING STATEMENT OF SENATOR DURBIN

    Senator Durbin. Thank you very much, Mr. Chairman.
    Mr. Gotbaum, thank you for being here today. I would like 
to just ask you basically two questions.
    The first relates to a friend of mine who had an experience 
back in Springfield, Illinois. He is an old buddy. He had a 
heart problem. And so he went to one doctor, and this doctor 
said, ``You are going to have to stop drinking beer,'' which 
was a big change in his life-style. So I said to him, ``What 
did you do?'' And he said, ``I got another doctor.'' And I 
said, ``What did that doctor tell you?'' He said, ``Well, if I 
gave up bread, I could keep drinking beer.'' And he said, ``I 
haven't touched a slice of bread in weeks.''
    We seem to have a similar thing going on here when it comes 
to the Congressional Budget Office and OMB and the budget 
process. It appears that those of us on both sides of the aisle 
here pick and choose from projections and forecasts from OMB 
and CBO when they help our case. I found that yesterday. We 
were in a markup, a conference committee on the Labor-HHS bill, 
and some moments the Chairman would be quoting CBO dogma and 
other moments OMB dogma when the occasion presented itself.
    Do you have any perspective on this role of the dueling 
agencies and whether or not this is healthy or whether there is 
any objective standard we can use to say here is credibility, 
here is partisanship? Where can we have a credible line drawn?
    Mr. Gotbaum. Well, Senator, since there are members of the 
OMB staff here and I am from the administration, of course, I 
should say that the OMB numbers are right. But let me be more 
direct.
    Chairman Thompson. They have been quoting CBO numbers 
lately. [Laughter.]
    Senator Durbin. We are all guilty of this.
    Mr. Gotbaum. I told you the partisanship in here was much 
more complicated than I could handle.
    I have participated in this as Executive Associate Director 
of OMB for a couple of years and watched it from other 
positions. I got to say, Senator--and this is a personal view--
these are two independent, very professional organizations--
even when I disagree with CBO or when I disagree with OMB 
staff, I got to say these are terrifically competent folks, 
very professional, very dedicated. I think the fact that there 
are two--that they are independent, that they are watching each 
other, that they are calling each other's fouls--probably at 
the end of the day is more helpful to the process than if you 
didn't have that.
    I think the OMB staff is fantastic; in a government that is 
full of very good civil servants, I think it is the best group 
of civil servants I have ever seen. However, it is also the 
case that because we are from the administration, even though 
we are professional, hard-working, dedicated, and try to call 
them as we see them, the folks up here in Congress are always 
going to have that nagging doubt.
    And so I think the CBO has made a real contribution. That 
doesn't mean, Senator, that we won't have differences on the 
margin. It doesn't mean we won't have differences on some call. 
We will and we do, etc. But I think given what you get for 
having a second opinion that is of quality, I think we are all 
a little better off. And that doesn't mean that I wouldn't be 
grateful if my former colleague, Barry Anderson, and Dan 
Crippen occasionally didn't lean our way, but the fact of the 
matter is the process works better than if we didn't have them.
    The other thing I should say, Senator is that one of the 
things that happens from the legislative process here is that 
you all focus necessarily on the disagreements. You focus on 
the cases where we and they disagree, or you focus on the cases 
where Democrats and Republicans disagree or the House and the 
Senate disagree. The vast majority of estimates that we do and 
they do, way over 90 percent, are close together. And that is 
part of the reason why we get some confidence when we have the 
disagreements.
    Senator Durbin. Let me address one other issue, and Senator 
Thompson has already alluded to it--the ``M'' part of OMB. You 
made a speech to the National Academy of Public Administration 
last summer and alluded to an issue of great concern to me. 
That is the issue of food safety and the multiplicity of 
Federal agencies, 12 different Federal agencies with 
jurisdiction over the safety of food in the United States, 35 
different laws, clearly duplication, overlap, and waste taking 
place.
    This is something that this Committee, the Governmental 
Affairs Committee, addressed over 22 years ago and said we have 
got to do something about that in a hurry--22 years ago. And 
the obvious conclusion is we haven't done much.
    I am just curious as to when it comes to the role of OMB 
and talking about this kind of duplication at the Federal 
level, where we have clearly mired ourselves down into a tangle 
of jurisdictional fights downtown, jurisdictional fights on the 
Hill, jurisdictional fights in the industry, what role can the 
voice of OMB play in changing this?
    Mr. Gotbaum. Senator, this is another example of the point 
I made to Senator Thompson. OMB has a very difficult, 
frequently misunderstood, and frequently painful role, which is 
it is our job, on behalf of the President, to reconcile 
disagreements, to force agencies to look beyond their 
stovepipes as best we can, and in some cases just keep score, 
make sure that we are doing this stuff.
    There is a young woman at OMB who actually was my special 
assistant for a while, a woman named Wendy Taylor. She is 30 
years old. She is from Lawrence, Kansas. She is as smart as my 
mother thinks I am. And she is in OIRA, the Information and 
Regulatory Affairs shop of OMB.
    She worked with the folks on the agriculture branch, the 
health branch, etc., Every couple of years we do go back and 
look at cross-cutting issues like food safety (we always look 
at cross-cutting issues, but which cross-cutting issue we look 
at changes over time). And so within the administration, we 
forced an inter-agency discussion.
    Now, we also work in the world of the real. There are still 
multiple agencies that do this stuff but that doesn't mean that 
we are not watching, working, and trying to make improvements. 
And let me just mention one that I--because Wendy was working 
for me at the time--participated in. We said, ``All right, 
maybe we are not going to consolidate all these agencies for 
internal reasons or legislative reasons. But we can certainly 
force them to talk to each other about their own research 
budgets. We can make sure that in the area that is most likely 
for there to be overlap, `Guys, you ought to sit down and we 
ought to have a single, coordinated research budget in this 
area.' ''
    And so we created a food safety research institute. It is 
in early days, it has been in operation for a year. But it is 
getting folks to the table, forcing them to have what I think 
of as an integrated agenda for research, making sure that the 
funds we do have, whatever they are, are used sensibly. It is 
an example of where and how OMB works.
    We do this a lot. Personally, I have spent a lot of time as 
Executive Associate Director in those issues which cross 
stovepipes, so counterterrorism and dealing with weapons of 
mass destruction because it covers the Justice Department and 
the Defense Department and Health and Human Services, etc., is 
something which I watch over. That is another example.
    Federal agencies, they are created, they have their 
histories, they have their skills, they have their people. And 
sometimes you can and should make wholesale changes, but you 
ought to recognize that there are real costs to making them. 
Even if we don't go that far, we nonetheless ask, ``Are we 
allocating the Nation's resources in some way that makes sense 
among them?'' When we were talking about who trains local 
governments to prepare for the possible terrorist incidents we 
had a discussion and we said, ``DOD could do it, FBI could do 
it, HHS could do it, but obviously you can't have all three.''
    This is the bread and butter business of OMB. It is 
something we do a lot.
    Senator Durbin. The real responsibility, of course, lies 
here on Capitol Hill, and perhaps at the initiative of the 
President, to change the laws to solve the problem. And at 
least you have to say that the conversation, the dialogue that 
you have discussed has to be positive. The only bad thing I 
know that came out of it was when one member of the Cabinet 
referred to it as ``a virtual unified food agency.'' I took 
that as in virtual reality, which is not reality but appears to 
be. And so that choice of words, I think, left something to be 
desired, but I thank you for your testimony, and I certainly 
support your nomination.
    Thank you, Mr. Chairman.
    Chairman Thompson. Thank you very much.
    I couldn't let this opportunity pass without once again 
referring to basic problems that we are dealing with here. GAO 
and agency IGs have identified about 300 major management 
problems for the 24 agencies collectively. This includes the 26 
problems on the GAO's current high-risk list. Agencies on the 
high-risk list specifically because of poor financial 
management include the Department of Defense, the Forest 
Service, Federal Aviation Administration, and the Internal 
Revenue Service.
    There are over 700 open GAO recommendations addressing 
high-risk problems alone, and another 450 open GAO 
recommendations on other major management problems. There are 
hundreds more open IG recommendations for the 300 management 
problems.
    Now, that is not to say that all the recommendations are 
even good ones or valid, but that is an awful lot of stuff out 
there that there seems to be not much happening on because so 
many of these agencies continue to appear on the high-risk list 
over and over and over again.
    Collectively, there are over 1,000 open, unresolved GAO and 
IG audit recommendations. Many of these major management 
problems, of course, relate to financial management.
    So this is just the scope, just reminding you of something 
that I know you know by now, the scope of the problem and what 
you are going to have to deal with. But I know you are 
committed to do that. You have an excellent background 
educationally and in terms of your work in the private sector 
and your government work, and I commend you for taking this on. 
And please work with us and give us your ideas and let us work 
together to see if we can't begin to address some of these 
things.
    You are right. A lot is going on. A lot of good work is 
being done by a lot of good people. But it is not our job to 
get together and congratulate each other on what we have done. 
It is our job to do better, because we are not doing as well as 
if we were in the private sector and had to be accountable. So 
we have got to move toward that.
    If you have nothing further, then I have nothing further, 
and we will try to move this nomination along as rapidly as 
possible.
    Mr. Gotbaum. Senator, let me just say thank you again. This 
is not to minimize the issues that you raise, because they are 
real and they are important. I think that in order for us to 
make progress, we need a combination of carrot and stick and 
reporting in the light and tough talk, privately, in the dark. 
I certainly don't want to dissemble under oath and to the 
chairman of my committee, so I am not going to tell you that we 
are going to solve all of this, but I can tell you that we are 
going to work at it seriously. We are going to work on the 
large ones that we can make progress on, and I look forward, if 
you all confirm me, to working with you in trying to get 
something accomplished.
    Thank you.
    Chairman Thompson. I appreciate that, and thank you very 
much.
    We are adjourned.
    [Whereupon, at 10:45 a.m., the Committee was adjourned.]


                            A P P E N D I X

                              ----------                              

[GRAPHIC] [TIFF OMITTED] T1294.001

[GRAPHIC] [TIFF OMITTED] T1294.002

[GRAPHIC] [TIFF OMITTED] T1294.003

[GRAPHIC] [TIFF OMITTED] T1294.004

[GRAPHIC] [TIFF OMITTED] T1294.005

[GRAPHIC] [TIFF OMITTED] T1294.006

[GRAPHIC] [TIFF OMITTED] T1294.007

[GRAPHIC] [TIFF OMITTED] T1294.008

[GRAPHIC] [TIFF OMITTED] T1294.009

[GRAPHIC] [TIFF OMITTED] T1294.010

[GRAPHIC] [TIFF OMITTED] T1294.011

[GRAPHIC] [TIFF OMITTED] T1294.012

[GRAPHIC] [TIFF OMITTED] T1294.013

[GRAPHIC] [TIFF OMITTED] T1294.014

[GRAPHIC] [TIFF OMITTED] T1294.015

[GRAPHIC] [TIFF OMITTED] T1294.016

[GRAPHIC] [TIFF OMITTED] T1294.017

[GRAPHIC] [TIFF OMITTED] T1294.018

                                  <all>

</pre></body></html>
