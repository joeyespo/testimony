<html>
<title> - STATE, FOREIGN OPERATIONS, AND RELATED PROGRAMS APPROPRIATIONS FOR 2009</title>
<body><pre>
[House Hearing, 110 Congress]
[From the U.S. Government Printing Office]


 
STATE, FOREIGN OPERATIONS, AND RELATED PROGRAMS APPROPRIATIONS FOR 2009

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED TENTH CONGRESS
                             SECOND SESSION
                                ________

     SUBCOMMITTEE ON STATE, FOREIGN OPERATIONS, AND RELATED PROGRAMS
                   NITA M. LOWEY, New York, Chairwoman
 JESSE L. JACKSON, Jr., Illinois    FRANK R. WOLF, Virginia
 ADAM SCHIFF, California            JOE KNOLLENBERG, Michigan
 STEVE ISRAEL, New York             MARK STEVEN KIRK, Illinois
 BEN CHANDLER, Kentucky             ANDER CRENSHAW, Florida
 STEVEN R. ROTHMAN, New Jersey      DAVE WELDON, Florida       
 BARBARA LEE, California            
 BETTY McCOLLUM, Minnesota          
                                    

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
    Nisha Desai, Craig Higgins, Steve Marchese, Michele Sumilas, and 
                            Clelia Alvarado,
                            Staff Assistants

                                ________

                                 PART 4

                     STATEMENTS OF OUTSIDE WITNESSES

                                   S

                                ________

         Printed for the use of the Committee on Appropriations
PART 4--STATE, FOREIGN OPERATIONS, AND RELATED PROGRAMS APPROPRIATIONS 
                                FOR 2009

STATE, FOREIGN OPERATIONS, AND RELATED PROGRAMS APPROPRIATIONS FOR 2009

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED TENTH CONGRESS
                             SECOND SESSION
                                ________
     SUBCOMMITTEE ON STATE, FOREIGN OPERATIONS, AND RELATED PROGRAMS
                   NITA M. LOWEY, New York, Chairwoman
 JESSE L. JACKSON, Jr., Illinois    FRANK R. WOLF, Virginia
 ADAM SCHIFF, California            JOE KNOLLENBERG, Michigan
 STEVE ISRAEL, New York             MARK STEVEN KIRK, Illinois
 BEN CHANDLER, Kentucky             ANDER CRENSHAW, Florida
 STEVEN R. ROTHMAN, New Jersey      DAVE WELDON, Florida       
 BARBARA LEE, California            
 BETTY McCOLLUM, Minnesota          

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
    Nisha Desai, Craig Higgins, Steve Marchese, Michele Sumilas, and 
                            Clelia Alvarado,
                            Staff Assistants

                                ________

                                 PART 4

                     STATEMENTS OF OUTSIDE WITNESSES

                                   S

                                ________

         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 48-574                     WASHINGTON : 2009

                                  COMMITTEE ON APPROPRIATIONS

                   DAVID R. OBEY, Wisconsin, Chairman
 
 JOHN P. MURTHA, Pennsylvania       JERRY LEWIS, California
 NORMAN D. DICKS, Washington        C. W. BILL YOUNG, Florida
 ALAN B. MOLLOHAN, West Virginia    RALPH REGULA, Ohio
 MARCY KAPTUR, Ohio                 HAROLD ROGERS, Kentucky
 PETER J. VISCLOSKY, Indiana        FRANK R. WOLF, Virginia
 NITA M. LOWEY, New York            JAMES T. WALSH, New York
 JOSE E. SERRANO, New York          DAVID L. HOBSON, Ohio
 ROSA L. DeLAURO, Connecticut       JOE KNOLLENBERG, Michigan
 JAMES P. MORAN, Virginia           JACK KINGSTON, Georgia
 JOHN W. OLVER, Massachusetts       RODNEY P. FRELINGHUYSEN, New Jersey
 ED PASTOR, Arizona                 TODD TIAHRT, Kansas
 DAVID E. PRICE, North Carolina     ZACH WAMP, Tennessee
 CHET EDWARDS, Texas                TOM LATHAM, Iowa
 ROBERT E. ``BUD'' CRAMER, Jr.,     ROBERT B. ADERHOLT, Alabama
Alabama                             JO ANN EMERSON, Missouri
 PATRICK J. KENNEDY, Rhode Island   KAY GRANGER, Texas
 MAURICE D. HINCHEY, New York       JOHN E. PETERSON, Pennsylvania
 LUCILLE ROYBAL-ALLARD, California  VIRGIL H. GOODE, Jr., Virginia
 SAM FARR, California               RAY LaHOOD, Illinois
 JESSE L. JACKSON, Jr., Illinois    DAVE WELDON, Florida
 CAROLYN C. KILPATRICK, Michigan    MICHAEL K. SIMPSON, Idaho
 ALLEN BOYD, Florida                JOHN ABNEY CULBERSON, Texas
 CHAKA FATTAH, Pennsylvania         MARK STEVEN KIRK, Illinois
 STEVEN R. ROTHMAN, New Jersey      ANDER CRENSHAW, Florida
 SANFORD D. BISHOP, Jr., Georgia    DENNIS R. REHBERG, Montana
 MARION BERRY, Arkansas             JOHN R. CARTER, Texas
 BARBARA LEE, California            RODNEY ALEXANDER, Louisiana
 TOM UDALL, New Mexico              KEN CALVERT, California
 ADAM SCHIFF, California            JO BONNER, Alabama                 
 MICHAEL HONDA, California          
 BETTY McCOLLUM, Minnesota          
 STEVE ISRAEL, New York             
 TIM RYAN, Ohio                     
 C.A. ``DUTCH'' RUPPERSBERGER,      
Maryland                            
 BEN CHANDLER, Kentucky             
 DEBBIE WASSERMAN SCHULTZ, Florida  
 CIRO RODRIGUEZ, Texas              
                                    

                  Rob Nabors, Clerk and Staff Director

               (ii)

<GRAPHICS NOT AVAILABLE IN TIFF FORMAT>


                           W I T N E S S E S

                              ----------                              
                                                                   Page
Ardouny, Bryan...................................................   102
Baguirov, Adil...................................................   107
Bereuter, Douglas................................................     7
Bush, D.L., Sr...................................................    57
Carey, R.J.......................................................   200
Chazottes, Elizabeth.............................................     1
Collins, J.F.....................................................    31
Davidson, D.E....................................................    36
Hastings, A.L....................................................    14
Huseynov, Javid..................................................   170
Kripke, Gawain...................................................   211
Lacy, James......................................................   188
Larigakis, Nick..................................................   207
Lawrence, Dr. L.R., Jr...........................................   182
Limon, Lavinia...................................................   204
Loucq, Christian.................................................    50
McGuire, D.J.....................................................    43
Moseley, S.F.....................................................    69
Neukom, W.H......................................................    74
Panosian, Claire.................................................   196
Spahn, Kathy.....................................................    23
Stern, C.M.......................................................    17
Vanderslice, Lane................................................    64
Volk, Joe........................................................   192
Weill, S.I.......................................................    95
Williams, Nat....................................................   114

                                  <all>
</pre></body></html>
