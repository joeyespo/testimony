<html>
<title> - PENSION SECURITY: DEPARTMENT OF LABOR [DOL] ENFORCEMENT OF THE EMPLOYEE RE- TIREMENT INCOME SECURITY ACT [ERISA] AND THE LIMITED SCOPE AUDIT EXEMPTION </title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                  PENSION SECURITY: DEPARTMENT OF LABOR
                  [DOL] ENFORCEMENT OF THE EMPLOYEE RE-
                TIREMENT INCOME SECURITY ACT [ERISA] AND
                    THE LIMITED SCOPE AUDIT EXEMPTION
=======================================================================

                                HEARING

                               before the

                    SUBCOMMITTEE ON HUMAN RESOURCES

                                  of the

                        COMMITTEE ON GOVERNMENT 
                          REFORM AND OVERSIGHT

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION

                               __________

                           FEBRUARY 12, 1998

                               __________

                           Serial No. 105-127

                               __________

   Printed for the use of the Committee on Government Reform and Oversight


                    U.S. GOVERNMENT PRINTING OFFICE
49-282                      WASHINGTON : 1998
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092250 Mail: Stop SSOP, Washington, DC 20402�090001

<TEXT FILE NOT AVAILABLE>

</pre></body></html>
