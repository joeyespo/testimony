<html>
<title> - ENERGY AND WATER DEVELOPMENT APPROPRIATIONS FOR 2011</title>
<body><pre>
[House Hearing, 111 Congress]
[From the U.S. Government Printing Office]




                      ENERGY AND WATER DEVELOPMENT

                        APPROPRIATIONS FOR 2011

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED ELEVENTH CONGRESS

                             SECOND SESSION
                                ________

              SUBCOMMITTEE ON ENERGY AND WATER DEVELOPMENT

                  PETER J. VISCLOSKY, Indiana, Chairman

 CHET EDWARDS, Texas             RODNEY P. FRELINGHUYSEN, 
 ED PASTOR, Arizona                New Jersey
 MARION BERRY, Arkansas          ZACH WAMP, Tennessee
 CHAKA FATTAH, Pennsylvania      MICHAEL K. SIMPSON, Idaho
 STEVE ISRAEL, New York          DENNIS R. REHBERG, Montana
 TIM RYAN, Ohio                  KEN CALVERT, California
 JOHN W. OLVER, Massachusetts    RODNEY ALEXANDER, Louisiana
 LINCOLN DAVIS, Tennessee        
 JOHN T. SALAZAR, Colorado       

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
              Taunja Berquam, Robert Sherman, Joseph Levin,
            James Windle, and Casey Pearce, Staff Assistants

                                ________

                                 PART 2

          Department of Energy Fiscal Year 2011 Justifications



<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>


                                ________

         Printed for the use of the Committee on Appropriations






 
                      ENERGY AND WATER DEVELOPMENT
                        APPROPRIATIONS FOR 2011

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED ELEVENTH CONGRESS
                             SECOND SESSION
                                ________
              SUBCOMMITTEE ON ENERGY AND WATER DEVELOPMENT
                  PETER J. VISCLOSKY, Indiana, Chairman
 CHET EDWARDS, Texas               RODNEY P. FRELINGHUYSEN, 
 ED PASTOR, Arizona                  New Jersey 
 MARION BERRY, Arkansas            ZACH WAMP, Tennessee
 CHAKA FATTAH, Pennsylvania        MICHAEL K. SIMPSON, Idaho
 STEVE ISRAEL, New York            DENNIS R. REHBERG, Montana
 TIM RYAN, Ohio                    KEN CALVERT, California
 JOHN W. OLVER, Massachusetts      RODNEY ALEXANDER, Louisiana 
 LINCOLN DAVIS, Tennessee          
 JOHN T. SALAZAR, Colorado         

 NOTE: Under Committee Rules, Mr. Obey, as Chairman of the Full 
Committee, and Mr. Lewis, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
              Taunja Berquam, Robert Sherman, Joseph Levin,
            James Windle, and Casey Pearce, Staff Assistants

                                ________

                                 PART 2
          Department of Energy Fiscal Year 2011 Justifications


<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>


                                ________

         Printed for the use of the Committee on Appropriations
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE

 54-928                     WASHINGTON : 2010











                                  COMMITTEE ON APPROPRIATIONS

                   DAVID R. OBEY, Wisconsin, Chairman

 NORMAN D. DICKS, Washington            JERRY LEWIS, California
 ALAN B. MOLLOHAN, West Virginia        C. W. BILL YOUNG, Florida
 MARCY KAPTUR, Ohio                     HAROLD ROGERS, Kentucky
 PETER J. VISCLOSKY, Indiana            FRANK R. WOLF, Virginia
 NITA M. LOWEY, New York                JACK KINGSTON, Georgia
 JOSE E. SERRANO, New York              RODNEY P. FRELINGHUYSEN, New   
 ROSA L. DeLAURO, Connecticut           Jersey
 JAMES P. MORAN, Virginia               TODD TIAHRT, Kansas
 JOHN W. OLVER, Massachusetts           ZACH WAMP, Tennessee
 ED PASTOR, Arizona                     TOM LATHAM, Iowa
 DAVID E. PRICE, North Carolina         ROBERT B. ADERHOLT, Alabama
 CHET EDWARDS, Texas                    JO ANN EMERSON, Missouri
 PATRICK J. KENNEDY, Rhode Island       KAY GRANGER, Texas
 MAURICE D. HINCHEY, New York           MICHAEL K. SIMPSON, Idaho
 LUCILLE ROYBAL-ALLARD, California      JOHN ABNEY CULBERSON, Texas
 SAM FARR, California                   MARK STEVEN KIRK, Illinois
 JESSE L. JACKSON, Jr., Illinois        ANDER CRENSHAW, Florida
 CAROLYN C. KILPATRICK, Michigan        DENNIS R. REHBERG, Montana
 ALLEN BOYD, Florida                    JOHN R. CARTER, Texas
 CHAKA FATTAH, Pennsylvania             RODNEY ALEXANDER, Louisiana
 STEVEN R. ROTHMAN, New Jersey          KEN CALVERT, California
 SANFORD D. BISHOP, Jr., Georgia        JO BONNER, Alabama
 MARION BERRY, Arkansas                 STEVEN C. LaTOURETTE, Ohio
 BARBARA LEE, California                TOM COLE, Oklahoma
 ADAM SCHIFF, California                
 MICHAEL HONDA, California              
 BETTY McCOLLUM, Minnesota              
 STEVE ISRAEL, New York                 
 TIM RYAN, Ohio                         
 C.A. ``DUTCH'' RUPPERSBERGER,          
Maryland                                
 BEN CHANDLER, Kentucky                 
 DEBBIE WASSERMAN SCHULTZ, Florida      
 CIRO RODRIGUEZ, Texas                  
 LINCOLN DAVIS, Tennessee               
 JOHN T. SALAZAR, Colorado              
 ------- ---------                                    

                 Beverly Pheto, Clerk and Staff Director

                                  (ii)










                             C O N T E N TS

                               __________
                                                                   Page

National Nuclear Security Administration.........................     1

Office of the Administrator......................................    17

Weapons Activities...............................................    37

Defense Nuclear Nonproliferation.................................   312

Naval Reactors...................................................   431

Other Defense Activities.........................................   567

Departmental Administration......................................   672

Inspector General................................................   773

Advanced Technology Vehicles Manufacturing Loan Program..........   787

Title 17 Innovative Technology Loan Guarantee Program............   798

Working Capital Fund.............................................   812

Energy Information Administration................................   850

Safeguards and Security Crosscut.................................   880

Domestic Utility Fee.............................................   891

Pensions.........................................................   894

Energy Efficiency and Renewable Energy...........................   922

Electricity Delivery and Energy Reliability......................  1399

Enery Transformation Acceleration Fund...........................  1490

Fossil Energy Research and Development...........................  1526

Naval Petroleum and Oil Shale Reserves...........................  1628

Strategic Petroleum Reserve......................................  1643

Northeast Home Heating Oil Reserve...............................  1665

Clean Coal Technology............................................  1673

Ultra-Deepwater Unconventional Natural Gas.......................  1675

                                 (iii)



<GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT>





</pre></body></html>
