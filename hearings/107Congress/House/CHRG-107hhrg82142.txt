<html>
<title> - COMPULSORY UNION DUES AND CORPORATE CAMPAIGNS </title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
            COMPULSORY UNION DUES AND CORPORATE CAMPAIGNS
=======================================================================

                                HEARING

                               before the

                 SUBCOMMITTEE ON WORKFORCE PROTECTIONS

                                 of the

                COMMITTEE ON EDUCATION AND THE WORKFORCE
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             SECOND SESSION

                               __________

             HEARING HELD IN WASHINGTON, DC, JULY 23, 2002

                               __________

                           Serial No. 107-74

                               __________

  Printed for the use of the Committee on Education and the Workforce






                       U. S. GOVERNMENT PRINTING OFFICE
82-142                          WASHINGTON : 2002
___________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001

</pre></body></html>
