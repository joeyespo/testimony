<html>
<title> - LICENSING AND CREDENTIALITY OF MILITARY JOB SKILLS FOR CIVILIAN EMPLOYMENT</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]


2007


 
                LICENSING AND CREDENTIALITY OF MILITARY
                   JOB SKILLS FOR CIVILIAN EMPLOYMENT

=======================================================================

                                HEARING

                               before the

                         SUBCOMMITTEE ON BENEFIT

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SIXENTH CONGRESS

                            SECOND SESSION
                               __________

                           SEPTEMBER 27, 2000

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                            Serial No. 106-48


                  U.S. GOVERNMENT PRINTING OFFICE
71-101                    WASHINGTON : 2000
-----------------------------------------------------------------------
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092104 Mail: Stop IDCC, Washington, DC 20402�090001

</pre></body></html>
