<html>
<title> - FLEXIBILITY, ACCOUNTABILITY, AND QUALITY EDUCATION </title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
           FLEXIBILITY, ACCOUNTABILITY, AND QUALITY EDUCATION
=======================================================================

                                HEARING

                               before the

                COMMITTEE ON EDUCATION AND THE WORKFORCE

                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             FIRST SESSION
                               __________

         HEARING HELD IN BRADENTON, FLORIDA, FEBRUARY 16, 2001
                               __________

                            Serial No. 107-1
                               __________

  Printed for the use of the Committee on Education and the Workforce









_____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001




</pre></body></html>
