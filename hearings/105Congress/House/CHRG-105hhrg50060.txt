<html>
<title> - OIRA IMPLEMENTATION OF THE CONGRESSIONAL REVIEW ACT </title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                 OIRA IMPLEMENTATION OF THE CONGRESSIONAL
                            REVIEW ACT
______________________________________________________________




                                 HEARING

                               BEFORE THE

            SUBCOMMITTEE ON NATIONAL ECONOMIC GROWTH,

           NATURAL RESOURCES, AND REGULATORY AFFAIRS

                                  OF THE

                      COMMITTEE ON GOVERNMENT 

                        REFORM AND OVERSIGHT

                     HOUSE OF REPRESENTATIVES

                    ONE HUNDRED FIFTH CONGRESS

                             SECOND SESSION

                              ___________

                             MARCH 10, 1998

                              ___________

                          Serial No. 105-145

Printed for the use of the Committee on Government Reform and Oversight






                        U.S. GOVERNMENT PRINTING OFFICE

50-060                        WASHINGTON : 1998
_____________________________________________________________________
For sale by the U.S. Government Printing Office 
Superintendent of Documents, Congressional Sales Office, 
Washington, DC 20402

                       ISBN 0-16-057311-4


<TEXT NOT AVAILABLE>

</pre></body></html>
