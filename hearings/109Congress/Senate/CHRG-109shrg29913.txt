<html>
<title> - TELEVISION BLOCKING TECHNOLOGY</title>
<body><pre>
[Senate Hearing 109-]
[From the U.S. Government Printing Office]




                                                         S. Prt. 109-67

                     TELEVISION BLOCKING TECHNOLOGY

=======================================================================

                                BRIEFING

                               before the

                         COMMITTEE ON COMMERCE,
                      SCIENCE, AND TRANSPORTATION
                          UNITED STATES SENATE

                       ONE HUNDRED NINTH CONGRESS

                             SECOND SESSION

                               __________

                             JULY 27, 2006

                               __________

    Printed for the use of the Committee on Commerce, Science, and 
                             Transportation




                    U.S. GOVERNMENT PRINTING OFFICE
                           WASHINGTON : 2006 
29-913 PDF

For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001




       SENATE COMMITTEE ON COMMERCE, SCIENCE, AND TRANSPORTATION

                       ONE HUNDRED NINTH CONGRESS

                             SECOND SESSION

                     TED STEVENS, Alaska, Chairman
JOHN McCAIN, Arizona                 DANIEL K. INOUYE, Hawaii, Co-
CONRAD BURNS, Montana                    Chairman
TRENT LOTT, Mississippi              JOHN D. ROCKEFELLER IV, West 
KAY BAILEY HUTCHISON, Texas              Virginia
OLYMPIA J. SNOWE, Maine              JOHN F. KERRY, Massachusetts
GORDON H. SMITH, Oregon              BYRON L. DORGAN, North Dakota
JOHN ENSIGN, Nevada                  BARBARA BOXER, California
GEORGE ALLEN, Virginia               BILL NELSON, Florida
JOHN E. SUNUNU, New Hampshire        MARIA CANTWELL, Washington
JIM DeMINT, South Carolina           FRANK R. LAUTENBERG, New Jersey
DAVID VITTER, Louisiana              E. BENJAMIN NELSON, Nebraska
                                     MARK PRYOR, Arkansas
             Lisa J. Sutherland, Republican Staff Director
        Christine Drager Kurth, Republican Deputy Staff Director
             Kenneth R. Nahigian, Republican Chief Counsel
   Margaret L. Cummisky, Democratic Staff Director and Chief Counsel
   Samuel E. Whitehorn, Democratic Deputy Staff Director and General 
                                Counsel
             Lila Harper Helms, Democratic Policy Director



                            C O N T E N T S

                              ----------                              
                                                                   Page
Briefing held on July 27, 2006...................................     1
Statement of Senator Inouye......................................     7
Statement of Senator Stevens.....................................     1

                               Witnesses

Conlon, Peggy, President/CEO, The Advertising Council............     2
Valenti, Jack, Former Chairman/CEO; Senior Consultant, Motion 
  Picture 
  Association of America, MPAA...................................     2

 
                     TELEVISION BLOCKING TECHNOLOGY

                              ----------                              


                        THURSDAY, JULY 27, 2006

                                       U.S. Senate,
        Committee on Commerce, Science, and Transportation,
                                                    Washington, DC.
    The Committee met, pursuant to notice, at 10:04 a.m. in 
room SR-253, Russell Senate Office Building, Hon. Ted Stevens, 
Chairman of the Committee, presiding.

            OPENING STATEMENT OF HON. TED STEVENS, 
                    U.S. SENATOR FROM ALASKA

    The Chairman. Let me thank you all for being here today, 
and thank our Co-Chairman, and all the Members of the Committee 
who've been involved in this activity with us. Our Committee 
has held one hearing and two forums on the decency issues, and 
one of those forums lasted a full day.
    On June 15th, the President signed into law the Broadcast 
Decency Enforcement Act, which increased tenfold the fines that 
the FCC may impose for indecent broadcasts. That bill was a 
step forward. However, during this Committee's daylong summit, 
it became apparent that increasing the fines is only one piece 
of the puzzle because over-the-air broadcasting is only 15 
percent of the problem. That is why the family tiers are an 
important answer, and the cable and satellite industries 
voluntary efforts to offer such tiers are to be commended.
    Additionally, Jack Valenti, who is at the witness table 
now, has worked with the Ad Council and various sectors of the 
industry to launch a broad education campaign, which he is here 
today, along with Peggy Conlon, President and CEO of the Ad 
Council, to unveil.
    I understand from Jack, he's had a series of exposures on 
various channels and spent a great deal of time this week 
explaining what the industry wishes to do, and I again commend 
Jack and thank him for his friendship and for his constant 
attention to this problem.
    I think his proposal has great merit, and we wish him every 
success in the world.
    Senator Inouye. I'd like to join you in commending our 
friend, Jack. Without him, the motion picture industry would be 
without leadership. Welcome back, sir.
    Mr. Valenti. Thank you.
    The Chairman. Ms. Conlon, we welcome you also. Jack and Ms. 
Conlon, we're at your call--we'd like to hear what you have to 
say, and I know you've got some presentations to make.

    STATEMENT OF JACK VALENTI, FORMER CHAIRMAN/CEO; SENIOR 
    CONSULTANT, MOTION PICTURE ASSOCIATION OF AMERICA, MPAA

    Mr. Valenti. Thank you, Mr. Chairman, Mr. Co-Chairman. I 
thank both of you.
    It was on November 29th, if you'll recall when you had that 
work forum that you and the Co-Chairman wanted to know if the 
television industry had any ideas about how we could benefit 
parents and allow them to monitor the television viewing of 
their children. Now, what, 7 months later we are here with our 
plan.
    I want to tell you this is an unprecedented unified effort, 
and I want to just briefly introduce people who really made 
this thing work.
    I first want to introduce Dan Glickman, the President of 
the Motion Picture Association; David Rehr, the President of 
the National Association of Broadcasters; Kyle McSlarrow, the 
President of the National Cable and Telecommunications 
Association: and I'd like to introduce Gary Shapiro, who is the 
President of the Consumer Electronics Association.
    Mr. Shapiro. Thank you.
    Mr. Valenti. Now, the irony of this, Mr. Chairman, is we 
all are bitter antagonists in the marketplace, but we've come 
together in what I think is an epiphany for parents. I've been 
searching for a couple of months to use that word epiphany, and 
I finally found a sentence to put it in. I thank you for that. 
But it is, I think, in the long run, this is a beneficent tool 
that allows parents to monitor and to guide and to block 
programs that they think might not be suitable for their 
younger children.
    Now, I have some other things to say about it, but this 
would not have occurred without the extraordinary partnership 
of the Ad Council, which, for 63 years, as Peggy will no doubt 
tell you, they have been a sustaining influence in this country 
in helping illuminate great public issues in easy to understand 
ways so that the public is benefited by it. And it was the Ad 
Council that's been with us every inch of the way, and I dare 
say, without the four associations and without the Ad Council, 
I wouldn't be here today.
    So, Peggy, would you tell us something about the Ad 
Council?

   STATEMENT OF PEGGY CONLON, PRESIDENT/CEO, THE ADVERTISING 
                            COUNCIL

    Ms. Conlon. Thank you very much, Jack.
    Good morning. Thank you, Senator Stevens and Inouye for 
inviting us here today and for your incredible leadership.
    I know we all share the same goal, to help America's 
parents manage their young children's television viewing, and I 
believe this morning marks a giant step in achieving that goal.
    I know that many of you are familiar with the Ad Council, 
but I do want to spend a few minutes this morning telling you a 
bit about who we are and what we've been able to accomplish 
over the last 63 years. Our campaigns have become part of the 
American culture, and our slogans a part of the vernacular. Not 
just because they're memorable, but because they make a 
profoundly positive difference in the lives of the American 
people.
    From our earliest days during World War II with campaigns 
like, ``Loose Lips, Sink Ships,'' ``Buy War Bonds,'' and 
``Plant Victory Gardens,'' to countless others that have 
followed, the Ad Council has kept its finger on the pulse of 
the American society and moved the needle on so many critically 
important issues.
    Thanks to Smokey Bear, Americans know that only they can 
prevent forest fires, and as a result, we've helped to reduce 
the number of acres lost annually from 22 million to 8.4 
million. And because we've taught the Nation that ``Friends 
Don't Let Friends Drive Drunk,'' almost 70 percent of Americans 
have acted to prevent a friend from driving drunk. And for 
almost 35 years, we've reminded Americans that ``A mind is a 
terrible thing to waste.'' As a result, the United Negro 
College Fund has raised more than $2.2 billion, and graduated 
more than 350,000 deserving minority students from college.
    Clearly, Ad Council campaigns have raised awareness, saved 
lives, and inspired countless Americans to take positive 
action. And today, I'm delighted to help introduce a wonderful 
new effort that will not only inform parents about how they can 
monitor and supervise their children's media consumption, but 
also inspire them to do so.
    Studies show that children spend almost 45 hours per week 
exposed to media, usually including more than one form at a 
time, and that parents have serious concerns about age-
inappropriate TV content. They say that they're in control of 
their children's viewing habits, but day-to-day, they're not as 
vigilant as they would like to be. Despite their general lack 
of awareness about blocking technologies, many parents are open 
to ideas that promise more control and agree that these 
technologies can be an effective tool.
    So in an effort to empower these parents to take a more 
active role in their children's media viewing habits, the Ad 
Council has proudly joined with a broad coalition of industry 
organizations to launch this national PSA campaign. We are 
incredibly enthusiastic about this partnership, particularly 
since it marks the first time that all TV media platforms are 
working together to amplify a single message. Think of the 
power that that holds.
    This unprecedented effort spearheaded by our friend, Jack 
Valenti, has been created pro bono by a world-class team from 
the ad agency, McCann Erickson New York. That's the very same 
agency that created the ``Priceless'' campaign for MasterCard.
    I'd like to thank Gayle Barlow and Sasha Schor, who are 
both with us this morning, both VPs and Creative Directors at 
McCann, for their incredible commitment. We think that they're 
priceless.
    As you'll see in a few minutes, the result of their 
commitment and hard work are fantastic. As you might imagine, 
it takes more than a catchy slogan to create effective 
advertising. The Ad Council knows how to create breakthrough 
messages that inspire our target audiences to take action. Much 
of that stems from the extensive research that we conduct 
before, during, and after our campaigns are developed. That 
includes testing the spots with the target audience before any 
of the work is approved for distribution. And we benchmark 
attitudes and behaviors, and track those over time. This 
campaign is no different.
    Certainly, the incredible commitment of our media partners, 
coupled with the magnificent creative developed by McCann, and 
the robust website developed by Ripple Effects Interactive will 
effectively educate and inspire parents to take control of 
their children's media viewing habits. At the end of the day, 
that is what this is all about.
    And now before I turn this back over to Jack Valenti, I 
want to thank him for being not only a champion of this issue, 
but of this campaign itself. Jack, we couldn't have done it 
without you. Thank you for your partnership.
    Mr. Valenti. Thank you very much, Peggy.
    I want to thank again Dan Glickman, and Kyle McSlarrow, and 
David Rehr, and Gary Shapiro for shepherding what I think is an 
unprecedented, seamless web of unity of every national 
broadcast network, every television station, every cable 
system, every direct broadcast satellite system, every movie 
studio, every TV programmer, have joined together. This has 
never happened before.
    This will be an 18-month campaign to try to tell the 
American people, you, Mr. and Ms. Parent, are the TV boss of 
your home. You have power right now to control every hour of 
entertainment programming in your home.
    Now, the Congress recently passed this indecency law--so 
called, in which it fines $325,000 for every indecent word. But 
the American public doesn't realize that if you have a hundred 
channel cable systems, it only covers about seven, eight, or 
nine channels. No more. What we're trying to do covers all 
channels that deal in entertainment programming, so that the 
parents can leave the home at night to go to dinner, and be 
secure in the knowledge that their children are not going to 
watch any program that they don't want them to watch. They've 
blocked the ones that they want to put in exile, and no matter 
the babysitter's choice, that TV set will be blank on those 
programs. That's a huge, huge step forward. And what we're 
going to present to you today is some of the visual spots that 
we'll be running. As a matter of fact, they began yesterday. 
They're on tonight in prime time. They were on yesterday in 
prime time, not only on national broadcast networks, but on the 
direct broadcast satellite, and they'll be on tonight, and this 
weekend, and you'll see them throughout this summer, and this 
year, and into the next year.
    We have two spots we're going to show you. Others are 
either in revision right now or in design, so that as these 
spots begin to wear out their welcome, we'll have the new ones 
coming on.
    The whole idea is to direct parents to a website called 
``TheTVBoss.org.'' Now, all of you around this table write that 
down because I hope that you're already going to that site. 
It's up, and it's functioning, and you'll see it now, and it 
will then tell the parents in easy to understand, easy to 
follow language how they can block.
    Now, Mr. Chairman, Mr. Co-Chairman, I am technologically 
illiterate. I admit that. I confess it. And when I go to mass 
this Sunday, I'll ask for absolution on that, but that's all I 
can do. The fact is, though, that after a couple tries at it, I 
got the hang of this, and if I can do it, anybody can do it. 
It's easy to do.
    Now, I say to you in advance, we can't command people to do 
what's best for them and their children. Lyndon Johnson used to 
say, ``You can tell a man to go to hell, but getting him to go 
there is another proposition.'' So what I'm saying to you is, 
that we're going to do our best, over, and over, and over, and 
over again, telling parents you can command television--all of 
it, not just part of it, but all of it--and you can do it 
easily, and you can do it by rating of a television show, by 
genre, by individual programming, by time, whatever. All easy 
to do.
    Now, finally--that's a wonderful word. Finally--it makes 
everybody feel good. There are only a few places where a moral 
shield can be built in a child, home, school, church, 
synagogue, mosque. That's it. And if the child builds--with the 
help of a parent, builds that moral shield, then they can 
navigate the dark corridors of the land we live in now, and the 
kind of digital world we live in. But if parents don't exercise 
this responsibility--and I think you'll agree--I don't think 
government can do it. Not only should government not do it, I 
think government is forbidden to do it by the Constitution. It 
has to be the parent, and I'm hopeful that over this next 18 
months, by the dent of the repetition of these visual 
announcements, that we'll make some impress. I don't guarantee 
anything, but I'll tell you it won't be for lack of trying.
    Now, the first thing I'd like to show you, Mr. Chairman, is 
two of these spots that are playing now in prime time. So let's 
run these two spots.
    [Plays video.]
    Mr. Valenti. So far, it's pretty good.
    I wonder if we could run those again. They're only 30 
seconds, let's get part of the repetition going right here now. 
Let's run them again.
    [Plays video.]
    Mr. Valenti. Now, the next thing I want to show you is the 
website itself. The Ad Council's done a little home movie of 
it, and I hope you'll like it. This is where we try to tell 
parents how to deal in this apparatus, how to be the TV boss, 
and the few steps you have to go through to do it. As I said 
earlier, I can do it, and therefore, I'm absolutely convinced 
that anybody else can do it. Because this is where the actual 
blocking will be done.
    Let's see our little Ad Council's movie.
    The Chairman. Who's got the volume? Turn it up a little 
bit, will you?
    [Plays video.]
    Mr. Valenti. Mr. Chairman, we have other things we're 
doing. We're going to be preparing this same material to send 
to churches and to advocacy groups, like the PTA, all over this 
country, and if they so choose, they can then distribute it to 
their parishioners and to their members. And we think this will 
give added reach, spacious new territory for us, so that 
parents will be aware of this and want to use it.
    And we're going to do something that I think you would be 
especially interested in. We're going to prepare a member's kit 
with all of this material in it to send to every single member 
of the House of Representatives and the U.S. Senate. We've 
already sent this to the Federal Communications Commission, I 
believe, and we'll send it to anybody else that wants to view 
it.
    These members can then put this on their own websites, so 
they can tell their constituents in their district and in their 
state what they are doing to help parents completely and 
totally monitor and have power over all of the television 
entertainment programs in their home. There's no other way to 
do this, except through parental responsibility, and the use of 
these blocking techniques.
    And we're going to be monitoring this as closely as we can, 
as Peggy said. They're in the midst of research right now to do 
a benchmark research level. At the end of nine months, there 
will be more research. At the end of an additional nine months, 
another piece of research, so we can measure whatever progress 
we think has been made, and we have the questions of these 
polling. If anybody in your staff would want to look at them, 
we will make those questions available to you, so you see the 
methodology that we're doing.
    As I said before, this is unique. It's never been tried 
before, and I'm hopeful that at the end of this period, that 
more parents will have taken the lead in their own home. If 
parents truly care--if they really care about what their 
children are watching, and the programs they think are 
unsuitable for their children--because many parents enjoy a lot 
of programs that they don't find suitable for their seven, 
eight or nine-year-olds. So if they care, we're saying we're 
giving you the tools to do this job, where you can truly and 
honestly and authentically be the TV boss of your home.
    This is our program, Mr. Chairman, Mr. Co-Chairman. We 
present it to you to redeem the pledge we made to you in 
November of last year that we would come back with our ideas of 
how this ought to be done without government interference.
    The Chairman. Thank you very much.
    And, Ms. Conlon, we certainly congratulate you for 
participating in this grand partnership. During my time here, 
and I'm sure my friend and Co-Chairman would say the same 
thing. I can't remember such a coordinated campaign. And we 
join you in thanking all of those that you've mentioned, The 
Consumer Electronics Association, National Association of 
Broadcasters, The National Cable and Telecommunications 
Association, Motion Picture Association, and we're impressed 
that the television broadcast networks, ABC, CBS, NBC, FOX, 
Direct to Home satellite providers, Direct TV, and EchoStar, 
they're all together. I can't remember when I've seen this 
communications industry totally united in one objective, and 
I'm certain that it will be effective over a period of time if 
people listen.
    I think parents may have the job of setting the standards, 
but their children themselves will enforce it once they get the 
message. I think they will understand what we're doing in 
trying to protect the coming generations from being exposed to 
excessive abuse, and to language, and to accidents that we do 
not think they should be able to witness during their formative 
years.
    So, again, I congratulate you very much.
    Senator Inouye.

              STATEMENT OF HON. DANIEL K. INOUYE, 
                    U.S. SENATOR FROM HAWAII

    Senator Inouye. Mr. Valenti, there's a bill called the 
Telecommunications Act of 2006. Can you help us get your troops 
together? They're all over the place.
    Mr. Valenti. Well, I'm not sure that I want to get into 
that cauldron of hell myself, but whenever Senator Inouye asks 
me something, I stand, salute, and say, by jingles, I'll try.
    The Chairman. Well, he's one up on me. I was about ready to 
ask Ms. Conlon if she'd join us, too. We need the Ad Council a 
little bit the next few weeks.
    But go ahead, Senator.
    Senator Inouye. But, seriously, you're remarkable, and I'm 
glad we have a program such as this, and I'm going to do my 
best to advise every member--I think the Chairman and I will 
advise every member to make sure that the information about 
that chip is on their website, that we're going to urge every 
member of the Senate to do the same thing. That's the least we 
can do, and I'm certain it will be a success.
    Congratulations, sir.
    Mr. Valenti. Thank you.
    The Chairman. Yes. And if you will tell us when you're 
going to deliver those kits, I'm sure my friend and I will go 
to the floor and make a statement on the floor to call 
attention to members, so that they don't just get put away 
somewhere by their staff, so that the members themselves 
address those kits. And I'm sure we'll both make a commitment 
to you, we'll put it on our website as soon as we get the 
information.
    I think that we must join you to carry this out, and I hope 
every Member of Congress will do that.
    Mr. Valenti. We're going to get on that. It's good to have 
a former Congressman sitting at our table, like Dan Glickman. 
He said, ``Well, you ought to have a kit that goes to members, 
so they can put it on their websites.'' And I thought it was a 
brilliant idea, and I'm not sure when those kits will be ready.
    Ms. Conlon. In just a few weeks, we're waiting for the 
spots to be finished, and they're in development now, so we 
will--when they're ready.
    Mr. Valenti. So by the time that the members get back from 
their August recess, they will--it will be sometime in 
November, the members will have those kits, and they can put it 
on their websites.
    In conclusion, Mr. Chairman, I want to emphasize again that 
I've had a little experience 38 years ago in inventing the 
movie rating system, and letting parents understand that the 
only people who can really guide their children is a parent, 
not a government, and if the parents don't care, if they're not 
doing it, then all the mighty powers of the President, the 
Congress, the army of regulatory agencies will be like tracings 
on dry leaves in the wind. It will make no difference unless 
parents enter this thing, and say, by jingles, I'm going to be 
TV boss of my home, and I'm going to control it, and my 
children are not going to watch things that I find to be 
unsuitable. If they do that, then we would have entered a more 
harmonious age, and I think all of us will welcome it.
    But this has been a great experience for me and my 
colleagues, and we're going to continue working on it.
    But most of all, I thank you, and the Co-Chairman, Mr. 
Chairman, for really being the champions of doing this the 
right way, and that is in the individual private sectors, and 
in the home. That's the only place it can be done.
    Thank you, sir.
    The Chairman. Well, thank you very much. And, Ms. Conlon, 
if it's agreeable with you, we would like to put on record that 
we will have a hearing when you get your first sort of progress 
report at the end of the 9-month period, if that's all right.
    Ms. Conlon. We'd be delighted.
    The Chairman. I think we would like to have a way to notify 
everyone on this Committee and the Congress of how this program 
has progressed. But, again, I congratulate all of you and all 
of your partners.
    And I've got to tell you, we could not legislate this. We 
could not legislate the reaction that you could produce with 
this program, and we're very hopeful that it will succeed 
because we have the demands from some people to legislate, and 
it's awfully difficult to define decency in this concept, and 
more than that, to find the constitutional powers to do it, and 
of these entities that are involved voluntarily now with you in 
a program to teach the parents how to control their own home 
and to control access for their children for the programs that 
they would not want them to watch.
    So thank you very much for your participation, Jack.
    Mr. Valenti. Thank you, sir.
    The Chairman. Happy to see you back.
    Ms. Conlon. Thank you, sir.
    Mr. Valenti. Thank you, Mr. Chairman, Mr. Co-Chairman. 
Thank you very much.
    The Chairman. This Committee will stand in recess until 
eleven o'clock when we've got another hearing.
    [Whereupon, at 10:40 a.m., the briefing was adjourned.]

                                  <all>

</pre></body></html>
