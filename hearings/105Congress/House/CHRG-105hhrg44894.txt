<html>
<title> - S. 923 AND H.R. 2040, TO DENY BURIAL IN A FEDERALLY FUNDED CEMETERY AND OTHER BENEFITS TO VETERANS CONVICTED OF CERTAIN CAPITAL CRIMES</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
         S. 923 AND H.R. 2040, TO DENY BURIAL IN A FEDERALLY 
            FUNDED CEMETERY AND OTHER BENEFITS TO VETERANS 
                 CONVICTED OF CERTAIN CAPITAL CRIMES 

=============================================================================


                              HEARING

                             before the

                   COMMITTEE ON VETERANS' AFFAIRS

                       HOUSE OF REPRESENTATIVES

                      ONE HUNDRED FIFTH CONGRESS

                             FIRST SESSION

                              ----------

                             JULY 9, 1997


                              ----------

      Printed for the use of the Committee on Veterans' Affairs


                           Serial No. 105-15


                  U.S. GOVERNMENT PRINTING OFFICE

  44-894 CC               WASHINGTON : 1997

-----------------------------------------------------------------------------

               For sale by the U.S. Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402
                          ISBN 0-16-055823-9

                    <TEXT NOT AVAILABLE IN TIFF FORMAT>


</pre></body></html>
