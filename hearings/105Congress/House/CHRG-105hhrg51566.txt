<html>
<title> - DRAFT LEGISLATION TO PROVIDE A COST-OF-LIVING ADJUSTMENT IN RATES OF COMPENSATION PAID TO VETERANS WITH SERVICE- CONNECTED DISABILITIES, TO MAKE VARIOUS IMPROVEMENTS IN EDUCATION, HOUSING, AND CEMETERY PROGRAMS OF THE DEPARTMENT OF VETERANS AFFAIRS, AND FOR OTHER PROGRAMS</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
      DRAFT LEGISLATION TO PROVIDE A COST-OF-LIVING ADJUSTMENT IN 
         RATES OF COMPENSATION PAID TO VETERANS WITH SERVICE- 
        CONNECTED DISABILITIES, TO MAKE VARIOUS IMPROVEMENTS IN 
      EDUCATION, HOUSING, AND CEMETERY PROGRAMS OF THE DEPARTMENT 
              OF VETERANS AFFAIRS, AND FOR OTHER PROGRAMS 

=======================================================================





                                 HEARING

                               before the

                         SUBCOMMITTEE ON BENEFITS

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                              SECOND SESSION

                               ------------



                               JUNE 18, 1998

                               ------------




        Printed for the use of the Committee on Veterans' Affairs


                           Serial No. 105-41


                    U.S. GOVERNMENT PRINTING OFFICE
51-566 CC                   WASHINGTON : 1998

------------------------------------------------------------------------


           For sale by the U.S. Government Printing Office

      Superintendent of Documents, Congressional Sales Office,
                           Washington, DC 20402

                            ISBN 0-16-057753-5

                  <TEXT NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
