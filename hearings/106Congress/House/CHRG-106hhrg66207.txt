<html>
<title> - HOMELESS VETERANS' ISSUES</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]




 
                       HOMELESS VETERANS' ISSUES

=======================================================================

                             JOINT HEARING

                               before the

                        SUBCOMMITTEE ON BENEFITS

                                  and

                         SUBCOMMITTEE ON HEALTH

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS

                             SECOND SESSION
                               __________

                             MARCH 9, 2000

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 106-31

                  U.S. GOVERNMENT PRINTING OFFICE
66-207                    WASHINGTON : 2000
-----------------------------------------------------------------------
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092104 Mail: Stop IDCC, Washington, DC 20402�090001

<TEXT FILE NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
