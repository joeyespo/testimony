<html>
<title> - REFORMING THE INDIVIDUALS WITH DISABILITIES EDUCATION ACT: RECOMMENDATIONS FROM THE PRESIDENT'S COMMISSION ON EXCELLENCE IN SPECIAL EDUCATION </title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
      REFORMING THE INDIVIDUALS WITH DISABILITIES EDUCATION ACT: 
   RECOMMENDATIONS FROM THE PRESIDENT'S COMMISSION ON EXCELLENCE IN SPECIAL
                                 EDUCATION 
=======================================================================

                                HEARING

                               before the

                COMMITTEE ON EDUCATION AND THE WORKFORCE
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             SECOND SESSION

                               __________

             HEARING HELD IN WASHINGTON, DC, JULY 10, 2002

                               __________

                           Serial No. 107-70

                               __________



  Printed for the use of the Committee on Education and the Workforce






                           U.S. GOVERNMENT PRINTING OFFICE
82-596                             WASHINGTON : 2002
_____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001

</pre></body></html>
