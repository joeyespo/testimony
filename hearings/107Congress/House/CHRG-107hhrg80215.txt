<html>
<title> - TRACKING INTERNATIONAL STUDENTS IN HIGHER EDUCATION-POLICY OPTIONS AND IMPLICATIONS FOR STUDENTS</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
                  TRACKING INTERNATIONAL STUDENTS IN
                 HIGHER EDUCATION-POLICY OPTIONS AND
                      IMPLICATIONS FOR STUDENTS

=======================================================================

                            JOINT HEARING

                              BEFORE THE

                   SUBCOMMITTEE ON SELECT EDUCATION

                                 AND THE

             SUBCOMMITTEE ON 21ST CENTURY COMPETITIVENESS


                      COMMITTEE ON EDUCATION AND
                             THE WORKFORCE


                      HOUSE OF REPRESENTATIVES


                      ONE HUNDRED SEVENTH CONGRESS

                              FIRST SESSION

                               __________

         HEARING HELD IN WASHINGTON, D.C. OCTOBER 31, 2001

                               __________

                           Serial No. 107-36
                               __________

          Printed for the use of the Committee on Education
                           and the Workforce


80-215              U.S. GOVERNMENT PRINTING OFFICE
                            WASHINGTON : 2002
____________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
      Internet: bookstore.gpo.gov  (202) 512�091800  Fax: (202) 512�092250 
                Mail: Stop SSOP, Washington, DC 20402�090001


<TEXT NOT AVAILABLE>

</pre></body></html>
