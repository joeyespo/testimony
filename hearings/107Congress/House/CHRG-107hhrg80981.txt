<html>
<title> - DEPARTMENTS OF LABOR, HEALTH AND HUMAN</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
                 DEPARTMENTS OF LABOR, HEALTH AND HUMAN

               SERVICES, EDUCATION, AND RELATED AGENCIES

                        APPROPRIATIONS FOR 2003

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS
                             SECOND SESSION
                                ________
  SUBCOMMITTEE ON THE DEPARTMENTS OF LABOR, HEALTH AND HUMAN SERVICES, 
                    EDUCATION, AND RELATED AGENCIES
                      RALPH REGULA, Ohio, Chairman
 C. W. BILL YOUNG, Florida           DAVID R. OBEY, Wisconsin
 ERNEST J. ISTOOK, Jr., Oklahoma     STENY H. HOYER, Maryland
 DAN MILLER, Florida                 NANCY PELOSI, California
 ROGER F. WICKER, Mississippi        NITA M. LOWEY, New York
 ANNE M. NORTHUP, Kentucky           ROSA L. DeLAURO, Connecticut
 RANDY ``DUKE'' CUNNINGHAM,          JESSE L. JACKSON, Jr., Illinois
California                           PATRICK J. KENNEDY, Rhode Island
 KAY GRANGER, Texas
 JOHN E. PETERSON, Pennsylvania
 DON SHERWOOD, Pennsylvania         

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
      Craig Higgins, Susan Quantius, Susan Ross Firth, Meg Snyder,
             and Francine Mack-Salvador, Subcommittee Staff
                                ________
                                 PART 4B

                      NATIONAL INSTITUTES OF HEALTH

                              <snowflake>

                                ________
         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 80-981                     WASHINGTON : 2002




                      COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                  DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky             NORMAN D. DICKS, Washington
 JOE SKEEN, New Mexico               MARTIN OLAV SABO, Minnesota
 FRANK R. WOLF, Virginia             STENY H. HOYER, Maryland
 TOM DeLAY, Texas                    ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                  MARCY KAPTUR, Ohio
 SONNY CALLAHAN, Alabama             NANCY PELOSI, California
 JAMES T. WALSH, New York            PETER J. VISCLOSKY, Indiana
 CHARLES H. TAYLOR, North Carolina   NITA M. LOWEY, New York
 DAVID L. HOBSON, Ohio               JOSE E. SERRANO, New York
 ERNEST J. ISTOOK, Jr., Oklahoma     ROSA L. DeLAURO, Connecticut
 HENRY BONILLA, Texas                JAMES P. MORAN, Virginia
 JOE KNOLLENBERG, Michigan           JOHN W. OLVER, Massachusetts
 DAN MILLER, Florida                 ED PASTOR, Arizona
 JACK KINGSTON, Georgia              CARRIE P. MEEK, Florida
 RODNEY P. FRELINGHUYSEN, New Jersey DAVID E. PRICE, North Carolina
 ROGER F. WICKER, Mississippi        CHET EDWARDS, Texas
 GEORGE R. NETHERCUTT, Jr.,          ROBERT E. ``BUD'' CRAMER, Jr., 
Washington                           Alabama
 RANDY ``DUKE'' CUNNINGHAM,          PATRICK J. KENNEDY, Rhode Island
California                           JAMES E. CLYBURN, South Carolina
 TODD TIAHRT, Kansas                 MAURICE D. HINCHEY, New York
 ZACH WAMP, Tennessee                LUCILLE ROYBAL-ALLARD, California
 TOM LATHAM, Iowa                    SAM FARR, California
 ANNE M. NORTHUP, Kentucky           JESSE L. JACKSON, Jr., Illinois
 ROBERT B. ADERHOLT, Alabama         CAROLYN C. KILPATRICK, Michigan
 JO ANN EMERSON, Missouri            ALLEN BOYD, Florida
 JOHN E. SUNUNU, New Hampshire       CHAKA FATTAH, Pennsylvania
 KAY GRANGER, Texas                  STEVEN R. ROTHMAN, New Jersey    
 JOHN E. PETERSON, Pennsylvania
 JOHN T. DOOLITTLE, California
 RAY LaHOOD, Illinois
 JOHN E. SWEENEY, New York
 DAVID VITTER, Louisiana
 DON SHERWOOD, Pennsylvania
   
 VIRGIL H. GOODE, Jr., Virginia     
   
                 James W. Dyer, Clerk and Staff Director

                                  (ii)


                                     
                                     
                                     

                     NATIONAL INSTITUTES OF HEALTH

                       2003 Budget Justifications

Overview
National Cancer Institute
National Heart, Lung and Blood Institute
National Institute of Dental and Craniofacial Research
National of Diabetes and Digestive and Kidney Diseases
National Institute of Neurological Disorders and Stroke
National Institute of Allergy and Infectious Diseases
National Institute of General Medical Sciences
National Institute of Child Health and Human Development
National Eye Institute
National Institute of Environmental Health Sciences
National Institute on Aging
National Institute of Arthritis and Musculoskeletal and Skin
    Diseases
National Institute on Deafness and Other Communication
    Disorders
National Institute of Mental Health
National Institute on Drug Abuse
National Institute of Alcohol Abuse and Alcoholism
National Institute of Nursing Research
National Institute of National Human Genome Research
    Institute
National Institute of Biomedical Imaging and
     Bioengineering
National Center for Resources
National Center for Complementary and Alternative
    Medicine
National Center for Minority Health and Health Disparities
Fogarty International Center
National Library of Medicine
Buildings and Facilities
Office of the Director
Office of AIDS Research

                                 (iii)



              [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]



</pre></body></html>
