<html>
<title> - MARKUP OF OMNIBUS COMMITTEE FUNDING RESOLUTION AND A COMMITTEE RESOLUTION</title>
<body><pre>
[House Hearing, 109 Congress]
[From the U.S. Government Printing Office]



 
    MARKUP OF OMNIBUS COMMITTEE FUNDING RESOLUTION AND A COMMITTEE 
                               RESOLUTION

=======================================================================

                                 MARKUP

                               before the

                   COMMITTEE ON HOUSE ADMINISTRATION
                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED NINTH CONGRESS

                             FIRST SESSION

                               __________

             HEARING HELD IN WASHINGTON, DC, APRIL 21, 2005

                               __________

      Printed for the use of the Committee on House Administration





                 U.S. GOVERNMENT PRINTING OFFICE

21-196                  WASHINGTON : 2005
_________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government 
Printing  Office Internet: bookstore.gpo.gov  Phone: toll free 
(866) 512-1800; DC area (202) 512-1800 Fax: (202) 512-2250 Mail:
Stop SSOP, Washington, DC 20402-0001




                   COMMITTEE ON HOUSE ADMINISTRATION

                        BOB NEY, Ohio, Chairman
VERNON J. EHLERS, Michigan           JUANITA MILLENDER-McDONALD, 
JOHN L. MICA, Florida                    California
CANDICE S. MILLER, Michigan            Ranking Minority Member
JOHN T. DOOLITTLE, California        ROBERT A. BRADY, Pennsylvania
THOMAS M. REYNOLDS, New York         ZOE LOFGREN, California

                           Professional Staff

                     Paul Vinovich, Staff Director
                George Shevlin, Minority Staff Director


    MARKUP OF OMNIBUS COMMITTEE FUNDING RESOLUTION AND A COMMITTEE 
                               RESOLUTION

                              ----------                              


                        THURSDAY, APRIL 21, 2005

                          House of Representatives,
                         Committee on House Administration,
                                                    Washington, DC.
    The committee met, pursuant to call, at 2:30 p.m., in room 
H-326, the Capitol, Hon. Robert W. Ney (chairman of the 
committee) presiding.
    Present: Representatives Ney, Ehlers, Miller, Millender-
McDonald, Brady and Lofgren.
    The Chairman. The Committee is now in order for the purpose 
of considering the 109th Congress Omnibus Committee Funding 
Resolution and a Committee Resolution establishing the 109th 
Congress franked mail allocation for committees. In February 
this year, the chairmen and ranking members of all the 
committees came before us and requested their budgets, the 
resolution along with the committee resolution setting the 
franked mail allocations to fully fund committees for the 109th 
Congress.
    This mark is an amendment in the nature of a substitute 
outlining each committee's authorized spending levels for 2005 
and 2006. I am pleased to put before the Committee a chairman's 
mark that can be supported, I believe, by a majority of members 
on both sides of the aisle. This is a great way for this 
institution to operate, with members and the ranking members. 
And this agreement, which has been in the works for the last 
two months, has finally been sanctified in the 11th hour and a 
half, and allows us to move forward with a bona fide resolution 
for committee funding.
    I feel that both the chairmen and ranking members will 
agree that this resolution provides sufficient funding for them 
to carry out the duties and responsibilities with which they 
are charged. As I said many times, we are in the world of the 
Internet and a world of information. And that is a wonderful 
thing, but it also creates a lot of work that none of the 
members mind doing or the staff, but we have an obligation to 
people to do the work for the people in the country. And so 
that is why this funding measure is so important.
    At the beginning of Congress, the Committee on Homeland 
Security was made a permanent standing committee, which made 
things tougher this year. But I think we have done a good job 
allocating our resources, because we have, for the first time 
in a long time, funded another full committee. The Committee 
will continue to provide important oversight functions, 
overseeing the Homeland Security Department and ensuring that 
the combined agencies are doing their job. As a result of its 
new standing within the House, the budget for Homeland Security 
that has been engrossed by this resolution no longer stands 
alone, so I feel it is important to note that although it would 
appear that the overall budget for committees has increased 
dramatically because of the inclusion of that committee, this 
is really not the case. This was again a tough task, but I 
believe that we have all handled it in the correct way.
    So this budget is in line with past committee funding 
resolutions, and represents a percent increase similar to that 
of the United States Senate. I believe it will have good 
bipartisan support on the floor. The Committee has requested a 
total of 273 million in spending for the 109th. This is 
approximately 39 million more than what was authorized in the 
108th Congress, and also represents a 17 percent request 
increase. That is a request. Removing Homeland Security from 
the equation, the request by committees total 257 million, 
approximately a 35 million increase over the 108th authorized 
levels. This mark reduces the amount requested by all 
committees to 162 million, or a 5.9 percent increase. So 5.9 
percent is, I feel, responsible and meets the obligations of 
the Chairs and ranking members and the staffs so they can carry 
out their business.
    The amendment in the nature of a substitute providing 
expenses for all committees other than Homeland Security 
authorizes $243 million, a 9.2 percent increase, and this is a 
$20 million increase over the 108th.
    It should be noted that the 109th Congress funding level of 
$243 million, excluding Homeland Security, is still lower than 
the funding levels of the 103rd Congress in both costs and 
actual dollars. So I am proud of the numbers we are putting 
forward with this mark, and look forward to passing a 
resolution out of the Committee today and then passing the 
House floor.
    I want to thank our Ranking Member, Congresswoman 
Millender-McDonald, all the members of this Committee, and the 
staffs who have worked very, very hard. We haven't always had 
bipartisan votes on these resolutions, as those who have been 
around for a while remember. This will mark the third round 
where we have had bipartisan support. I am proud of the fact 
that both sides of the aisle are coming together. Also, a lot 
of kudos and credit to our ranking member and the other 
minority members of the Committee who pushed to make sure that 
their ranking members had the one third and the things that 
they needed, as we did in consulting our Chairs.
    And, with that, I will yield to our Ranking Member. And 
thank you, once again, publicly, for all your help, concern, 
and your caring of the system of the institution of the House.
    Ms. Millender-McDonald. Mr. Chairman, I want to, again, 
thank you for your tireless efforts to bring this funding 
resolution before this committee. This chairman has worked 
vehemently on the floor. He would either find me or I would 
find he on the floor, and we worked through this whole process. 
And you have worked to ensure that the fairness principle 
espoused by Speaker Hastert, former chair Thomas, and yourself 
is being followed by all committee chairs. I would like to also 
thank my lead staff person, George Schefland, and also your 
lead person, Paul, in helping to craft this and constantly 
talking to us about the nuances of getting to this point.
    The principle that you are following in the footsteps of 
your predecessor, first articulated by the Republican 
leadership and signed on to by every Republican ranking member 
was the Republicans, and when the Republicans were in the 
minority, is simply straightforward and straightforward--and 
forward.
    The minority of each committee is entitled to one third of 
the staff, one third of the funds, and control of those funds. 
The current Chair of the Senate Intelligence Committee, Senator 
Pat Roberts, who in 1994 was on this committee, best 
articulated this principle with language he offered to the 
omnibus funding.
    Mr. Chairman, the minority has not had to resort to such 
specific language largely because of you and your predecessor 
and Speaker Hastert believe in this fairness principle and 
lived it each day. The House Administration Committee is one of 
only five House committees which operate on this principle with 
no strings attached, and virtually all other committees have 
found a way to satisfactorily apply the principle with some 
conditions to the mutual satisfaction of the chairman and 
ranking member. I cannot tell you how pleased I am, Mr. 
Chairman, that the one committee which appears to be at an 
impasse has found a way to apply the fairness principle to the 
mutual satisfaction of the chairman and the ranking member.
    We have come a long way in a decade, and you have 
demonstrated this fairness principle and how it works. It has 
governed our committee for over 10 years without compromising 
your capacity and your integrity, Mr. Chairman, and I thank you 
so much for that. Because of your leadership, we have arrived 
at a point where I can honestly represent that the fairness 
principle is at work on every committee. You ought to be 
congratulated on navigating this, and at the proper time I will 
be offering an amendment to your chairman's mark. This 
amendment is intended to limit mass mailers by committees and 
to ensure that any self-initiated mass mailers are reviewed and 
that they are directly related to the ordinary and necessary 
responsibilities of the committee. Last year, just prior to the 
general election we recognize what went on, and this amendment 
will fix this problem and hope that every member of the 
committee will give it serious consideration. We hope to 
resolve this franking problem, and I will be offering 
limitations to this second resolution that will act upon--we 
act upon today, and that amendment will give each standing 
committee 5,000 per session in franking authority.
    And, Mr. Chairman, that amendment will require that each 
committee to come back to this committee for supplemental 
authorization if the need arises. These two committee 
amendments taken together will assure taxpayers that their 
franking tax dollars will not be subject to any future waste, 
fraud, or abuse in mass mailing franking transitions in 
furtherance of this committee's business. I am hopeful that my 
amendments will be adopted so that we have to take this 
resolution to the floor and we will take it to the floor with 
broad support of the resolution.
    In closing, again, I want to say what a pleasure it is to 
work with you, Mr. Chairman, and to express my hope that, 
working together, we can get the entire House behind the 
funding resolution when we take it up on the floor next week. 
Thank you, sir.
    [The statement of Ms. Millender-McDonald follows:]
    [GRAPHIC] [TIFF OMITTED] 21196A.001
    
    [GRAPHIC] [TIFF OMITTED] 21196A.002
    
    [GRAPHIC] [TIFF OMITTED] 21196A.003
    
    [GRAPHIC] [TIFF OMITTED] 21196A.004
    
    The Chairman. Thank you.
    Other comments by other members?
    Ms. Lofgren. Thank you, Mr. Chairman. Just a question in 
making sure that I am understanding the ranking member's 
comments. And with a great deal of gratitude both to you and to 
the chairman for the heavy lifting that I am sure is involved 
in getting agreement. But when we left the hearings unresolved 
in terms of the chairman and ranking members seem to be the 
agriculture and small business committees. Those have now been 
resolved to their satisfaction?
    Ms. Millender-McDonald. Those have both have been resolved.
    Ms. Lofgren. That is good. Congratulations.
    The Chairman. We thought we would go on the road and 
negotiate. A couple of countries are having disputes and are 
having disputes with each other, too.
    Ms. Lofgren. Well, if I may, and I will quickly close. I 
would certainly like to support the work of the chairman and 
ranking member. There is at least one question outstanding, but 
it should not stand in the way of supporting this. So good 
work.
    The Chairman. Thank you.
    Mr. Ehlers.
    Mr. Ehlers. I notice the Science Committee is just about 
the very lowest in percentage increase. They submitted a modest 
increase, but I hope they weren't given the lowest increase 
because they submitted a modest increase. Rules, which also 
submitted a fairly modest increase, got the entire amount, and 
the Science Committee got substantially less. I wondered what 
the reason was for that. The Science Committee, I know from 
being on it, runs a very tight ship, and a much tighter ship 
with less spending and waste than any of the other committees, 
and I would hate to see them penalized because they are frugal.
    The Chairman. The one thing on Rules. At least since I have 
been here, and when Mr. Thomas was chairman, the Rules 
Committee was almost a flat-line committee. And this time we 
talked to Mrs. Slaughter and also Mr. Dreier. They needed to do 
something to modernize their system.
    Mr. Ehlers. I am not questioning theirs. I am just saying 
why----
    The Chairman. And Science. There is, of course, a limited 
amount to the entire picture. But we did transmit the numbers 
to the Science Committee, and to the best of my knowledge, they 
had no objection. We didn't hear objection from them.
    Mr. Ehlers. Did they see what other committees got?
    The Chairman. We gave them the numbers. Correct?
    Mr. Ehlers. For every committee? Did they see that other 
committees were getting substantially more than they?
    The Chairman. If you start that----
    Mr. Ehlers. No. Then it is immaterial. You can't expect 
them to comment on the amount if they don't know what they are 
getting compared to other committees. I mean.
    Ms. Millender-McDonald. Government Reform is getting a 4.5 
increase.
    Mr. Ehlers. I understand that.
    Ms. Millender-McDonald. So it is not the lowest.
    Mr. Ehlers. No. There is one more.
    Ms. Millender-McDonald. Two, really.
    Mr. Ehlers. Well, Budget generally gets standardized. You 
can't count them.
    The Chairman. Committees will always look and see that one 
committee got more or less. Ways and Means, for example, might 
have a lower percentage increase, but has a larger dollar 
figure than some committees. But I do know we transmitted these 
numbers and did not hear an objection back.
    Mr. Ehlers. Well, at least you could have transmitted it to 
the members of this committee, too. It is awfully tough to come 
in a meeting and get it cut and dry and say, okay, take it or 
leave it. And the point is this is one committee--I serve on 
three committees; I know how many use their money. This 
committee uses it very wisely and judiciously. And just because 
they request a modest amount, I don't think they should be 
given one of the lowest figures. It is not a very expensive 
committee to begin with compared to many of the others.
    The Chairman. Let me ask this question of our staff. What 
was the total dollar figures Science had requested?
    Mr. Ehlers. I am just looking for equity. When I see 18 
percent increases, 10 percent increases, 12 percent, 22 
percent, et cetera. Then why not request a huge amount? I mean, 
this committee is supposed to make judicious judgments.
    Ms. Millender-McDonald. When we were going through the 
whole process----
    Mr. Ehlers. I never saw it.
    The Chairman. We did transmit the dollar figures to all the 
chairs and the ranking members. Heard back from some that they 
weren't happy and we couldn't accommodate them. Some people 
came back and said, ``well, you know, we need a bit more,'' and 
gave a rationale, and we considered it. But, like I said, these 
numbers were given and we didn't hear back.
    One thing I would argue is that a committee has to function 
by what they need. If a committee starts to say well, ``Ways 
and Means got more and we should have got more,'' then this 
whole thing would be another 70 million dollars.
    Mr. Ehlers. I understand that. But I don't understand why 
we are asked, just present the material and supposed to, in 5 
minutes, make the decision when we don't know what the factors 
were in the decisions.
    Ms. Lofgren. Would the gentleman yield?
    Mr. Ehlers. Yes.
    Ms. Lofgren. Which committee, either chairman or ranking 
members, raised objections to this that remain unresolved?
    Ms. Millender-McDonald. There aren't any.
    Ms. Lofgren. So everybody has----
    Ms. Millender-McDonald. I called many and the Chairman 
spoke with many. So there aren't any others on----
    The Chairman. We actually provided these--I personally 
provided these numbers to most of the Chairs and our staff went 
through the process providing numbers in the last week. This 
has just been formulated, frankly, and I apologize for that. We 
normally get things in time; this was just put together because 
there were going to be votes and we would have to pass 
continuing resolutions. But I just never heard back from 
anybody on that Committee to say this is just not----
    Mr. Ehlers. I understand. I am just frustrated.
    Mr. Brady. Mr. Chairman, I mean, I am on the Armed Services 
Committee. I know what committee I am on. I know what committee 
comes in front, I know what request they make. I vote on my 
ranking member and I vote on my chairman. I say, are you guys 
okay? That is the part of being on this committee that I 
thought was supposed to happen, and we are okay with this. And 
if I was on Science, I would have done that too. So this is 
like the----
    Mr. Ehlers. If you knew the numbers had been prepared.
    Mr. Brady. They know the numbers. They do. Your ranking 
member and your chairman. I take it that it is my 
responsibility being on the committee that is going to have any 
kind of financing of their committee to go to them and say, are 
you guys okay? I didn't just go to the ranking member, I went 
to the chairman. And they said, well, we would like more, but 
we could live with this. And I said fine. And and I checked 
back with the chairman and the Ranking Member and I said, look, 
they are okay.
    So I thought maybe that was what I was supposed to do. I 
mean, I can't imagine walking in and letting everybody see 
these numbers, because then you get into a bidding war. But 
that is what I did. I took that upon myself to do that.
    Mr. Ehlers. So you are saying the members of this committee 
shouldn't see the numbers before?
    Mr. Brady. No. I am saying the members of this committee 
should see the numbers.
    Mr. Ehlers. That is my point.
    Mr. Brady. That is well taken. But my chairman and my 
ranking member of the committee saw the numbers and they shared 
with me they were fine. But I think that maybe you are right, 
you should have them a little sooner.
    The Chairman. I would say, Mr. Ehlers, your office was 
informed if they would like to convey these numbers and chose 
to let us do that. I hesitate to mention that, but that is what 
happened.
    Mr. Ehlers. I will talk to them about that.
    The Chairman. Thank you. Any other questions?
    Ms. Lofgren. I just want to make sure that Chairman 
Sensenbrenner is not going to be angry.
    The Chairman. He will probably always be angry, about 
something else, though, probably not this.
    Mr. Brady. When they get happy, they should get happy with 
us. When they get angry, they should get angry with the 
chairman.
    The Chairman. And there is one thing. I went to the other 
chairs, and I gave them a number, I tried personally to get to 
all I could, but I believe the staff got to the rest. But every 
once in a while, I would have somebody say, let me see the 
list. And I would say, no. You tell me whether you are happy 
with your number. Because they are going to see the list and 
they are going to say, ``Well, they got a 15 percent 
increase.'' Well, actually, if you look at raw dollar figures, 
some of the 15 percents are not as high in raw dollars as the 
lower increases. So if you get into that, it is an unwinnable 
situation.
    Ms. Millender-McDonald. Mr. Chairman, the one thing, we 
have spoken with both Chairman Sensenbrenner and also the 
ranking member. They have come to their own agreement which 
they said is fine with them. So the same as with the small 
business and the same as with agriculture.
    Ms. Lofgren. Good work.
    The Chairman. There are still some slot issues that will be 
dealt with that we don't make a mark on. That is decided at the 
leader's level, above our pay grades, with the Speaker and the 
leader.
    Other questions? The Chair lays before the Committee bill 
H.R. 224 open to amendment, and the Chair offers an amendment 
in the nature of a substitute. Is there any discussion or 
amendments? The clerk will report the amendment. And we will 
recognize the ranking member.
    [The information follows:]
    [GRAPHIC] [TIFF OMITTED] 21196A.005
    
    [GRAPHIC] [TIFF OMITTED] 21196A.006
    
    [GRAPHIC] [TIFF OMITTED] 21196A.007
    
    [GRAPHIC] [TIFF OMITTED] 21196A.008
    
    Ms. Millender-McDonald. Thank you, Mr. Chairman. As I 
indicated in my opening statement, I have this amendment to the 
funding resolution. The amendment limits overall mass mailers 
by committees and ensures that each mass mailer is reviewed by 
the franking commission. It also requires that committee mass 
mailers pertain directly to the normal and regular business of 
the committee. Last year, just prior to the general election 
and earlier in the 108th Congress just prior to some primary 
elections we saw some self-initiated committee mass mailers, 
which I can only characterize as deeply troubled. If unchecked, 
and if this trend were to expand or continue, I believe that 
the frank would be placed in jeopardy for the entire House, and 
taxpayers would be rightly critical of our stewardship on this 
important communication.
    My amendment will fix this problem, and I hope that every 
member of the committee will consider it. I thank you, Mr. 
Chairman.
    The Chairman. I support the amendment. We have Jack Dail 
here. We have franking people who have a tough job, but they 
sit down and go over everything. And they will be able to more 
than adequately handle this end from the committees; I think it 
has the blackout date in it. These are some changes to existing 
law, and no problem to support.
    Ms. Miller. Could you expand a little bit on what the 
changes are?
    Ms. Millender-McDonald. The changes will be that all 
members, irrespective of committee chairs and all regular 
members, come under the 90-day rule before elections; and, 
secondarily, that they do get an advisory opinion by the 
franking commission before that material goes out.
    The Chairman. Also, a very good provision in this amendment 
enables us to plan for certain scenarios. Let us say there is 
some type of crisis in the country and these committees need to 
communicate, we can always come back. And if we need to add 
something on, in a unique situation, we can come back and 
discuss it and make a decision. I think that is a good 
provision. And again, they will go into franking just as all 
Members currently do.
    Ms. Miller. Have the committee chairs been made aware of 
this amendment as well?
    The Chairman. Yes. And there is no one that disagrees with 
it. Any questions on the amendment? Those in favor of the 
amendment will say aye. Those opposed will say nay. The 
question is now on the substitute as amended. Those in favor 
will say aye. Those opposed say nay. The bill is adopted as 
amended.
    Ms. Millender-McDonald. Mr. Chairman, pursuant to Clause 
2(l) of Rule 911, I am providing notice that I am requesting 
not less than 2 additional calendar days as provided by the 
rules to submit additional views to accompany the committee's 
report on this resolution.
    The Chairman. Without objection. And I would also like to 
recognize our ranking member for the purpose of offering a 
motion.
    Ms. Millender-McDonald. Mr. Chairman, I move that H.R. 224 
as amended be reported favorably to the House.
    The Chairman. The question is on the motion. Those in favor 
say aye. Those opposed nay. And the motion is agreed to, and 
H.R. 224 as amended is reported favorably to the House.
    Ms. Millender-McDonald. Thank you, Mr. Chairman.
    The Chairman. And now we lay before the Committee a 
Committee Resolution establishing the 109th Congress franked 
mail allocation for the committees. Is there a discussion?
    Ms. Millender-McDonald. Mr. Chairman, you and I have 
discussed this and we have discussed it with our leadership, 
and they are in agreement with that. And I move that we adopt 
this substitute motion.
    The Chairman. I recognize the gentlelady for the purpose of 
offering the motion. And it has been moved that the Committee 
Resolution establishing 109th Congress franked mail allocation 
be adopted. The question is on the motion. Those in favor will 
say aye. Those opposed will say nay. The motion is agreed to, 
and the Committee Resolution establishing 109th Congress 
franked mail allocation for the committees is adopted.
    As is technical, I ask unanimous consent that members be 
given 7 legislative days for statements and materials to be 
entered into the appropriate place in the record. Without 
objection, the material will be entered.
    I ask unanimous consent that staff be authorized to make 
technical and conforming changes on all matters considered by 
the Committee at today's meeting. Without objection, so 
ordered.
    Again, I want to thank all the members for your time and 
your concern with the funding resolution, and also the franking 
resolution. We are therefore adjourned.
    [Whereupon, at 2:59 p.m., the Committee was adjourned.]

</pre></body></html>
