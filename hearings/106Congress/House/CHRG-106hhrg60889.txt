<html>
<title> - JOHNNY CHUNG: FOREIGN CONNECTIONS, FOREIGN CONTRIBUTIONS</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]




                                [ERRATA]
 
        JOHNNY CHUNG: FOREIGN CONNECTIONS, FOREIGN CONTRIBUTIONS

=======================================================================

                                HEARING

                               before the

                              COMMITTEE ON
                           GOVERNMENT REFORM

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS

                             FIRST SESSION

                               __________

                              MAY 11, 1999

                               __________

                           Serial No. 106-23

                               __________

       Printed for the use of the Committee on Government Reform


     Available via the World Wide Web: http://www.house.gov/reform



                    U.S. GOVERNMENT PRINTING OFFICE
60-889 CC                   WASHINGTON :  1999
_______________________________________________________________________
 For sale by the Superintendent of Documents, U.S. Government Printing 
                                 Office
 Internet: bookstore.GPO.gov Phone: (202) 512-1800 Fax: (202) 512-2250
               Mail: Stop SSOP, Washington, DC 20402-0001



                                 ______
                                 ERRATA

    The following document should have been inserted at page 3 
of the printed hearing.
    [The document referred to follows:]
    [GRAPHIC] [TIFF OMITTED] T1901.003
    
    [GRAPHIC] [TIFF OMITTED] T1901.004
    
    [GRAPHIC] [TIFF OMITTED] T1901.005
    
    [GRAPHIC] [TIFF OMITTED] T1901.006
    
    [GRAPHIC] [TIFF OMITTED] T1901.007
    
    [GRAPHIC] [TIFF OMITTED] T1901.008
    
    [GRAPHIC] [TIFF OMITTED] T1901.009
    
    [GRAPHIC] [TIFF OMITTED] T1901.010
    
    [GRAPHIC] [TIFF OMITTED] T1901.011
    
    [GRAPHIC] [TIFF OMITTED] T1901.012
    
    [GRAPHIC] [TIFF OMITTED] T1901.013
    
    [GRAPHIC] [TIFF OMITTED] T1901.014
    
                                   - 
</pre></body></html>
