<html>
<title> - FOREIGN OPERATIONS, EXPORT FINANCING, AND RELATED PROGRAMS APPROPRIATIONS FOR 2002</title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
      FOREIGN OPERATIONS, EXPORT FINANCING, AND RELATED PROGRAMS 
                        APPROPRIATIONS FOR 2002

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS
                              FIRST SESSION
                                ________
   SUBCOMMITTEE ON FOREIGN OPERATIONS, EXPORT FINANCING, AND RELATED 
                                PROGRAMS
                      JIM KOLBE, Arizona, Chairman
 SONNY CALLAHAN, Alabama            NITA M. LOWEY, New York
 JOE KNOLLENBERG, Michigan          NANCY PELOSI, California
 JACK KINGSTON, Georgia             JESSE L. JACKSON, Jr., Illinois
 JERRY LEWIS, California            CAROLYN C. KILPATRICK, Michigan
 ROGER F. WICKER, Mississippi       STEVEN R. ROTHMAN, New Jersey  
 HENRY BONILLA, Texas
 JOHN E. SUNUNU, New Hampshire      
                                    
                                    
                                    
                                    

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
    Charles Flickner, John Shank, and Alice Grant, Staff Assistants,
                     Lori Maes, Administrative Aide
                                ________
                                 PART 1B

                             (Pages 1-3332)

               OFFICIAL JUSTIFICATION OF BUDGET ESTIMATES

                  AGENCY FOR INTERNATIONAL DEVELOPMENT

                                ________

         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 73-222                     WASHINGTON : 2001





                       COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                 DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California            JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky            NORMAN D. DICKS, Washington
 JOE SKEEN, New Mexico              MARTIN OLAV SABO, Minnesota
 FRANK R. WOLF, Virginia            STENY H. HOYER, Maryland
 TOM DeLAY, Texas                   ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                 MARCY KAPTUR, Ohio
 SONNY CALLAHAN, Alabama            NANCY PELOSI, California
 JAMES T. WALSH, New York           PETER J. VISCLOSKY, Indiana
 CHARLES H. TAYLOR, North Carolina  NITA M. LOWEY, New York
 DAVID L. HOBSON, Ohio              JOSE E. SERRANO, New York
 ERNEST J. ISTOOK, Jr., Oklahoma    ROSA L. DeLAURO, Connecticut
 HENRY BONILLA, Texas               JAMES P. MORAN, Virginia
 JOE KNOLLENBERG, Michigan          JOHN W. OLVER, Massachusetts
 DAN MILLER, Florida                ED PASTOR, Arizona
 JACK KINGSTON, Georgia             CARRIE P. MEEK, Florida
 RODNEY P. FRELINGHUYSEN, New JerseyDAVID E. PRICE, North Carolina
 ROGER F. WICKER, Mississippi       CHET EDWARDS, Texas
 GEORGE R. NETHERCUTT, Jr.,         ROBERT E. ``BUD'' CRAMER, Jr., 
Washington                          Alabama
 RANDY ``DUKE'' CUNNINGHAM,         PATRICK J. KENNEDY, Rhode Island
California                          JAMES E. CLYBURN, South Carolina
 TODD TIAHRT, Kansas                MAURICE D. HINCHEY, New York
 ZACH WAMP, Tennessee               LUCILLE ROYBAL-ALLARD, California
 TOM LATHAM, Iowa                   SAM FARR, California
 ANNE M. NORTHUP, Kentucky          JESSE L. JACKSON, Jr., Illinois
 ROBERT B. ADERHOLT, Alabama        CAROLYN C. KILPATRICK, Michigan
 JO ANN EMERSON, Missouri           ALLEN BOYD, Florida
 JOHN E. SUNUNU, New Hampshire      CHAKA FATTAH, Pennsylvania
 KAY GRANGER, Texas                 STEVEN R. ROTHMAN, New Jersey    
 JOHN E. PETERSON, Pennsylvania
 JOHN T. DOOLITTLE, California
 RAY LaHOOD, Illinois
 JOHN E. SWEENEY, New York
 DAVID VITTER, Louisiana
 DON SHERWOOD, Pennsylvania
 VIRGIL H. GOODE, Jr., Virginia     
                                    
                 James W. Dyer, Clerk and Staff Director

                                  (ii)





[Pages 1 - 3332--The official Committee record contains additional material here]
</pre></body></html>
