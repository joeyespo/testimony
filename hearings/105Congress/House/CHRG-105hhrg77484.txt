<html>
<title> - DEPARTMENT OF DEFENSE APPROPRIATIONS FOR 1998</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
             DEPARTMENT OF DEFENSE APPROPRIATIONS FOR 1998

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS
                              FIRST SESSION
                                ________
                    SUBCOMMITTEE ON NATIONAL SECURITY
                   C. W. BILL YOUNG, Florida, Chairman
 JOSEPH M. McDADE, Pennsylvania      JOHN P. MURTHA, Pennsylvania
 BOB LIVINGSTON, Louisiana           NORMAN D. DICKS, Washington
 JERRY LEWIS, California             W. G. (BILL) HEFNER, North Carolina
 JOE SKEEN, New Mexico               MARTIN OLAV SABO, Minnesota
 DAVID L. HOBSON, Ohio               JULIAN C. DIXON, California
 HENRY BONILLA, Texas                PETER J. VISCLOSKY, Indiana        
 GEORGE R. NETHERCUTT, Jr., 
Washington
 ERNEST J. ISTOOK, Jr., Oklahoma
 RANDY ``DUKE'' CUNNINGHAM, 
California                          
          
 NOTE: Under Committee Rules, Mr. Livingston, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
 Kevin M. Roper, John G. Plashal, David F. Kilian, Alicia Jones, Juliet 
  Pacquing,P Gregory J. Walters, Patricia Ryan, Doug Gregory, Paul W. 
       Juola, Tina Jonas, andP Steven D. Nixon, Staff Assistants
       Stacy A. Trimble and Jennifer Mummert, Administrative Aides
                                ________
                                 PART 1
                                                                   Page
 Secretary of Defense and Chairman, Joint Chiefs of Staff.........    1
 Secretary and Chief of Staff of the Army.........................  541
 Secretary of the Navy, Chief of Naval Operations, and Commandant 
of the Marine Corps...............................................  731
 Secretary and Chief of Staff of the Air Force....................  895
                                ________

         Printed for the use of the Committee on Appropriations

                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
 77-484                     WASHINGTON : 2002





                     COMMITTEE ON APPROPRIATIONS

                   BOB LIVINGSTON, Louisiana, Chairman

 JOSEPH M. McDADE, Pennsylvania      DAVID R. OBEY, Wisconsin
 C. W. BILL YOUNG, Florida           SIDNEY R. YATES, Illinois
 RALPH REGULA, Ohio                  LOUIS STOKES, Ohio
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois        NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky             MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico               JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia             VIC FAZIO, California
 TOM DeLAY, Texas                    W. G. (BILL) HEFNER, North Carolina
 JIM KOLBE, Arizona                  STENY H. HOYER, Maryland
 RON PACKARD, California             ALAN B. MOLLOHAN, West Virginia
 SONNY CALLAHAN, Alabama             MARCY KAPTUR, Ohio
 JAMES T. WALSH, New York            DAVID E. SKAGGS, Colorado
 CHARLES H. TAYLOR, North Carolina   NANCY PELOSI, California
 DAVID L. HOBSON, Ohio               PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma     THOMAS M. FOGLIETTA, Pennsylvania
 HENRY BONILLA, Texas                ESTEBAN EDWARD TORRES, California
 JOE KNOLLENBERG, Michigan           NITA M. LOWEY, New York
 DAN MILLER, Florida                 JOSE E. SERRANO, New York
 JAY DICKEY, Arkansas                ROSA L. DeLAURO, Connecticut
 JACK KINGSTON, Georgia              JAMES P. MORAN, Virginia
 MIKE PARKER, Mississippi            JOHN W. OLVER, Massachusetts
 RODNEY P. FRELINGHUYSEN, New Jersey ED PASTOR, Arizona
 ROGER F. WICKER, Mississippi        CARRIE P. MEEK, Florida
 MICHAEL P. FORBES, New York         DAVID E. PRICE, North Carolina
 GEORGE R. NETHERCUTT, Jr.,          CHET EDWARDS, Texas                
Washington
 MARK W. NEUMANN, Wisconsin
 RANDY ``DUKE'' CUNNINGHAM, 
California
 TODD TIAHRT, Kansas
 ZACH WAMP, Tennessee
 TOM LATHAM, Iowa
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama        

                 James W. Dyer, Clerk and Staff Director

                                  (ii)




              [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]




                           W I T N E S S E S

                              ----------                              
                                                                   Page
Cohen, Hon. W. S.................................................     1
Dalton, Hon. J. H................................................   731
Fogleman, Gen. R. R..............................................   895
Hamre, Hon. John.................................................     1
Johnson, Adm. J. L...............................................   731
Krulak, Gen. C. C................................................   731
Reimer, Gen. D. J................................................   541
Shalikashvili, Gen. J. M.........................................     1
West, Hon. T. D., Jr.............................................   541
Widnall, Hon. S. E...............................................   895


                               I N D E X

                              ----------                              

                   FISCAL YEAR 1998 AIR FORCE POSTURE

                                                                   Page
Additional Funding...............................................   991
Airborne Laser Program...........................................  1001
Air Combat Proficiency...........................................   976
Aircraft Costs...................................................   963
Air Expeditionary Forces (AEF)...................................  1009
Air Force Institute of Technology................................   985
Airlift Capacity.................................................   979
Air National Guard F-16 Aircraft.................................   935
Bomber Aircraft..................................................   965
    B-1 Bomber...................................................   940
    B-2 Stealth Bomber...........................................   999
    B-52 Bomber..................................................  1000
        Engine Lease.............................................  1003
CHAMPUS Reform Initiative (CRI)..................................   988
Deep Attack Weapons Mix..........................................   999
Depot Maintenance, Air Force.....................................  1007
F-22 Aircraft..................................................971, 983
    Cost Growth and Threat.......................................   997
Flight Hours.....................................................   982
Global Engagement: A Vision for the 21st Century Air Force.......  1063
Information Technology...........................................   986
Information Warfare...........................................977, 1004
Introduction.....................................................   895
Issues Book, The 1996 Nation's Air Force.........................  1015
Joint Surveillance and Target Attack Radar System (JSTARS).......  1000
    Re-Engining..................................................   987
Lab Consolidation................................................   986
Medical Program..................................................   939
Military Personnel Strength Levels...............................  1012
Modernization Shortfall..........................................   989
Navigation and Safety Equipment..................................   988
Permanent Change of Station (PCS) Moves..........................  1013
Precision Guided Munitions.......................................  1003
Privatization.................................................970, 1009
Quadrennial Defense Review (QDR).................................   941
Real Property Maintenance (RPM)..................................  1008
Recruiting Challenges............................................  1011
Reengine Program.................................................  1002
Remarks of Mr. Cunningham........................................   974
Remarks of Mr. Lewis.............................................   935
Research and Development.........................................   986
    Infrastructure...............................................  1004
Reusable Launch Vehicle Program..................................   964
Satellites, Potential Loss of....................................   969
Ship-Self Defense................................................  1006
Space Plane Program..............................................   964
Statement of Sheila E. Widnall and General Ronald R. Fogleman, 
  The Joint......................................................   905
Summary Statement of General Fogleman............................   900
    OPTEMPO/PERSTEMPO............................................   903
    Reenlistment.................................................   901
    Special Operations...........................................   900
    Summary......................................................   903
    TRICARE......................................................   902
Summary Statement of Secretary Widnall...........................   896
    Air and Space Superiority....................................   897
    Core Competencies............................................   897
    Missile Defense..............................................   898
    Quality of Life..............................................   899
    Space-Based Infrared System..................................   898
Tactical Aircraft Modernization................................941, 995
    Cost Comparisons, 35-Year Life-Cycle.........................   946
    Coast Savings Initiatives....................................   946
    Fighter Aircraft Roadmap...................................944, 984
    Foreign Air-to-Air Fighters..................................   942
    F-22 Restructured Program....................................   945
    F-22 Shrinks SAM Engagement Zones............................   943
    Investment by Decade.........................................   944
    Investment Funding...........................................   944
    Tactical Aircraft Investments, Department of Defense.........   944
    Tactical Aircraft Investments, United States Air Force.......   945
    Tactical Aircraft Roadmap....................................   944
    Threats to United States Forces..............................   942
    Time Phased Modernization Plan...............................   945
    Topline, Department of Defense...............................   943
    United States Forces Nearing Parity..........................   943
Tanker Aircraft..................................................   972
Terrorism.....................................................981, 1010
Test and Evaluation..............................................   981
Titan Vehicle....................................................   979
Troop Quality....................................................   938
Unmanned Aerial Vehicle (UAV)....................................   936
Voting, Absentee.................................................   968

                     FISCAL YEAR 1998 ARMY POSTURE
                                                                   Page
Advanced Warfighting Equipment...................................   635
Ammunition.......................................................   625
Black Hawk Multi-Year Procurement................................   622
Bosnia Supplemental, Fiscal Year 1997............................   588
Depot Maintenance Privatization................................598, 639
Digitization.....................................................   631
Fiscal Year 1998 Budget Request..................................   612
Force XXI Initiatives............................................   628
Force Reductions.................................................   599
Gender Integrated Training...........................588, 601, 605, 639
Information Systems Security...................................606, 636
Introduction.....................................................   541
Logistics Capability (LOGCAP)....................................   643
M1A2 Tank........................................................   600
Military Personnel Strength Levels...............................   636
Modernization Programs...........................................   619
National Training Center.......................................589, 608
NATO Headquarters Support........................................   642
Patriot Battalions...............................................   645
Peacekeeping Costs...............................................   596
Permanent Change of Station (PCS) Moves..........................   641
Personnel Issues.................................................   592
Privatization....................................................   642
Quadrennial Defense Review (QDR)...............................597, 604
Quality of Life..................................................   602
Recruiting.......................................................   598
    Quality Personnel............................................   637
    Other Challenges.............................................   638
Remarks of Mr. Dicks.............................................   542
Research and Development Infrastructure..........................   628
Reserve Mobilization Insurance Program...........................   640
Rocky Mountain Arsenal...........................................   643
Sexual Harassment................................................   604
Statement of General Dennis J. Reimer............................   567
Statement of Honorable Togo D. West, Jr..........................   549
Statement, The Department of the Army Posture, Fiscal Year 1998..   646
Strategic Sealift Support Equipment Lighterage...................   626
Summary Statement of General Reimer..............................   563
    Force XXI Initiatives........................................   565
    Readiness....................................................   564
Summary Statement of Secretary West..............................   543
    Aberdeen Proving Ground Investigation........................   546
    Army National Guard and Army Reserve.........................   546
    Fiscal Year 1998 Budget Request............................544, 548
    Modernization Programs.......................................   545
    Recruiting...................................................   545
Tactical High Energy Laser Program...............................   611
Tactical Unmanned Aerial Vehicle (TAUV)..........................   596
Tactical Wheeled Vehicles........................................   623
Terrorism/Force Protection.......................................   609
Test and Evaluation Infrastructure...............................   610
Unfunded Requirements............................................   614
Unmanned Aerial Vehicles (UAVs)..................................   591
      
              FISCAL YEAR 1998 DEFENSE POSTURE
                                                                   Page
Airborne Laser...................................................   106
Ballistic Missile Defense........................................   122
    Navy Upper Tier Program......................................   123
    Theater High Altitude Area Defense (THAAD) Program...........   123
Base Realignment and Closure.....................................90, 93
Bosnia Implementation Force/Stabilization Force:
    Costs........................................................   117
    Exit Strategy................................................   115
    LOGCAP Support...............................................   117
    Policy Issues..............................................101, 114
    Troop Strength...............................................   116
Bottom-Up Review Report..........................................    90
Budget Shortfalls and Deficiencies...............................   120
Chemical Weapons Stockpile Disestablishment, United States.......   100
Command, Control, and Communications.............................   128
Defense Health Program...........................................   153
Depot Maintenance Workload, Transition of........................    64
Fiscal Year 1998 Budget Request..................................    67
    Budget Rescission............................................    63
Gender Integrated Training......................................69, 153
General Provisions...............................................   157
Government Performance and Results Act...........................   112
Information Security.............................................   104
Introduction.....................................................     1
    Budget Request, President's..................................     2
    Partisanship.................................................     1
    Quadrennial Defense Review (QDR).............................     3
Iraqi and North Korean Military Threat...........................    78
Joint Surveillance Target Attack Radar System (JSTARS)..........89, 127
KC-135 Tanker Aircraft...........................................    94
    Next Generation Tankers......................................   103
Logistics Management and Reform..................................   106
Marine Corps Helicopter Programs.................................   130
Military Personnel Strength Levels...............................   154
Multiyear Procurement............................................   111
National Training Center Expansion...............................    81
NATO Expansion.............................................80, 102, 119
    Political Ramification.......................................    78
Peacekeeping and Contingency Operations..........................   117
Privatization Initiatives, Department of Defense.................   155
Procurement Funding..............................................   108
Quadrennial Defense Review (QDR)...............................109, 120
Quality of Life..................................................   111
Quality of Troops................................................    65
Recruiting Challenges............................................   154
Remarks of Mr. Hobson............................................    84
Remarks of Mr. Lewis.............................................    81
Report to the President and the Congress, The 1997 Annual........   167
Research and Development, Competitive Defense....................   102
Reserve Forces and National Guard Force Protection...............    97
Russian Arms Control Ratification................................    80
Shipbuilding Issues............................................110, 125
Southwest Asia Contingency Flying Hours..........................   117
Statement of General John M. Shalikashvili.......................    31
Statement of William S. Cohen....................................     7
Stealth Technology...............................................    86
Submarines.......................................................   110
Summary Statement of General Shalikashvili.......................    26
    Acquisition..................................................    29
    Bosnia.......................................................    27
    International Terrorism......................................    27
    Joint Vision 2010............................................    29
    Operations...................................................    26
    Priorities...................................................    28
    Readiness....................................................    28
Summary Statement of Secretary Cohen.............................     3
    Priorities...................................................     4
    Quadrennial Defense Review...................................     5
    Supplemental Request.........................................     6
Tactical Aircraft Modernization..................................   124
Terrorism........................................................    95
Test and Evaluation..............................................    98
Texas Rural Legal Aid (TRLA) Lawsuit.............................   107
    Military Voting Rights......................................88, 107
Training.........................................................    94
Travel, Department of Defense and Government....................92, 109
Tri-Service Standoff Attack Missile (TSSAM) Lawsuit..............   156
Unfunded Requirements............................................    62
Unmanned Aerial Vehicles (UAVs)..................................    82
V-22 Aircraft....................................................   126
Vaccines.........................................................   100
Volunteer Force..................................................   108
Weapons of Mass Destruction/Strategy.............................    99
Year 2000 Computer Problem.......................................   131

                     FISCAL YEAR 1998 NAVY POSTURE
                                                                   Page
Additional Funding...............................................   769
Aircraft Accidents...............................................   807
Aircraft Carriers..............................................749, 792
Arsenal Ship.....................................................   791
Budget Shortfalls................................................   755
Conventional Weapons.............................................   744
Cooperative Engagement Capability................................   781
DDG-51 Destroyer.................................................   779
    Multi-Year Program...........................................   741
Depot Maintenance:
    Aviation Backlogs............................................   802
    Policy.......................................................   761
    Ship.........................................................   803
Force Structure Levels...........................................   801
Ground Systems Modernization, Marine Corps.......................   809
Helicopter Programs, Marine Corps................................   813
Infrared Detection of Advanced Anti-Ship Cruise Missiles.........   787
Integrated Ship Defense..........................................   785
Introduction.....................................................   731
Joint Strike Fighter (JSF)...........................743, 745, 753, 762
Louisville Privatization.........................................   766
LPD-17 Amphibious Assault Ship...................................   793
Manufacturing Technology.........................................   795
Mobile Offshore Base (MOB).......................................   758
Modernization Shortfall..........................................   767
Naval Academy Organization and Regulations.......................   763
New Attack Submarine...........................................754, 779
Nimitz Aircraft Carrier Overhaul.................................   783
Operations Tempo.................................................   757
Permanent Change of Station (PCS) Moves..........................   798
Precision Strike Standoff Weapons................................   794
Privatization Initiatives, Navy..................................   806
Quadrennial Defense Review.......................................   746
Quality of Life..................................................   751
Readiness........................................................   747
Real Property Maintenance (RPM)..................................   804
Recruiting Challenges............................................   797
Remarks of Mr. Bonilla...........................................   746
Research and Development Infrastructure..........................   796
Sea Dragon.......................................................   811
Seahawk/Black Hawk Helicopters...................................   761
Ship Self-Defense..............................................739, 742
Shipboard Pregnancy..............................................   759
Shipbuilding Plan................................................   778
Shipbuilding Rate................................................   775
Small Boat Terrorist Threats.....................................   786
Statement, The Department of the Navy Posture, Fiscal Year 1997..   816
Summary Statement of Admiral Johnson.............................   737
Summary Statement of General Krulak..............................   738
Summary Statement of Secretary Dalton............................   732
    Acquisition Reform...........................................   734
    Personnel Issues.............................................   735
    Programs and Budget..........................................   734
Quadrennial Defense Review.......................................   733
    Strategy and Operations......................................   733
Supplemental Request, Emergency................................741, 752
Theater Ballistic Missile Defense:
    Lower Tier...................................................   788
    Upper Tier.................................................758, 790
Transfer of Costs from Domestic Agencies to DoD..................   802
Trident Submarine..............................................744, 763
V-22 Aircraft.............................................752, 763, 808
Vertical Launch Anti-Submarine Rocket (VLA)......................   761
Working Capital Fund, Navy.......................................   805

                                <greek-d>


</pre></body></html>
