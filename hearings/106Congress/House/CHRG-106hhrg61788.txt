<html>
<title> - H.R. 605, COURT OF APPEALS FOR VETERANS CLAIMS ACT OF 1999; H.R. 690, RELATED TO BRONCHIOLO-ALVEOLAR CARCINOMA; H.R. 708, SURVIVING SPOUSES BENEFIT RESTORATION ACT; H.R. 784, REGARDING DEPENDENCY AND INDEMNITY COMPENSATION FOR SURVIVING SPOUSES OF CERTAIN FORMER PRISONERS OF WAR; H.R. 1214, VETERANS' CLAIMS ADJUDICATION IMPROVEMENT OF 1999; AND H.R. 1765, VETERANS' COMPENSATION COST-OF-LIVING ADJUSTMENT ACT OF 1999</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]




 
 H.R. 605, COURT OF APPEALS FOR VETERANS CLAIMS ACT OF 1999; H.R. 690, 
 RELATED TO BRONCHIOLO-ALVEOLAR CARCINOMA; H.R. 708, SURVIVING SPOUSES 
 BENEFIT RESTORATION ACT; H.R. 784, REGARDING DEPENDENCY AND INDEMNITY 
COMPENSATION FOR SURVIVING SPOUSES OF CERTAIN FORMER PRISONERS OF WAR; 
H.R. 1214, VETERANS' CLAIMS ADJUDICATION IMPROVEMENT OF 1999; AND H.R. 
   1765, VETERANS' COMPENSATION COST-OF-LIVING ADJUSTMENT ACT OF 1999

=======================================================================

                                HEARING

                               before the

                        SUBCOMMITTEE ON BENEFITS

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                        HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS

                             FIRST SESSION
                               __________

                             JUNE 10, 1999

                               __________

       Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 106-15

                  U.S. GOVERNMENT PRINTING OFFICE
61-788                    WASHINGTON : 2000
-----------------------------------------------------------------------
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512�091800  
Fax: (202) 512�092104 Mail: Stop IDCC, Washington, DC 20402�090001

<TEXT FILE NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
