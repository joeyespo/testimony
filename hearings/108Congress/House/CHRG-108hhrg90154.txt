<html>
<title> - DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE JUDICIARY, AND RELATED</title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]


'              
 
 DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE JUDICIARY, AND RELATED

                    AGENCIES APPROPRIATIONS FOR 2003

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                      ONE HUNDRED EIGHTH CONGRESS

                             FIRST SESSION
                                ________
  SUBCOMMITTEE ON THE DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE 
                    JUDICIARY, AND RELATED AGENCIES

                    FRANK R. WOLF, Virginia, Chairman

 HAROLD ROGERS, Kentucky             JOSE E. SERRANO, New York
 JIM KOLBE, Arizona                  ALAN B. MOLLOHAN, West Virginia 
 CHARLES H. TAYLOR North Carolina,   ROBERT E. ``BUD'' CRAMER, Jr.
 RALPH REGULA, Ohio                   Alabama 
 DAVID VITTER, Louisiana             PATRICK J. KENNEDY, Rhode Island
 JOHN E. SWEENEY, New York           MARTIN OLAV SABO, Minnesota 
 MARK STEVEN KIRK, Illinois,   

 
                                    
 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
   Mike Ringler, Christine Kojac, Leslie Albright, and John F. Martens
                           Subcommittee Staff
                                ________
                                 PART 4
                                                                   Page
 The Judiciary....................................................    1
 Commission on Civil Rights.......................................  273
 Commission on International Religious Freedom....................  325
 Commission on Security and Cooperation in Europe.................  355
 Commission for Preservation of America's Heritage Abroad.........  393
 Congressional-Executive Commission on China......................  401
 Equal Employment Opportunity Commission..........................  409
 Federal Communications Commission................................  485
 Federal Trade Commission.........................................  617
 International Trade Commission...................................  745
 Legal Services Corporation.......................................  849
 Marine Mammal Commission.........................................  881
 Office of the United States Trade Representative.................  885
 Securities and Exchange Commission...............................  953
 Small Business Administration.................................... 1089
 State Justice Institute.......................................... 1237
                                ________
         Printed for the use of the Committee on Appropriations
                                ________
                     U.S. GOVERNMENT PRINTING OFFICE
90-154                     WASHINGTON : 2003




                       COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                  DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky             NORMAN D. DICKS, Washington
 FRANK R. WOLF, Virginia             MARTIN OLAV SABO, Minnesota
 JIM KOLBE, Arizona                  STENY H. HOYER, Maryland
 JAMES T. WALSH, New York            ALAN B. MOLLOHAN, West Virginia
 CHARLES H. TAYLOR, North Carolina   MARCY KAPTUR, Ohio
 DAVID L. HOBSON, Ohio               PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma     NINA M. LOWEY, New York  
 HENRY BONILLA, Texas                JOSE E. SERRANO, New York
 JOE KNOLLENBERG, Michigan           ROSA L DeLAURO, Connecticut
 JACK KINGSTON, Georgia              JAMES P. MORAN, Virginia
 RODNEY P. FRELINGHUYSEN, New Jersey JOHN W. OLVER, Massachusetts
 ROGER F. WICKER, Mississippi        ED PASTOR, Arizona
 GEORGE R. NETHERCUTT, Jr.,          DAVID E. PRICE, North Carolina   
  Washington                         CHET EDWARDS, Texas                                           
  RANDY ``DUKE'' CUNNINGHAM,         ROBERT E. ``BUD'' CRAMER, Jr., 
  California                           Alabama
  TODD TIAHRT, Kansas                PATRICK J. KENNEDY, Rhode Island
  ZACH WAMP, Tennessee               JAMES E. CLYBURN, South Carolina
  TOM LATHAM, Iowa                   MAURICE D. HINCHEY, New York   
  ANNE M. NORTHUP, Kentucky          LUCILLE ROYBAL-ALLARD, California
  ROBERT B. ADERHOLT, Alabama        SAM FARR, California
  JO ANN EMERSON, Missouri           JESSE L. JACKSON, Jr., Illinois
  KAY GRANGER, Texas                 CAROLYN C. KILPATRICK, Michigan
 JOHN E. PETERSON, Pennsylvania      ALLEN BOYD, Florida
 VIRGIL H. GOODE, Jr., Virginia      CHAKA FATTAH, Pennsylvania
 JOHN T. DOOLITTLE, California       STEVEN R. ROTHMAN, New Jersey
 RAY LaHOOD, Illinois                SANFORD D. BISHOP, Jr. Georgia 
 JOHN E. SWEENEY, New York           MARION BERRY, Arkansas                               
 DAVID VITTER, Louisiana             
 DON SHERWOOD, Pennsylvania         
 DAVE WELDON, Florida 
 MICHEAL K. SIMPSON, Idaho
 JOHN ABNEY CULBERSON, Texas 
 MARK STEVEN KIRK, Illinois 
 ANDER CRENSHAW, Florida 





                 James W. Dyer, Clerk and Staff Director

                                  (ii)




              [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]



</pre></body></html>
