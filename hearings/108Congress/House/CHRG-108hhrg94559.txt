<html>
<title> - WEAPONS OF MASS DESTRUCTION: IS OUR NATION'S MEDICAL COMMUNITY READY? </title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]



 
                   WEAPONS OF MASS DESTRUCTION: IS OUR
                   NATION'S MEDICAL COMMUNITY READY?
----------------------------------------------------------------------- 


                              HEARING

                             BEFORE THE

             SUBCOMMITTEE OVERSIGHT AND INVESTIGATIONS

                               OF THE

                  COMMITTEE ON VETERANS' AFFAIRS

                      HOUSE OF REPRESENTATIVES 

                     ONE HUNDRED EIGHT CONGRESS 

                            FIRST SESSION

                           --------------

                          APRIL 10, 2003 

                             ----------- 

     Printed for the use of the Committee on Veterans' Affairs

                           Serial No. 108-8




                     U.S. GOVERNMENT PRINTING OFFICE
94-559 PDF                 WASHINGTON DC:  2004
---------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing
Office  Internet: bookstore.gpo.gov Phone: toll free (866)512-1800
DC area (202)512-1800  Fax: (202) 512-2250 Mail Stop SSOP, 
Washington, DC 20402-0001


<TEXT NOT AVAILABLE>

</pre></body></html>
