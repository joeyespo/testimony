<html>
<title> - AGRICULTURE, RURAL DEVELOPMENT, FOOD</title>
<body><pre>
[House Hearing, 108 Congress]
[From the U.S. Government Printing Office]




 
                 AGRICULTURE, RURAL DEVELOPMENT, FOOD

                  AND DRUG ADMINISTRATION, AND RELATED

                    AGENCIES APPROPRIATIONS FOR 2004

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED EIGHTH CONGRESS
                              FIRST SESSION
                                ________
     SUBCOMMITTEE ON AGRICULTURE, RURAL DEVELOPMENT, FOOD AND DRUG 
                  ADMINISTRATION, AND RELATED AGENCIES
                     HENRY BONILLA, Texas, Chairman

 JAMES T. WALSH, New York              MARCY KAPTUR, Ohio
 JACK KINGSTON, Georgia                ROSA L. DeLAURO, Connecticut
 GEORGE R. NETHERCUTT, Jr.,            MAURICE D. HINCHEY, New York
 Washington                            SAM FARR, California
 TOM LATHAM, Iowa                      ALLEN BOYD, Florida          
 JO ANN EMERSON, Missouri
 VIRGIL H. GOODE, Jr., Virginia
 RAY LaHOOD, Illinois               
                                    
                                    
                                    
                                    

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
   Henry E. Moore, Martin P. Delgado, Maureen Holohan, and Joanne L. 
                        Perdue, Staff Assistants
                                ________
                                 PART 5

                          AGRICULTURAL PROGRAMS
                                                                   Page
 Food and Drug Administration.....................................    1
 Farm Credit Administration.......................................  892
 Commodity Futures Trading Commission............................. 1103



                                ________
         Printed for the use of the Committee on Appropriations
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
 86-642                     WASHINGTON : 2003





                   COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                      DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California                 JOHN P. MURTHA, Pennsylvania
 HAROLD ROGERS, Kentucky                 NORMAN D. DICKS, Washington
 FRANK R. WOLF, Virginia                 MARTIN OLAV SABO, Minnesota
 JIM KOLBE, Arizona                      STENY H. HOYER, Maryland
 JAMES T. WALSH, New York                ALAN B. MOLLOHAN, West Virginia
 CHARLES H. TAYLOR, North Carolina       MARCY KAPTUR, Ohio
 DAVID L. HOBSON, Ohio                   PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma         NITA M. LOWEY, New York
 HENRY BONILLA, Texas                    JOSE E. SERRANO, New York
 JOE KNOLLENBERG, Michigan               ROSA L. DeLAURO, Connecticut
 JACK KINGSTON, Georgia                  JAMES P. MORAN, Virginia
 RODNEY P. FRELINGHUYSEN, New Jersey     JOHN W. OLVER, Massachusetts
 ROGER F. WICKER, Mississippi            ED PASTOR, Arizona
 GEORGE R. NETHERCUTT, Jr.,              DAVID E. PRICE, North Carolina
Washington                               CHET EDWARDS, Texas
 RANDY ``DUKE'' CUNNINGHAM,              ROBERT E. ``BUD'' CRAMER, Jr., 
California                               Alabama
 TODD TIAHRT, Kansas                     PATRICK J. KENNEDY, Rhode Island
 ZACH WAMP, Tennessee                    JAMES E. CLYBURN, South Carolina
 TOM LATHAM, Iowa                        MAURICE D. HINCHEY, New York
 ANNE M. NORTHUP, Kentucky               LUCILLE ROYBAL-ALLARD, California
 ROBERT B. ADERHOLT, Alabama             SAM FARR, California
 JO ANN EMERSON, Missouri                JESSE L. JACKSON, Jr., Illinois
 KAY GRANGER, Texas                      CAROLYN C. KILPATRICK, Michigan
 JOHN E. PETERSON, Pennsylvania          ALLEN BOYD, Florida
 VIRGIL H. GOODE, Jr., Virginia          CHAKA FATTAH, Pennsylvania
 JOHN T. DOOLITTLE, California           STEVEN R. ROTHMAN, New Jersey
 RAY LaHOOD, Illinois                    SANFORD D. BISHOP, Jr., Georgia
 JOHN E. SWEENEY, New York               MARION BERRY, Arkansas    
 DAVID VITTER, Louisiana
 DON SHERWOOD, Pennsylvania
 DAVE WELDON, Florida
 MICHAEL K. SIMPSON, Idaho
 JOHN ABNEY CULBERSON, Texas
 MARK STEVEN KIRK, Illinois
 ANDER CRENSHAW, Florida  
                                    
                                                                                                                                                        
                                                                                                                     
                                                                        

                 James W. Dyer, Clerk and Staff Director

                                  (ii)


                [GRAPHIC(S) NOT AVAILABLE IN TIFF FORMAT]



                           W I T N E S S E S

                              ----------                              
                                                                   Page
McClellan, Mark..................................................     1
Reyna, M. M......................................................   892
Weber, J. M......................................................     1
Weems, K. N......................................................     1


                               I N D E X

                              ----------    

                      Food and Drug Administration

                                                                   Page
Advisory Committees..............................................    96
Agriculturally Derived Pharmaceuticals...........................    44
AIDS Activities..................................................    98
AIDS Therapies...................................................    98
Animal Drugs and Feeds:
    Animal Drug Review....................................101, 247, 248
    BSE-Prevention Activities....................................   114
    BSE-Resources..............................................116, 119
    BSE-Vaccine Safety...........................................   118
    User Fees, Animal Drug.................................45, 235, 269
Antibiotic/Antimicrobial Resistance:
    Antimicrobial Resistance Activities..........................   102
    Antimicrobial Resistance Resources...........................   101
    Guidance for Industry......................................104, 262
    Package Inserts, Antibiotic..................................   262
    Proposed Rule................................................   262
    Withdrawal of Product Approval...............................   104
Biographical Sketches:
    Dr. Mark McClellan, M.D., Ph.D...............................    32
    Mr. Kerry N. Weems...........................................    33
    Mr. Jeffrey M. Weber.........................................    34
Biologics:
    Blood Safety.................................................   108
    Blood Safety--Donation Deferrals.............................   113
    Blood Safety--West Nile Virus..............................110, 112
    Blood Safety Resources.......................................   114
    CBER/CDER Consolidation......................................   213
    PDUFA........................................................   197
    PDUFA Financial Report.......................................   203
    PDUFA Performance Goals......................................   201
    Plasma Therapies.............................................   250
Bovine Spongiform Encephalopathy (BSE):
    Prevention Activities........................................   114
    Resources..................................................116, 119
    Vaccine Safety.............................................118, 226
Budget:
    Congressional Justification..................................   285
        Budget in Brief..........................................   290
        Budget in Brief to Reflect FY 2003 Appropriation, Revised   618
        Foods....................................................   367
        Human Drugs..............................................   391
        Biologics................................................   426
        Animal Drugs and Feeds...................................   447
        Devices and Radiological Health..........................   468
        National Center for Toxicological Research...............   491
        Office of Regulatory Affairs.............................   501
        Other Activities.........................................   531
        Rent.....................................................   549
        Buildings and Facilities.................................   554
    Contingency Fund.............................................   125
    Crosswalk from Current Estimate to Actuals...................   120
    Counter Terrorism Funding....................................   209
    Current Services.............................................   125
    Expiring Authorizations......................................   208
    FY 2004 Budget Request.......................................   285
    FY 2004 Budget Initiatives by Program......................123, 212
    FY 2003 Savings..............................................    62
    Funding to Meet Statutory Requirements.......................   165
    Research.....................................................   205
    Reductions and Cost Savings..................................    61
    Unauthorized Appropriations..................................   208
Buildings and Facilities:
    CDER Move....................................................   215
    Consolidation Plan and Status................................   168
    Los Angeles Laboratory.......................................   194
Chronic Diseases Undertreated....................................    90
Closing Comments.................................................    91
Color Certification.............................................35, 231
Congressional Directives:
    Adverse Events Reporting System--Medical Devices.............   158
    Biotechnology................................................   152
    Blood Safety and Adequacy....................................   145
    Blood Supply...............................................147, 154
    Building International Regulatory Capacity...................   148
    Dietary Supplement Study.....................................   142
    Drug Patent Listing..........................................   143
    Ephedra Products.............................................   142
    Food Allergies...............................................   142
    Foodborne Illness from Raw Shellfish.........................   151
    Food Safety..................................................   150
    Generic Drugs................................................   153
    Genetically Engineered Foods.................................   138
    Ginseng......................................................   140
    HACCP Compliance.............................................   152
    Intravenous Immune Globulin..................................   144
    Juice HACCP Implementation...................................   138
    Listeria Risk Assessment.....................................   139
    Medical Devices--Class II....................................   157
    Office of Women's Health.....................................   159
    Preventing Allergic Reactions to Natural Rubber Latex in the 
      Workplace..................................................   156
    Reused Medical Devices.......................................   157
    Seafood Safety...............................................   150
    Shellfish Safety.............................................   139
    Standards of Identity--Milk Protein Concentrate..............   153
    Tissue Safety Rules..........................................   156
Congressional Reports--Status....................................   160
Counter Terrorism:
    Funding, Counter Terrorism...................................    72
    Food Safety.................................................38, 277
FDA Initiatives and Timeline.....................................    75
First Amendment Issues...........................................   260
Foods/Food Safety:
    Acrylamide...................................................   273
    Allergens....................................................   284
    Application Review Statutory Requirement and Performance 
      Level......................................................   163
    Bioengineered Foods........................................229, 252
    Budget, Food Safety.........................................77, 174
    CFSAN Adverse Event Reporting System (CAERS).................   227
    Codex Alimentarius Commission................................   125
    Counter Terrorism...........................................38, 277
    Dietary Supplements........................................133, 251
        News Release--Proposed Regulation........................    93
    Egg Safety.................................................167, 279
    Ephedra................................................57, 134, 136
    Foodborne Illness, Source of.................................    59
    Food Safety............................................76, 175, 265
    Food Safety Definition.......................................   174
    Food Safety Initiative.....................................174, 180
    Gel Candies..................................................   230
    HAACP-Juice or Seafood.......................................   205
    Homeland Security and Food Safety............................    83
    Irradiation.................................................71, 263
    Labeling...................................................171, 232
    Listeria Monocytogenes.....................................139, 275
    Mercury Levels in Seafood....................................   279
    Milk Protein Concentrate.....................................   281
    Organic Food Labeling........................................    52
    Pesticides...................................................    53
    Recalls...............................................172, 254, 278
    Seafood Safety/Shellfish Safety.......................205, 257, 272
    Single Food Agency...........................................    82
    Threat Assessment Report.....................................   260
Good Manufacturing for the 21st Century..........................    49
Human Drugs:
    Adverse Events--Recalls......................................   105
    Adverse Drug Information, Source of..........................    82
    Adverse Medical Event Activity Funding.......................   105
    Agriculturally Derived Pharmaceuticals.......................   247
    AIDS Drug Approval Times.....................................    98
    Application Review Statutory Requirement and Performance 
      Level......................................................   163
    Aseptic Processing of Drugs..................................    49
    Best Pharmaceuticals for Children Act........................   259
    CDER/CBER Consolidation......................................   213
    Cost of Drug Advertising............................55, 87, 91, 241
    Cost of Prescription Drugs...................................    56
    Cuban Pharmaceutical Industry................................    84
    Drug Application Review Times...............................61, 161
    Federal Subsidies to Drug Companies..........................    70
    Gender Based Trials..........................................   270
    Generic Drugs:
        Approvals................................................   183
        Cost of Pharmaceuticals..................................    60
        Education Campaign......................................41, 267
        Proposed Rule...................................39, 42, 75, 267
        Proposed Rule Timeline...................................    40
        Review Times..............................41, 74, 183, 268, 270
        Patents and Generic Drugs................................    73
        Resources....................................182, 184, 261, 270
    Grapefruit/Drug Interactions.................................    78
    Guaifenesin.................................................69, 236
    Importation/Re-importation of Drugs..........................50, 57
    Internet Drug Sales........................................192, 244
    Marketing and Advertising, Drug.....................55, 87, 91, 241
    Marketing, Drug and Enforcement..............................75, 85
    Medical Gas..................................................   250
    Mental Illness..........................................62, 68, 257
    Misuse of Pharmaceuticals....................................    60
    Orange Book..................................................   234
    Overmedicated and Undermedicated Society.....................    88
    Over the Counter Drugs (OTC)..........................239, 248, 249
    Patient Safety/Adverse Events..............................107, 224
    PDUFA........................................................   197
    PDUFA Financial Report.......................................   203
    PDUFA Performance Goals......................................   201
    Pediatric Rule..........................................45, 54, 221
    Personal Importation Policy..................................   193
    Pre- and Postmarketing of Drugs..............................    81
    Prescription Drug Benefits for Seniors.......................   249
    Re-importation of Drugs.....................................50, 244
    Safety of Approved Drugs.....................................    80
Imports and Inspections:
    Food Inspections--Domestic...................................   170
    Foreign Facility, Inspection of.............................78, 265
    Funding to Meet Statutory Requirements.......................   165
    High Risk Food Inspections...................................   170
    Imported Food Inspections....................................   170
    Inventory of Firms.........................................171, 187
    Laboratory Capacity..........................................   233
    Line Entries, Imports........................................   187
    Personal (Prescription Drug) Importation Policy..............   193
    Re-importation of Drugs.....................................50, 244
    Risk-based Inspections and New Approach......................   223
    Safety of Imported Products..................................    73
    Tampering....................................................   206
International Activities:
    Implementation of the US-EU Mutual Recognition Agreement.....   191
    International Cooperative Agreements.......................189, 265
    International Health Regulations.............................   265
    Priorities in International Activities.......................   190
Introduction of Witnesses........................................     1
Justification of Estimates for Appropriations Committees:
    Organization Chart, Current..................................   288
    Organization Chart, Proposed.................................   289
    Budget in Brief..............................................   290
    Performance Plan Summary.....................................   340
    Appropriations Language and Analysis.........................   344
    Base Resources...............................................   345
    Summary of Change............................................   347
    Crosswalk for All Purpose Table--Budget Authority Changes....   348
    Crosswalk to Summary of Change--Program Level................   351
    Budget Authority by Activity (All Purpose Tables)............   352
    Comparable Tables............................................   355
    Program Justifications.......................................   367
    Exhibits.....................................................   557
    Glossary.....................................................   612
    Resubmission of Budget in Brief..............................   617
Management Activities:
    Consolidation................................................   219
    Office of Legislative Affairs................................   215
    Reorganizations..............................................   215
Medical Devices and Radiological Products:
    510(k) Applications..........................................   126
    Application Review Statutory Requirement and Performance 
      Level......................................................   163
    Approval Times...............................................   128
    Devices--Establishment Types.................................   130
    Devices--Extramural Projects.................................   131
    Devices--Regulatory Actions and Enforcement..................   129
    Devices Resources..........................................128, 129
    Mammography Quality Standards Act (MQSA).....................   195
    MedSuN.......................................................   227
    Regulatory Actions...........................................   129
    User Fees, Medical Device..........................47, 51, 268, 269
    User Fees, Medical Device and Budget Authority...............    51
Office of Criminal Investigations................................   196
Opening Remarks by Dr. McClellan, FDA Commissioner...............     2
Patents and Generic Drugs........................................    73
Patient Safety/Adverse Events..................................107, 224
Pediatric Rule..............................................45, 54, 221
Pediatric Rule Codification......................................    54
Pediatric Rule Litigation........................................    46
Performance Plan:
    Performance Plan.............................................   684
    Performance Plan Executive Summary...........................   688
    Performance Plan Report by Programs/Areas....................   732
    Performance Plan Appendices..................................   858
Personnel:
    Hiring Rate..................................................   195
    New Hires....................................................   195
Questions Submitted for the Record:
    Mr. Bonilla..................................................    96
    Mr. Nethercutt...............................................   244
    Mr. Latham...................................................   247
    Mr. LaHood...................................................   250
    Ms. Kaptur...................................................   254
    Ms. DeLauro..................................................   265
    Mr. Obey.....................................................   281
    Mrs. Lowey...................................................   284
Public Perception of FDA.........................................   256
Recalls:
    Products Recovered in a Recall........................172, 254, 278
    Recall Authority.............................................   255
Rent.............................................................   204
Research.........................................................   205
Statement by Dr. McClellan, M.D., Ph.D., DFA Commissioner........     6
United Financial Management System (UFMS)........................   218
U.S. Code Citations..............................................   207
Warning Letters, Procedures for Clearing.........................   264
Witness List.....................................................     1
Women's Health:
    Demographic Database on Clinical Trials......................   270
    Hormone Replacement Therapy.............................35, 45, 266
    Hormone Replacement Therapy Recommendations..................    37

                       Farm Credit Administration

Annual Performance Plan..........................................   973
Audit............................................................   915
Bio--Michael M. Reyna............................................   907
Budget Trends....................................................   945
Compensation Study...............................................   933
Equipment........................................................   934
Examination Issues...............................................   915
Farm Credit System:
    Annual Report................................................   931
    Financial Condition........................................899, 912
    Market Share.................................................   929
    Senior Officer Compenastion................................913, 920
    Structure....................................................   911
Farm Credit System Insurance Corporation.........................   922
FCA:
    Operations...................................................   908
    Staffing.....................................................   908
Federal Agricultural Mortgage Corporation......................900, 924
Fiscal Year 2002 Accomplishments.................................   894
Fiscal Year 2004 Budget Request..................................   903
Fiscal Year 2004 Proposed Budget and Annual Performance Plan.....   940
Litigation Involving FCA.........................................   919
Loans for Designated Parties.....................................   930
Major Accomplishments and Projects During FY 2002................   951
Performance and Accountability Report Fiscal Year 2002...........   990
Questions Submitted by Chairman Bonilla..........................   908
Risk Assessment..................................................   915
Role and Responsibility of the Farm Credit Administration........   893
Sole Source Contracts and Consulting Services Contracts..........   918
Status of Regulations............................................   934
Summaries of Studies.............................................   938
Testimony--Michael M. Reyna......................................   892
Young, Beginning, and Small Farmers..............................   928

                  Commodity Futures Trading Commission

Advisory Committees..............................................  1126
Agricultural Trade Options Rules.................................  1106
Agricultural Trading Volume......................................  1107
Appropriations and Authorized FTEs...............................  1125
Budget Reduction, Impact of......................................  1133
Carryover Funds..................................................  1103
Civil and Administrative Proceedings.............................  1118
Civil Monetary Penalties.........................................  1112
Civil Monetary Penalties Imposed and Collected...................  1112
Contract Designation Applications, Pending.......................  1119
Contract Market Designations and Rules Reviews Under Fast Track 
  Review.........................................................  1129
Contracts Approved, New..........................................  1119
Cooperative Enforcement and Regulation...........................  1117
Counter-Terrorism................................................  1136
Derivatives Clearing Organizations...............................  1140
Dual Trading.....................................................  1109
    Dual Trading Fraud...........................................  1110
    Dual Trading Investigations..................................  1110
Emergency Supplemental:
    Expenditures, Description of.................................  1133
    Status of FY 2003 Funds......................................  1132
Employment, FY 2003 Monthly On-Board.............................  1134
Energy Markets...................................................  1140
Enforcement Investigations.......................................  1116
Equipment Costs..................................................  1136
Firm Failures....................................................  1111
Foreign Assistance...............................................  1113
Foreign Currency Transactions....................................  1138
Inspector General Audits.........................................  1103
International Affairs, Activities of the Office of...............  1127
Introducing Brokers, Registered..................................  1126
New Authorities Under the CFMA, Impact of........................  1144
NFA Disciplinary Actions.........................................  1107
OMB Request......................................................  1103
Opt Out of Segregation...........................................  1132
Other Services Costs.............................................  1136
Pay Parity.......................................................  1135
President's Working Group on Financial Markets...................  1110
Rental Costs.....................................................  1136
Retention Bonuses................................................  1137
Reparations Program..............................................  1128
Rule Enforcement Program.........................................  1109
SEC and CFTC Cooperative Efforts.................................  1108
Security Futures Products........................................  1131
Security Futures Products, CFTC-SEC Joint Oversight of...........  1143
Self-Policing Activities.........................................  1111
Service Fees.....................................................  1107
Single-Stock Futures Regulations.................................  1132
Staffing Level, Reduced:
    Plans to Reach...............................................  1135
    Use of Attrition in Reaching.................................  1135
Studies Required by the CFMA:
    Intermediary Study Pursuant to the CFMA......................  1137
    Study Required by Section 125 of CFMA........................  1145
Travel Budget....................................................  1113
Unauthorized Appropriations......................................  1131
USA Patriot Act..................................................  1141

                                  <all>

</pre></body></html>
