<html>
<title> - MILITARY CONSTRUCTION APPROPRIATIONS FOR 1999</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
              MILITARY CONSTRUCTION APPROPRIATIONS FOR 1999

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS
                             SECOND SESSION
                                ________

          SUBCOMMITTEE ON MILITARY CONSTRUCTION APPROPRIATIONS

                    RON PACKARD, California, Chairman
 JOHN EDWARD PORTER, Illinois       W. G. (BILL) HEFNER, North Carolina
 DAVID L. HOBSON, Ohio              JOHN W. OLVER, Massachusetts
 ROGER F. WICKER, Mississippi       CHET EDWARDS, Texas
 JACK KINGSTON, Georgia             ROBERT E. (BUD) CRAMER, Jr., Alabama
 MIKE PARKER, Mississippi           NORMAN D. DICKS, Washington
 TODD TIAHRT, Kansas
 ZACH WAMP, Tennessee


 NOTE: Under Committee Rules, Mr. Livingston, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.

Elizabeth C. Dawson, Henry E. Moore, and Mary C. Arnold, Subcommittee Staff
                                ________

                                 PART 4
                  JUSTIFICATION OF THE BUDGET ESTIMATES

                      BASE REALIGNMENT AND CLOSURE
                                                                   Page
 Executive Summary................................................    1
 Army--Part III...................................................   49
 Army--Part IV....................................................   83
 Navy--Part II....................................................  255
 Navy--Part III...................................................  335
 Navy--Part IV....................................................  517
 Air Force--Part III..............................................  727
 Air Force--Part IV...............................................  793
 Defense Logistics Agency--Part III...............................  921
 Defense Logistics Agency--Part IV................................  947
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
 46-537 O                   WASHINGTON : 1998
------------------------------------------------------------------------------
           For sale by the U.S. Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402



                      COMMITTEE ON APPROPRIATIONS

                   BOB LIVINGSTON, Louisiana, Chairman

 JOSEPH M. McDADE, Pennsylvania      DAVID R. OBEY, Wisconsin
 C. W. BILL YOUNG, Florida           SIDNEY R. YATES, Illinois
 RALPH REGULA, Ohio                  LOUIS STOKES, Ohio
 JERRY LEWIS, California             JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois        NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky             MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico               JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia             VIC FAZIO, California
 TOM DeLAY, Texas                    W. G. (BILL) HEFNER, North Carolina
 JIM KOLBE, Arizona                  STENY H. HOYER, Maryland
 RON PACKARD, California             ALAN B. MOLLOHAN, West Virginia
 SONNY CALLAHAN, Alabama             MARCY KAPTUR, Ohio
 JAMES T. WALSH, New York            DAVID E. SKAGGS, Colorado
 CHARLES H. TAYLOR, North Carolina   NANCY PELOSI, California
 DAVID L. HOBSON, Ohio               PETER J. VISCLOSKY, Indiana
 ERNEST J. ISTOOK, Jr., Oklahoma     ESTEBAN EDWARD TORRES, California
 HENRY BONILLA, Texas                NITA M. LOWEY, New York
 JOE KNOLLENBERG, Michigan           JOSE E. SERRANO, New York
 DAN MILLER, Florida                 ROSA L. DeLAURO, Connecticut
 JAY DICKEY, Arkansas                JAMES P. MORAN, Virginia
 JACK KINGSTON, Georgia              JOHN W. OLVER, Massachusetts
 MIKE PARKER, Mississippi            ED PASTOR, Arizona
RODNEY P. FRELINGHUYSEN, New Jersey  CARRIE P. MEEK, Florida
 ROGER F. WICKER, Mississippi        DAVID E. PRICE, North Carolina
 MICHAEL P. FORBES, New York         CHET EDWARDS, Texas                                                      GEORGE R. NETHERCUTT, Jr.,           ROBERT E. (BUD) CRAMER, Jr., Alabama
Washington
 MARK W. NEUMANN, Wisconsin
 RANDY ``DUKE'' CUNNINGHAM, 
California
 TODD TIAHRT, Kansas
 ZACH WAMP, Tennessee
 TOM LATHAM, Iowa
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama

                 James W. Dyer, Clerk and Staff Director


                                  (ii)
</pre></body></html>
