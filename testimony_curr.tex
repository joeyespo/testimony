\RequirePackage[l2tabu, orthodox]{nag}
\documentclass[11pt]{article}

%%% remove comment delimiter ('%') and specify encoding parameter if required,
 %%% see TeX documentation for additional info(cp1252-Western,cp1251-Cyrillic)
%\usepackage[cp1252]{inputenc}

%%% remove comment delimiter ('%') and select language if required
%\usepackage[english,spanish]{babel}
\usepackage{geometry}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{setspace}
\usepackage{rotating}
\usepackage{subfigure}
\usepackage{longtable}
\usepackage{color}
\usepackage{type1cm}
\usepackage{lettrine}
\usepackage{epstopdf}
\usepackage[center]{caption}
\usepackage[table]{xcolor}
\usepackage{harvard}
\usepackage{microtype}
\usepackage{appendix}
\usepackage{hyperref}
\renewcommand{\|}{\mid}
\setlength{\parindent}{20pt}
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}


\title{It Depends on Who's Asking: Oversight Forum Choice and the Strategic Content of Bureaucratic Testimony}

\author{Ian Palmer Cook\footnote{Ph.D.  Student, Department of Political
Science, University of Pittsburgh. Pittsburgh, Pennsylvania
15260. ipc4@pitt.edu (e--mail).}}
\date{\today}
\begin{document}

\maketitle
\singlespacing
%\chapter{It Depends on Who's Asking: The Strategic Content of Bureaucratic Testimony}

% Notes:
% \begin{itemize}
% \item Diermeier and Feddersen (2000) on informativeness of hearings assumes that hearings are both costly and informative, without showing exactly why that should be. My approach suggests that these might arise because the person testifying might be auditable, and suffer a reputation for under-reporting or prevaricating. DF assume the information and cost are exogenous -- maybe this approach could go towards making it endogenous?
% \item The real importance of the hearing in DF is to get information transmitted from the committee to the floor. I'm more interested in the transmission from bureau to committee.
% \item
%\end{itemize}


\doublespacing \lettrine[lines=2, nindent=-.25pt]{H}{ow} much useful
information can Congress extract from the bureaucracy? Agency
functionaries are usually better informed about policy
implementation. They also have personal interest in preventing
punishment or a loss of autonomy. Useful information, in the sense of
being verifiably correct and having policy implications, need not be
forthcoming. Oversight mechanisms may keep agencies from drifting too
far from legislative intent \cite{mccubbins1984}, but do not speak to
the specific content of findings that result. Making appropriate use
of bureaucratic information requires that a less expert principal be
able to assess the technical competence of its agent. Bureaucrats
delivering testimony to Congress may shade their true efforts if they
perceive threats to their agency for being found non-compliant with
Congress' intent. Or they may leverage their relative expertise and
simply omit important technical detail that could feed legislation
that limits agency discretion. Congress, in turn, has multiple methods
for conducting reviews, including hearings, auditing by its own
agency, or delegating to independent commissions. The choice of tools,
and the response by the bureaucracy, has implications for the
effectiveness of oversight and, more broadly, policymaking. If
Congress cannot extract what it needs from an agency, legislative
control is undermined, and responsiveness to constituents is
weakened. In more formal language, what methods does Congress have to
compel the bureaucracy to reveal information it would rather keep
private? I argue that Congress' strategic choice of forum for agency
reporting -- changing the literal person asking -- helps answer this
question. This essay ultimately suggests that effectiveness of
legislative oversight is a function of the representative chosen to
conduct it.

Congress has the ability to compel the bureaucracy to provide testimony in several ways. First, and most visible, are direct hearings held by committees and subcommittees. These hearings are conducted by the members of Congress themselves, and frequently feature cabinet-level agency representatives as the targets of questioning. The purpose of these hearings are political as often as they are informational, with legislators availing themselves of the public attention \cite{Leyden1992}. A second choice is the creation of an independent commission. These commissions are often derided as either an abdication of responsibility \cite{Schoenbrod1995}, or a stalling tool to avoid sensitive topics \cite{Weaver1986}. Despite this negative view, commissions are a regular feature of policymaking. In the wake of many major events, including disasters of some magnitude, these commissions arise to confront the problem: terrorist attacks on September 11, 2001; failures of levees in New Orleans during Hurricane Katrina; collapse of financial institutions. A third option is to direct The Government Accountability Office (GAO) to assign employees to audit an executive agency. GAO can work on any policy area, but has no ability to bind Congress to following its recommendations. This option is the least costly in terms of Congressional resources, but is also the least visible to other members and the public (making the output less politically useful).

I argue that Congress chooses fora according to the level of expertise that can be arrayed to hear bureaucratic information. Greater expertise is preferred when Congress' goal is policy-motivated, less when the goal is more political. Consider the case of the 2010 British Petroleum Deepwater Horizon drilling platform explosion and oil spill. Secretary of the Interior Ken Salazar was called before the Senate, along with industry representatives to testify about his department's activities. His testimony noted his attempts to make reforms, but cited the previous administration as the true source of problems \cite{111thCongres}. Months later, in front of the presidential National Commission on the BP Deepwater Horizon Oil Spill and Offshore Drilling, several former Minerals Management Services (MMS) administrators would provide detail on numerous topics, including failure to correctly update technical operating and safety specifications for deep water oil drilling equipment \cite{Drilling2010}.\footnote{The MMS was effectively eliminated on June 19, 2010, when Secretary Salazar restructured the component groups, moving them into other agencies. By August, all directors were ``former''.} The theory I propose explains the observed behavior. Rough explanations of the explosion, claiming little responsibility,  were given during the hearings, the purpose of which was to provide Senators a forum to take public credit for action \cite{Mayhew1974}. Detailed information that requires expertise to both understand and verify was given to a commission, who reported policy problems and technical recommendations for correction.

To develop and test this theory I propose examining a history of forum choice and the delivered testimony of bureaucratic agents. Pairing the choice of venue with actual content will allow me to examine the strategic relationship. If the theory is correct, more technical information will be provided to commissions, which will be chosen as Congress' own expertise is low, when their policy interest increases, or some combination thereof. In the following section I detail the theoretical argument. Next I provide detail on the empirical strategy an the required data.

\section{Forum Choice and the Quality of Bureaucratic Reporting}
Agency oversight is, in part, a question of designing institutional mechanisms that increase the likelihood that the agent will work to implement policy in accordance with the wishes of the principal \cite{mccubbins1984}. Work on this topic is wide and deep, producing a rich picture of the problem Congress faces when attempting achieve goals by signing over policy implementation to the bureaucracy \cite{aberbach1990,Hopenhayn1996,Epstein1999a,Huber2002,Shipan2004,Gailmard2008a}. Congressional hearings, like independent commissions, however, do not attempt to construct such mechanisms, but instead attempt to discover \textit{how} the agent has been going about its tasks. The question of task performance becomes important, certainly, once some version of a ``fire-alarm'' or ``police-patrol'' has highlighted an outcome that the principal believes deviates from her intentions. The crux of the issue is a moral hazard problem. Political outcomes Congress dislikes may arise from either agency disregard for proscribed action, or from random misfortune. Investigating agency activity can only resolve the uncertainty if the investigating body can fully understand the agency's behavior.

Agencies are aware of the asymmetry in expertise between themselves and Congress. Theoretical models of delegation recognize that agents of all types will often work at the limit of their discretionary bounds \cite{Fudenberg1986,Bolton2005,Miller2006}. Empirical studies demonstrate agencies that routinely attempt to expand their discretionary range, and sometimes subvert it \cite{Balla1998,Spence1999}. Agencies called to report on their behavior should be expected to exploit this situation. Bureaucrats act to pursue their own goals, whether this means maximizing budgets \cite{Niskanen2007}, or increasing policy expertise for its own sake \cite{Rourke1992a,Gailmard2007}. Acting independently requires agencies to build and maintain autonomy \cite{Carpenter2001}. This is not identical to discretion, however, for which much work assumes bureaucracy has an insatiable demand \cite{Huber2002}. Autonomy can be expanded within a set amount of policy discretion; some agencies may even have incentives to seek less discretion \cite{Krause2003}. Reductions in autonomy may result from increased scrutiny: Congress may limit how policy is implemented, proscribe methods of regulatory review, or direct funding in ways the agency finds constraining. Congress can only do this if it has clear information about agency operations, however. To keep autonomy and limit the amount of Congressional interference, the agency must preserve its informational advantage.

When called upon to testify, an agency can attempt to retain its advantage by strategically choosing the content of its testimony. The less specific it can be, the less Congress will be able to control policy implementation through statutory constraints. Observing the choice of forum in which it is called to testify communicates to the agency the specificity Congress expects. Members of a subcommittee typically have less expertise than employees of the GAO or current and former agency employees. Of all the options, they likely to be satisfied with superficial information. The GAO is more technically informed than the subcommittees because it has full-time staff dedicated to auditing; agency information will need to be much more specific. However, even the GAO is at disadvantage relative to carefully selected panel of career and former agency workers, academics, and industry experts. Such a commission represents the greatest policy-specific knowledge base, representing the greatest Congressional demand for bureaucratic information.

The content of testimony is a strategic choice for the bureaucracy. Testimony given before one body should be distinct from that given to another. I capture this difference by examining the \emph{technical} content of testimony. This is defined as specificity of information relating to agency operation. More specific information requires more context in order to understand its purpose as importance. It is thus also more identifiable as being in compliance with directives.

A statement by a former director of the MMS includes the following passage:
\singlespacing
\begin{quote}``And after Bhopal, OSIA had done a process safety management recommendations and EPA had its own risk assessment program. And the industry in response to the MMS’ proposal preferred to draw up API recommended practice. It was a recommended practice 75 and 14J and they had to do with the development of a safety and environmental management programs for operations and facilities and recommended practice for designing hazards analysis. And it was determined that the time that MMS would not make SEMP as it was called mandatory but that the industry would implement as voluntarily. But the MMS would monitor to see -- to make sure that companies were following these best practices. And there was a pretty high degree of adoption in the late 1980s but I believe the SEMP just sort of lost momentum in the 2000s.'' \cite{111thCongres}
\end{quote}
\doublespacing
Contrast this with a statement from Secretary Ken Salazar during Congressional hearings:
\singlespacing
\begin{quote}
``I went through some of the changes that we have made at MMS over the last year. One that I wanted to spend just a few seconds on has to do with opening up the renewable energy functions within MMS and the Department of the Interior. We work closely with the Governors is the States, Democrats and Republicans alike, to usher in a new effort on offshore renewable energy, especially along the Atlantic States. There’s a whole host of opportunities there that can help us really grasp this new energy future. MMS and the agencies to be created from what we will be announcing in the days ahead will help us grasp that new energy future.'' \cite{111thCongres}
\end{quote}
\doublespacing
Both statements deal with internal functions of the MMS. The first is directed at a panel of experts with backgrounds in oil drilling and energy programs. The second is directed to a Congressional committee being featured on television. The first statement is largely impenetrable without either more detailed explanation or the relevant background knowledge, whereas the second quote is accessible by most any audience in the United States.

Technicality can be thought of as the inverse of vagueness. Clearly, the first statement above is less vague than the second, noting specific programs, numbered references, acronyms for initiatives, and reference to particular agencies. Vagueness as a feature of communication appears in recent work by \citeasnoun{Blume2010}. While the results showing that vague speech can improve utility for the sender and receiver in a signaling game, the more applicable feature is the conception of vagueness as the size of partitions of a space. This clearly builds on \possessivecite{Crawford1982} foundational work on strategic information transmission. Transfer becomes more possible the smaller is the partition of a space over which information is arrayed (messages, for those models). That is, being less vague makes information transfer more likely. Technical content of testimony is my operationalization of partitioning the message space.

Given a particular choice of forum, bureaucrats can strategically alter their delivery of information. The theory outlined so far gives rise to two testable hypotheses:
\begin{itemize}
\item[\textbf{H1}] Bureaucratic testimony in Congressional hearings will be less technical than that provided to independent commissions.
\item[\textbf{H2}] Greater expertise on a commission will positively correlate with more technical content in bureaucratic testimony (to commissions).
\end{itemize}

Congress faces the problem of choosing the forum that best suits their information needs. These needs vary because politicians differ in their interests in policy versus politics. For some issues, members of Congress may have concern for finding the correct policy. For a different issue, members may be more interested in the political benefits of appearing engaged. Writing better policy requires better information, which is held by the bureaucracy. A political response to a problem requires little beyond making public recriminations. The choice of venue in which to audit agency behavior depends on this balance of preferences. The members of Congress face a problem in selecting the forum that best satisfies their interest: more politically visible venues (hearings), or those more likely to produce better information (commissions).

Congress may opt to hold public hearings itself. Reasons to do so include the standard arguments about political positioning \cite{Mayhew1974}, as well as more nuanced efforts to operate within the chamber (issue attention among colleagues, for instance) \cite{Leyden1992}. Members of Congress have the opportunity in hearings to garner media attention, thump the dais in opprobrium, and generally act in ways consistent with looking to win the next election. Hearings do not, however, offer a chance for the transmission of detailed, policy-relevant information.\footnote{This should be distinguished from the regular series of meetings held by committees. There, staff are tasked with orchestrating substantive talks with representatives from the bureaucracy. As part of the regular operation of Congress, they do not have public involvement, and are rarely covered by the media.} Arrogating direct review to itself does not solve the information asymmetry, and thus may not produce quality legislation. Indeed, the only output from hearings may be the documented minutes.

An informational justification for using independent committees is analogous to an argument for committee hearings offered by \citeasnoun{Diermeier2000}. The Congressional chamber floor is concerned about the cost and informativeness of hearings, and can decided whether to keep or delegate the choice of holding hearings to a committee that, in turn, has a choice of whether or not to specialize. Choosing between committee hearings and independent commissions is an extension of this logic. Committees specialization is only relative to the chamber as a whole \cite{Krehbiel1991}. Committees ceding discretion to a commission would be, in effect, encouraging further specialization.

A strategic justification for using committees recognizes that the bureaucracy will know the level of expertise present on the panel. Congressional committees can endow commissions with greater expertise than their own, giving auditing greater bite. The range of potential assemblages is limited only by the method chosen to appoint them\cite{Glassman2011}. Of course, a commission is not devoid of political concerns. The ideological balance of commissions is often contested \cite{Zegart2004}. A general finding of the delegation literature is robust support for the``ally-principle'', where a principal concedes more discretion the closer the agent's ideology is to the principals \cite{Bendor2004}. We should expect, then, a range of appointee types: current and former members of Congress, academic experts, think-tank employees, lobbyists, members of industry, and former leaders from the bureaucracy.

There are costs associated with commissions as well. A commission's lack of electoral concerns encourages specialization, but weakens the connection between the initiating members of Congress and the political benefits of the policy recommendations produced. Commissioners do not campaign to obtain, or retain, office and so are likely to care more about a commission's policy effects regardless of the intent behind its creation. For policy-minded members, a committee of experts mitigates the information disadvantage \cite{Balla2001}, but does so at a cost to control and time. A committee poses a risk since it may offer recommendations that are at odds with a politician's preferences. Ignoring the recommendations means the politician may have to justify either why she is right when the empanelled experts are wrong.\footnote{Commission members are able to exert moderate political pressure on Congress. The members of the 9/11 panel toured the United States to discuss their findings, making the adoption or avoidance of their recommendations salient to the public.} Also, commissions take much longer to complete their work, often six months to a year.

Between the poles of Congressional hearings and commission
investigations, the GAO functions as a continual auditing
mechanism. They produce reports (often nearly 1000 a year) that detail
the fiscal performance of federal programs, which requires knowledge
of how the programs are implemented. The gap between GAO employee
expertise and that of commissioners stems from several aspects of the
GAO: the need to cover multiple topics, even within a single policy
area; an employee base that rotates topics within the agency or out to
other jobs; a portfolio of overall work directed by the interests of
the legislature that makes requests for reports. GAO reports take time
as well, and can still result in recommendations that contradict
legislator desires.

Given the possible bureaucratic responses to the choice of forum,
Congress must make a choice that balances their political and policy
interests according to issue it faces. Testable hypotheses can be
stated based on this logic.
\begin{itemize}
\item[\textbf{H3}] As public attention to an issue increases, the more
  likely Congress will be to prioritize political interests over
  policy interests, and will hold public hearings.
\item[\textbf{H4}] The lower Congress' informational disadvantage, represented by longer tenure on a specific subcommittee, the less likely it will be to form an independent commission.
\end{itemize}

\section{Research Design: Data and Methods}
My theory concerns how much of the bureaucracy's private information can be extracted by Congressional tools. Within this there are two primary relationships that need to be explained. I address these in the following order. First is the bureaucrat's choice of technical content level, primarily as a function of the forum. Second is Congress' choice of forum in which to review agency activity, primarily as a function of its interest in the quality of the resulting policy. At the heart of each is the question of testimony.

\subsection{Explaining Variation in the Content of Bureaucracy Testimony}
The dependent variable to be explained here is the technical content of testimony delivered by a bureaucratic representative. Developing a measure of technical content will be done through the use of automated text processing -- using text as data.

To transform testimony into data I will employ a software tool that extracts quantitative information from text documents, using information theoretical methods. Use of such methods is growing in frequency.\footnote{A selection of relevant work includes: \cite{Laver2003,Quinn2008,Monroe2009,Grimmer2009,Francesconi2010}} Among its merits are a natural connection between my theory of word-choice-level responses by the bureaucracy and the statistical methods that use word-level information to conduct classification. Automated methods of text classification also provide consistency over a scale of documents for which human coding would be far more error prone. Moreover, alterations to coding rules based on human verification can be implemented in a fraction of the time it would take to redo human classification. Computational processing will be done using the Linguistic Inquiry and Word Count (LIWC) software. Output from the LIWC program ranks documents according to linguistic characteristics, including references, emotional content, and a range of others. Most importantly, the software allows the user to develop personal dictionaries of terms to be used in the process. Hand inspection of testimony will produce key terms that should be included in the dictionary, improving the statistical ranking of technicality.

Several independent variables for estimating this relationship can be obtained from the transcripts as well. For the person testifying, these include their agency, position (in the organization), and date of testimony. Aspects of the agency being called to testify will also be collected: agency budget, number of personnel, responsible sub/committee (to indicate policy topic), and Clinton-Lewis measures of agency ideology \cite{Clinton2007b}. A few key variables should be discussed. The organizational level of the person testifying is an indicator of how much technical knowledge she has; senior executive service members and political appointees are expected to have less expertise than bureaucrats who routinely deal with implementation. The subcommittee or committee to which the agency reports for budgeting is an indicator of policy area. Agency size represents a measure of policy complexity. Expertise on less complex topics -- food safety programs --- may be easier to obtain for a commissions than more complex topics -- defense acquisition contracting. Finally, measures of agency ideology may reflect the organizations attitude towards the inquiring body, with an expectation that they are more forthcoming to a subcommittee with (or commission formed by a subcommittee with) similar ideology.

\subsection{Explaining Variation in Congress' Choice of Forum}
The dependent variable to be explained is the particular venue in which testimony is received. I will assemble panel data that covers testimony type (Congressional hearings and commission minutes) over a range of years. The coding for forum is dichotomous (0 for hearings, 1 for commissions). This ordering is important: hearings often happen prior to commissions being formed on the same topic. The test is to see what variables explain Congress deciding to extract greater information than available in a hearing.

Counts of the fora used by Congress will be collected. The number of GAO reports over the timeframe of the hearings and commissions will be identified through the agencies searchable database. The number of reports represents a form of institutional knowledge for Congress; if this is low, an independent commission is expected to be more likely. All hearings and commissions will be recorded for the time period available from public sources. Transcripts of all testimony in front of Congressional hearings covering the past 25 years is electronically available. The Government Printing Office makes transcripts available online. Hearings held during Congresses 99--112 are available, including both chambers, broken down by relevant committee.\footnote{Refer to \url{http://www.gpo.gov/fdsys/browse/collection.action?collectionCode=CHRG} for more details.} Independent commission meeting minutes and agenda are made available through websites (for recent examples) or publicly printed documents provided by the GPO.

These documents can be coded for: date, attendees, policy issue (according to title of subcommittee holding the hearing). Bureaucratic attendees will be coded for: agency affiliation, and relative organizational level (agency head, assistant, etc.). Agency affiliation indicates the policy area of expertise, and serves as an indicator of relevance to the hearings (tangential issues may require testimony, even if they are not the direct subject of the hearing). Organizational position is an indicator of expertise as well as political interest of those holding the hearings. Political appointees may have managerial capabilities, and the ability to steer the ideology of an agency \cite{Clinton2007b}, but do not have significant technical expertise. Lower in the organization, employees should have more specific information about agency behavior, but less visibility into or control of general organizational motivation. My expectation would be to see less technical content delivered by higher-level bureaucrats.

Legislators conducting the hearings will be coded for: party affiliation, ideology (NOMINATE scores), time on committee/subcommittee. These variables reflect the policy interest of the members in attendance. Greater time on a committee indicates both expertise as well as a substantive interest in the topic. I would expect that longer time dealing with an issue positively correlates with policy-motivation, even if the committee members are not preference outliers \cite{Krehbiel1990}.

In addition, variables that reflect the political environment in which the hearings are conducted will be collected. These include a majority party indicator for each chamber, presidential party affiliation, and Congressional and presidential approval ratings (from polling data at the Roper Center for Public Opinion Research). Hearings are not conducted in vacuums, and so it should be expected that the Congressional choice of venue should be affected by political conditions. Congressional approval ratings, for instance, should correlate inversely with the choice hold only hearings; the need to look politically active would outweigh policy-related motivations.

The bureaucracy has to decide how much technical information it will provide, conditional on the method by which Congress has asked for it. This decision can be explained, if my theory is correct, by Congress' choice of forum as well as policy topic, expertise level of the \emph{people} conducting the inquiry (over and above the forum itself), and agency representative. Legislators' choice is over forum, and can be explained by the expected content of the testimony received, primacy of political interests over policy-quality (or vice versa), existing expertise, and factors of the political environment. These choices, then, are simultaneously determined, and will require modeling them as such for empirical analysis \cite{Greene2011}.

% \section{Development Planning}
% The primary concern in next steps is data collection. Coming to a clear, convincing measure of technical content will require learning and implementing the software. Options also exist to employ different statistical methods in order to avoid the application of a ``black box''. Information theoretic concepts of conditional entropy and mutual information between words and the documents that contain them have been profitably employed \cite{Grimmer2010}, but require to develop a facility with the underlying theory.

% Defining an appropriate model of the simultaneous decisions being made (venue and content) will also require some additional work. In this case, I need to ensure I have a solid understanding of the precise parameters my theory suggests need to be estimated, and to confront the data requirements this demands.

%Holding committee hearings and delegating to an independent commission are two options noted so far. Using the GAO presents a third option. The GAO is one of three agencies usually considered part of the Congress' \emph{own} bureaucracy, along with the Congressional Research Service and the Congressional Budget Office. GAO's purpose is to evaluate the ongoing performance and results of federal programs, the majority of which are operated by agencies in the executive branch. The problem Congress faces is to decide, on any given attempt at review, which of these three options (or combinations thereof) to employ.

%Specifically, I follow \cite{Grimmer2010} by measuring the mutual information that exists between words and the documents from which they are drawn. Mutual information is a statistical concept that identifies how much uncertainty in one random variable is reduced by knowing the value of another, related random variable. I take one random variable to be the kind of testimony delivered. The second random variable is the distribution words within the document. The baseline assumption is that a choice of any document out of the whole set is equal to the proportion of document types. Mutual information allows me to answer the question: ``Given that I know a specific word appears in a document, how much more certain can I be that the document is of a particular type?'' The theory that testimony differs according to forum leads to the hypothesis that different words should reduce uncertainty in different amounts. Referring to the example above, knowing that the words ``operations,'' ``facilities,'' or ``program'' appear, uncertainty over the document being commission testimony should be reduced\emph{more} than uncertainty that the document is from congressional hearings.

%Information theory relies heavily on the concept of entropy. Though it is related to the physics concept of entropy, it is best understood as the uncertainty inherent in a random process. I use \citeasnoun{MacKay2003} as the source for the technical details.

\newpage
\singlespacing
\bibliography{/Users/ian/Dropbox/common/library}
\bibliographystyle{apsr}
\end{document}
