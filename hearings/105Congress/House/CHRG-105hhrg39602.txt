<html>
<title> - DEPARTMENTS OF COMMERCE, JUSTICE, AND</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]


<DOC>

From the U.S. Government Printing Office Online via GPO	Access
[House Committee on Appropriations]
[Commerce, Justice, and State, The Judiciary, and Related Agencies Appropriations for 1998]
[Part 1]

 
                 DEPARTMENTS OF COMMERCE, JUSTICE, AND

                   STATE, THE JUDICIARY, AND RELATED

                    AGENCIES APPROPRIATIONS FOR 1998

========================================================================

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED FIFTH CONGRESS

                              FIRST SESSION
                                ________

  SUBCOMMITTEE ON THE DEPARTMENTS OF COMMERCE, JUSTICE, AND STATE, THE 
                    JUDICIARY, AND RELATED AGENCIES

                    HAROLD ROGERS, Kentucky, Chairman

JIM KOLBE, Arizona                 ALAN B. MOLLOHAN, West Virginia
CHARLES H. TAYLOR, North Carolina  DAVID E. SKAGGS, Colorado
RALPH REGULA, Ohio                 JULIAN C. DIXON, California
MICHAEL P. FORBES, New York        
TOM LATHAM, Iowa                   

 NOTE: Under Committee Rules, Mr. Livingston, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.

Jim Kulikowski, Therese McAuliffe, and Jennifer Miller, Subcommittee Staff
                                ________

                                 PART 1

                  Justification of the Budget Estimates

                         DEPARTMENT OF COMMERCE

                              <snowflake>

                                ________

         Printed for the use of the Committee on Appropriations
                                ________

                     U.S. GOVERNMENT PRINTING OFFICE

 39-602 O                   WASHINGTON : 1997

------------------------------------------------------------------------

             For sale by the U.S. Government Printing Office            
        Superintendent of Documents, Congressional Sales Office,        
                          Washington, DC 20402                          







                       COMMITTEE ON APPROPRIATIONS                      

                   BOB LIVINGSTON, Louisiana, Chairman                  

JOSEPH M. McDADE, Pennsylvania         DAVID R. OBEY, Wisconsin            
C. W. BILL YOUNG, Florida              SIDNEY R. YATES, Illinois           
RALPH REGULA, Ohio                     LOUIS STOKES, Ohio                  
JERRY LEWIS, California                JOHN P. MURTHA, Pennsylvania        
JOHN EDWARD PORTER, Illinois           NORMAN D. DICKS, Washington         
HAROLD ROGERS, Kentucky                MARTIN OLAV SABO, Minnesota         
JOE SKEEN, New Mexico                  JULIAN C. DIXON, California         
FRANK R. WOLF, Virginia                VIC FAZIO, California               
TOM DeLAY, Texas                       W. G. (BILL) HEFNER, North Carolina 
JIM KOLBE, Arizona                     STENY H. HOYER, Maryland            
RON PACKARD, California                ALAN B. MOLLOHAN, West Virginia     
SONNY CALLAHAN, Alabama                MARCY KAPTUR, Ohio                  
JAMES T. WALSH, New York               DAVID E. SKAGGS, Colorado           
CHARLES H. TAYLOR, North Carolina      NANCY PELOSI, California            
DAVID L. HOBSON, Ohio                  PETER J. VISCLOSKY, Indiana         
ERNEST J. ISTOOK, Jr., Oklahoma        THOMAS M. FOGLIETTA, Pennsylvania   
HENRY BONILLA, Texas                   ESTEBAN EDWARD TORRES, California   
JOE KNOLLENBERG, Michigan              NITA M. LOWEY, New York             
DAN MILLER, Florida                    JOSE E. SERRANO, New York           
JAY DICKEY, Arkansas                   ROSA L. DeLAURO, Connecticut        
JACK KINGSTON, Georgia                 JAMES P. MORAN, Virginia            
MIKE PARKER, Mississippi               JOHN W. OLVER, Massachusetts        
RODNEY P. FRELINGHUYSEN, New Jersey    ED PASTOR, Arizona                  
ROGER F. WICKER, Mississippi           CARRIE P. MEEK, Florida             
MICHAEL P. FORBES, New York            DAVID E. PRICE, North Carolina      
GEORGE R. NETHERCUTT, Jr., Washington  CHET EDWARDS, Texas                 
MARK W. NEUMANN, Wisconsin             
RANDY ``DUKE'' CUNNINGHAM, California  
TODD TIAHRT, Kansas                    
ZACH WAMP, Tennessee                   
TOM LATHAM, Iowa                       
ANNE M. NORTHUP, Kentucky              
ROBERT B. ADERHOLT, Alabama            

                 James W. Dyer, Clerk and Staff Director






                            C O N T E N T S

                              ----------                              

                         DEPARTMENT OF COMMERCE

                                                                    Page
General Administration...........................................   1-35
Office of Inspector General......................................  36-65
Economic Development Administration.............................. 66-130
Bureau of the Census............................................ 131-255
Economics and Statistics Analysis............................... 256-324
International Trade Administration.............................. 325-395
Bureau of Export Administration................................. 396-445
Minority Business Development Agency............................ 446-487
National Oceanic and Atmospheric Administration................ 488-1024
Patent and Trademark Office................................... 1025-1084
Technology Administration..................................... 1085-1134
National Technical Information Service........................ 1135-1153
National Institute of Standards and Technology................ 1154-1353
National Telecommunications and Information Administration.... 1354-1493




[Pages 1 - 1493--The official Committee record contains additional material here.]

                                   <all>
</pre></body></html>
