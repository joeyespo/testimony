<html>
<title> - DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES APPROPRIATIONS FOR 2000</title>
<body><pre>
[House Hearing, 106 Congress]
[From the U.S. Government Printing Office]



 
 DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES APPROPRIATIONS FOR 2000

_______________________________________________________________________

                                HEARINGS

                                BEFORE A

                           SUBCOMMITTEE OF THE

                       COMMITTEE ON APPROPRIATIONS

                         HOUSE OF REPRESENTATIVES

                       ONE HUNDRED SIXTH CONGRESS
                              FIRST SESSION

                                ________

   SUBCOMMITTEE ON THE DEPARTMENT OF THE INTERIOR AND RELATED AGENCIES
                      RALPH REGULA, Ohio, Chairman
 JIM KOLBE, Arizona                 NORMAN D. DICKS, Washington
 JOE SKEEN, New Mexico              JOHN P. MURTHA, Pennsylvania
 CHARLES H. TAYLOR, North Carolina  JAMES P. MORAN, Virginia
 GEORGE R. NETHERCUTT, Jr.,         ROBERT E. ``BUD'' CRAMER, Jr., 
Washington                           Alabama
 ZACH WAMP, Tennessee               MAURICE D. HINCHEY, New York
 JACK KINGSTON, Georgia
 JOHN E. PETERSON, Pennsylvania     

 NOTE: Under Committee Rules, Mr. Young, as Chairman of the Full 
Committee, and Mr. Obey, as Ranking Minority Member of the Full 
Committee, are authorized to sit as Members of all Subcommittees.
   Deborah Weatherly, Loretta Beaumont, Joel Kaplan, and Christopher 
                                 Topik,
                            Staff Assistants

                                ________

                                 PART 1
                  Justification of the Budget Estimates
                                                                   Page
 Bureau of Land Management........................................    1
 Fish and Wildlife Service........................................  617
 National Park Service............................................ 1215

                                ________

         Printed for the use of the Committee on Appropriations

                                ________

                     U.S. GOVERNMENT PRINTING OFFICE
 55-253                     WASHINGTON : 1999




                        COMMITTEE ON APPROPRIATIONS

                   C. W. BILL YOUNG, Florida, Chairman

 RALPH REGULA, Ohio                    DAVID R. OBEY, Wisconsin
 JERRY LEWIS, California               JOHN P. MURTHA, Pennsylvania
 JOHN EDWARD PORTER, Illinois          NORMAN D. DICKS, Washington
 HAROLD ROGERS, Kentucky               MARTIN OLAV SABO, Minnesota
 JOE SKEEN, New Mexico                 JULIAN C. DIXON, California
 FRANK R. WOLF, Virginia               STENY H. HOYER, Maryland
 TOM DeLAY, Texas                      ALAN B. MOLLOHAN, West Virginia
 JIM KOLBE, Arizona                    MARCY KAPTUR, Ohio
 RON PACKARD, California               NANCY PELOSI, California
 SONNY CALLAHAN, Alabama               PETER J. VISCLOSKY, Indiana
 JAMES T. WALSH, New York              NITA M. LOWEY, New York
 CHARLES H. TAYLOR, North Carolina     JOSE E. SERRANO, New York
 DAVID L. HOBSON, Ohio                 ROSA L. DeLAURO, Connecticut
 ERNEST J. ISTOOK, Jr., Oklahoma       JAMES P. MORAN, Virginia
 HENRY BONILLA, Texas                  JOHN W. OLVER, Massachusetts
 JOE KNOLLENBERG, Michigan             ED PASTOR, Arizona
 DAN MILLER, Florida                   CARRIE P. MEEK, Florida
 JAY DICKEY, Arkansas                  DAVID E. PRICE, North Carolina
 JACK KINGSTON, Georgia                CHET EDWARDS, Texas
 RODNEY P. FRELINGHUYSEN, New Jersey   ROBERT E. ``BUD'' CRAMER, Jr., 
 ROGER F. WICKER, Mississippi            Alabama
 MICHAEL P. FORBES, New York           JAMES E. CLYBURN, South Carolina
 GEORGE R. NETHERCUTT, Jr.,            MAURICE D. HINCHEY, New York
Washington                             LUCILLE ROYBAL-ALLARD, California
 RANDY ``DUKE'' CUNNINGHAM,            SAM FARR, California
California                             JESSE L. JACKSON, Jr., Illinois
 TODD TIAHRT, Kansas                   CAROLYN C. KILPATRICK, Michigan
 ZACH WAMP, Tennessee                  ALLEN BOYD, Florida
 TOM LATHAM, Iowa
 ANNE M. NORTHUP, Kentucky
 ROBERT B. ADERHOLT, Alabama
 JO ANN EMERSON, Missouri
 JOHN E. SUNUNU, New Hampshire
 KAY GRANGER, Texas
 JOHN E. PETERSON, Pennsylvania     

                 James W. Dyer, Clerk and Staff Director

                                  (ii)



[The official Commmittee record contains additional material here--Pages 1-1855.]
</pre></body></html>
