<html>
<title> - SOLVING THE MEDICAL ISOTOPE CRISIS</title>
<body><pre>
[House Hearing, 111 Congress]
[From the U.S. Government Printing Office]



 
                   SOLVING THE MEDICAL ISOTOPE CRISIS

=======================================================================

                                HEARING

                               BEFORE THE

                 SUBCOMMITTEE ON ENERGY AND ENVIRONMENT

                                 OF THE

                    COMMITTEE ON ENERGY AND COMMERCE
                        HOUSE OF REPRESENTATIVES

                     ONE HUNDRED ELEVENTH CONGRESS

                             FIRST SESSION

                               __________

                           SEPTEMBER 9, 2009

                               __________

                           Serial No. 111-61


      Printed for the use of the Committee on Energy and Commerce

                        energycommerce.house.gov



                  U.S. GOVERNMENT PRINTING OFFICE
74-094                    WASHINGTON : 2012
-----------------------------------------------------------------------
For sale by the Superintendent of Documents, U.S. Government Printing Office, 
http://bookstore.gpo.gov. For more information, contact the GPO Customer Contact Center, U.S. Government Printing Office. Phone 202�09512�091800, or 866�09512�091800 (toll-free). E-mail, gpo@custhelp.com.  


                    COMMITTEE ON ENERGY AND COMMERCE

                 HENRY A. WAXMAN, California, Chairman

JOHN D. DINGELL, Michigan            JOE BARTON, Texas
  Chairman Emeritus                    Ranking Member
EDWARD J. MARKEY, Massachusetts      RALPH M. HALL, Texas
RICK BOUCHER, Virginia               FRED UPTON, Michigan
FRANK PALLONE, Jr., New Jersey       CLIFF STEARNS, Florida
BART GORDON, Tennessee               NATHAN DEAL, Georgia
BOBBY L. RUSH, Illinois              ED WHITFIELD, Kentucky
ANNA G. ESHOO, California            JOHN SHIMKUS, Illinois
BART STUPAK, Michigan                JOHN B. SHADEGG, Arizona
ELIOT L. ENGEL, New York             ROY BLUNT, Missouri
GENE GREEN, Texas                    STEVE BUYER, Indiana
DIANA DeGETTE, Colorado              GEORGE RADANOVICH, California
  Vice Chairman                      JOSEPH R. PITTS, Pennsylvania
LOIS CAPPS, California               MARY BONO MACK, California
MICHAEL F. DOYLE, Pennsylvania       GREG WALDEN, Oregon
JANE HARMAN, California              LEE TERRY, Nebraska
TOM ALLEN, Maine                     MIKE ROGERS, Michigan
JANICE D. SCHAKOWSKY, Illinois       SUE WILKINS MYRICK, North Carolina
CHARLES A. GONZALEZ, Texas           JOHN SULLIVAN, Oklahoma
JAY INSLEE, Washington               TIM MURPHY, Pennsylvania
TAMMY BALDWIN, Wisconsin             MICHAEL C. BURGESS, Texas
MIKE ROSS, Arkansas                  MARSHA BLACKBURN, Tennessee
ANTHONY D. WEINER, New York          PHIL GINGREY, Georgia
JIM MATHESON, Utah                   STEVE SCALISE, Louisiana
G.K. BUTTERFIELD, North Carolina
CHARLIE MELANCON, Louisiana
JOHN BARROW, Georgia
BARON P. HILL, Indiana
DORIS O. MATSUI, California
DONNA M. CHRISTENSEN, Virgin 
Islands
KATHY CASTOR, Florida
JOHN P. SARBANES, Maryland
CHRISTOPHER S. MURPHY, Connecticut
ZACHARY T. SPACE, Ohio
JERRY McNERNEY, California
BETTY SUTTON, Ohio
BRUCE BRALEY, Iowa
PETER WELCH, Vermont

                                  (ii)
                 Subcommittee on Energy and Environment

               EDWARD J. MARKEY, Massachusetts, Chairman
MICHAEL F. DOYLE, Pennsylvania       RALPH M. HALL, Texas
G.K. BUTTERFIELD, North Carolina          Ranking Member
CHARLIE MELANCON, Louisiana          FRED UPTON, Michigan
BARON HILL, Indiana                  ED WHITFIELD, Kentucky
DORIS O. MATSUI, California          JOHN SHIMKUS, Illinois
JERRY McNERNEY, California           HEATHER WILSON, New Mexico
PETER WELCH, Vermont                 JOHN B. SHADEGG, Arizona
JOHN D. DINGELL, Michigan            STEVE BUYER, Indiana
RICK BOUCHER, Virginia               GREG WALDEN, Oregon
FRANK PALLONE, Jr., New Jersey       SUE WILKINS MYRICK, North Carolina
ELIOT L. ENGEL, New York             JOHN SULLIVAN, Oklahoma
GENE GREEN, Texas                    MICHAEL C. BURGESS, Texas
LOIS CAPPS, California
JANE HARMAN, California
CHARLES A. GONZALEZ, Texas
TAMMY BALDWIN, Wisconsin
MIKE ROSS, Arkansas
JIM MATHESON, Utah
JOHN BARROW, Georgia
  


                             C O N T E N T S

                              ----------                              
                                                                   Page
Hon. Edward J. Markey, a Representative in Congress from the 
  Commonwealth of Massachussetts, opening statement..............     1
Hon. Fred Upton, a Representative in Congress from the State of 
  Michigan, prepared statement...................................     4
Hon. Jay Inslee, a Representative in Congress from the State of 
  Washington, prepared statement.................................    46
Hon. Joe Barton, a Representative in Congress from the State of 
  Texas, prepared statement......................................    47

                               Witnesses

Parrish Staples, Director, European and African Threat Reduction, 
  Office of Global Threat Reduction, National Nuclear Security 
  Administration, United States Department of Energy.............     6
    Prepared statement...........................................     9
    Answers to submitted questions...............................    57
Steven Larson, M.D., Chief, Nuclear Medicine Service, Department 
  of Radiology, Memorial Sloan-Kettering Cancer Center, Vice-
  Chairman, Committee on Medical Isotope Production without 
  Highly Enriched Uranium, National Academy of Sciences..........    15
    Prepared statement...........................................    17
    Answers to submitted questions...............................    78
Michael Duffy, Vice President and General Counsel, Lantheur 
  Medical Imaging, Member of the Board, Council on Radionuclides 
  and Radiopharmaceuticals.......................................    26
    Prepared statement...........................................    28

                           Submitted Material

Letter from Director of University of Missouri Research Reactor 
  to Committee...................................................    50
Letter of September 9, 2009, from Washington State University to 
  Mr. Inslee.....................................................    54


                   SOLVING THE MEDICAL ISOTOPE CRISIS

                              ----------                              


                      WEDNESDAY, SEPTEMBER 9, 2009

                  House of Representatives,
            Subcommittee on Energy and Environment,
                          Committee on Energy and Commerce,
                                                    Washington, DC.
    The subcommittee met, pursuant to call, at 2:09 p.m., in 
Room 2322, Rayburn House Office Building, Hon. Edward J. Markey 
[chairman of the subcommittee] presiding.
    Present: Representatives Markey, Barrow, Upton, Shimkus, 
and Scalise.
    Staff Present: Jeff Baran, Counsel; Matt Weiner, Special 
Assistant; and Peter Ketcham-Colwill, Special Assistant.

OPENING STATEMENT OF HON. EDWARD J. MARKEY, A REPRESENTATIVE IN 
        CONGRESS FROM THE COMMONWEALTH OF MASSACHUSETTS

    Mr. Markey. Welcome ladies and gentlemen to the 
Subcommittee on Energy and the Environment and our very 
important hearing.
    Every day in the United States, thousands of people go to 
the hospital to be treated for life-threatening illnesses such 
as heart disease and cancer. But right now, due to the 
breakdown of a nuclear reactor in Canada, many of these 
critical procedures are being delayed and compromised.
    The United States is facing a crisis in nuclear medicine. 
We face a severe shortage of a crucial radioactive isotope 
which is required for nearly 50,000 medical procedures every 
day, usually to produce a detailed image such as a cancer or a 
bone scan. The shortage of this isotope, which usually costs 
only $10 of a multi-thousand dollar procedure, is threatening 
the health care of millions of Americans. Worst of all, the 
United States does not currently produce any of the isotope 
domestically. Instead, we are entirely dependent on a handful 
of foreign nuclear reactors, most of which are several decades 
old, some of which are literally falling apart and which rely 
upon weapon usable highly enriched uranium for their operation.
    In May, the 51-year-old Canadian NRU reactor broke down. It 
is not yet clear whether the reactor will ever operate again. 
In mid July, the 47-year-old HFR reactor in the Netherlands was 
taken off-line for maintenance for 1 month. Together, these two 
reactors usually produce our entire isotope supply. While the 
United States was able to secure a small supply during this 
time from other reactors, Americans health care suffered as a 
result. A recent survey of the nuclear medicine community 
provided sobering results. Eighty percent said their practice 
was impacted by the shortage. Eighty percent said they have 
postponed procedures. Forty-seven percent said they have 
cancelled procedures, and 57 percent said they had substituted 
alternative procedures. Unfortunately, in most cases the 
alternative procedures are more invasive, less effective, more 
costly, and pose greater radiation risk to both patients and 
technicians.
    We don't need alternatives. We need the state of the art to 
be fully available again. Medical care in this country for 
cancer, heart disease, bone scans simply cannot be held hostage 
to the maintenance schedules of a 50-year-old reactor in 
Europe. It is absolutely vital that we act to bring a robust 
domestic supply of these critical medical isotopes online as 
soon as possible.
    In order to address the crisis in nuclear medicine, I have 
introduced, along with my good friend, colleague and partner, 
Fred Upton, H.R. 3276, the American Medical Isotopes Production 
Act of 2009. The bill will provide the Department of Energy new 
authorities and resources to assist the private sector in 
establishing as rapidly as possible a robust medical isotope 
production capacity here in the United States. In addition, the 
bill will end the export of bomb-usable highly enriched uranium 
for medical isotope production in 7 to 10 years, as recommended 
in a recent National Academy of Sciences report that also said 
there was no reason that these isotopes couldn't be made using 
low enriched uranium. In fact, both Argentina and Australia 
have started producing medical isotopes with low enriched 
uranium. Highly enriched uranium is nuclear bomb material, and 
the national security of the United States demands that we 
never export it again.
    The Markey-Upton bill is a bipartisan bill. It has been 
endorsed by the Society for Nuclear Medicine, the American 
College of Radiology, the American Society for Radiation 
Oncology, the American College of Cardiology, the American 
Society of Nuclear Cardiology, the American Association of 
Physicists in Medicine, the Health Physics Society, the Council 
on Radionuclides, a list of companies as well plus the Nuclear 
Threat Initiative, the Union of Concerned Scientists and 
Physicians For Social Responsibility.
    I would like to ask for unanimous consent that the letters 
of endorsement from these organizations be entered into the 
record. I also would like to ask unanimous consent that members 
will have 5 legislative days to revise and extend their remarks 
and to insert extraneous material in the record.
    Today's hearing will allow the subcommittee to explore this 
important issue and to hear the panel's views on H.R. 3276. I 
hope that we can all work together to address this crucial 
problem facing American hospitals and patients.
    Now I would like to turn and recognize my good friend, the 
gentleman from Michigan, Mr. Upton for his opening statement.
    Mr. Upton. Thank you, Mr. Chairman. I am going to ask 
unanimous consent that my full statement be made part of the 
record.
    And trying to shorten up some of the time as we are 
expecting votes soon, let me just make a couple of comments. We 
are really at a crisis. As you indicated, 16 million medical 
procedures in the U.S. that rely on the import of Moly-99. 
That's 50,000 a day. It is clear that our Nation must produce 
these lifesaving isotopes domestically to ensure that the 
public health is protected. And when I learned of this 
situation when you and I talked about it in July before the 
August break, I was delighted to partner with you to introduce 
legislation that I hope can move quickly.
    There are a good number of organizations that are onboard. 
I would like to think that this hearing will catapult us into 
getting a bill to the House very soon. And at this point, I 
yield back the balance of my time.
    [The prepared statement of Mr. Upton follows:]

    [GRAPHIC] [TIFF OMITTED] T4094A.001
    
    [GRAPHIC] [TIFF OMITTED] T4094A.002
    
    Mr. Markey. Thank you. The Chair recognizes the gentleman 
from Illinois.
    Mr. Shimkus. I will waive.
    Mr. Markey. The Chair recognizes the gentleman from 
Louisiana.
    Mr. Scalise. I will waive.
    Mr. Markey. That is great. We will recognize our very 
distinguished panel. Our first witness is Dr. Parrish Staples, 
director of the Office of European and African Threat Reduction 
at the National Nuclear Security Administration of the 
Department of Energy. Dr. Staples has played a leading role 
within the Department of Energy to help solve the medical 
isotopes crisis. In addition, his office is responsible for 
implementing the Department of Energy's efforts to reduce the 
use of highly enriched uranium around the world including in 
the production of medical isotopes.

 STATEMENTS OF PARRISH STAPLES, DIRECTOR, EUROPEAN AND AFRICAN 
 THREAT REDUCTION, OFFICE OF GLOBAL THREAT REDUCTION, NATIONAL 
 NUCLEAR SECURITY ADMINISTRATION, UNITED STATES DEPARTMENT OF 
 ENERGY; STEVEN LARSON, M.D., CHIEF, NUCLEAR MEDICINE SERVICE, 
   DEPARTMENT OF RADIOLOGY, MEMORIAL SLOAN-KETTERING CANCER 
CENTER, VICE-CHAIRMAN, COMMITTEE ON MEDICAL ISOTOPE PRODUCTION 
WITHOUT HIGHLY ENRICHED URANIUM, NATIONAL ACADEMY OF SCIENCES; 
AND MICHAEL DUFFY, VICE PRESIDENT AND GENERAL COUNSEL, LANTHEUR 
MEDICAL IMAGING, MEMBER OF THE BOARD, COUNCIL ON RADIONUCLIDES 
                    AND RADIOPHARMACEUTICALS

    Mr. Markey. We welcome you, Dr. Staples. Whenever you're 
ready please, again.


                  STATEMENT OF PARRISH STAPLES

    Dr. Staples. Thank you, Chairman Markey, Ranking Member 
Upton, and the subcommittee members. Thank you for the 
opportunity to testify on the National Nuclear Security 
Administration's Global Threat Reduction Initiative's role in 
minimizing, and to the extent possible, eliminating the use of 
highly enriched uranium in civilian nuclear applications 
including in the production medical radioisotopes. As part of 
my testimony, I will briefly describe recent efforts to 
mitigate the impact of the current and anticipated shortages of 
the medical isotope moly-99 and discuss in more detail our 
efforts to accelerate the establishment of a domestic 
commercial supply of moly-99 that does not use highly enriched 
uranium.
    Finally, I will highlight how the proposed American Medical 
Isotopes Production Act of 2009 can greatly help to advance our 
progress on the dual U.S. policy priorities to, one, establish 
a secure supply of this critical medical isotope for U.S. 
citizens, and two, to minimize the civilian use of 
proliferation-sensitive highly enriched uranium around the 
globe.
    First, section 2 of the American Medical Isotope Production 
Act of 2009 very appropriately and succinctly covers the 
history, the use of moly-99, the decay product technecium-99 
nine, the issues surrounding the current medical isotope 
production industry, the current acute shortage of moly-99 
within the medical community, and the critical importance that 
this isotope provides to the health care of Americans on a 
daily basis. And most importantly, it also covers the state of 
the art technology regarding conversion to low enriched 
uranium.
    Very important to us, on January 14, 2009, the National 
Academy published a report confirming that the production of 
moly-99 without the use of highly enriched uranium is both 
technically and economically feasible. In addition to the 
National Academy's determination that there are no technical 
reasons that adequate quantities of medical isotope cannot be 
produced without the use of HEU, the National Academy also 
stated that the single greatest threat to the supply 
reliability is the approaching obsolescence of the aging 
reactors that current large-scale producers utilize to 
irradiate HEU targets to obtain moly-99. The findings of this 
report unambiguously support the consistency of HEU 
minimization policies with the full-scale production of medical 
isotopes while highlighting the fragile nature of the current 
supply chain due to the age of the foreign moly-99 production 
facilities.
    Now, with our mission to reduce and eliminate the use of 
HEU in civilian applications NNSA has been working for many 
years to convert research reactors from the use of HEU to LEU 
fuel. We agree with the language in the proposed legislation 
which makes clear that the U.S. should accelerate its effort to 
convert HEU research reactors worldwide from the use of HEU. In 
fact, this acceleration is already underway at NNSA.
    We and the Global Threat Reduction Initiative have 
significantly accelerated our efforts over the past several 
years and to date have converted 57 highly enriched uranium 
fuel research reactors globally from the use of HEU to LEU 
fuels. Through GTRI efforts, another seven HEU research 
reactors have been verified to shut down prior to their 
conversion. These activities have resulted in more than 320 
kilograms of HEU no longer being used annually for reactor 
operations.
    In addition, NNSA has also been working with both existing 
and potential moly-99 producers for several years to convert or 
develop their moly-99 production processes to utilize non-HEU-
based technologies. NNSA provides technical expertise on a 
nonproprietary basis to all existing and potential producers to 
assist in converting and developing their moly-99 production 
processes in accordance with the U.S. HEU minimization policy. 
Through these efforts NNSA has established longstanding 
relationships with current and future moly-99 suppliers and we 
are uniquely suited to accelerate efforts to establish a 
reliable domestic supply without the use of HEU. Due to the 
current lack of global production capacity of moly-99 by 
industry, we are actively engaging in discussions with all 
current and possible producers to best determine how to rapidly 
transform the industry into a diverse, stable, commercial 
supply network that will not use HEU for production of this 
vital medical isotope.
    Given the market dynamics with the current supply shortage, 
we fully expect that the 7-year timeframe referenced in the 
American Medical Isotopes Production Act of 2009 is more than 
adequate to ensure that a sufficient supply of non-HEU moly-99 
can be available to the medical community. Further we believe 
that the development of new producers or the conversion of 
existing producers to low enriched uranium can be accomplished 
with no impact upon the current supply availability. In fact, 
through the current acute supply shortage of this critical 
medical isotope and the associated market dynamics with the 
focus of the American Medical Isotopes Production Act of 2009 
on this issue, we believe that we can ensure the successful 
development of a diverse, reliable supply of moly-99 to the 
medical community that will also help to accomplish an 
important and longstanding nuclear nonproliferation mission.
    Now, the United States has approached the moly-99 supply 
problem by----
    Mr. Markey. If you could summarize please, Dr. Staples.
    Dr. Staples. I would just go to my closing paragraph then. 
The American Medical Isotope Production Act of 2009 is crucial 
of ensuring the success of our efforts to accelerate 
development of a domestic supply of moly-99 nine without the 
use of HEU. This legislation will accelerate greatly and 
enhance the development of reliable supply of this isotope for 
the use in the U.S. medical community and further support U.S. 
objectives to reduce the use of proliferation-sensitive HEU in 
civilian applications.
    I thank the subcommittee and Chairman Markey, in 
particular, for your continued leadership on such a crucial 
nuclear energy and civil nuclear application issue, and we 
stand ready to answer questions.
    Mr. Markey. Thank you, sir.
    [The prepared statement of Dr. Staples follows:]

    [GRAPHIC] [TIFF OMITTED] T4094A.003
    
    [GRAPHIC] [TIFF OMITTED] T4094A.004
    
    [GRAPHIC] [TIFF OMITTED] T4094A.005
    
    [GRAPHIC] [TIFF OMITTED] T4094A.006
    
    [GRAPHIC] [TIFF OMITTED] T4094A.007
    
    [GRAPHIC] [TIFF OMITTED] T4094A.008
    
    Mr. Markey. Our second witness is Dr. Steven Larson the 
Chair of the Nuclear Medicine Service, Department of Radiology 
at the Memorial Sloan-Kettering Cancer Institute. Whenever you 
are ready, please begin.

                STATEMENT OF STEVEN LARSON, M.D.

    Dr. Larson. Good afternoon, chairman and members of the 
committee. My name is Steven Larson, and I am chief of nuclear 
medicine, as you have heard at Memorial Sloan-Kettering Cancer 
Institute in New York. I also served as vice chair of the 
National Research Council's Committee on medical isotope 
production without highly enriched uranium. I was asked to 
testify today regarding the report from the study, but first I 
want to offer some personal observations as a practicing 
nuclear medicine physician.
    I am the director of a large nuclear medicine clinic at 
Memorial Sloan-Kettering Cancer Center. For most of the summer 
like other nuclear medicine clinics in the northeast, we have 
seen a reduction of 20 to 25 percent in the optimum amount of 
technetium for clinical use. Now, technetium 99m, as you have 
heard, is by far the most common clinical isotope and a 
bellwether for nuclear medicine isotope supplied health care. 
This reduction supply has negatively impacted our ability to 
efficiently deliver nuclear medicine-based care to patients.
    Furthermore, medical isotope providers are telling us to 
expect continued shortages of technetium 99m during 2009 and 
beyond, and they are warning about the possibility of even 
deeper reductions in technetium 99m availability. Clearly we 
are in the need of a more reliable supply of medical isotope 
for American health care.
    Let me turn to the National Research Council's study on 
medical isotope production without highly enriched uranium. The 
mandate for this study came from section 630 of the Energy 
Policy Act of 2005. Our study was completed in late 2008, and 
the final report was issued in January, 2009. It focused 
primarily on the use of HEU for the production of medical 
isotope molybdenum 99.
    Briefly, some key findings. Adequate quantities of medical 
isotopes to meet U.S. demands could be produced without HEU. A 
report found that an anticipated average cost increase to 
convert to the production of medical isotopes without the use 
of HEU would likely be less than 10 percent for most current 
large-scale producers. Reliability of medical isotope supply is 
a significant problem now and likely to be a problem for the 
foreseeable future with demand close to total capacity for 
production and with little margin for additional production 
capacity in the event of an interruption of supply.
    On the other hand demand for nuclear medicine services are 
stable with likely growth rates of utilization of 3 to 5 
percent per year.
    Several steps could be taken by the U.S. Government and 
others to improve the feasibility of eliminating the use of HEU 
for medical isotope production. I note that H.R. 3276 
legislatively enshrines several of these steps. It authorizes 
the Department of Energy to provide technical assistance to 
producers who wish to convert to production without HEU. It 
provides financial assistance to develop a domestic isotope 
production capacity, and it provides for a 7-year phaseout 
period for HEU exports for medical isotope production.
    When I began work on this study I was skeptical about the 
economic feasibility of conversion to LEU-based medical isotope 
production and the potential impact that that might have on 
conversion or supply reliability. But based on the information 
I received during this National Research Council study, I now 
believe that if medical isotope producers have the will to 
convert that, they can do so without undue costs. My opinion is 
based on the observations we made during the site visits to the 
medical isotope production facilities in Argentina and 
Australia and discussion with technical experts about 
conversion. Under modest circumstances and without elaborate 
additional infrastructure, Argentina was able to convert to 
LEU-based production in less than 2 years and for less than a 
million dollars in supplies and facilities modification.
    The Argentina process is now being implemented in Australia 
and the Australian company, ANSTO, hopes to begin exporting 
small quantities of molybdenum 99 to the United States in the 
near future.
    This concludes my oral testimony to the committee and I 
would be pleased to answer any questions.
    Mr. Markey. Thank you so much, Dr. Larson.
    [The prepared statement of Dr. Larson follows:]

    [GRAPHIC] [TIFF OMITTED] T4094A.009
    
    [GRAPHIC] [TIFF OMITTED] T4094A.010
    
    [GRAPHIC] [TIFF OMITTED] T4094A.011
    
    [GRAPHIC] [TIFF OMITTED] T4094A.012
    
    [GRAPHIC] [TIFF OMITTED] T4094A.013
    
    [GRAPHIC] [TIFF OMITTED] T4094A.014
    
    [GRAPHIC] [TIFF OMITTED] T4094A.015
    
    [GRAPHIC] [TIFF OMITTED] T4094A.016
    
    [GRAPHIC] [TIFF OMITTED] T4094A.017
    
     Mr. Markey. And our final witness is Michael Duffy, vice 
president and general counsel of Lantheus Medical Imaging, one 
of only two U.S. manufacturers of these type of generators. 
Welcome, sir.

                   STATEMENT OF MICHAEL DUFFY

    Mr. Duffy. Good afternoon, Mr. Chairman, Mr. Upton, members 
of the committee, staff. I am here today to testify on behalf 
of the bill on behalf of both Lantheus and CORAR, the Council 
on Radionuclides and Radiopharmaceuticals. Lantheus endorses 
H.R. 3276. We strongly support the committee's efforts to 
promote the protection of moly in the United States for medical 
isotope applications. We have been a worldwide leader in 
diagnostic medical imaging for the past 50 years. Lantheus is 
the home to leading diagnostic imaging brands, including the 
Technelite generator which I have for those who want to take a 
look, and this is what actually causes the--the radioactive 
salient is put through here. It comes out here and is mixed 
with a powder and becomes the injection which is injected into 
the patient.
    So what's so important about these imaging agents, these 
radionuclides? They allow a clinician to have a functional view 
of an organ like the heart more than just a mere anatomical 
image. Instead, a picture from the outside looking in, they 
allow the physician to see what is inside projecting out, for 
example, blood flow, heart function, tissue health. These are 
extremely helpful and important in the diagnosis and treatment 
of disease. The moly supply crisis is a chronic crisis 
resulting from an aging supply infrastructure and a market 
failure to attract sufficient replacement capacity. As Chairman 
Markey said, the crisis has become acute because of the ongoing 
shutdown in Canada at the NRU reactor and the ongoing repairs 
in the Netherlands at the HFR reactor. Although Lantheus has 
had access to moly supply from the major moly-producing 
reactors around the world, because of this crisis, we have not 
been fully able to meet our customers' needs and we are having 
to ration the generators on a weekly basis.
    Approximately a third of the moly manufactured outside of 
North America decays before it reaches our manufacturing 
facility in Massachusetts. Some of the old hands at Lantheus 
refer to this as buying ice on a warm day. Lantheus believes 
that a robust U.S. Supply of moly is an important U.S. policy 
for reasons of accessible and affordable health care, efficient 
waste management, and nuclear nonproliferation.
    As a result of the moly supply crisis, important diagnostic 
procedures are being postponed or cancelled. Clinicians are 
turning to older nuclear isotopes with potentially less 
diagnostic certainty and more patient risk. Clinicians may even 
be foregoing nuclear medicine entirely, opting instead for more 
invasive, more expensive, higher risk surgical procedures. 
Lantheus believes that the private sector should have a major 
role in the resolution of this issue. However, we also believe 
there is a strong role for government to play. The U.S. 
Government's financial support of multiple projects with 
appropriate investment risk profiles will be the best way to 
develop a robust domestic supply of moly. And as a matter of 
health care policy, medical imaging procedures that rely on 
moly-derived imaging agents can improve patient outcomes and 
reduce costs. Strategic investments to help develop a domestic 
supply of moly should pay large dividends for both U.S. 
patients and U.S. taxpayers.
    Wearing my CORAR hat now, CORAR supports H.R. 3276 and 
increasing capacity for medical isotopes in the United States. 
CORAR has two concerns about the bill. First, how do we ensure 
a full supply of moly if we get to the 7- to 10-year period and 
we don't yet have sufficient commercial quantities of 
domestically-produced LEU available? Second, CORAR would like 
the bill to contain specific language that would direct the 
Nuclear Regulatory Commission to allow the new aqueous 
homogenous reactors which have been recently proposed to be 
properly licensed as research reactors. CORAR believes the bill 
provides good support to bring new and alternative supplies to 
moly online quickly and believes it is prudent to back several 
alternative technologies and multiple reactor sites in order to 
avoid a repeat of the current availability in capacity issues.
    In sum, as H.R. 3276 moves forward, both Lantheus and CORAR 
hope to continue to work with the committee and staff to ensure 
both a swift and long-term solution to the moly crisis.
    Thank you for the consideration of our perspectives. We 
look forward to working with you moving forward and I would be 
glad to answer any questions.
    [The prepared statement of Mr. Duffy follows:]

    [GRAPHIC] [TIFF OMITTED] T4094A.018
    
    [GRAPHIC] [TIFF OMITTED] T4094A.019
    
    [GRAPHIC] [TIFF OMITTED] T4094A.020
    
    [GRAPHIC] [TIFF OMITTED] T4094A.021
    
    [GRAPHIC] [TIFF OMITTED] T4094A.022
    
    [GRAPHIC] [TIFF OMITTED] T4094A.023
    
    [GRAPHIC] [TIFF OMITTED] T4094A.024
    
    [GRAPHIC] [TIFF OMITTED] T4094A.025
    
    [GRAPHIC] [TIFF OMITTED] T4094A.026
    
    [GRAPHIC] [TIFF OMITTED] T4094A.027
    
    [GRAPHIC] [TIFF OMITTED] T4094A.028
    
    [GRAPHIC] [TIFF OMITTED] T4094A.029
    
    [GRAPHIC] [TIFF OMITTED] T4094A.030
    
    [GRAPHIC] [TIFF OMITTED] T4094A.031
    
    [GRAPHIC] [TIFF OMITTED] T4094A.032
    
    [GRAPHIC] [TIFF OMITTED] T4094A.033
    
    Mr. Markey. They have called four roll calls on the House 
floor. There are 10 minutes left to go before we have to go to 
that roll call. Let me recognize the gentleman from Michigan, 
Mr. Upton.
    Mr. Upton. Thanks.
    I have one very quick question, and that is the bill as you 
know has a 7- to 10-year timeframe. Do you think that is long 
enough? Is that the right amount of time or should we look at 
extending that? Go ahead, Mr. Duffy, Dr. Larson too.
    Mr. Duffy. The National Academies had a point of view on 
that, and I welcome Dr. Larson's views on that. The concern 
that industry has about the timeframe is one of technology and 
one of regulation. On the technology side can we design targets 
appropriately which generate commercially sustainable amounts 
of molybdenum and can we do this in a way that is going to pass 
muster with the EPA, the Nuclear Regulatory Commission, the FDA 
and all the other State and Federal regulatory authorities? 
Industry embraces the proposal, is going to work hard to try to 
implement it, but CORAR, in particular, is concerned that for 
reasons beyond its control the period may not be sufficient.
    Mr. Upton. Dr. Larson.
    Dr. Larson. The main report felt that after careful 
deliberation that it would be feasible to bring this conversion 
in a 7-year timeframe; however, it does depend on the actual 
type of conversion that was required. If one was talking about 
a complete refilled new facility, it may, in fact, take a bit 
longer. So we can--my feeling is that the 7 years is probably 
enough especially with the 3-year window that is offered. The 
committee does have expertise to bring to bear upon this, and 
if you wish, we can certainly review this issue and give you a 
more full description of it since it is such a key.
    Mr. Upton. That would be great. If you would like to do 
that that would be great. I will yield back.
    Mr. Markey. Let me just ask one question, Dr. Larson. The 
National Academy of Sciences report recommended that Congress 
set a deadline to end the export of HEU for medical isotope. 
The report concluded that a 7- to 10-year phaseout period would 
likely allow enough time for all current HEU-based producers to 
convert to low enriched uranium. How did you arrive at that 
conclusion of 7 to 10 years?
    Dr. Larson. I think the conclusion was based on interviews, 
discussions, field trips, and reviews of time of development of 
new facilities. We had the opportunity to visit, for example, 
Australia and to observe their process and plan for this. But 
since this is such a key point, we certainly would be glad to 
provide some more detailed background on how this was arrived 
at.
    Mr. Markey. Thank you, sir. The gentleman from Georgia.
    Mr. Barrow. No questions.
    Mr. Markey. Thank you.
    Dr. Staples, I am going to ask you what I think is the 
single most important question and that is how long will it 
take for the U.S. industry with the help of the Department of 
Energy to establish a robust domestic supply without the use of 
HEU?
    Dr. Staples. Thank you. We believe that to develop a robust 
supply without the use of HEU would take on the order of 5 
years with the type of attention focus that this bill could 
bring to the industry and the issue given the acute shortage of 
this isotope we are currently experiencing. As a clarification, 
we are not just talking about conversion of existing 
facilities. We are talking about looking at a diverse reliable 
supply network that would be implemented using non-HEU, not 
just LEU fission target-based technologies.
    Mr. Markey. Dr. Staples, is DOE working with the foreign 
medical isotope producers to help them convert from HEU to LEU?
    Dr. Staples. In fact, yes, we are. Given the recent supply 
shortage, we have engaged in at least informal discussions with 
all current producers regarding options and process and 
procedure for the conversion----
    Mr. Markey. Have they asked for your help in conversions?
    Dr. Staples. Yes. They have solicited our help and 
assistance in conversions.
    Mr. Markey. Have they made those requests recently?
    Dr. Staples. As recently as last week, yes sir.
    Mr. Markey. Great. Does there seem to be a renewed interest 
in converting from HEU to LEU at this time?
    Dr. Staples. Absolutely. Again, I think primarily driven by 
the shortage of supply in the current industry.
    Mr. Markey. Would phaseout of export of HEU for medical 
isotope production in 7 to 10 years given the handful of 
foreign producers present a window of opportunity for their 
operations?
    Dr. Staples. If I understand, we believe that the 7-year 
timeframe would give more than a sufficient timeframe for these 
facilities to work on conversion and for the development of a 
diverse domestic supply of moly 99 not using enriched uranium.
    Mr. Markey. Does 7 to 10 years give the foreign producers 
adequate time to convert to LEU?
    Dr. Staples. Yes.
    Mr. Markey. The bill authorizes $163 million over 5 years 
for DOD to help establish a domestic supply. Is that the right 
of amount of money and is that the right amount of time?
    Dr. Staples. Yes. We believe that is consistent with a 
program plan that we have in place where we would intend this 
year to place up to $10 million on programs to support the 
development of commercial industry and $30 million in each of 
the respective outyears to support the developments of domestic 
and/or commercial supply of isotope.
    Mr. Markey. I have a group of other written questions here, 
and I am going to submit to you each for your response to the 
committee. We apologize to you. There is a whole series of roll 
calls which are House floor. We apologize to you for that, and 
with the thanks of the subcommittee and apologies because of 
the truncated form of the hearing, this hearing is adjourned. 
Thank you.
    [Whereupon, at 2:42 p.m., the subcommittee was adjourned.]
    [Material submitted for inclusion in the record follows:]

    [GRAPHIC] [TIFF OMITTED] T4094A.034
    
    [GRAPHIC] [TIFF OMITTED] T4094A.035
    
    [GRAPHIC] [TIFF OMITTED] T4094A.036
    
    [GRAPHIC] [TIFF OMITTED] T4094A.037
    
    [GRAPHIC] [TIFF OMITTED] T4094A.038
    
    [GRAPHIC] [TIFF OMITTED] T4094A.039
    
    [GRAPHIC] [TIFF OMITTED] T4094A.040
    
    [GRAPHIC] [TIFF OMITTED] T4094A.041
    
    [GRAPHIC] [TIFF OMITTED] T4094A.042
    
    [GRAPHIC] [TIFF OMITTED] T4094A.043
    
    [GRAPHIC] [TIFF OMITTED] T4094A.044
    
    [GRAPHIC] [TIFF OMITTED] T4094A.045
    
    [GRAPHIC] [TIFF OMITTED] T4094A.046
    
    [GRAPHIC] [TIFF OMITTED] T4094A.047
    
    [GRAPHIC] [TIFF OMITTED] T4094A.048
    
    [GRAPHIC] [TIFF OMITTED] T4094A.049
    
    [GRAPHIC] [TIFF OMITTED] T4094A.050
    
    [GRAPHIC] [TIFF OMITTED] T4094A.051
    
    [GRAPHIC] [TIFF OMITTED] T4094A.052
    
    [GRAPHIC] [TIFF OMITTED] T4094A.053
    
    [GRAPHIC] [TIFF OMITTED] T4094A.054
    
    [GRAPHIC] [TIFF OMITTED] T4094A.055
    
    [GRAPHIC] [TIFF OMITTED] T4094A.056
    
    [GRAPHIC] [TIFF OMITTED] T4094A.057
    
    [GRAPHIC] [TIFF OMITTED] T4094A.058
    
    [GRAPHIC] [TIFF OMITTED] T4094A.059
    
    [GRAPHIC] [TIFF OMITTED] T4094A.060
    
    [GRAPHIC] [TIFF OMITTED] T4094A.061
    
    [GRAPHIC] [TIFF OMITTED] T4094A.062
    
    [GRAPHIC] [TIFF OMITTED] T4094A.063
    
    [GRAPHIC] [TIFF OMITTED] T4094A.064
    
    [GRAPHIC] [TIFF OMITTED] T4094A.065
    
    [GRAPHIC] [TIFF OMITTED] T4094A.066
    
    [GRAPHIC] [TIFF OMITTED] T4094A.067
    
    [GRAPHIC] [TIFF OMITTED] T4094A.068
    

</pre></body></html>
