<html>
<title> - MISMANAGEMENT ISSUES AT THE CHARLESTON, SOUTH CAROLINA AND PITTSBURGH, PENNSYLVANIA VETERANS AFFAIRS MEDICAL CENTERS</title>
<body><pre>
[House Hearing, 105 Congress]
[From the U.S. Government Printing Office]



 
                MISMANAGEMENT ISSUES AT THE CHARLESTON, 
              SOUTH CAROLINA AND PITTSBURGH, PENNSYLVANIA 
                         VETERANS AFFAIRS MEDICAL 
                                  CENTERS 

=============================================================================


                                HEARING

                               before the

              SUBCOMMITTEE ON OVERSIGHT AND INVESTIGATIONS

                                 of the

                     COMMITTEE ON VETERANS' AFFAIRS

                         HOUSE OF REPRESENTATIVES

                        ONE HUNDRED FIFTH CONGRESS

                               FIRST SESSION

                                 ----------

                              OCTOBER 23, 1997

                                 ----------

         Printed for the use of the Committee on Veterans' Affairs


                             Serial No. 105-25


                     U.S. GOVERNMENT PRINTING OFFICE

  47-766 CC                  WASHINGTON : 1998

-----------------------------------------------------------------------------

               For sale by the U.S. Government Printing Office
Superintendent of Documents, Congressional Sales Office, Washington, DC 20402
                            ISBN 0-16-056530-8

                    <TEXT NOT AVAILABLE IN TIFF FORMAT>


</pre></body></html>
