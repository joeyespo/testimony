<html>
<title> - PHONE RECORDS FOR SALE: WHY AREN'T PHONE RECORDS SAFE FROM PRETEXTING?</title>
<body><pre>
[House Hearing, 109 Congress]
[From the U.S. Government Printing Office]


                           
 
                       PHONE RECORDS FOR SALE:  WHY 
                         AREN'T PHONE RECORDS SAFE 
                              FROM PRETEXTING?


                                  HEARING

                                BEFORE THE


                        COMMITTEE ON ENERGY AND 
                                 COMMERCE
                        HOUSE OF REPRESENTATIVES


                       ONE HUNDRED NINTH CONGRESS

                              SECOND SESSION

                                 -------

                              FEBRUARY 1, 2006

                                 -------

                             Serial No. 109-53

                                 -------

        Printed for the use of the Committee on Energy and Commerce



Available via the World Wide Web:  http://www.access.gpo.gov/congress/house


                                 -------

                    U.S. GOVERNMENT PRINTING OFFICE
26-442                      WASHINGTON : 2006
_____________________________________________________________________________
For sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; DC area (202) 512-1800 Fax: (202) 512-2250  Mail: Stop  SSOP, Washington, DC 20402-0001

<TEXT NOT AVAILABLE IN TIFF FORMAT>

</pre></body></html>
