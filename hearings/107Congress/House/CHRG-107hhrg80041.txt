<html>
<title> - PREVENTION AND TREATMENT OF CHILD ABUSE AND NEGLECT: POLICY DIRECTIONS FOR THE FUTURE </title>
<body><pre>
[House Hearing, 107 Congress]
[From the U.S. Government Printing Office]



 
PREVENTION AND TREATMENT OF CHILD ABUSE AND NEGLECT: POLICY DIRECTIONS 
                            FOR THE FUTURE
=======================================================================


                                HEARING

                               before the

                    SUBCOMMITTEE ON SELECT EDUCATION

                                 of the

                COMMITTEE ON EDUCATION AND THE WORKFORCE
                        HOUSE OF REPRESENTATIVES

                      ONE HUNDRED SEVENTH CONGRESS

                             FIRST SESSION
                               __________

            HEARING HELD IN WASHINGTON, DC, OCTOBER 17, 2001

                               __________

                           Serial No. 107-35
                               __________

  Printed for the use of the Committee on Education and the Workforce




                     U.S. GOVERNMENT PRINTING OFFICE
80-041                       WASHINGTON : 2002
________________________________________________________________________
For Sale by the Superintendent of Documents, U.S. Government Printing Office
Internet: bookstore.gpo.gov  Phone: toll free (866) 512-1800; (202) 512-1800  
Fax: (202) 512-2250 Mail: Stop SSOP, Washington, DC 20402-0001

</pre></body></html>
